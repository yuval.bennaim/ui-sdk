import { PolymerElement } from '../assets/vendor/@polymer/polymer/polymer-element.js';
import '../assets/vendor/@polymer/polymer/lib/elements/dom-if.js';
import '../assets/vendor/@polymer/polymer/lib/elements/dom-repeat.js';
import '../assets/vendor/@polymer/polymer/import-href.js';
import '../assets/vendor/@polymer/polymer/app-route/app-route.js';
import '../assets/vendor/@polymer/polymer/app-route/app-location.js';
import '../assets/vendor/@polymer/polymer/import-href.js';
import '../assets/vendor/@polymer/polymer/iron-ajax/iron-ajax.js';

import '../assets/vendor/@google-web-components/google-chart/google-chart.js';

import '../assets/webcomponents/core/dr-zp-module-base.js';

import '../assets/webcomponents/core/form/dr-zp-button.js';
import '../assets/webcomponents/core/form/dr-zp-calendar.js';
import '../assets/webcomponents/core/form/dr-zp-checkbox.js';
import '../assets/webcomponents/core/form/dr-zp-datepicker.js';
import '../assets/webcomponents/core/form/dr-zp-date-dropdown.js';
import '../assets/webcomponents/core/form/dr-zp-radio-group.js';
import '../assets/webcomponents/core/form/dr-zp-dropdown.js';
import '../assets/webcomponents/core/form/dr-zp-file-selector.js';
import '../assets/webcomponents/core/form/dr-zp-input.js';
import '../assets/webcomponents/core/form/dr-zp-color-input.js';
import '../assets/webcomponents/core/form/dr-zp-numeric-input.js';
import '../assets/webcomponents/core/form/dr-zp-textarea.js';
import '../assets/webcomponents/core/form/dr-zp-time-input.js';
import '../assets/webcomponents/core/form/dr-zp-label.js';
import '../assets/webcomponents/core/form/dr-zp-slider.js';
import '../assets/webcomponents/core/form/dr-zp-list.js';

import '../assets/webcomponents/core/misc/dr-zp-image.js';
import '../assets/webcomponents/core/misc/dr-zp-icon.js';
import '../assets/webcomponents/core/misc/dr-zp-modal.js';
import '../assets/webcomponents/core/misc/dr-zp-pagination.js';
import '../assets/webcomponents/core/misc/dr-zp-notification.js';
import '../assets/webcomponents/core/misc/dr-zp-spinner.js';
import '../assets/webcomponents/core/misc/dr-zp-tooltip.js';
import '../assets/webcomponents/core/misc/dr-zp-toggle.js';
import '../assets/webcomponents/core/misc/dr-zp-badge.js';

import '../assets/webcomponents/core/containers/dr-zp-tabs.js';
import '../assets/webcomponents/core/containers/dr-zp-collapse.js';
import '../assets/webcomponents/core/containers/dr-zp-list-selector.js';
import '../assets/webcomponents/core/containers/dr-zp-panel.js';
import '../assets/webcomponents/core/containers/dr-zp-multi-select.js';
import '../assets/webcomponents/core/containers/dr-zp-accordion.js';
import '../assets/webcomponents/core/containers/dr-zp-pill-box.js';

import '../assets/webcomponents/reusable/data/tree/dr-zp-tree.js';
import '../assets/webcomponents/reusable/data/grid/dr-zp-grid.js';

/* for production build purpose only*/

class Comp extends PolymerElement {}
window.customElements.define('component-prod-bundle', Comp);
