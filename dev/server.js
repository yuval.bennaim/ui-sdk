var express = require('express');
var app = express()
var https = require('https')
var fs = require('fs');
var path = require('path');
var port = 3338;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', req.header('origin') );
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/', express.static(path.join(__dirname, '../')));

app.listen(port, function() {
    console.info(`Shell App Server is running at http://localhost:${port}  root: ${__dirname}`);
});
