import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { ModuleBase } from '../../../webcomponents/core/dr-zp-module-base.js';

import './examples/module-sdk-accordion.js';
import './examples/module-sdk-badge.js';
import './examples/module-sdk-button.js';
import './examples/module-sdk-calendar.js';
import './examples/module-sdk-checkbox.js';
import './examples/module-sdk-collapse.js';
import './examples/module-sdk-commet.js';
import './examples/module-sdk-datepicker.js';
import './examples/module-sdk-dropdown.js';
import './examples/module-sdk-file-selector.js';
import './examples/module-sdk-grid.js';
import './examples/module-sdk-grid-custom.js';
import './examples/module-sdk-grid-trinity.js';
import './examples/module-sdk-icon.js';
import './examples/module-sdk-input.js';
import './examples/module-sdk-label.js';
import './examples/module-sdk-list.js';
import './examples/module-sdk-list-selector.js';
import './examples/module-sdk-modal.js';
import './examples/module-sdk-multi-select.js';
import './examples/module-sdk-notification.js';
import './examples/module-sdk-numeric-input.js';
import './examples/module-sdk-pagination.js';
import './examples/module-sdk-panel.js';
import './examples/module-sdk-radio-group.js';
import './examples/module-sdk-spinner.js';
import './examples/module-sdk-table.js';
import './examples/module-sdk-tabs.js';
import './examples/module-sdk-pill-box.js';
import './examples/module-sdk-textarea.js';
import './examples/module-sdk-slider.js';
import './examples/module-sdk-time-input.js';
import './examples/module-sdk-toggle.js';
import './examples/module-sdk-tooltip.js';
import './examples/module-sdk-tree.js';

class Comp extends ModuleBase {
    static get template() {
        const superTemp = super.template;

        return html`
            ${superTemp}

            <iron-ajax id="getResources" method="GET" on-response="handleResourcesResponse" handle-as="json"></iron-ajax>

            <div class="wrapper">
                <div class="content-wrapper">
                    <dr-zp-bindable-html value="[[wcStr]]"></dr-zp-bindable-html>  
                </div>
            </div>
        `;
    }

    static get is() {
        return 'module-sdk';
    }

    static get properties() {
        return {
            module: {
                type: Object
            },
            wcStr: {
                type: String,
                value: ''
            },
            selectedNavTitle: {
                type: String,
                value: 'module-sdk-desc'
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
        super.initResources(Comp.is);

        document.addEventListener('context-navigation-action', this.contextNavigationActionHandler.bind(this));
        document.addEventListener('locale-changed', this.localeChangedHandler.bind(this));

        this.generateModuleMenu();
        this.parseRoute('module-sdk', true);
    }

    localeChangedHandler(e) {
        if (this.isActive()) {
            this.generateModuleMenu();
        }
    }

    contextNavigationActionHandler(e) {
        var active = this.isActive();

        if (active) { //if match on this module, handle internal navigation
            var module = e.detail.module;
            var nav = e.detail.nav;
            var rejected = false;
            var context = "";

            if (nav != null && nav.sub_route == undefined) {
                this.selectedNav = this.findPathInMenu(nav, true);
            }
            else {
                this.selectedNav = nav;
            }

            this.selectedNavTitle = this.selectedNav.webcomponent_name + "-example";

            console.info('context-navigation-action', active, module, this.selectedNav);

            if (this.selectedNav == null || this.selectedNav.webcomponent_name == null) {
                this.wcStr = '<h3 class="error-message">No view found for: ' + nav + '</h3>';
            }
            else { // check if the route is permitted on this site  
                this.wcStr = '<' + this.selectedNav.webcomponent_name + ' context="' + context + '"></' + this.selectedNav.webcomponent_name + '>';
            }
        }
    }

    generateModuleMenu() {
        this.menu = {
            "type": "menu",
            "source": "module-sdk",
            "navigation": [
                {
                    "sub_route": "form",
                    "menu_name": this.getString('module-sdk-form', this.locale),
                    "menu_group": true,
                    "navigation": [
                        {
                            "sub_route": "form/button",
                            "menu_name": this.getString('module-sdk-button', this.locale),
                            "webcomponent_name": "module-sdk-button",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-button.js`
                        },
                        {
                            "sub_route": "form/dropdown",
                            "menu_name": this.getString("module-sdk-dropdown", this.locale),
                            "webcomponent_name": "module-sdk-dropdown",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-dropdown.js`
                        },
                        {
                            "sub_route": "form/radio-group",
                            "menu_name": this.getString("module-sdk-radio-group", this.locale),
                            "webcomponent_name": "module-sdk-radio-group",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-radio-group.js`
                        },
                        {
                            "sub_route": "form/checkbox",
                            "menu_name": this.getString("module-sdk-checkbox", this.locale),
                            "webcomponent_name": "module-sdk-checkbox",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-checkbox.js`
                        },
                        {
                            "sub_route": "form/label",
                            "menu_name": this.getString("module-sdk-label", this.locale),
                            "webcomponent_name": "module-sdk-label",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-label.js`,

                        },
                        {
                            "sub_route": "form/list",
                            "menu_name": this.getString("module-sdk-list", this.locale),
                            "webcomponent_name": "module-sdk-list",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-list.js`,

                        },
                        {
                            "sub_route": "form/text-area",
                            "menu_name": this.getString("module-sdk-text-area", this.locale),
                            "webcomponent_name": "module-sdk-textarea",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-textarea.js`
                        },
                        {
                            "sub_route": "form/slider",
                            "menu_name": this.getString("module-sdk-slider", this.locale),
                            "webcomponent_name": "module-sdk-slider",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-slider.js`
                        },
                        {
                            "sub_route": "form/input",
                            "menu_name": this.getString("module-sdk-input", this.locale),
                            "webcomponent_name": "module-sdk-input",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-input.js`
                        },
                        {
                            "sub_route": "form/numeric-input",
                            "menu_name": this.getString("module-sdk-numeric-input", this.locale),
                            "webcomponent_name": "module-sdk-numeric-input",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-numeric-input.js`
                        },
                        {
                            "sub_route": "form/file-selector",
                            "menu_name": this.getString("module-sdk-file-selector", this.locale),
                            "webcomponent_name": "module-sdk-file-selector",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-file-selector.js`
                        },
                        {
                            "sub_route": "form/calendar",
                            "menu_name": this.getString("module-sdk-calendar", this.locale),
                            "webcomponent_name": "module-sdk-calendar",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-calendar.js`
                        },
                        {
                            "sub_route": "form/datepicker",
                            "menu_name": this.getString("module-sdk-date-picker", this.locale),
                            "webcomponent_name": "module-sdk-datepicker",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-datepicker.js`
                        },
                        {
                            "sub_route": "form/time-input",
                            "menu_name": this.getString("module-sdk-time-input", this.locale),
                            "webcomponent_name": "module-sdk-time-input",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-time-input.js`
                        },
                        {
                            "sub_route": "form/color-input",
                            "menu_name": this.getString("module-sdk-color-input", this.locale),
                            "webcomponent_name": "module-sdk-color-input",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-color-input.js`
                        }
                    ]
                },
                {
                    "sub_route": "data",
                    "menu_name": this.getString("module-sdk-data", this.locale),
                    "menu_group": true,
                    "navigation": [
                        {
                            "sub_route": "data/grid",
                            "menu_name": this.getString("module-sdk-grid", this.locale),
                            "webcomponent_name": "module-sdk-grid",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-grid.js`
                        },
                        {
                            "sub_route": "data/grid/trinity",
                            "menu_name": "Trinity Grid",
                            "webcomponent_name": "module-sdk-grid-trinity",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-grid-trinity.js`
                        },
                        {
                            "sub_route": "data/grid/custom",
                            "menu_name": this.getString("module-sdk-grid-custom", this.locale),
                            "webcomponent_name": "module-sdk-grid-custom",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-grid-custom.js`
                        },
                        {
                            "sub_route": "data/table",
                            "menu_name": this.getString("module-sdk-table", this.locale),
                            "webcomponent_name": "module-sdk-table",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-table.js`
                        },
                        {
                            "sub_route": "data/tree",
                            "menu_name": this.getString("module-sdk-tree", this.locale),
                            "webcomponent_name": "module-sdk-tree",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-tree.js`
                        }
                    ]
                },
                {
                    "sub_route": "containers",
                    "menu_name": this.getString("module-sdk-containers", this.locale),
                    "menu_group": true,
                    "navigation": [
                        {
                            "sub_route": "containers/panel",
                            "menu_name": this.getString("module-sdk-panel", this.locale),
                            "webcomponent_name": "module-sdk-panel",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-panel.js`
                        },
                        {
                            "sub_route": "containers/collapse",
                            "menu_name": this.getString("module-sdk-collapse", this.locale),
                            "webcomponent_name": "module-sdk-collapse",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-collapse.js`
                        },
                        {
                            "sub_route": "containers/accordion",
                            "menu_name": this.getString("module-sdk-accordion", this.locale),
                            "webcomponent_name": "module-sdk-accordion",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-accordion.js`
                        },
                        {
                            "sub_route": "containers/list-selector",
                            "menu_name": this.getString("module-sdk-list-selector", this.locale),
                            "webcomponent_name": "module-sdk-list-selector",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-list-selector.js`
                        },
                        {
                            "sub_route": "containers/multi-select",
                            "menu_name": this.getString("module-sdk-multi-select", this.locale),
                            "webcomponent_name": "module-sdk-multi-select",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-multi-select.js`
                        },
                        {
                            "sub_route": "containers/tabs",
                            "menu_name": this.getString("module-sdk-tabs", this.locale),
                            "webcomponent_name": "module-sdk-tabs",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-tabs.js`
                        },
                        {
                            "sub_route": "containers/pillbox",
                            "menu_name": this.getString("module-sdk-filters-example", this.locale),
                            "webcomponent_name": "module-sdk-pill-box",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-pill-box.js`
                        }
                    ]
                },
                {
                    "sub_route": "misc",
                    "menu_name": this.getString("module-sdk-misc", this.locale),
                    "menu_group": true,
                    "navigation": [
                        {
                            "sub_route": "misc/icon",
                            "menu_name": this.getString("module-sdk-icons", this.locale),
                            "webcomponent_name": "module-sdk-icon",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-icon.js`
                        },
                        {
                            "sub_route": "misc/spinner",
                            "menu_name": this.getString("module-sdk-spinner", this.locale),
                            "webcomponent_name": "module-sdk-spinner",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-spinner.js`
                        },
                        {
                            "sub_route": "misc/modal",
                            "menu_name": this.getString("module-sdk-modal", this.locale),
                            "webcomponent_name": "module-sdk-modal",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-modal.js`
                        },
                        {
                            "sub_route": "misc/badge",
                            "menu_name": this.getString("module-sdk-badge", this.locale),
                            "webcomponent_name": "module-sdk-badge",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-badge.js`
                        },
                        {
                            "sub_route": "misc/tooltip",
                            "menu_name": this.getString("module-sdk-tooltip", this.locale),
                            "webcomponent_name": "module-sdk-tooltip",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-tooltip.js`
                        },
                        {
                            "sub_route": "misc/toggle-switch",
                            "menu_name": this.getString("module-sdk-toggle-switch", this.locale),
                            "webcomponent_name": "module-sdk-toggle",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-toggle.js`
                        },
                        {
                            "sub_route": "misc/pagination",
                            "menu_name": this.getString("module-sdk-pagination", this.locale),
                            "webcomponent_name": "module-sdk-pagination",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-pagination.js`
                        },
                        {
                            "sub_route": "misc/notification",
                            "menu_name": this.getString("module-sdk-notification", this.locale),
                            "webcomponent_name": "module-sdk-notification",
                            "webcomponent_path": `${this.module.base_path}/dev/examples/module-sdk-notification.js`
                        }
                    ]
                }
            ]
        }

        var msgJSON = JSON.stringify(this.menu);
        top.window.postMessage(msgJSON, "*");
    }
}

window.customElements.define(Comp.is + '-home', Comp);