import { html } from '../../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import { BaseComponent } from '../../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const trinityStyles = this.styleStore.getSheetStyles("trinity.css");
        const gridStyles = this.styleStore.getSheetStyles("grid.css");

        const template = html`
            ${superTemp}
            ${trinityStyles}
            ${gridStyles}

            <div id="container" class="item-view-wrapper Trinity"></div>
        `;

        return template;
    }

    static get properties() {
        return {
            url: {
                type: String
            },
            item: {
                type: Object,
              value: null,
              observer: "itemChanged"
            },
            api: {
                type: String,
                observer: "apiChanged"
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    itemChanged() {
        if(this.item != null) {
            console.info('itemChanged', this.item);
            this.loadData();
        }
    }

    async loadData() {
        if(this.item) {
            console.info('loadData', this.item);
            this.loading = true;
            this.error = null;
            let url = this.api + "/edit.html?id=" + this.item.playerID;

            const response = await fetch(url, {
                method: 'GET',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                  'Content-Type': 'application/json'
                },
                redirect: 'follow',
                referrerPolicy: 'no-referrer',
            }).then(res => {
                return res.text();
            })
            .then(data => {
                this.processData(data);
            })
            .catch((err) => {
                console.log();
                this.loading = false;
                this.error = err.message;
            });
        }        
    }

    processData(data) {
        console.log(data);
        let start = data.indexOf('<div class="Page">');
        let end = data.indexOf('</body>');
        let snipped = data.substring(start, end);
        console.log(snipped);
        this.$.container.innerHTML = snipped;
    }
}

window.customElements.define('dr-trinity-item-view', Comp);