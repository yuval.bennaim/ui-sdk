
import './dr-trinity-viewer.js';

class Comp extends HTMLElement {

  connectedCallback() {
      this.render();
  }

  attributeChangedCallback(name, oldValue, newValue) {
    console.log('Custom square element attributes changed.');
    updateStyle(this);
  }
  
  render() {
    this.name = this.getAttribute("name");
    this.api = this.getAttribute("api");

    if(history.api != null) {
      console.log("render", this.api);
      const html = `<dr-trinity-viewer name="${this.name}" api="${this.api}">`;
      this.shadow = this.attachShadow({ mode: "open" });
      this.shadow.innerHTML = html;
    }
  }
}

window.customElements.define('dr-trinity-wrapper', Comp);