import { html } from '../../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import { BaseComponent } from '../../../../../webcomponents/core/dr-zp-base-component.js';
import './dr-trinity-item-view.js'

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const trinityStyles = this.styleStore.getSheetStyles("trinity.css");
        const gridStyles = this.styleStore.getSheetStyles("grid.css");

        const template = html`
            ${superTemp}
            ${trinityStyles}
            ${gridStyles}

           <div class="grid-title">[[info.title]]</div>

            <template is="dom-if" if="[[error]]">
                <div class="error-message">[[error]] [[api]]</div>
            </template>
           
            <div class$="[[determineGridClass(gridRows, error)]]">

                <div id="actionBar" class="action-bar">
                    <template is="dom-repeat" items="[[info.actions]]" as="action">
                        <dr-zp-button class="action-bar-button" type="link" icon="[[action.icon]]" label="[[action.caption]]" data="[[action]]"></dr-zp-button>
                    </template>
                    <tr>
                </div>

                <dr-zp-grid 
                    id="trinityGrid"
                    grid-title=""
                    gid="[[generateGridKey(name)]]"
                    primary-key="id"
                    columns="{{columnDefs}}"
                    data="[[gridRowsPage]]"
                    filters="[[filters]]"
                    selected="[[selected]]"
                    loading="[[loading]]"
                    allow-action-slot="[[allowActionSlot]]">
                </dr-zp-grid>

                <div class$="[[determineOverlayClass(selected)]]" on-click="closeOverlay">  
                    
                    <!--<iframe src="http://localhost:1080/web.example/edit.html?id=[[selected.playerID]]" style="width:100%; height: 100%; border: none"></iframe>-->
                    <dr-trinity-item-view item="[[selected]]" api="[[api]]" class="item-viewer"/>

                </div>
            </div>

            <div class$="[[determinePaginationClass(loading, error)]]">  
                <dr-zp-pagination id="pagination" style="dispay: block" active-page="[[page]]" per-page-opt="[[pageSizesArr]]" total="[[filteredRows.length]]" increment="[[pageSize]]" unit="[[getString('module-sdk-things', locale)]]"></dr-zp-pagination>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            columnDefs: {
                type: Array,
                value: null
            },
            gridRows: {
                type: Array,
                value: null
            },
            loading: {
                type: Boolean,
                value: false
            },
            pageSizesArr: {
                type: Array,
                value: [25, 50, 75, 100]
            },
            pageSize: {
                type: Number,
                observer: 'pageSizeChanged'
            },
            pages: {
                type: Number,
                value: 0
            },
            page: {
                type: Number,
                value: 1,
                observer: 'pageChanged'
            },
            allowActionSlot: {
                type: Boolean,
                value: false
            },
            title: {
                type: String,
                value: "Trinity Grid"
            },
            name: {
                type: String
            },
            api: {
              type: String,
              observer: "apiChanged"
            },
            selected: {
              type: Object,
              value: null,
              observer: "selectedChanged"
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();

        this.pageSize = this.pageSizesArr[0];

        this.filters = [];
        var key = "trinityGrid_filters";

        if (localStorage.getItem(key)) {
            this.filters = JSON.parse(localStorage.getItem(key));
        }

        document.addEventListener('escape-action', this.escapeActionHandler.bind(this));

        this.$.pagination.addEventListener('pagination-per-page-change', e => {
            var perPage = e.detail.perPage;
            console.info('pagination-per-page-change', perPage);
            this.pageSize = perPage;
        });

        this.$.trinityGrid.addEventListener('row-action', e => {
            var row = e.detail.row;
            console.info('row-action', row);
        });

        this.$.trinityGrid.addEventListener('link-action', e => { //row-selected
            var action = e.detail.action;
            this.selected = e.detail.row;
            console.info('row-selected', this.selected);
        });

        this.$.trinityGrid.addEventListener('row-button-click', e => {
            var row = e.detail.row;
            var action = e.detail.action;
            console.info('trinityGrid row-button-click', action);

            var timestamp = new Date().getTime();
            var msg = `row-button-click ${action} for row ${row.id}`;

            document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                detail: {
                    timestamp: timestamp,
                    source: 'dr-zp SDK Grid',
                    type: 'info',
                    message: msg,
                    autodismissable: true,
                    duration: 5000
                }
            }));

            /**/
        });

        this.$.trinityGrid.addEventListener('filter-change', e => {
            this.filters = e.detail.filters;
            console.info('sort-change', this.filters);
            /* call API */
        });

        this.$.trinityGrid.addEventListener('sort-change', e => {
            var col = e.detail.column;
            console.info('sort-change', col);
            
            /* call API */
        });

        this.$.pagination.addEventListener('pagination-page-change', e => {
            this.page = e.detail.page;
            console.info('pagination-page-change', this.page);
        });

        this.$.actionBar.addEventListener("dom-change", (event) => {
            var buttons = dom(this.root).querySelectorAll('dr-zp-button');

            if(!this.buttonsReady) {
                this.buttonsReady = true;

                buttons.forEach((button) => {
                    button.addEventListener('dr-zp-button-click', e => {
                        var source = e.detail.source;
                        var butt = source.label;
                        var data = e.detail.data;
                        var butt = source.label;
                        var timestamp = new Date().getTime();
                        var msg = `You clicked on ${butt}`;

                        document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                            detail: { 
                                timestamp: timestamp,
                                source: 'dr-zp SDK Tree',
                                type: 'info',
                                message: msg,
                                autodismissable: true,
                                duration: 5000 
                            }
                        }));

                        /**/
                        this.selected = {"name": {"caption": "???"}}
                    }); 
                });
            }
        });
    }

    selectedChanged() {
        if(this.selected != null) {
            console.info('selectedChanged', this.selected);
        }
    }

    escapeActionHandler(e) {
        this.selected = null;
    }

    apiChanged() {
        if(this.api != null) {
            console.info('apiChanged', this.api);
            this.name = this.api.substring(this.api.lastIndexOf('/') + 1);
            this.loadData();
        }
        else {
            this.error = "No API provided...";
        }
    }

    async loadData() {
        if(this.api) {
            console.info('loadData', this.api);
            this.loading = true;
            this.error = null;
            this.gridRows = null;
            this.columnDefs = null;
            let url = this.api + "/list.vu";

            const response = await fetch(url, {
                method: 'GET',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                  'Content-Type': 'application/json'
                },
                redirect: 'follow',
                referrerPolicy: 'no-referrer',
                //body: JSON.stringify(data)
              }).then((response) => {
                return response.json();
              }).then((json) => {
                this.processData(json);
              }).catch((err) => {
                console.log();
                this.loading = false;
                this.error = err.message;
              });
        }        
    }

    processData(data) {
        console.log(data);
        this.loading = false;
        this.info = data.info;
        let cols = data.columns;

        const newCol = {
            "field": "image",
            "caption": "Image",
            "type": "image",
            "visible": false,
            "sortable": false,
            "filterable": false
        };

        cols.unshift(newCol);

        this.gridRows = data.rows;
        const storageKey = this.generateGridKey(this.name) + "_columns";
        var lsCols = localStorage.getItem(storageKey);

        let keys = [];

        cols.forEach((col) => {
            let colKey = keys[col.field];
            let key = col.field;

            if(colKey == null) {
                keys[col.field] = 1;
            }
            else {
                colKey++;
                keys[col.field] = colKey;
                key += colKey;
            }            

            if(col.width == null) {
                if(lsCols == null) {
                    col.width = 120;
                }

                col.key = key;
            }
        });

        this.gridRows.forEach((row) => {
           row.image = {
                "caption": "Giannis Antetokounmpo",
                "src": "https://a.espncdn.com/combiner/i?img=/i/headshots/nba/players/full/3032977.png&w=350&h=254",
                "height": "3em"
            }
            
            row.playerID = row.name.url.split("/")[4];
        });

        this.columnDefs = this.utils._helper_copy(cols);
        this.filteredRows = this.utils._helper_copy(this.gridRows);
        this.pageChanged();
    }

    generateGridKey(name) {
        return "trinity_" + name + "_grid";
    }

    determinePaginationClass(loading, error) {
        var cls = 'input-fields-wrapper';

        if (loading || error) {
            cls += ' hidden';
        }

        return cls;
    }

    closeOverlay () {
        this.selected = null;
    }

    determineOverlayClass(selected) {
        let cls = "item-overlay";

        if(selected == null) {
            cls = "hidden";
        }

        return cls;
    }

    determineGridClass(gridRows, error) {
        let cls = "grid-wrapper";

        if(error) {
            cls =" hidden";
        }

        return cls;
    }

    pageSizeChanged() {
        this.page = 1;
        this.pageChanged();
    }

    pageChanged() {
        if (this.filteredRows) {
            for (var r = 0; r < this.filteredRows.length; r++) {
                this.filteredRows[r].expanded = false;
            }

            var from = (this.page - 1) * this.pageSize;
            var to = this.page * this.pageSize;
            console.info('pageChanged', this.page, from, to);
            this.gridRowsPage = this.filteredRows.slice(from, to);
            this.loading = false;
        }
    }
}

window.customElements.define('dr-trinity-viewer', Comp);