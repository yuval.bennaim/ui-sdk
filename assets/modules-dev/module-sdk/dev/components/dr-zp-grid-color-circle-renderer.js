import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class GridColorCircleRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                :host {padding-left: 0px;}
                .color-chip-wrap {
                    display: block;
                    padding: 5px;
                }
                .color-chip {
                    display: inline-block;
                    width: 1em;
                    height: 1em;
                    border-radius: 50%;
                    border: 2px solid #fff;
                }
                .color-hex {
                    position: relative;
                    top: -3px;
                }
            </style>
            
            <template is="dom-if" if="true">
                <div class="color-chip-wrap">
                    <span class="color-chip" style$="[[getColor(col, obj, field)]]"></span>
                    <span class="color-hex">[[hexVal]]</span>
                </div>
            </template>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object
            },
            field: {
                type: String
            }
        }
    }

    getColor(col, obj, field) {
        var style;

        if(this.col.callback != null) {
            this.hexVal = this.col.callback(this.obj);
            console.info('getColor returned from callback', this.hexVal);
        }

        if(obj !== undefined) {
            if(obj.val !== undefined) {
                style = "background-color: " + obj.val;  
            }
            else {
                var clr = obj[field];

                if(!clr.startsWith('#')) {
                    clr = '#' + clr;
                }
                
                style = "background-color: " + clr;  
            }
        }
        else {
            style = "background-color: grey";
        }

        return style;
    }
}

window.customElements.define('fe-grid-color-circle-renderer', GridColorCircleRenderer);