import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class GridComplexDataRenderer extends BaseComponent {
  static get template() {
        return html`
            [[display]]
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            field: {
                type: String
            }
        }
    }

    objObserved() {
        if(this.obj !== undefined) {
            var path = this.col.path;

            if(path != null) {
                var obj = this.obj[this.field];
                var tokens = path.split(".");

                for(var t = 0; t < tokens.length; t++) {
                    var key = tokens[t];
                    obj = obj[key];
                }

                this.display = obj;
            }
        }
    }
}

window.customElements.define('fe-grid-complex-data-renderer', GridComplexDataRenderer);