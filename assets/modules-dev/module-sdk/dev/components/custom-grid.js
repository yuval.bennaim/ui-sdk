import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { FEGrid } from '../../../../webcomponents/reusable/data/grid/dr-zp-grid.js';
import './dr-zp-grid-color-circle-renderer.js';
import './dr-zp-grid-complex-data-renderer.js';

class CustomGrid extends FEGrid {
    static get renderersTemplate() {
        var superRendereresTemp = super.renderersTemplate;
        var renderersTemplate = html`
            ${superRendereresTemp}
           
            <template is="dom-if" if="[[isFieldType(col.type, 'circle')]]">
                <dr-zp-grid-color-circle-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-color-circle-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'complex')]]">
                <dr-zp-grid-complex-data-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-complex-data-renderer>
            </template>
        `;

        return renderersTemplate;
    }
}

window.customElements.define('custom-grid', CustomGrid);