import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        .make-smaller {
          width: 500px;
        }
        table {
          padding-left: 10px;
        }
        td {
          padding: 20px 40px;
        }
        div.body-content { padding-top: 20px; padding-bottom: 20px; }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              <dr-zp-tooltip tabindex="0" text="The top tooltip">
                <div><dr-zp-icon type="info" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
            <td>Basic tooltip - top</td>
            <td>
              <dr-zp-tooltip text="The top tooltip">
                <div><dr-zp-icon type="info" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
          </tr>

          <tr>
            <td>
              <dr-zp-tooltip placement="right" text="The right tooltip">
                <div><dr-zp-icon type="help-circle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
            <td>Basic tooltip - right</td>
            <td>
              <dr-zp-tooltip placement="right" text="The right tooltip">
                <div><dr-zp-icon type="help-circle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
          </tr>

          <tr>
            <td>
              <dr-zp-tooltip placement="bottom" text="The bottom tooltip">
                <div><dr-zp-icon type="alert-triangle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
            <td>Basic tooltip - bottom</td>
            <td>
              <dr-zp-tooltip placement="bottom" text="The bottom tooltip">
                <div><dr-zp-icon type="alert-triangle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
          </tr>

          <tr>
            <td>
              <dr-zp-tooltip placement="left" text="The left tooltip">
                <div><dr-zp-icon type="alert-circle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
            <td>Basic tooltip - left</td>
            <td>
              <dr-zp-tooltip placement="left" text="The left tooltip">
                <div><dr-zp-icon type="alert-circle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
          </tr>

          <tr>
            <td>
              <dr-zp-tooltip placement="left" text="[[getString('module-sdk-default', locale)]]">
                <div><dr-zp-icon type="alert-circle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
            <td>Basic tooltip with locale</td>
            <td>
              <dr-zp-tooltip placement="left" text="[[getString('module-sdk-default', locale)]]">
                <div><dr-zp-icon type="alert-circle" size="lg"></dr-zp-icon></div>
              </dr-zp-tooltip>
            </td>
          </tr>

        </table>
    </div>
    `;

    return template;
  }
  static get properties() {
    return {
      value: {
        type: Boolean,
        value: false
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-tooltip', Comp);