import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
        ${superTemp}
            <style>
                .wrapper {
                }
                td {
                    padding: 8px 16px;
                }
                dr-zp-dropdown {
                    width: 100px; 
                    display: block;
                }
            </style>

            <div class="wrapper">
            
                <table>
                    <tr>
                        <td>
                            Filled
                        </td>
                        <td>
                            Hollow
                        </td>
                        <td>
                            Link
                        </td>
                        <td>
                           With Style [Attributes whih are not part of the style class]
                           Ex: Padding:20px
                        </td>
                        <td>
                            Set Data
                        </td>
                        <td>
                            Disabled
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dr-zp-dropdown localized id="dropdown" items="[[listItems]]" selecteditem="{{selecteditem}}" full-width></dr-zp-dropdown>
                        </td>
                        <td>
                            <dr-zp-dropdown localized dropdowntype="bordered" items="[[listItems]]" selecteditem="{{selecteditem}}" full-width></dr-zp-dropdown>
                        </td>
                        <td>
                            <dr-zp-dropdown localized dropdowntype="link" items="[[listItems]]" selecteditem="{{selecteditem}}"></dr-zp-dropdown>
                        </td>
                        <td>
                            <dr-zp-dropdown localized items="[[listItems]]" selecteditem="{{selecteditem}}" list-style="padding:20px" full-width></dr-zp-dropdown>
                        </td>
                        <td>
                            <dr-zp-dropdown localized id="sd" full-width></dr-zp-dropdown>
                        </td>
                        <td>
                            <dr-zp-dropdown localized items="[[listItems]]" selecteditem="{{selecteditem}}" disabled full-width></dr-zp-dropdown>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Iconed, multi layered
                        </td>
                        <td>
                            Scrolled
                        </td>
                        <td>
                            Default  Date
                        </td>
                        <td>
                            Link  Date
                        </td>
                        <td>
                            Disabled Date
                        </td>
                        <td>
                            Filled with Localization
                        </td>
                    </tr>

                    <tr>                       
                        <td>
                            <dr-zp-dropdown localized dropdowntype="bordered" items="[[listItems3]]" field="name" full-width style="width: 200px"></dr-zp-dropdown>
                        </td> 
                        <td>
                            <dr-zp-dropdown localized id="dropdown" items="[[listItems2]]" full-width scrollable></dr-zp-dropdown>
                        </td>
                        <td>
                            <dr-zp-date-dropdown localized id="calendarInput" date="[[selectedDate]]"></dr-zp-date-dropdown>
                        </td>
                        <td>
                            <dr-zp-date-dropdown localized id="calendarInput" date="[[selectedDate]]" dropdowntype="link"></dr-zp-date-dropdown>
                       </td>
                        <td>
                            <dr-zp-date-dropdown localized id="calendarInput" date="[[selectedDate]]" disabled></dr-zp-date-dropdown>
                        </td>
                        <td>
                            <dr-zp-dropdown localized id="dropdown" items="[[listItems]]" selecteditem="{{selecteditem}}" full-width></dr-zp-dropdown>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Filled with Localization
                        </td>
                        <td>
                            Iconed, multi layered with Localization
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <dr-zp-dropdown localized id="dropdown" items="[[listItems]]" full-width></dr-zp-dropdown>
                        </td>
                        <td>
                            <dr-zp-dropdown localized dropdowntype="bordered" items="[[listItems5]]" field="name" full-width style="width: 200px"></dr-zp-dropdown>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            Selected = [[getString(selecteditem, locale)]]
                        </td>
                    </tr>
                </table>  
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            listItems: {
                type: Array,
                value: ['module-sdk-hello', 'module-sdk-goodbye', 'module-sdk-seeya']
            },
            listItems2: {
                type: Array,
                value: ['module-sdk-hello', 'module-sdk-goodbye', 'module-sdk-seeya', 'module-sdk-image', 'module-sdk-spinner', 'module-sdk-modal']
            },
            listItems3: {
                type: Array,
                value: [
                    { name: 'Joe', icon: 'user' },
                    { name: 'Steve', icon: 'user' },
                    {
                        name: 'Management Team', icon: 'users',
                        children: [
                            { name: 'Mike', icon: 'user' },
                            { name: 'Rick', icon: 'user' },
                            {
                                name: 'Project A Team', icon: 'users',
                                children: [
                                    { name: 'Jim', icon: 'user' },
                                    { name: 'Jane', icon: 'user' }
                                ]
                            },
                            {
                                name: 'Project B Team', icon: 'users',
                                children: [
                                    { name: 'Suzy', icon: 'user' },
                                    {
                                        name: 'Project B Web Team', icon: 'user',
                                        children: [
                                            { name: 'Lilly', icon: 'user' },
                                            { name: 'Chuck', icon: 'user' }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            listItems5: {
                type: Array,
                value: [
                    { name: 'module-sdk-input', icon: 'user' },
                    {
                        name: 'module-sdk-am-chart', icon: 'users',
                        children: [
                            { name: 'module-sdk-charts', icon: 'user' },
                            { name: 'module-sdk-bar-chart', icon: 'user' },
                            {
                                name: 'module-sdk-pie-chart', icon: 'users',
                                children: [
                                    { name: 'module-sdk-geo-chart', icon: 'user' },
                                    { name: 'module-sdk-donut-chart', icon: 'user' }
                                ]
                            }
                        ]
                    }
                ]
            },
            selecteditem: {
                type: String,
                value: null
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.selecteditem = this.listItems[0];

        this.$.sd.items = this.listItems; //when no databinding, set the value programatically
        this.$.sd.selecteditem = this.listItems[2];

        this.$.sd.addEventListener('dropdown-change', e => {
            console.info('dropdown-change', e);
        });

        this.$.dropdown.addEventListener('dropdown-change', e => {
            console.info('dropdown-change', e);
        });

        this.selectedDate = {
            year: 2020,
            month: 9,
            day: 24,
            hour: 1,
            minute: 2,
            second: 3
        };
    }
}

window.customElements.define('module-sdk-dropdown', Comp);