import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-toggle {
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default
            </td>
            <td>
              <dr-zp-toggle checked="{{checked}}"></dr-zp-toggle>
            </td>
            <td>
              Checked: [[checked]]
            </td>
          </tr>

          <tr>
            <td>
              Custom On/Off Labeling
            </td>
            <td colspan="2">
              <dr-zp-toggle onlabel="[[getString('module-sdk-hello', locale)]]" offlabel="[[getString('module-sdk-goodbye', locale)]]" checked="{{checked}}"></dr-zp-toggle>
            </td>
          </tr>

          <tr>
            <td>
              Disabled
            </td>
            <td colspan="2">
              <dr-zp-toggle disabled="true"></dr-zp-toggle>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      checked: {
        type: Boolean,
        value: false
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();
    var toggles = dom(this.root).querySelectorAll('dr-zp-toggle');

    toggles.forEach((toggle) => {
        toggle.addEventListener('dr-zp-toggle-clicked', e => {
            console.info('dr-zp-toggle-clicked', e.detail.checked);
        }); 
    });
  }
}

window.customElements.define('module-sdk-toggle', Comp);