import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-file-selector {
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default with Icon
            </td>
            <td>
              <dr-zp-file-selector label="[[getString('module-sdk-file-label', locale)]]" value="{{value}}" icon="upload"></dr-zp-file-selector>
            </td>
          </tr>

          <tr>
            <td>
              Multiple
            </td>
            <td>
              <dr-zp-file-selector label="[[getString('module-sdk-multiple-file-label', locale)]]" value="{{value}}" multiple="true"></dr-zp-file-selector>
            </td>
          </tr>

          <tr>
            <td>
              Disabled
            </td>
            <td>
              <dr-zp-file-selector label="[[getString('module-sdk-disabled-file-label', locale)]]" value="{{value}}" disabled="true"></dr-zp-file-selector>
            </td>
            <td></td>
          </tr>

          <tr>
            <td>
              Has Validation Error
            </td>
            <td>
              <dr-zp-file-selector label="[[getString('module-sdk-error-file-label', locale)]]" value="{{value}}" has-validation-error="true" message="[[getString('module-sdk-invalid-input', locale)]]"></dr-zp-file-selector>
            </td>
            <td></td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      value: {
        type: String,
        value: null
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();

    document.body.addEventListener('file-selection-changed', (e) => {
      console.info('file-selection-changed', e.detail.files);
    })
  }
}

window.customElements.define('module-sdk-file-selector', Comp);