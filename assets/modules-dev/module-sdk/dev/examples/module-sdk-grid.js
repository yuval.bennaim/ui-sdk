import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
            ${superTemp}
            
            <div class="grid-wrapper">
                <dr-zp-grid 
                    id="sampleGrid"
                    grid-title="[[getString('module-sdk-simulated-grid-title', locale)]]"
                    primary-key="id"
                    columns="{{columnDefs}}"
                    data="[[gridRowsPage]]"
                    filters="[[filters]]"
                    selected="[[selected]]"
                    details="[[details]]"
                    loading="[[loading]]"
                    allow-action-slot="[[allowActionSlot]]">
                    <h4> [ Action Button Holder ]</h4>
                </dr-zp-grid>
            </div>

            <div class$="[[determinePaginationClass(loading)]]">  
                <dr-zp-pagination id="pagination" class="pagination" active-page="[[page]]" per-page-opt="[[pageSizesArr]]" total="[[filteredRows.length]]" increment="[[pageSize]]" unit="[[getString('module-sdk-things', locale)]]"></dr-zp-pagination>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            columnDefs: {
                type: Array,
                value: [
                    {
                        caption: '%module-sdk-selected',
                        field: '_selected',
                        key: '_selected', //must be unique
                        type: 'checkbox',
                        visible: true,
                        width: 40,
                        dock: 'left'
                    },
                    {
                        caption: '%module-sdk-id',
                        field: 'index',
                        key: 'index', //must be unique
                        type: 'number',
                        visible: true,
                        filterable: false,
                        sortable: true,
                        width: 70,
                        dock: 'left'
                    },
                    {
                        caption: '%module-sdk-os',
                        field: 'os',
                        key: 'os', //must be unique
                        type: 'text',
                        visible: true,
                        filterable: false,
                        sortable: true,
                        width: 70,
                        iconMap: {
                            windows: { icon: 'windows-logo', color: 'skyblue' },
                            apple: { icon: 'apple-logo', color: 'white' }
                        },
                        iconField: 'os',
                    },
                    {
                        caption: '%module-sdk-name-title',
                        field: 'name',
                        key: 'name', //must be unique
                        type: 'link',
                        visible: true,
                        filterable: true,
                        sortable: true,
                        width: 100
                    },
                    {
                        caption: '%module-sdk-date',
                        field: 'date',
                        key: 'date', //must be unique
                        type: 'date',
                        visible: true,
                        width: 170,
                    },
                    {
                        caption: '%module-sdk-enabled',
                        field: 'enabled',
                        key: 'enabled', //must be unique
                        type: 'boolean',
                        filterable: true,
                        trueLabel: 'enabled',
                        falseLabel: 'disabled',
                        visible: true,
                        width: 100
                    },
                    {
                        caption: '%module-sdk-toogle',
                        field: 'enabled',
                        key: 'enabled_toggle', //must be unique
                        type: 'toggle', // toogle renderer enabled
                        on: 'enabled',
                        off: 'disabled',
                        visible: false,
                        width: 70
                    },
                    {
                        caption: '%module-sdk-paths',
                        field: 'paths',
                        key: 'paths', //must be unique
                        type: 'array',
                        visible: true,
                        width: 70
                    },
                    {
                        caption: '%module-sdk-color',
                        field: 'hex',
                        key: 'hex', //must be unique
                        type: 'color',
                        visible: true,
                        width: 70
                    },
                    {
                        field: "status",
                        caption: "%module-sdk-status",
                        key: "status", //must be unique
                        sortable: true,
                        type: "status",
                        visible: true,
                        width: 70,
                        editable: true,
                        iconMap: {
                            enabled: {
                                color: "#02b548", // status renderer with icon and color map
                            },
                            disabled: {
                                color: "#b55402",
                            }
                        }
                    },
                    {
                        caption: '%module-sdk-hex',
                        field: 'hex',
                        key: 'hexstr', //must be unique
                        type: 'text',
                        visible: true,
                        filterable: true,
                        sortable: true,
                        width: 70
                    },
                    {
                        caption: '%module-sdk-systems',
                        field: 'systems',
                        key: 'systems', //must be unique
                        type: 'icon',
                        visible: true,
                        width: 70
                    },
                    // {
                    //     caption: '%module-sdk-do-a',
                    //     key: 'doa', //must be unique
                    //     type: 'button',
                    //     action: 'Do A',
                    //     visible: true
                    // },
                    // {
                    //     caption: '%module-sdk-do-b',
                    //     key: 'dob', //must be unique
                    //     type: 'button',
                    //     action: 'Do B',
                    //     visible: true
                    // },
                    {
                        caption: '%module-sdk-host-info',
                        field: 'hostInfo',
                        key: 'hostInfo', //must be unique
                        type: 'icon-text',
                        visible: true,
                        width: 70
                    },
                    {
                        caption: '%module-sdk-actions',
                        key: 'actions', //must be unique
                        type: 'actions',
                        options: [{ label: "Do Something Here" }, { label: "Do Something There" }],
                        visible: true,
                        dock: 'right'
                    }
                ]
            },
            gridRows: {
                type: Array,
                value: []
            },
            loading: {
                type: Boolean,
                value: false
            },
            pageSizesArr: {
                type: Array,
                value: [25, 50, 75, 100]
            },
            pageSize: {
                type: Number,
                observer: 'pageSizeChanged'
            },
            pages: {
                type: Number,
                value: 0
            },
            page: {
                type: Number,
                value: 1,
                observer: 'pageChanged'
            },
            allowActionSlot: {
                type: Boolean,
                value: false
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();

        this.pageSize = this.pageSizesArr[0];

        this.filters = [];
        var key = "sampleGrid_filters";

        if (localStorage.getItem(key)) {
            this.filters = JSON.parse(localStorage.getItem(key));
        }

        setTimeout(() => {
            this.initData(1000);
            this.filterData();
        }, 1000);

        this.$.pagination.addEventListener('pagination-per-page-change', e => {
            var perPage = e.detail.perPage;
            console.info('pagination-per-page-change', perPage);
            this.pageSize = perPage;
        });

        this.$.sampleGrid.addEventListener('row-action', e => {
            var row = e.detail.row;
            console.info('row-action', row);
        });

        this.$.sampleGrid.addEventListener('row-selected', e => {
            var action = e.detail.action;
            var row = e.detail.row;
            console.info('row-selected', row);
        });

        this.$.sampleGrid.addEventListener('row-button-click', e => {
            var row = e.detail.row;
            var action = e.detail.action;
            console.info('sampleGrid row-button-click', action);

            var timestamp = new Date().getTime();
            var msg = `row-button-click ${action} for row ${row.id}`;

            document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                detail: {
                    timestamp: timestamp,
                    source: 'dr-zp SDK Grid',
                    type: 'info',
                    message: msg,
                    autodismissable: true,
                    duration: 5000
                }
            }));
        });

        this.$.sampleGrid.addEventListener('list-action', e => {
            var row = e.detail.row;
            var action = e.detail.action;
            console.info('sampleGrid list-action', action);

            var timestamp = new Date().getTime();
            var msg = `list-action ${action} for row ${row.id}`;

            document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                detail: {
                    timestamp: timestamp,
                    source: 'dr-zp SDK Grid',
                    type: 'info',
                    message: msg,
                    autodismissable: true,
                    duration: 5000
                }
            }));
        });

        this.$.sampleGrid.addEventListener('filter-change', e => {
            this.filters = e.detail.filters;
            console.info('sort-change', this.filters);
            this.filterData();
        });

        this.$.sampleGrid.addEventListener('sort-change', e => {
            var col = e.detail.column;
            console.info('sort-change', col);
            this.sortData(col);
        });

        this.$.pagination.addEventListener('pagination-page-change', e => {
            this.page = e.detail.page;
            console.info('pagination-page-change', this.page);
        });
    }

    sortData(col) {
        var field = col.field;
        var dir = col.asc;
        console.info('sortData', field, dir);

        /* local sort example  */
        this.gridRows.sort((aVal, bVal) => {
            var a = aVal[field];
            var b = bVal[field];

            if (dir > 0) { //sort string ascending
                return (a > b) ? 1 : (a < b) ? -1 : 0;
            }
            else { //descending
                return (a < b) ? 1 : (a > b) ? -1 : 0
            }
        });

        this.filteredRows = this.utils._helper_copy(this.gridRows);
        this.filterData();
    }

    filterData() {
        if (this.filters.length > 0) {
            var filterdData = this.gridRows.filter((item) => {
                var pass = true;

                for (var f = 0; f < this.filters.length; f++) {
                    var fltr = this.filters[f];
                    var field = fltr.sortKey;
                    var val = fltr.value.toLowerCase();
                    var itemField = item[field];

                    if (pass) {
                        if (itemField === undefined) { //if undefined, fail
                            pass = false;
                        }
                        else {
                            if (typeof itemField === "boolean") {
                                if (val === "true" || val === "false") {
                                    var bVal = (val === 'true');

                                    if (bVal !== itemField) {
                                        pass = false;
                                    }
                                }
                                else {
                                    pass = false;
                                }
                            }
                            else { //string convert to regex
                                if (val.length > 0) {
                                    itemField = itemField.toLowerCase();
                                    var regex = new RegExp(val);
                                    var patt = new RegExp(val);
                                    var found = patt.test(itemField);
                                    pass = found === true;
                                }
                            }
                        }
                    }
                }

                return pass;
            });

            this.filteredRows = this.utils._helper_copy(filterdData);
        }
        else {
            this.filteredRows = this.utils._helper_copy(this.gridRows);
        }

        console.info('filterData', this.filteredRows);
        this.pageChanged();
    }

    determinePaginationClass(loading) {
        var cls = 'input-fields-wrapper';

        if (loading) {
            cls += ' hidden';
        }

        return cls;
    }

    generteIPSub() {
        var sub = parseInt(Math.random() * 256);
        sub = Math.min(sub, 255);
        sub = Math.max(sub, 0);
        return sub;
    }

    componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    rgbToHex(r, g, b, a) {
        return this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b); // + this.componentToHex(a);
    }

    initData(num) {
        this.gridRows = [];
        var arr = [];

        for (var n = 0; n < num; n++) {
            var row = {};
            row.index = n;
            row.id = 'row_' + n;
            row.index = n;
            row.expanded = false;
            row.date = new Date().getTime();
            row._selected = false;
            row.enabled = (n % 5 == 0) ? true : false;
            row.name = 'Row ' + n;
            row.longText = "Some_very_long_non_breaking_text";

            var ip = {};
            var a = this.generteIPSub();
            var b = this.generteIPSub();
            var c = this.generteIPSub();
            var d = this.generteIPSub();
            row.paths = [a + "." + b + "." + c + ".1",
            a + "." + b + "." + c + ".2",
            a + "." + b + "." + c + ".3"];

            if (n % 5 == 0) {
                row.paths.push(a + "." + b + "." + c + ".4");
            }

            row.hex = this.rgbToHex(a, b, c, d);
            row.systems = ["apple-logo", "linux-logo", "windows-logo"];
            row.hostInfo = [{ icon: "monitor", text: 45 }, { icon: "shield", text: 1 }, { icon: "tag", text: 0 }];
            row.status = (n % 5 == 0) ? "enabled" : "disabled"; // pushing status field
            row.os = (n % 3 == 0) ? "windows" : "apple"; // pushing logo field
            arr.push(row);
        }

        this.gridRows = arr;
        this.filteredRows = this.utils._helper_copy(this.gridRows);
        this.pageChanged();
    }

    pageSizeChanged() {
        this.page = 1;
        this.pageChanged();
    }

    pageChanged() {
        setTimeout(() => {
            this.loading = true;
        });

        setTimeout(() => {
            if (this.filteredRows) {
                for (var r = 0; r < this.filteredRows.length; r++) {
                    this.filteredRows[r].expanded = false;
                }

                var from = (this.page - 1) * this.pageSize;
                var to = this.page * this.pageSize;
                console.info('pageChanged', this.page, from, to);
                this.gridRowsPage = this.filteredRows.slice(from, to);
                this.loading = false;
            }
        }, 1000);
    }
}

window.customElements.define('module-sdk-grid', Comp);