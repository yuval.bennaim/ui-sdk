import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
            <style>
                .wrapper {
                }
                td {
                    padding: 20px;
                }
            </style>

            <div class="wrapper">
                <table>
                    <tr>
                        <td>
                            Small
                        </td>
                        <td>
                            <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-primary', locale)]]"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="secondary" size="small" label="[[getString('module-sdk-secondary', locale)]]"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="link" size="small" label="[[getString('module-sdk-link', locale)]]"></dr-zp-button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Medium (Default)
                        </td>
                        <td>
                            <dr-zp-button type="primary" label="[[getString('module-sdk-primary', locale)]]"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="secondary" label="[[getString('module-sdk-secondary', locale)]]"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="link" label="[[getString('module-sdk-link', locale)]]"></dr-zp-button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Large
                        </td>
                        <td>
                            <dr-zp-button type="primary" size="large" label="[[getString('module-sdk-primary', locale)]]"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="secondary" size="large" label="[[getString('module-sdk-secondary', locale)]]"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="link" size="large" label="[[getString('module-sdk-link', locale)]]"></dr-zp-button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Danger
                        </td>
                        <td>
                            <dr-zp-button type="primary" label="[[getString('module-sdk-primary', locale)]]" danger="true"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="secondary" label="[[getString('module-sdk-secondary', locale)]]" danger="true"></dr-zp-button>
                        </td>
                        <td>
                            <dr-zp-button type="link" label="[[getString('module-sdk-link', locale)]]" danger="true"></dr-zp-button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            With Icon
                        </td>
                        <td>
                            <dr-zp-button type="primary" label="[[getString('module-sdk-settings', locale)]]" icon="gear"></dr-zp-button>
                        </td>
                    </tr>      
                    <tr>
                        <td>
                            With Slotted label and Icon
                        </td>
                        <td>
                            <dr-zp-button type="primary">
                                <span slot="slot_content">
                                    <dr-zp-icon type="gear" size="sm" style="position: relative; top: 3px; margin-right: 2px;"></dr-zp-icon>
                                    [[getString('module-sdk-settings', locale)]]
                                </span>
                            </dr-zp-button>
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            Disabled
                        </td>
                        <td>
                            <dr-zp-button disabled="true" type="primary" size="small" label="[[getString('module-sdk-disabled', locale)]]"></dr-zp-button>
                        </td>
                        <td>
                    </tr> 
                    <tr>
                        <td>
                            Full Width
                        </td>
                        <td>
                            <dr-zp-button label="[[getString('module-sdk-fullWidth', locale)]]" full-width=true></dr-zp-button>
                        </td>
                        <td>
                    </tr>                       
                </table>              
            </div>
        `;

        return template;
    }

    static get properties() {
        return {            
        }
    }

    connectedCallback() {
        super.connectedCallback();
        var buttons = dom(this.root).querySelectorAll('dr-zp-button');

        buttons.forEach((button) => {
            button.addEventListener('dr-zp-button-click', e => {
                var source = e.detail.source;
                var butt = source.label;
                var timestamp = new Date().getTime();
                var msg = `You clicked on ${butt}`;

                document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                    detail: { 
                        timestamp: timestamp,
                        source: 'dr-zp SDK Tree',
                        type: 'info',
                        message: msg,
                        autodismissable: true,
                        duration: 5000 
                    }
                }));
            }); 
        });
    }
}

window.customElements.define('module-sdk-button', Comp);