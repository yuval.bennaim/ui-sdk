import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
        ${superTemp}
        <style>
        </style>

        <div>
            <dr-zp-tree data="[[treeData]]"></dr-zp-tree>
        </div>
    `;

        return template;
    }

    static get properties() {
        return {
            alertCounter: {
                type: Number,
                value: 0
            },
            treeData: {
                type: Array,
                value: [
                    {
                        "name": "Branch A",
                        "nodes": [
                            {
                                "name": "Branch A.1",
                                "nodes": [
                                    {
                                        "name": "Leaf A.1.1"
                                    },
                                    {
                                        "name": "Leaf A.1.2",
                                        "nodes": [
                                            {
                                                "name": "Leaf A.1.2.1"
                                            },
                                            {
                                                "name": "Leaf A.1.2.2"
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "name": "Branch A.2",
                                "nodes": [
                                    {
                                        "name": "Leaf A.2.1"
                                    },
                                    {
                                        "name": "Leaf A.2.2"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "name": "Branch B",
                        "expanded": true,
                        "nodes": [
                            {
                                "name": "Branch B.1",
                                "nodes": [
                                    {
                                        "name": "Leaf B.1.1"
                                    },
                                    {
                                        "name": "Leaf B.1.2"
                                    }
                                ]
                            },
                            {
                                "expanded": true,
                                "name": "Branch B.2",
                                "nodes": [
                                    {
                                        "name": "Leaf B.2.1"
                                    },
                                    {
                                        "name": "Leaf B.2.2"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
        var trees = dom(this.root).querySelectorAll('dr-zp-tree');

        trees.forEach((tree) => {
            tree.addEventListener('tree-navigation', e => {
                var leafNode = e.detail.data.name;
                var timestamp = new Date().getTime();
                var msg = `You clicked on ${leafNode}`;

                document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                    detail: {
                        timestamp: timestamp,
                        source: 'dr-zp SDK Tree',
                        type: 'info',
                        message: msg,
                        autodismissable: true,
                        duration: 5000
                    }
                }));
            });
        });
    }
}

window.customElements.define('module-sdk-tree', Comp);