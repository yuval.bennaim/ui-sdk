import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
            <style>
                .wrapper {
                    border: 1px;
                }
                table, th, td {
                    border: 1px solid rgb(255, 255, 255);
                    border-collapse: collapse;
                }
                td, th {
                    padding: 20px;
                }
                dr-zp-input {
                    width: 300px; 
                    display: block;
                }
            </style>

            <div class="wrapper">
                <table>
                    <tr>
                        <th>Types</th>
                        <th>Component</th>
                        <th>Selected Color</th>
                    </tr>
                    <tr>
                        <td>Default</td>
                        <td>
                            <dr-zp-color-input label="[[getString('module-sdk-default-color-input', locale)]]" value="{{color}}"></dr-zp-color-input>
                        </td>
                        <td><strong>{{color}}</strong></td>
                    </tr>
                    <tr>
                        <td>Disabled</td>
                        <td>
                            <dr-zp-color-input label="[[getString('module-sdk-disabled-color-input', locale)]]" value="{{color}}" disabled="true"></dr-zp-color-input>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Has Validation Error</td>
                        <td>
                        <dr-zp-color-input label="[[getString('module-sdk-error-color-input', locale)]]" value="{{color}}" has-validation-error="true" message="invalid input"></dr-zp-color-input>
                        </td>
                        <td></td>
                    </tr>
                </table>              
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            color: {
                type: String,
                value: '#ffffff',
                notify: true
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }
}

window.customElements.define('module-sdk-color-input', Comp);