import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
        ${superTemp}
        <style>
            td {
                padding: 8px 16px;
            }
            dr-zp-multi-select {
                width: 120px;
                display: inline-block;
            }
            
        </style>
        <div class="wrapper">
            <table>
            <tr>
                <td>
                    <dr-zp-multi-select id="multiselect" label="[[getString('module-sdk-select', locale)]]" items="{{listItems}}" full-width></dr-zp-multi-select>
                </td>
                <td>
                    Original<br>
                    [[arrToJSON(listItems)]]
                </td>
                
            </tr>
            <tr>
            <td>
                <dr-zp-multi-select id="multiselect" label="[[getString('module-sdk-select', locale)]]" items="{{listItems}}" full-width dropdowntype="icon" icon="compact" size="xl"></dr-zp-multi-select>
            </td>
            <td>
                Original<br>
                [[arrToJSON(listItems)]]
            </td>
            
        </tr>
            <tr>
                <td>
                    <dr-zp-multi-select id="multiselect2" label="[[getString('module-sdk-select', locale)]]" items="{{listItems}}" field="type" field-checked="other" full-width></dr-zp-multi-select>
                </td>
                <td>
                    Evented<br>
                    [[arrToJSON(selectedItems)]]
                </td>
            </tr>
            </table>
        </div>`;

        return template;
    }

    static get properties() {
        return {
            listItems: {
                type: Array,
                value: [
                    { name: 'Hello', type: "a", "selected": true, "other": false, },
                    { name: 'Goodbye', type: "b", "selected": false, "other": false },
                    { name: 'See Ya!', type: "c", "selected": false, "other": false }
                ]
            },
            selectedItems: {
                type: String,
                value: []
            }
        };
    }

    connectedCallback() {
        super.connectedCallback();
        this.selectedItems = this.utils._helper_copy(this.listItems);

        this.$.multiselect2.addEventListener('multi-select-change', e => {
            this.selectedItems = e.detail.items;
            console.info('multi-select-change', this.selectedItems);
        });
    }

    arrToJSON(arr) {
        return JSON.stringify(arr);
    }
}

window.customElements.define('module-sdk-multi-select', Comp);