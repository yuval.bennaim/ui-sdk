import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}

            <style>
                .wrapper {
                }
                th {
                    text-align: left;
                }
                td {
                    padding: 5px 20px 5px 0px;
                    vertical-align: top;
                }
            </style>

            <div class="wrapper">
                <table>
                    <tr>
                        <td colspan="4">
                            Selected: [[getString(selectedItem, locale)]]
                        </td>                       
                    </tr>
                    <tr>
                        <td style="padding-top: 12px;">Horizontal</td>
                        <td>
                            <dr-zp-radio-group localized name="test1" items="[[listItems]]" selected-item="{{selectedItem}}" direction="horizontal"></dr-zp-radio-group>
                        </td>                        
                    </tr>
                    <tr>
                        <td style="padding-top: 12px;">Vertical</td>
                        <td>
                            <dr-zp-radio-group localized name="test2" items="[[listItems]]" selected-item="{{selectedItem}}" direction="vertical"></dr-zp-radio-group>
                        </td>                        
                    </tr>
                </table>              
            </div>    
        `;

        return template;
    }

    static get properties() {
        return {  
            listItems: {
                type: Array,
                value: ['module-sdk-hello', 'module-sdk-goodbye', 'module-sdk-seeya']
            },
            selectedItem: {
                type: String,
                value: null,
                observer: 'selectedChanged'
            },          
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.selectedItem = this.listItems[1];
        var rgroups = dom(this.root).querySelectorAll('dr-zp-radio-group');
 
        rgroups.forEach(rgroup => {
            rgroup.addEventListener('radio-group-change', e => {
                console.info('radio-group-change', e.detail.selected);
            }); 
        });
    }

    selectedChanged() {
        console.info('selectedChanged', this.selectedItem);
    }
}

window.customElements.define('module-sdk-radio-group', Comp);