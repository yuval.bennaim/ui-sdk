import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-numeric-input {
          width: 300px; 
          display: block;
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default with min, max & step
            </td>
            <td>
              <dr-zp-numeric-input label="[[getString('module-sdk-input-label', locale)]]" value="{{value}}" min="1" max="10" step="2"></dr-zp-numeric-input>
            </td>
          </tr>

          <tr>
            <td>
              Readonly
            </td>
            <td>
              <dr-zp-numeric-input label="[[getString('module-sdk-readonly-input-label', locale)]]" value="{{value}}" readonly="true"></dr-zp-numeric-input>
            </td>
          </tr>

          <tr>
            <td>
              Disabled
            </td>
            <td>
              <dr-zp-numeric-input label="[[getString('module-sdk-disabled-input-label', locale)]]" value="{{value}}" disabled="true"></dr-zp-numeric-input>
            </td>
          </tr>

          <tr>
            <td>
              With Validation Error
            </td>
            <td>
              <dr-zp-numeric-input label="[[getString('module-sdk-error-input-label', locale)]]" value="{{value}}" has-validation-error="true" message="[[getString('module-sdk-invalid-input', locale)]]"></dr-zp-numeric-input>
            </td>
          </tr>

        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      value: {
        type: Number,
        value: null
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-numeric-input', Comp);