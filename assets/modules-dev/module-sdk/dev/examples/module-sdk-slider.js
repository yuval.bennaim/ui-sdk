import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        td {
          padding: 20px;
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default (Max = 100)
            </td>
            <td>
              <dr-zp-slider value="{{value}}"></dr-zp-slider>
            </td>
          </tr>

          <tr>
            <td>
              With label (Max = 100)
            </td>
            <td>
              <dr-zp-slider label="[[getString('module-sdk-label-slider', locale)]]" value="{{value}}"></dr-zp-slider>
            </td>
          </tr>

          <tr>
            <td>
              With Binding (Max = 200)
            </td>
            <td>
              <dr-zp-slider label="[[getString('module-sdk-higher-label-slider', locale)]]" value-label="module-sdk-units" value="{{value}}" max="200" show="true"></dr-zp-slider>
            </td>
          </tr>

          <tr>
            <td>
              Disabled
            </td>
            <td>
              <dr-zp-slider label="[[getString('module-sdk-disabled-label-slider', locale)]]" value="50" disabled="true" show="true"></dr-zp-slider>
            </td>
          </tr>
          
          <tr>         
            <td>
              Stepped and unbound
            </td>
            <td>
              <dr-zp-slider min="5" max="100" label="[[getString('module-sdk-stepped-unbound-label-slider', locale)]]" value="15" step="5" show="true"></dr-zp-slider>
            </td>  
          </tr>
          
        </table>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      value: {
        type: Number,
        value: 90
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-slider', Comp);