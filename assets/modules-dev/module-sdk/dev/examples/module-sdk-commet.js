import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
            <style>
                .wrapper {
                }

                .fire {
                    font-size: 24px;
                    filter: blur(0.02em);
                    -webkit-filter: blur(0.02em);
                    margin: 3em auto 0 auto;
                    position: relative;
                    width: 6em;
                    height: 12em;
                    animation: spin 4s linear infinite;
                  }
                  
                  .particle {
                    animation: rise 1s ease-in infinite;
                    background-image: radial-gradient(#ff5000 20%, rgba(255, 80, 0, 0) 70%);
                    border-radius: 50%;
                    mix-blend-mode: screen;
                    opacity: 0;
                    position: absolute;
                    bottom: 0;
                    width: 5em;
                    height: 5em;
                  }

                  .particle:nth-of-type(1) {
                    animation-delay: 0.9829926508s;
                    left: calc((100% - 5em) * 0);
                  }
                  .particle:nth-of-type(2) {
                    animation-delay: 0.2405480922s;
                    left: calc((100% - 5em) * 0.02);
                  }
                  .particle:nth-of-type(3) {
                    animation-delay: 0.0942391915s;
                    left: calc((100% - 5em) * 0.04);
                  }
                  .particle:nth-of-type(4) {
                    animation-delay: 0.86229945s;
                    left: calc((100% - 5em) * 0.06);
                  }
                  .particle:nth-of-type(5) {
                    animation-delay: 0.3452851461s;
                    left: calc((100% - 5em) * 0.08);
                  }
                  .particle:nth-of-type(6) {
                    animation-delay: 0.8947142866s;
                    left: calc((100% - 5em) * 0.1);
                  }
                  .particle:nth-of-type(7) {
                    animation-delay: 0.5150258662s;
                    left: calc((100% - 5em) * 0.12);
                  }
                  .particle:nth-of-type(8) {
                    animation-delay: 0.3536178445s;
                    left: calc((100% - 5em) * 0.14);
                  }
                  .particle:nth-of-type(9) {
                    animation-delay: 0.9892403514s;
                    left: calc((100% - 5em) * 0.16);
                  }
                  .particle:nth-of-type(10) {
                    animation-delay: 0.488001217s;
                    left: calc((100% - 5em) * 0.18);
                  }
                  .particle:nth-of-type(11) {
                    animation-delay: 0.6313408249s;
                    left: calc((100% - 5em) * 0.2);
                  }
                  .particle:nth-of-type(12) {
                    animation-delay: 0.9588814592s;
                    left: calc((100% - 5em) * 0.22);
                  }
                  .particle:nth-of-type(13) {
                    animation-delay: 0.254266199s;
                    left: calc((100% - 5em) * 0.24);
                  }
                  .particle:nth-of-type(14) {
                    animation-delay: 0.4203261411s;
                    left: calc((100% - 5em) * 0.26);
                  }
                  .particle:nth-of-type(15) {
                    animation-delay: 0.9844818757s;
                    left: calc((100% - 5em) * 0.28);
                  }
                  .particle:nth-of-type(16) {
                    animation-delay: 0.7595378603s;
                    left: calc((100% - 5em) * 0.3);
                  }
                  .particle:nth-of-type(17) {
                    animation-delay: 0.2199106262s;
                    left: calc((100% - 5em) * 0.32);
                  }
                  .particle:nth-of-type(18) {
                    animation-delay: 0.4229898688s;
                    left: calc((100% - 5em) * 0.34);
                  }
                  .particle:nth-of-type(19) {
                    animation-delay: 0.6713799295s;
                    left: calc((100% - 5em) * 0.36);
                  }
                  .particle:nth-of-type(20) {
                    animation-delay: 0.4069332076s;
                    left: calc((100% - 5em) * 0.38);
                  }
                  .particle:nth-of-type(21) {
                    animation-delay: 0.8899307133s;
                    left: calc((100% - 5em) * 0.4);
                  }
                  .particle:nth-of-type(22) {
                    animation-delay: 0.5926211421s;
                    left: calc((100% - 5em) * 0.42);
                  }
                  .particle:nth-of-type(23) {
                    animation-delay: 0.1176650068s;
                    left: calc((100% - 5em) * 0.44);
                  }
                  .particle:nth-of-type(24) {
                    animation-delay: 0.6094523976s;
                    left: calc((100% - 5em) * 0.46);
                  }
                  .particle:nth-of-type(25) {
                    animation-delay: 0.6132736605s;
                    left: calc((100% - 5em) * 0.48);
                  }
                  .particle:nth-of-type(26) {
                    animation-delay: 0.6657017713s;
                    left: calc((100% - 5em) * 0.5);
                  }
                  .particle:nth-of-type(27) {
                    animation-delay: 0.1679431525s;
                    left: calc((100% - 5em) * 0.52);
                  }
                  .particle:nth-of-type(28) {
                    animation-delay: 0.0497096368s;
                    left: calc((100% - 5em) * 0.54);
                  }
                  .particle:nth-of-type(29) {
                    animation-delay: 0.4637049785s;
                    left: calc((100% - 5em) * 0.56);
                  }
                  .particle:nth-of-type(30) {
                    animation-delay: 0.5527035821s;
                    left: calc((100% - 5em) * 0.58);
                  }
                  .particle:nth-of-type(31) {
                    animation-delay: 0.0429744619s;
                    left: calc((100% - 5em) * 0.6);
                  }
                  .particle:nth-of-type(32) {
                    animation-delay: 0.1919481437s;
                    left: calc((100% - 5em) * 0.62);
                  }
                  .particle:nth-of-type(33) {
                    animation-delay: 0.9434603495s;
                    left: calc((100% - 5em) * 0.64);
                  }
                  .particle:nth-of-type(34) {
                    animation-delay: 0.431474s;
                    left: calc((100% - 5em) * 0.66);
                  }
                  .particle:nth-of-type(35) {
                    animation-delay: 0.4632997972s;
                    left: calc((100% - 5em) * 0.68);
                  }
                  .particle:nth-of-type(36) {
                    animation-delay: 0.5585720194s;
                    left: calc((100% - 5em) * 0.7);
                  }
                  .particle:nth-of-type(37) {
                    animation-delay: 0.2652990973s;
                    left: calc((100% - 5em) * 0.72);
                  }
                  .particle:nth-of-type(38) {
                    animation-delay: 0.9173138411s;
                    left: calc((100% - 5em) * 0.74);
                  }
                  .particle:nth-of-type(39) {
                    animation-delay: 0.0542719838s;
                    left: calc((100% - 5em) * 0.76);
                  }
                  .particle:nth-of-type(40) {
                    animation-delay: 0.5356891705s;
                    left: calc((100% - 5em) * 0.78);
                  }
                  .particle:nth-of-type(41) {
                    animation-delay: 0.2789015526s;
                    left: calc((100% - 5em) * 0.8);
                  }
                  .particle:nth-of-type(42) {
                    animation-delay: 0.9796953009s;
                    left: calc((100% - 5em) * 0.82);
                  }
                  .particle:nth-of-type(43) {
                    animation-delay: 0.6919063095s;
                    left: calc((100% - 5em) * 0.84);
                  }
                  .particle:nth-of-type(44) {
                    animation-delay: 0.2113044329s;
                    left: calc((100% - 5em) * 0.86);
                  }
                  .particle:nth-of-type(45) {
                    animation-delay: 0.1752525205s;
                    left: calc((100% - 5em) * 0.88);
                  }
                  .particle:nth-of-type(46) {
                    animation-delay: 0.7845429929s;
                    left: calc((100% - 5em) * 0.9);
                  }
                  .particle:nth-of-type(47) {
                    animation-delay: 0.2279969309s;
                    left: calc((100% - 5em) * 0.92);
                  }
                  .particle:nth-of-type(48) {
                    animation-delay: 0.1715358783s;
                    left: calc((100% - 5em) * 0.94);
                  }
                  .particle:nth-of-type(49) {
                    animation-delay: 0.2095735419s;
                    left: calc((100% - 5em) * 0.96);
                  }
                  .particle:nth-of-type(50) {
                    animation-delay: 0.9571269635s;
                    left: calc((100% - 5em) * 0.98);
                  }
                  
                  @keyframes rise {
                    from {
                      opacity: 0;
                      transform: translateY(0) scale(1);
                    }
                    25% {
                      opacity: 1;
                    }
                    to {
                      opacity: 0;
                      transform: translateY(-10em) scale(0);
                    }
                  }

                  @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
                  @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
                  @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }
                
            </style>

            <div class="wrapper">
                <div class="fire">
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                    <div class="particle"></div>
                </div>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {            
        }
    }

    connectedCallback() {
        super.connectedCallback();
        
    }
}

window.customElements.define('module-sdk-commet', Comp);