import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
  
      <style>
        .wrapper {
        }
        table { width: 100%; }
        td {
          padding: 20px;
        }
        dr-zp-tabs {
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Horizontal
            </td>
            <td class="content">
              <dr-zp-tabs
                items={{tabs}}
                activeindex={{activeindex}}
                title-key="title"
                primary-key="id"
                direction="row"
                localized
              >
                <div slot="slot_tab1">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
                <div slot="slot_tab2">
                  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                </div>
                <div slot="slot_tab3">
                  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.
                </div>
              </dr-zp-tabs>
            </td>
          </tr>

          <tr>
            <td>
              Vertical
            </td>
            <td class="content">
              <dr-zp-tabs
                items={{tabs}}
                activeindex={{activeindex}}
                title-key="title"
                primary-key="id"
                direction="column"
                localized
                tab-width="140"
              >
                <div slot="slot_tab1">
                  1 - [[getString('module-sdk-description', locale)]]
                </div>
                <div slot="slot_tab2">
                  2 - [[getString('module-sdk-description', locale)]]
                </div>
                <div slot="slot_tab3">
                  3 - [[getString('module-sdk-description', locale)]]
                </div>
                <div slot="slot_tab4">
                  4 - [[getString('module-sdk-description', locale)]]
                </div>
              </dr-zp-tabs>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      tabs: {
        type: Array,
        value: [
          { id: "tab1", title: 'module-sdk-tab1'},
          { id: "tab2", title: 'module-sdk-tab2' },
          { id: "tab3", title: 'module-sdk-tab3' },
          { id: "tab4", title: 'module-sdk-tab4', "disabled": true  }
        ]
      },
      activeindex: {
        type: Boolean,
        value: 0,
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-tabs', Comp);