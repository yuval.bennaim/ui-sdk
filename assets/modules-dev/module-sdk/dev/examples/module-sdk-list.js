import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
   static get template() {
      const superTemp = super.template;
      const template = html`
      ${superTemp}
  
      <style>
        .wrapper {
        }
        table, th, td {
            border: 1px solid var(--color-border);
            border-collapse: collapse;
        }
        td, th {
            padding: 20px;
            vertical-align:top
        }
        dr-zp-list {
           
            display: block;
        }
        
      </style>
      

<div class="wrapper">
   <table>
      <tr>
         <td>
            Default List
         </td>
         <td>Selected List Item 
         </td>
         <td>listStyle
         </td>
      </tr>
      <tr>
         <td>
            <dr-zp-list localized id="list" items="[[listItems]]"></dr-zp-list>
         </td>
         <td>
            <dr-zp-list localized id="ls" selecteditem={{selecteditem}}></dr-zp-list>
         </td>
         <td>
            <dr-zp-list localized id="list" items="[[listItems]]" list-style="color:red"></dr-zp-list>
         </td>
      </tr>
      <tr>
         <td colspan="3" style="text-align:center">
            Selected Item= [[getString(selecteditem, locale)]]
         </td>
      </tr>
      <tr>
         <td>
            Iconed, multi layered
         </td>
         <td>
            Scrolled
         </td>
         <td>
            localized
         </td>
      </tr>
      <tr>
         <td>
            <dr-zp-list localized items="[[listItems3]]" field="name" style="width: 200px"></dr-zp-list>
         </td>
         <td>
            <dr-zp-list localized items="[[listItems2]]"  selecteditem={{selecteditem}} scrollable></dr-zp-list>
         </td>
         <td>
            <dr-zp-list items="[[listItems]]" localized="true"></dr-zp-list>
         </td>
      </tr>
   </table>
</div>
    `;

      return template;
   }

   static get properties() {
      return {
         listItems: {
            type: Array,
            value: ['module-sdk-hello', 'module-sdk-goodbye', 'module-sdk-seeya']
         },
         listItems2: {
            type: Array,
            value: ['module-sdk-hello', 'module-sdk-goodbye', 'module-sdk-seeya', 'module-sdk-image', 'module-sdk-spinner', 'module-sdk-modal']
         },
         listItems3: {
            type: Array,
            value: [
               { name: 'module-sdk-am-chart', icon: 'user' },
               { name: 'module-sdk-grid', icon: 'user' },
               {
                  name: 'module-sdk-am-chart', icon: 'users',
                  children: [
                     { name: 'module-sdk-bar-chart', icon: 'user' },
                     { name: 'module-sdk-line-chart', icon: 'user' },
                     { name: 'module-sdk-pie-chart', icon: 'users' }]
               }],

            selecteditem: {
               type: String,
               value: 0
            }
         }

      }
   }
   connectedCallback() {
      super.connectedCallback();
      this.$.ls.items = this.listItems2; //when no databinding, set the value programatically
      this.$.ls.selecteditem = this.listItems2[1];


      this.addEventListener('list-change', e => {
         var selecteditem = e.detail.selected;
         console.log("list_selected", this.selecteditem);
         var display = typeof selecteditem == "object" ? selecteditem.name : selecteditem
         this.selecteditem = display

      });

   }
}

window.customElements.define('module-sdk-list', Comp);