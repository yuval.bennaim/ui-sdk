import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        .make-smaller {
          width: 500px;
        }
        td {
          padding: 20px;
        }
        div.body-content { padding-top: 20px; padding-bottom: 20px; }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>Basic panel</td>
            <td>
              <div class="make-smaller">
                <dr-zp-panel tabindex="0">
                  <span slot="panel-title">The Title for Basic Panel</span>
                  <div class="body-content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar, mi ut tincidunt feugiat, dolor neque facilisis dolor, id tristique orci odio nec mi. Sed bibendum elementum diam, vel tincidunt ligula ultricies ac. Quisque placerat ultricies velit nec venenatis. Pellentesque ac venenatis mi, vitae bibendum odio. Sed quis tortor sit amet erat iaculis rutrum.
                  </div>
                </dr-zp-panel>
              </div>
            </td>
          </tr>

          <tr>
            <td>Panel with icons</td>
            <td>
              <div class="make-smaller">
                <dr-zp-panel >
                  <span slot="panel-title">Title for Icons</span>
                  <span slot="panel-icons">
                    <dr-zp-icon type="info" size="md" display="inline"></dr-zp-icon>
                    <dr-zp-icon type="more-vertical" size="md" display="inline"></dr-zp-icon>
                  </span>
                  <div class="body-content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar, mi ut tincidunt feugiat, dolor neque facilisis dolor, id tristique orci odio nec mi. Sed bibendum elementum diam, vel tincidunt ligula ultricies ac. Quisque placerat ultricies velit nec venenatis. Pellentesque ac venenatis mi, vitae bibendum odio. Sed quis tortor sit amet erat iaculis rutrum.
                  </div>
                </dr-zp-panel>
              </div>
            </td>
          </tr>

          <tr>
            <td>Panel with Transparent Header</td>
            <td>
              <div class="make-smaller">
                <dr-zp-panel transparent-header="true">
                  <span slot="panel-title">Title for Transparent Header</span>
                  <div class="body-content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pulvinar, mi ut tincidunt feugiat, dolor neque facilisis dolor, id tristique orci odio nec mi. Sed bibendum elementum diam, vel tincidunt ligula ultricies ac. Quisque placerat ultricies velit nec venenatis. Pellentesque ac venenatis mi, vitae bibendum odio. Sed quis tortor sit amet erat iaculis rutrum.
                  </div>
                </dr-zp-panel>
              </div>
            </td>
          </tr>

          <tr>
            <td>Panel with icons & localization</td>
            <td>
              <div class="make-smaller">
                <dr-zp-panel >
                  <span slot="panel-title">[[getString('module-sdk-title-panel', locale)]]</span>
                  <span slot="panel-icons">
                    <dr-zp-icon type="info" size="md" display="inline"></dr-zp-icon>
                    <dr-zp-icon type="more-vertical" size="md" display="inline"></dr-zp-icon>
                  </span>
                  <div class="body-content">
                    [[getString('module-sdk-description', locale)]]
                  </div>
                </dr-zp-panel>
              </div>
            </td>
          </tr>
        </table>
    </div>
    `;

    return template;
  }
  static get properties() {
    return {
      value: {
        type: Boolean,
        value: false
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-panel', Comp);