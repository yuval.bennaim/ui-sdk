import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
  
      <style>
        .wrapper {
        }
        table { width: 100%; }
        td {
          padding: 20px;
        }
        dr-zp-list-selector {
        }
      </style>

      <div class="wrapper">
        <table>
        <tr><td>List Text</td><td> [[list]]</td></tr>
          <tr>
            <td>
              Default
            </td>
            <td class="content">
              <dr-zp-list-selector label="[[getString('module-sdk-list-selector-label', locale)]]" list="{{list}}" value="List 1"></dr-zp-list-selector>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      list: {
        type: Array,
        value: [],
        notify: true
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();

    this.addEventListener('dr-zp-list-selector-add', (e) => {
      console.info('dr-zp-list-selector-add', e.detail.action, e.detail.item, e.detail.list);
    });

    this.addEventListener('dr-zp-list-selector-remove', (e) => {
      console.info('dr-zp-list-selector-remove', e.detail.action, e.detail.item, e.detail.list);
    });

    this.addEventListener('dr-zp-list-selector-remove-all', (e) => {
      console.info('dr-zp-list-selector-remove-all', e.detail.action, e.detail.list);
    });
  }
}

window.customElements.define('module-sdk-list-selector', Comp);