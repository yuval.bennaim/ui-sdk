import { html, PolymerElement } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>Default Checkbox w/Databinding</td>
            <td>
              <dr-zp-checkbox checked="{{value}}" label="[[getString('module-sdk-hello', locale)]]"></dr-zp-checkbox>
            </td>
            <td>Checked: [[value]]</td>
          </tr>

          
          <tr>
            <td>Checked Checkbox</td>
            <td colspan="2">
              <dr-zp-checkbox checked label="[[getString('module-sdk-hello', locale)]]"></dr-zp-checkbox>
            </td>
          </tr>

          <tr>
            <td>Mixed Checkbox</td>
            <td colspan="2">
              <dr-zp-checkbox checked mixed label="[[getString('module-sdk-hello', locale)]]"></dr-zp-checkbox>
            </td>
          </tr>

          <tr>
            <td>Disabled Checkbox</td>
            <td colspan="2">
              <dr-zp-checkbox disabled label="[[getString('module-sdk-hello', locale)]]"></dr-zp-checkbox>
            </td>
          </tr>

          <tr>
            <td>Disabled Checkbox</td>
            <td colspan="2">
              <dr-zp-checkbox disabled checked label="[[getString('module-sdk-hello', locale)]]"></dr-zp-checkbox>
            </td>
          </tr>

        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      value: {
        type: Boolean,
        value: false
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();

    var cbs = dom(this.root).querySelectorAll('dr-zp-checkbox');

    cbs.forEach((cb) => {
      cb.addEventListener('checkbox-clicked', e => {
        var checked = e.detail.checked;
        console.info('checkbox-clicked', checked);
      });
    });
  }
}

window.customElements.define('module-sdk-checkbox', Comp);