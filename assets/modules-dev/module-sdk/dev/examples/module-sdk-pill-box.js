import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
     
      </style>
      <div class="wrapper">

        <table style="height:350px; width:100%">
          <tr>
            <td>
              <p>Objects:</p>
              <dr-zp-pill-box id="pb1" pills="[[list1]]" addable-list="[[addableList]]" style="width: 100%; display: inline-block;" display-field="display"></dr-zp-pill-box>   
            </td>
          </tr>
          <tr>
            <td>
              <p>Objects with Editable:</p>
              <dr-zp-pill-box pills="[[list2]]" lookups="[[lookups]]" icon="filter" display-field="display"></dr-zp-pill-box>
            </td>
          </tr>
          <tr>
            <td>
              <p>Strings:</p>
              <dr-zp-pill-box id="pb3" pills="[[list3]]" style="width: 100%; display: inline-block;"></dr-zp-pill-box> 
            </td>
          </tr>
          <tr>
            <td>
              <p>Locale:</p>
              <dr-zp-pill-box pills="[[filters]]" actions="[[pillActions]]"></dr-zp-pill-box>
            </td>
          </tr>
        </table>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      list1: {
        type: Array,
        value: [
          { "field": "field one", "name": "field one", "value": "value one", "display": "field one: value one" },
          { "field": "field two", "name": "field two", "value": "value two", "display": "field two: value two" },
          { "field": "field three", "name": "field three", "value": "value three", "display": "field three: value three" }
        ],
        notify: true
      },
      list2: {
        type: Array,
        value: [
          { "field": "field one", "name": "field one", "value": "value one", "display": "field one:", "editable": true },
          { "field": "field two", "name": "field two", "value": "", "display": "field two:", "editable": true },
          { "field": "field three", "name": "field three", "value": "value three", "display": "field three", "editable": true }
        ],
        notify: true
      },
      list3: {
        type: Array,
        value: ["hello", "goodbye", "see ya"],
        notify: true
      },
      lookups: {
        type: Array,
        //   value: ["Tabs", "Panel", "Tree", {}]
        value: [{ "name": "field one", "display_name": "bar-chart", "options": [{ "value": "value one", "count": 1 }] }]
      },
      addableList: {
        type: Array,
        value: [{
          "field": "name",
          "filterable": true,
          "caption": "Name",
          "key": "name",
          "order": 1,
          "sortable": true,
          "type": "link",
          "visible": true,
          "width": 70
        },
        {
          "falseLabel": "disabled",
          "field": "enabled",
          "filterable": true,
          "caption": "Enabled",
          "key": "enabled",
          "order": 3,
          "trueLabel": "enabled",
          "type": "boolean",
          "visible": true,
          "width": 70
        }]
      },
      filters: {
        type: Array,
        value: [{
          "editable": true,
          "name": "%module-sdk-name-title",
          "sortKey": "name",
          "value": ""
        },
        {
          "editable": true,
          "name": "%module-sdk-enabled",
          "sortKey": "enabled",
          "value": ""
        }]
      },
      pillActions: {
        type: Array,
        value: ["apply", "save"]
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();

    this.$.pb1.addEventListener('dr-zp-pill-box-remove', (e) => {
      console.info('dr-zp-pill-box-remove', e.detail.list);
      this.list1 = this.utils._helper_copy(e.detail.list);
    });

    this.$.pb1.addEventListener('dr-zp-pill-box-clear', (e) => {
      console.info('dr-zp-pill-box-clear', e.detail.list);
      this.list1 = this.utils._helper_copy(e.detail.list);
    });

    this.$.pb1.addEventListener('dr-zp-pill-box-add', (e) => {
      console.info('dr-zp-pill-box-add', e.detail.pill);
      var pill = e.detail.pill
      var newPill = { "name": pill.caption, "value": "", editable: true, field: pill.field }
      this.list1.push(newPill);
      this.list1 = this.utils._helper_copy(this.list1);
    });
  }

  arrToJSON(arr) {
    return JSON.stringify(arr);
  }
}

window.customElements.define('module-sdk-pill-box', Comp);