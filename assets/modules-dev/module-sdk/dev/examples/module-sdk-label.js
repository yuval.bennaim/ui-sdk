import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
      .wrapper {
      }
      td {
        padding: 20px;
      }
      </style>

      <div class="wrapper">
        <table>
       
          <tr>
            <td>
              Label
            </td>
            <td>
              <dr-zp-label label="[[getString('module-sdk-label', locale)]]"></dr-zp-label>
            </td>
          </tr>

          <tr>
            <td>
              Transparent Label
            </td>
            <td>
              <dr-zp-label  is-transparent="true" label="[[getString('module-sdk-transparent', locale)]]"></dr-zp-label>
            </td>
          </tr>

          <tr>
            <td>
              Truncated Labels
            </td>
            <td>
              <dr-zp-label label="[[getString('module-sdk-truncate', locale)]]"></dr-zp-label>
            </td>
          </tr>

        </table>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      isTransparent: {
        type: Boolean,
        value: false,
      },
      label: {
        type: String
      }
    };
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define("module-sdk-label", Comp);
