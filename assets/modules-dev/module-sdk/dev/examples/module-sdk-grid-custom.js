import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';
import '../components/custom-grid.js';

class Comp extends BaseComponent { 
    static get template() {
        const superTemp = super.template;
        const template = html`
            ${superTemp}
           
            <div class="grid-wrapper">
                <custom-grid 
                    id="customGrid"
                    grid-title="[[getString('module-sdk-custom-grid-title', locale)]]"
                    primary-key="id"
                    columns="{{columnDefs}}"
                    data="[[gridRowsPage]]"
                    filter-sets="[[filterSets]]"
                    selected="[[selected]]"
                    details="[[details]]"
                    allow-drag
                    loading="[[loading]]">
                </custom-grid>
            </div>

            <div class$="[[determinePaginationClass(loading)]]">  
                <dr-zp-pagination id="pagination" class="pagination" total="[[dataSize]]" increment="[[pageSize]]"></dr-zp-pagination>
            </div>
        `;

        return template;
    }

    static get properties() {

        return {
            columnDefs: {
                type: Array,
                value: [
                    {
                        caption: '%module-sdk-selected',
                        field: '_selected',
                        key: '_selected', //must be unique
                        type: 'checkbox',
                        visible: true,
                        width: 40,
                    },
                    { 
                        caption: '%module-sdk-id',
                        field: 'id',
                        key: 'id', //must be unique
                        type: 'text',
                        visible: true,
                        filterable: false,
                        sortable: true,
                        width: 70
                    },
                    {
                        caption: '%module-sdk-name-title',
                        field: 'name',
                        key: 'name', //must be unique
                        type: 'link',
                        visible: true,
                        sortable: true,
                        width: 70
                    },
                    { 
                        caption: '%module-sdk-complex',
                        field: 'complex',
                        path: 'parent.child.grandchild.name',
                        key: 'complex', //must be unique
                        type: 'complex',
                        visible: true,
                        width: 70
                    },
                    { 
                        caption: '%module-sdk-color',
                        field: 'hex',
                        key: 'hex', //must be unique
                        type: 'circle',
                        visible: true,
                        width: 70
                    },
                    { 
                        caption: '%module-sdk-systems',
                        field: 'systems',
                        key: 'systems', //must be unique
                        type: 'icon',
                        visible: true,
                        width: 70
                    },
                    {
                        caption: '%module-sdk-actions',
                        key: 'actions', //must be unique
                        type: 'actions',
                        options: ["Do A", "Do B"],
                        visible: true
                    }
                ]
            },
            loading: {
                type: Boolean,
                value: false
            },
            gridRows: {
                type: Array,
                value: []
            },
            dataSize: {
                type: Number,
                value: 200,
                observer: 'dataSizeChanged'
            },
            pageSize: {
                type: Number,
                value: 10,
                observer: 'dataSizeChanged'
            },
            pages: {
                type: Number,
                value: 0
            },
            page: {
                type: Number,
                value: 1,
                observer: 'pageChanged'
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();          
        
        /* example for using a callback function */

        setTimeout(() => {
            var indexCol = this.$.customGrid.getColumnDefByKey('hex');

            indexCol.callback = (obj) => {
                console.info('callbackFunc...', obj);
                return obj.hex;
            };

            this.columnDefs = _.cloneDeep(this.columnDefs);
        }, 2000);


        this.$.customGrid.addEventListener('row-action', e => {
            var row = e.detail.row;
            console.info('row-action', row);
        });

        this.$.customGrid.addEventListener('row-selected', e => {
            var action = e.detail.action;
            var row = e.detail.row;
            console.info('row-selected', row);
            //this.showDetails(row);
        });        

        this.$.customGrid.addEventListener('row-button-click', e => {
            var row = e.detail.row;
            var action = e.detail.action;
            console.info('customGrid row-button-click', action);

            var timestamp = new Date().getTime();
            var msg = `row-button-click ${action} for row ${row.id}`;

            document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                detail: { 
                    timestamp: timestamp,
                    source: 'dr-zp SDK Grid',
                    type: 'info',
                    message: msg,
                    autodismissable: true,
                    duration: 5000 
                }
            }));
        }); 

        this.$.customGrid.addEventListener('list-action', e => {
            var row = e.detail.row;
            var action = e.detail.action;
            console.info('customGrid list-action', action);

            var timestamp = new Date().getTime();
            var msg = `list-action ${action} for row ${row.id}`;

            document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                detail: { 
                    timestamp: timestamp,
                    source: 'dr-zp SDK Grid',
                    type: 'info',
                    message: msg,
                    autodismissable: true,
                    duration: 5000 
                }
            }));
        }); 

        this.$.customGrid.addEventListener('sort-change', e => {
            var col = e.detail.column;
            var field = col.field;
            var dir = col.asc;
            console.info('sort-change', field, dir);

            /* local sort example  */
            this.gridRows.sort((aVal, bVal) => {
                var a = aVal[field];
                var b = bVal[field];
    
                if (dir > 0) { //sort string ascending
                    return (a > b) ? 1 : (a < b) ? -1 : 0;
                }
                else { //descending
                    return (a < b) ? 1 : (a > b) ? -1 : 0
                }
            });

            this.gridRows = this.utils._helper_copy(this.gridRows);
        });

        this.$.customGrid.addEventListener('drag-complete', e => {
            var source = e.detail.source;
            var target = e.detail.target;
            var sourceIndex = this.gridRows.findIndex((item) => {return item.id === source});
            var targetIndex = this.gridRows.findIndex((item) => {return item.id === target});
            this.gridRows.move(sourceIndex, targetIndex);
            this.gridRows = this.utils._helper_copy(this.gridRows);
            console.info('drag-complete', source, target, this.gridRows);
        });

        this.$.pagination.addEventListener('pagination-page-change', e => {
            this.page = e.detail.page;
            console.info('pagination-page-change', this.page);
        });
    }

    determinePaginationClass(loading) {
        var cls = 'input-fields-wrapper';

        if(loading) {
            cls += ' hidden';
        }

        return cls;
    }

    dataSizeChanged() {
        if(this.dataSize > 0) {
            this.loading = true;

            setTimeout(() => {
                this.initData(this.dataSize);
            }, 1000);    
        }
    }

    generteIPSub() {
        var sub = parseInt(Math.random() * 256);
        sub = Math.min(sub, 255);
        sub = Math.max(sub, 0);
        return sub;
    }

    componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    rgbToHex(r, g, b, a) {
        return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b); // + this.componentToHex(a);
    }

    initData(num) {
        console.info('initData', num);

        this.gridRows = [];
        var arr = [];

        for(var n = 0; n < num; n++) {
            var row = {};
            row.id = 'row_' + n;
            row.index = n;
            row.date = new Date().getTime();
            row._selected = false;
            row.enabled = (n%5 == 0)? true : false;
            row.name = 'Row ' + n;
            row.longText = "Some_very_long_non_breaking_text";

            row.complex = {};
            row.complex.parent = {name: "parent_" + n, child: {name: "child_" + n, grandchild: {name: "grandchild_" + n}}};

            var ip = {};
            var a = this.generteIPSub();
            var b = this.generteIPSub();
            var c = this.generteIPSub();
            var d = this.generteIPSub();
            row.paths = [a + "." + b + "." + c + ".1", 
                         a + "." + b + "." + c + ".2", 
                         a + "." + b + "." + c + ".3"];
            
            if(n % 5 == 0) {
                row.paths.push(a + "." + b + "." + c + ".4");
            }

            row.hex = this.rgbToHex(a, b, c, d);
            row.systems = ["apple-logo", "linux-logo", "windows-logo"];
            arr.push(row);
        }

        this.gridRows = arr;
        this.pages = this.dataSize / this.pageSize;
        this.pageChanged();
    }

    pageChanged() {
      setTimeout(() => {
            this.loading = true;
        });  

        setTimeout(() => {
            var from = (this.page-1) * this.pageSize;
            var to = this.page * this.pageSize;
            console.info('pageChanged', this.page, from, to);
            this.gridRowsPage = this.gridRows.slice(from, to);
            this.loading = false;
        }, 1000);  
    }
}

window.customElements.define('module-sdk-grid-custom', Comp);