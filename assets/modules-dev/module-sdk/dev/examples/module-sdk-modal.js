import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}

      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-modal {
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Small Modal
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal1Open}}"
                modal-title="Small Modal"
                size="sm"
                show-footer="true"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]">
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>

          <tr>
            <td>
             Medium Modal
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal2Open}}"
                modal-title="Medium Modal"
                size="md"
                show-footer="true"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]">
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>

          <tr>
            <td>
             Large Modal
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal3Open}}"
                modal-title="Large Modal"
                size="lg"
                show-footer="true"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]">
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>

          <tr>
            <td>
             Full Modal
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal4Open}}"
                modal-title="Full Modal"
                size="full"
                show-footer="true"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]">
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>

          <tr>
            <td>
              Modal without Footer
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal5Open}}"
                modal-title="Modal without Footer"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]">
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>

          <tr>
            <td>
              Modal with Danger Button
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal6Open}}"
                modal-title="Modal with Danger Button"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]"
                show-footer="true"
                danger="true"
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>

          <tr>
            <td>
              Modal with Disabled Button
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal7Open}}"
                modal-title="Modal with Danger Button"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]"
                show-footer="true"
                disabled="true"
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>

          <tr>
            <td>
              Transparent Modal
            </td>
            <td>
              <dr-zp-button type="primary" size="small" label="[[getString('module-sdk-open-modal', locale)]]"></dr-zp-button>
              <dr-zp-modal
                is-open="{{isModal8Open}}"
                modal-title="Transparent Modal"
                size="sm"
                transparent="true"
                show-footer="true"
                confirmation-label="[[getString('module-sdk-add', locale)]]"
                cancel-label="[[getString('module-sdk-cancel', locale)]]">
              >
                <div slot="slot-content">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </dr-zp-modal>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      isModal1Open: {
        type: Boolean,
        value: false
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();

    var buttons = dom(this.root).querySelectorAll('dr-zp-button');

    buttons.forEach((btn, i) => {
      btn.addEventListener('dr-zp-button-click', e => {
        console.info('dr-zp button clicked');
        this[`isModal${i + 1}Open`] = true;
      });
    });

    this.addEventListener('dr-zp-modal-confirmation-clicked', e => {
      console.info('dr-zp modal confirmation clicked');
      for (let i = 0; i < buttons.length; i++) {
        this[`isModal${i + 1}Open`] = false;
      }
    });

    this.addEventListener('dr-zp-modal-cancel-clicked', e => {
      console.info('dr-zp modal cancel clicked');
      for (let i = 0; i < buttons.length; i++) {
        this[`isModal${i + 1}Open`] = false;
      }
    });
  }
}

window.customElements.define('module-sdk-modal', Comp);