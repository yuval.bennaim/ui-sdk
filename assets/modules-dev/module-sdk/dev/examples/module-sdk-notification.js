import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
        ${superTemp}
        <style>
            .wrapper {
            }
            td {
            padding: 20px;
            }
            dr-zp-notification {
            }
        </style>

        <div class="wrapper">
            <table>
                <tr>
                    <td>
                        Banner of type Notifications
                    </td>
                    <td>
                        <dr-zp-button id="btn--banner-icon-success" type="primary" size="small" label="[[getString('module-sdk-success', locale)]]"></dr-zp-button>
                        <dr-zp-button id="btn--banner-icon-warning" type="primary" size="small" label="[[getString('module-sdk-warning', locale)]]"></dr-zp-button>
                        <dr-zp-button id="btn--banner-icon-error" type="primary" size="small" label="[[getString('module-sdk-error', locale)]]"></dr-zp-button>
                        <dr-zp-button id="btn--banner-icon-info" type="primary" size="small" label="[[getString('module-sdk-info', locale)]]"></dr-zp-button>
                    </td>
                </tr>
                <tr>
                    <td><strong>Below are examples of List of type Notifications...</strong></td>
                </tr>
                <tr>
                    <td>
                        Success 
                    </td>
                    <td>
                        <dr-zp-notification type="list" dismissable="[[myDis]]" alert-type="success" source="[[getString('module-sdk-title', locale)]]" message="[[getString('module-sdk-notification-message', locale)]]" with-icon="true"></dr-zp-notification>
                    </td>
                </tr>
                <tr>
                    <td>
                        Warning
                    </td>
                    <td>
                        <dr-zp-notification type="list" alert-type="warning" source="[[getString('module-sdk-title', locale)]]" message="[[getString('module-sdk-notification-message', locale)]]" with-icon="true"></dr-zp-notification>
    
                    </td>
                </tr>
                <tr>
                    <td>
                        Error
                    </td>
                    <td>
                        <dr-zp-notification type="list" alert-type="error" source="[[getString('module-sdk-title', locale)]]" message="[[getString('module-sdk-notification-message', locale)]]" with-icon="true"></dr-zp-notification>
                    </td>
                </tr>
                <tr>
                    <td>
                        Info
                    </td>
                    <td>
                        <dr-zp-notification type="list" alert-type="info" source="[[getString('module-sdk-title', locale)]]" message="[[getString('module-sdk-notification-message', locale)]]" with-icon="true"></dr-zp-notification>
                    </td>
                </tr>

                <tr>
                <td>
                    Without Icon
                </td>
                <td>
                    <dr-zp-notification type="list" alert-type="info" source="[[getString('module-sdk-title', locale)]]" message="[[getString('module-sdk-notification-message', locale)]]"></dr-zp-notification>
                </td>
            </tr>
            </table>              
        </div>
    `;

        return template;
    }

    static get properties() {
        return {
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.myDis = false;

        ['success', 'warning', 'error', 'info'].forEach(type => {
            dom(this.root).querySelector(`#btn--banner-icon-${type}`).addEventListener('dr-zp-button-click', () => {
                var timestamp = new Date().getTime();

                document.dispatchEvent(new CustomEvent('dr-zp-notification', {
                    detail: {
                        timestamp: timestamp,
                        source: 'dr-zp SDK Example',
                        type: type,
                        message: `Some ${type} Notification Message at ${timestamp}`,
                        autodismissable: (type == 'error') ? false : true,
                        duration: 5000
                    }
                }));
            });
        });

        this.addEventListener('dr-zp-notification-close-clicked', (e) => {
            console.info('dr-zp-notification-close-clicked');
            dom(e.detail.target).node.style.display = "none";
        })
    }
}

window.customElements.define('module-sdk-notification', Comp);