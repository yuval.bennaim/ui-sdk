import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
            <style>
                .icon-wrapper {
                    overflow: auto;
                    position: absolute;
                    top: 80px;
                    left: 0;
                    right: 0;
                    bottom: 20px;
                }
                .tile {
                    display: inline-grid;
                    margin: 0px 10px 10px 0px;
                    padding: 5px;
                    text-align: center;
                    width: 100px;
                    height: 100px;
                }
                dr-zp-icon {
                    margin-top: 10px;
                }
                .color-field {
                    display: inline-block;
                    width: 50px;
                    position: relative;
                    top: 0;
                    left: 0;
                    margin-bottom: 20px;
                }
                .size-field {
                    display: inline-grid;
                    width: 80px;
                    position: relative;
                    top: -7px;
                    left: 10px;                
                }
                .search-field {
                    display: inline-block;
                    width: 150px;
                    position: relative;
                    top: 14px;
                    float: right;
                    margin-right: 10px;
                }
            </style>

            <dr-zp-color-input label="Color" value="{{color}}" class="color-field"></dr-zp-color-input>
            <dr-zp-dropdown id="dropdown" items="[[sizes]]" selecteditem="{{size}}" class="size-field"></dr-zp-dropdown>

            <dr-zp-input id="search" placeholder="[[getString('module-sdk-search', locale)]]" value="{{search}}" class="search-field"></dr-zp-input >

            <div class="icon-wrapper" style="color:[[color]];">
                <template is="dom-if" if="[[_ready]]">
                    <template is="dom-repeat" items="[[allIcons]]" filter="{{_filterFunc(search)}}" as="iconType">
                        <div class="tile">
                            [[iconType]]</br>
                            <dr-zp-icon type="[[iconType]]" size="[[size]]" display="inline"></dr-zp-icon></br>
                        <div>
                    </template>        
                </template>    
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            size: {
                type: String,
                value: 'lg'
            },
            color: {
                type: String,
                value: '#ffffff'
            },
            sizes: {
                type: Array,
                value: ['sm', 'md', 'lg', 'xl']
            },
            search: {
                type: String,
                value: '',
                observer: 'searchObserved'
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.allIcons = this.iconStore.getIconKeys();

        setTimeout(() => {
            this._ready = true;
        }, 2000);
    }

    _filterFunc(search) {
        return function (icon) {
            return (icon.indexOf(search) > -1);
        };
    }
}

window.customElements.define('module-sdk-icon', Comp);