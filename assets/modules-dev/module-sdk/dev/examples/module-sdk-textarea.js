import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-textarea {
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default
            </td>
            <td>
              <dr-zp-textarea label="[[getString('module-sdk-textarea-label', locale)]]" value="{{value}}"></dr-zp-textarea>
            </td>
          </tr>

          <tr>
            <td>
                No Resize
            </td>
            <td>
                <dr-zp-textarea noresize label="[[getString('module-sdk-textarea-label', locale)]]" value="{{value}}"></dr-zp-textarea>
            </td>
            </tr>

          <tr>
          <td>
            Custom rows and columns [cols=10 rows=5]
          </td>
          <td>
            <dr-zp-textarea label="[[getString('module-sdk-textarea-label', locale)]]" cols=10 rows=5 value="{{value}}"></dr-zp-textarea>
          </td>
        </tr>
          <tr>
            <td>
              Disabled
            </td>
            <td>
              <dr-zp-textarea label="[[getString('module-sdk-disabled-textarea-label', locale)]]" value="{{value}}" disabled="true"></dr-zp-textarea>
            </td>
          </tr>

          <tr>
            <td>
              With a Validation Error
            </td>
            <td>
              <dr-zp-textarea label="[[getString('module-sdk-error-textarea-label', locale)]]" value="{{value}}" has-validation-error="true" message="[[getString('module-sdk-invalid-input', locale)]]"></dr-zp-textarea>
            </td>
          </tr>
        </table>              
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      value: {
        type: String,
        value: null
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-textarea', Comp);