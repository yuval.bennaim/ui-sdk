import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';
import '../../../../webcomponents/core/misc/dr-zp-badge.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default
            </td>
            <td>
              <dr-zp-badge tabindex="0" label="[[getString('module-sdk-default', locale)]]"></dr-zp-badge>
            </td>
          </tr>

          <tr>
            <td>
              Alert
            </td>
            <td>
              <dr-zp-badge label="[[getString('module-sdk-alert', locale)]]" alert="true"></dr-zp-badge>
            </td>
          </tr>

          <tr>
            <td>
              Alert with interval increment
            </td>
            <td>
              <dr-zp-badge label="[[alertCounter]]" alert="true"></dr-zp-badge>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      alertCounter: {
        type: Number,
        value: 0
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();

    setInterval(() => {
      this.alertCounter++;
    }, 5000);
  }
}

window.customElements.define('module-sdk-badge', Comp);