import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
        ${superTemp}
            <style>
                .wrapper {
                }
                td {
                    padding: 8px 16px;
                    vertical-align: top;
                }
                dr-zp-dropdown {
                    width: 100px; 
                    display: block;
                }
            </style>

            <div class="wrapper">
                <table>
                    <tr>
                        <td>
                            Inline
                        </td>
                        <td>
                            Dropdown
                        </td>
                    </tr>
                    <tr> 
                        <td aria-label="Calendar Example">
                            <dr-zp-calendar tabindex="0" aria-label="Selected Date : [[dateLabel]]" id="calendar" date="[[selectedDate]]"></dr-zp-calendar>
                        </td>
                        <td>
                            <dr-zp-date-dropdown id="calendarInput" date="[[selectedDate]]"></dr-zp-date-dropdown>
                        </td>                       
                    </tr>
                    <tr>
                        <td colspan="3">
                            Selected Date is: [[selectedDateEventedString]]
                        </td>
                    </tr>   
                </table>              
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            selectedDate: {
                type: Date,
                value: null
            },
            dateLabel: {
                type: String,
                value: ''
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        console.info('connectedCallback', this.selectedDate);

        this.$.calendar.addEventListener('dr-zp-calendar-date-changed', e => {
            this.selectedDate = e.detail.date;
            var selectedDateStr = moment([e.detail.date.year, e.detail.date.month - 1, e.detail.date.day]).format('L');
            this.dateLabel = moment(selectedDateStr).format("Do MMMM YYYY");
            console.info('dr-zp-calendar-date-changed-calendar', selectedDateStr, this.selectedDate);
            this.selectedDateEventedString = selectedDateStr;
        });

        this.$.calendarInput.addEventListener('dr-zp-calendar-date-changed', e => {
            this.selectedDate = e.detail.date;
            var selectedDateStr = moment([e.detail.date.year, e.detail.date.month - 1, e.detail.date.day]).format('L');
            console.info('dr-zp-calendar-date-changed-dropdown', selectedDateStr, this.selectedDate);
            this.selectedDateEventedString = selectedDateStr;
        });
        this.selectedDate = {
            year: 2021,
            month: 2,
            day: 28,
            hour: 1,
            minute: 2,
            second: 3
        };
        // this.selectedDate = {
        //     year: 2021,
        //     month: 2,
        //     day: 29,
        //     hour: 1,
        //     minute: 2,
        //     second: 3
        // };
        //this.selectedDate = null;
    }

}

window.customElements.define('module-sdk-calendar', Comp);