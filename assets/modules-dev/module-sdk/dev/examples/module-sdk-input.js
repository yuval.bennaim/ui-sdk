import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-input {
          width: 300px; 
          display: block;
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default
            </td>
            <td>
              <dr-zp-input label="[[getString('module-sdk-input-label', locale)]]" value="{{value}}" placeholder="Enter Text"></dr-zp-input>
            </td>
          </tr>

          <tr>
            <td>
              Read Only
            </td>
            <td>
              <dr-zp-input label="[[getString('module-sdk-readonly-input-label', locale)]]" value="{{value}}" readonly="true"></dr-zp-input>
            </td>
            <td></td>
          </tr>

          <tr>
            <td>
              Disabled
            </td>
            <td>
              <dr-zp-input label="[[getString('module-sdk-disabled-input-label', locale)]]" value="{{value}}" disabled="true"></dr-zp-input>
            </td>
            <td></td>
          </tr>

          <tr>
            <td>
              Has Validation Error
            </td>
            <td>
              <dr-zp-input label="[[getString('module-sdk-error-input-label', locale)]]" value="{{value}}" has-validation-error="true" message="[[getString('module-sdk-invalid-input', locale)]]"></dr-zp-input>
            </td>
            <td></td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      value: {
        type: String,
        value: null
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-input', Comp);