import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
        ${superTemp}
        <style>
            .wrapper-one {
                margin-top: 20px;
            }
            .wrapper-two {
                margin-top: 20px;
                background-color: var(--color-background-primary);
            }
            td {
                padding: 20px;
            }
        </style>
        
        <div class="wrapper-one">
            <dr-zp-table tabindex="0" caption="Simple Table Example with first-col-header" data="[[tableData]]" first-col-header></dr-zp-table>
        </div>

        <div class="wrapper-two">
            <dr-zp-table  caption="Simple Table Example with Background" data="[[tableData]]"></dr-zp-table>
        </div>
    `;

        return template;
    }

    static get properties() {
        return {
            tableData: {
                type: Array,
                value: [
                    ["module-sdk-column-one", "module-sdk-column-two", "module-sdk-column-three", "module-sdk-column-four"],
                    ["Row One Column One", "Row One Column Two", "Row One Column Three", "Row One Column Four"],
                    ["Row Two Column One", "Row Two Column Two", "Row Two Column Three", "Row Two Column Four"],
                    ["Row Three Column One", "Row Three Column Two", "Row Three Column Three", "Row Three Column Four"],
                    ["Row Four  Column One", "Row Four Column Two", "Row Four Column Three", "Row Four Column Four"],
                ]
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }
}

window.customElements.define('module-sdk-table', Comp);