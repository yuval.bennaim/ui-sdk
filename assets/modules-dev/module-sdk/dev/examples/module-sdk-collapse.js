import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        const template = html`
        ${superTemp}
        <style>
            .wrapper {
            }
            td {
            padding: 20px;
            vertical-align: top;
            }
            tr.collapse-row {
            height: 260px;
            }
            p, .make-smaller {
            width: 500px;
            }
            .in-panel {
            padding-top: 20px;
            padding-bottom: 20px;
            }
        </style>
        <div class="wrapper">
            <table>
            <tr class="collapse-row">
                <td>Basic collapse</td>
                <td><dr-zp-button id="toggler" type="primary" label="Toggle Collapse"></dr-zp-button></td>
                <td>
                    <dr-zp-collapse is-open="{{isFirstOpen}}">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies, quam ut rhoncus maximus, libero diam posuere ipsum, non tincidunt purus nunc a tortor. Ut elementum diam a mauris volutpat, id pellentesque elit porttitor. Donec laoreet fringilla enim, id efficitur nibh feugiat ut. Ut cursus nunc nisl, a scelerisque urna fermentum vitae. In placerat gravida quam, vitae fringilla risus. Ut nec sem vel tortor congue lacinia in sed enim. Praesent a rhoncus neque. Pellentesque finibus tincidunt lectus, in suscipit ipsum condimentum ac. Nullam porttitor quam diam, a ornare nisi malesuada in.</p>
                    </dr-zp-collapse>
                </td>
            </tr>

            <tr class="collapse-row">
                <td>Panel collapse example</td>
                <td colspan="2">
                <dr-zp-panel>
                    <div slot="panel-title">
                    <dr-zp-button id="open-panel" type="link" label="[[getString('module-sdk-toggle-panel', locale)]]"></dr-zp-button>
                    </div>
                    <dr-zp-collapse is-open="{{isSecondOpen}}">
                    <p class="in-panel">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ultricies, quam ut rhoncus maximus, libero diam posuere ipsum, non tincidunt purus nunc a tortor. Ut elementum diam a mauris volutpat, id pellentesque elit porttitor. Donec laoreet fringilla enim, id efficitur nibh feugiat ut. Ut cursus nunc nisl, a scelerisque urna fermentum vitae. In placerat gravida quam, vitae fringilla risus. Ut nec sem vel tortor congue lacinia in sed enim. Praesent a rhoncus neque. Pellentesque finibus tincidunt lectus, in suscipit ipsum condimentum ac. Nullam porttitor quam diam, a ornare nisi malesuada in.</p>
                    </dr-zp-collapse>
                </dr-zp-panel>
                </td>
            </tr>

            <tr class="collapse-row">
                <td>Panel collapse example with locale</td>
                <td colspan="2">
                <dr-zp-panel>
                    <div slot="panel-title">
                    <dr-zp-button id="open-panel-with-locale" type="link" label="[[getString('module-sdk-toggle-panel', locale)]]"></dr-zp-button>
                    </div>
                    <dr-zp-collapse is-open="{{isSecondOpen}}">
                    <p class="in-panel">[[getString('module-sdk-description', locale)]]</p>
                    </dr-zp-collapse>
                </dr-zp-panel>
                </td>
            </tr>
            </table>
        </div>
    `;

        return template;
    }
    static get properties() {
        return {
            isFirstOpen: {
                type: Boolean,
                value: false
            },
            isSecondOpen: {
                type: Boolean,
                value: false
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();

        var toggleButton = dom(this.root).querySelector('#toggler');
        toggleButton.addEventListener('dr-zp-button-click', e => {
           var temp = this.isFirstOpen;
           this.isFirstOpen = !temp;
        });

        var panelOpener = dom(this.root).querySelector('#open-panel');
        panelOpener.addEventListener('dr-zp-button-click', e => {
           var temp = this.isSecondOpen;
           this.isSecondOpen = !temp;
        });

        var panelOpenerWthLocale = dom(this.root).querySelector('#open-panel-with-locale');
        panelOpenerWthLocale.addEventListener('dr-zp-button-click', e => {
           var temp = this.isSecondOpen;
           this.isSecondOpen = !temp;
        });
    }
}

window.customElements.define('module-sdk-collapse', Comp);