import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-datepicker {
        }
      </style>
      <div class="wrapper">
        <table>
        <tr>
        <td>
          Default
        </td>
        <td>
       <dr-zp-datepicker timezone="IST" checked></dr-zp-datepicker>
        </td>
      </tr>
          <tr>
            <td>
              Correct Date and Inavlid Date
            </td>
            <td>
            <dr-zp-datepicker start-date="{{startDate}}" end-date="{{endDate}}"></dr-zp-datepicker>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
      startDate: {
        type: Object,
        value: {
          year: 2020,
          month: 1,
          day: 20,
          hour: 1,
          minute: 2,
          second: 3
        }
      },
      endDate: {
        type: Object,
        value: {
          year: 2021,
          month: 2,
          day: 28,
          hour: 1,
          minute: 2,
          second: 3
        }
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();

    document.addEventListener('dr-zp-datepicker-apply-clicked', (e) => {
      console.info('dr-zp-datepicker-apply-clicked', e.detail);
    })
  }
}

window.customElements.define('module-sdk-datepicker', Comp);