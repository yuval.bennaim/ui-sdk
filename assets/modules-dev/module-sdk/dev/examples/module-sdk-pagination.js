import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-pagination {
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default
            </td>
            <td>
              <dr-zp-pagination total="100"></dr-zp-pagination>
            </td>
          </tr>
          <tr>
            <td>
              With Per Page Options
            </td>
            <td>
              <dr-zp-pagination per-page-opt="[10, 25, 50, -1]" total="318"></dr-zp-pagination>
            </td>
          </tr>

          <tr>
            <td>
            With Custom units 
            </td>
            <td>
              <dr-zp-pagination per-page-opt="[10, 25, 50, -1]" total="318" unit="module-sdk-products"></dr-zp-pagination>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
    }
  }

  connectedCallback() {
    super.connectedCallback();

  }
}

window.customElements.define('module-sdk-pagination', Comp);