import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';
import '../components/trinity/dr-trinity-wrapper.js';
import '../.././../../webcomponents/core/form/dr-zp-dropdown.js'

class Comp extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        
        const template = html`
            ${superTemp}
            <dr-zp-dropdown localized id="dropdown" items="[[apis]]" selecteditem="{{api}}" button-style="width:280px" class="trinity-api-select"></dr-zp-dropdown>
            <dr-trinity-viewer api="[[api]]" class="trinity-viewer"></dr-trinity-viewer>
        `;

        return template;
    }

    static get properties() {
        return {
            apis: {
                type: Array,
                value: ["http://localhost:1080/web.example", "http://localhost:1080/other.example"]
            },
            api: {
              type: String,
              value: null,
              observer: "apiChanged"
          }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.api = this.apis[0];
    }

    apiChanged() {
        console.info('apiChanged', this.api);
    }
}

window.customElements.define('module-sdk-grid-trinity', Comp);