import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}
      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
        dr-zp-spinner {
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Small
            </td>
            <td>
              <dr-zp-spinner size="sm"></dr-zp-spinner>
            </td>
          </tr>

          <tr>
            <td>
              Medium
            </td>
            <td>
              <dr-zp-spinner size="md"></dr-zp-spinner>
            </td>
          </tr>

          <tr>
            <td>
              Large
            </td>
            <td>
              <dr-zp-spinner size="lg"></dr-zp-spinner>
            </td>
          </tr>

          <tr>
            <td>
              Extra Large
            </td>
            <td>
              <dr-zp-spinner size="xl"></dr-zp-spinner>
            </td>
          </tr>

          <tr>
            <td>
              Label
            </td>
            <td>
              <dr-zp-spinner tabindex=0" label="[[getString('sdk-loading', locale)]]"></dr-zp-spinner>
            </td>
          </tr>

          <tr>
            <td>
              Inline
            </td>
            <td>
              <dr-zp-spinner tabindex=0" label="[[getString('module-sdk-inline-loading', locale)]]" is-inline="true"></dr-zp-spinner>
            </td>
          </tr>
        </table>              
    </div>
    `;

    return template;
  }

  static get properties() {
    return {
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }
}

window.customElements.define('module-sdk-spinner', Comp);