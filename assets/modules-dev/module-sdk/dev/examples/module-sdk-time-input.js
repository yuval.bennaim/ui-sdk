import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../../webcomponents/core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    const superTemp = super.template;
    const template = html`
      ${superTemp}

      <style>
        .wrapper {
        }
        td {
          padding: 20px;
        }
      </style>

      <div class="wrapper">
        <table>
          <tr>
            <td>
              Default
            </td>
            <td>
              <dr-zp-time-input label="Default dr-zp Time Input"></dr-zp-time-input>
            </td>
          </tr>

          <tr>
            <td>
              With Display Time
            </td>
            <td>
              <dr-zp-time-input label="dr-zp Time Input with Display Time" show-time="true"></dr-zp-time-input>
            </td>
          </tr>

          <tr>
            <td>
              Disabled
            </td>
            <td>
              <dr-zp-time-input label="Disabled dr-zp Time Input" disabled="true" hour="12" minute="30" second="10"></dr-zp-time-input>
            </td>
          </tr>
        </table>              
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      value: {
        type: String,
        value: null
      },
    }
  }

  connectedCallback() {
    super.connectedCallback();

    var timeInputs = dom(this.root).querySelectorAll('dr-zp-time-input');

    timeInputs.forEach((input) => {
      input.addEventListener('time-input-change', e => {
        console.info('time-input-change', e.detail.value);
      });
    });
  }
}

window.customElements.define('module-sdk-time-input', Comp);