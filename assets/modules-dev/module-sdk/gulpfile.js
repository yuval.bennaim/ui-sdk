const { task, series, src, dest, parallel } = require('gulp');
const del = require('del');
const { exec, execSync } = require('child_process');
const replaceString = require('gulp-replace');
const fs = require('fs');
const Server = require('karma').Server;
const zip = require('gulp-zip');
const path = require('path');
const semver = require('semver');

var timestamp = new Date().getTime();
var isoDate = new Date().toISOString();
var buildDate = isoDate.substring(0, isoDate.lastIndexOf('.'));
var fileDelimeter = process.platform === "win32" ? "\\" : "/";


const componentBundleName = 'component-prod-bundle.js';

console.log(timestamp + " buildDate = " + buildDate);

function count(mainStr, subStr) {
    return (mainStr.split(subStr).length - 1);
}

function getRemotePackageVersion() {
    try {
        const npmShowCommand = `npm show --json`;
        return JSON.parse(execSync(npmShowCommand));
    } catch (error) {
        console.error(error);
    }
}

function getLocalPackageVersion() {
    try {
        return JSON.parse(fs.readFileSync('package.json'));
    } catch (error) {
        console.error(error);
    }
}

function getUIShellPackageVersion() {
    try {
        return JSON.parse(fs.readFileSync('../../../package.json'));
    } catch (error) {
        console.error(error);
    }
}

const pkgDetails = getUIShellPackageVersion();

task('prep', function () {
    console.log('\n\n-----------------------------------\n\nTASK prep');

    return del(['./dist', './coverage', './build', './stage_build', './prod']);
})

task('stage-module', function () {
    console.log('\n\n-----------------------------------\n\nTASK stage-module');
    return src(['./dev/**/*'])
        .pipe(replaceString(/import[^'"]+['"]\s*([^'"]+?)\s*['"]/g, function (str) {
            let depthStr = './';
            let dirStr = '../';

            if ((str.indexOf('{') > - 1) && (str.indexOf('../vendor') > - 1) || (str.indexOf('../webcomponents') > - 1)) {
                let offsetDepth = count(this.file.relative, fileDelimeter);

                if (offsetDepth > 0) {
                    depthStr = dirStr.repeat(offsetDepth);
                }
                var newStr = str.replace(/['"](.*?)['"]/g, `\'${depthStr}${componentBundleName}\'`);
                return newStr;
            }
            else if ((str.indexOf('../vendor') > - 1) || (str.indexOf('../webcomponents') > - 1)) {
                return "";
            }
            else {
                return str;
            }
        }))
        .pipe(dest('./stage_build'));
});

task('create-symlink', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK create-symlink');

    exec(`ln -sf ../../../webcomponents/prod/${componentBundleName} ./stage_build/${componentBundleName}`, { maxBuffer: Infinity }, function (err, stdout, stderr) {
        if (err) {
            done(err)
        } else {
            done();
        }

    })
});

task('create-index-html', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK create-index-html');

    fs.writeFile('./stage_build/index.html', '<html></html>', done);
})

task('build', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK build');

    exec('polymer build', { maxBuffer: Infinity }, function (err, stdout, stderr) {
        if (err != null) {
            console.log(err);
            console.log('Polymer build FAILED...');
            done(err);
        }
        else {
            console.log('polymer build SUCCESS...');
            done();
        }
    });
});

task('prod', function () {
    console.log('\n\n-----------------------------------\n\nTASK prod');

    return src(['./build/es6-bundled/stage_build/**/*', './build/es6-bundled/*.*'])
        .pipe(replaceString(/import[^']+'\s*([^']+?)\s*'/g, function (str) {
            var newStr = str.replace(/'(.*?)'/g, `\'../../../webcomponents/prod/${componentBundleName}?v=${pkgDetails.version}\'`);
            return newStr;
        }))
        .pipe(dest('./prod'));
});

task('cleanup', async function (done) {
    console.log('\n\n-----------------------------------\n\nTASK cleanup');

    await del(['./build', './stage_build', `./prod/${componentBundleName}`, './prod/index.html']);
    done();
});

task('run-test', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK run-test');

    let karmaServer = new Server({
        configFile: '../../../karma.conf.js',
        grep: __dirname + '/test/**/*.test.js',
        singleRun: true,
        coverageReporter: {
            dir: __dirname + '/coverage',
        }
    }, (code) => {
        if (code) {
            console.log('Some test cases are Failing !');
            // done(code);
        }
        done();
        // } else {
        //     done();
        // }
    });

    karmaServer.start();
});

task('npm-publish', function (done) {
    let npmPkgDetails = getRemotePackageVersion();
    let npmPkgVersion = npmPkgDetails["dist-tags"].latest;

    let pkgDetails = getLocalPackageVersion();
    let pkgVersion = pkgDetails.version;

    let newVersion;
    if (semver.gt(pkgVersion, npmPkgVersion)) {
        //Publish Local Package Version
        console.log("Publish Local Package Version")
        newVersion = pkgVersion;
    } else {
        //Increment and Publish new Version
        console.log("Increment and Publish new Version")
        newVersion = semver.inc(npmPkgVersion, 'patch');
    }
    console.log("Final Version", newVersion);

    exec(`npm version ${newVersion}`, { maxBuffer: Infinity }, function (err, stdout, stderr) {
        if (err != null) {
            console.log(err);
            console.log('NPM Publish FAILED...');
            done(err);
        }
        else {
            console.log('NPM Publish SUCCESS...');
            done();
        }
    });
})

task('default',
    series(
        'prep',
        'run-test',
        'stage-module',
        parallel(
            'create-symlink',
            'create-index-html'),
        'build',
        'prod',
        'cleanup',
        (cb) => {
            cb();
            process.exit()
        }
    )
);