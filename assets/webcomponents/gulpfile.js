const { task, series, src, dest, parallel } = require('gulp');
const del = require('del');
const { exec } = require('child_process');
const replaceString = require('gulp-replace');
const fs = require('fs');
const Server = require('karma').Server;
const zip = require('gulp-zip');
const path = require('path');

var timestamp = new Date().getTime();
var isoDate = new Date().toISOString();
var buildDate = isoDate.substring(0, isoDate.lastIndexOf('.'));

const componentBundleName = 'component-prod-bundle.js';
var fileDelimeter = process.platform === "win32" ? "\\" : "/";

console.log("" + timestamp + " buildDate = " + buildDate);

function count(mainStr, subStr) {
    return (mainStr.split(subStr).length - 1);
}

function getLocalPackageVersion() {
    try {
        return JSON.parse(fs.readFileSync('../../package.json'));
    } catch (error) {
        console.error(error);
    }
}

const pkgDetails = getLocalPackageVersion();

task('prep', function () {
    console.log('\n\n-----------------------------------\n\nTASK prep');

    return del(['./build', './stage_build', './app/prod']);
})

function doesContainComponentsPath(str) {
    return (
        (str.indexOf('../vendor') > - 1) ||
        (str.indexOf('../webcomponents') > - 1) ||
        (str.indexOf('../core') > - 1) ||
        (str.indexOf('../reusable') > - 1)
    )
}

task('stage-module', function () {
    console.log('\n\n-----------------------------------\n\nTASK stage-module');
    return src(['./app/dev/**/*', './app/*.*'])
        .pipe(replaceString(/import[^'"]+['"]\s*([^'"]+?)\s*['"]/g, function (str) {
            let depthStr = './';
            let dirStr = '../';

            if ((str.indexOf('{') > - 1) && doesContainComponentsPath(str)) {
                let offsetDepth = count(this.file.relative, fileDelimeter);
                if (offsetDepth > 0) {
                    depthStr = dirStr.repeat(offsetDepth);
                }
                var newStr = str.replace(/['"](.*?)['"]/g, `\'${depthStr}${componentBundleName}\'`);
                return newStr;
            }
            else if (doesContainComponentsPath(str)) {
                return "";
            }
            else {
                return str;
            }
        }))
        .pipe(dest('./stage_build'));
});

task('create-symlink', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK create-symlink');
    exec(`ln -sf ../prod/${componentBundleName} ./stage_build/${componentBundleName}`, { maxBuffer: Infinity }, function (err, stdout, stderr) {
        if (err) {
            done(err)
        } else {
            done();
        }

    })
});

task('create-index-html', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK create-index-html');

    fs.writeFile('./stage_build/index.html', '<html></html>', done);
})

task('build', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK build');

    exec('polymer build', { maxBuffer: Infinity }, function (err, stdout, stderr) {
        if (err != null) {
            console.log(err);
            console.log('Polymer build FAILED...');
            done(err);
        }
        else {
            console.log('polymer build SUCCESS...');
            done();
        }
    });
});

task('prod', function () {
    console.log('\n\n-----------------------------------\n\nTASK prod');

    return src(['./build/es6-bundled/stage_build/**/*', './build/es6-bundled/*.*'])
        .pipe(replaceString(/import [^']+'\s*([^']+?)\s*'/g, function (str) {
            var newStr = str.replace(/'(.*?)'/g, `\'../../prod/${componentBundleName}?v=${pkgDetails.version}\'`);

            return newStr;
        }))
        .pipe(dest('./app/prod'));
});

task('cleanup', async function (done) {
    console.log('\n\n-----------------------------------\n\nTASK cleanup');

    await del(['./build', './stage_build', `./app/prod/${componentBundleName}`, './app/prod/index.html']);
    done();
    process.exit();
});

task('run-test', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK run-test');

    let karmaServer = new Server({
        configFile: '../../../karma.conf.js',
        grep: __dirname + '/test/**/*.test.js',
        enableCoverage: true,
        coverageReporter: {
            dir: __dirname + '/coverage',
        },
        sonarQubeUnitReporter:{
            outputFile: './ui-shell/assets/webcomponents/coverage/app-results.xml'
        }
    }, (code) => {
        if (code) {
            console.log('Some test cases are Failing !');
            // done(code);
        }
        done();
        // } else {
        //     done();
        // }
    });
    karmaServer.start();
});

task('export-zip', function (done) {
    console.log('\n\n-----------------------------------\n\nTASK export-dist');
    let moduleName = path.basename(process.cwd());
    let packageJson = JSON.parse(fs.readFileSync('../../../package.json'));
    return src('prod/*')
        .pipe(zip(`${moduleName}-${packageJson.version}-${timestamp}.zip`))
        .pipe(dest('dist'))
})

task('default',
    series(
        'run-test',
        'prep',
        'stage-module',
        parallel(
            'create-symlink',
            'create-index-html'),
        'build',
        'prod',
        'cleanup'
    )
);