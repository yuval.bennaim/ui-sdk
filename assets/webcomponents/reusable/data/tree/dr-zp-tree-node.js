import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../core/dr-zp-base-component.js';

class NodeComp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
            <style> 
                .branch-name:focus, .leaf-name:focus {
                    outline: 0 !important;
                    outline-offset: 0 !important;
                    box-shadow: none !important;
                    color: var(--color-action-primary--bright) !important
                }
                .node-wrapper {
                    padding-left: 1.5em;
                    cursor: pointer;
                }
                .node-wrapper-root {
                    padding-left: 0em;
                    cursor: pointer;
                }
                .branch-name {
                    font-weight: bold;
                    padding: 5px;
                    font-weight: bold;
                    padding: 5px;
                    text-align: left;
                    white-space: nowrap;
                    width: 100%;
                }
                .branch-name:hover, .leaf-name:hover {
                    color: var(--color-action-primary--bright) !important
                }
                .bordered {                    
                    width: 100%;
                    border-bottom: 1px solid #777;
                }
                .leaf-name {
                    font-weight: normal;
                    padding: 5px;
                    text-align: left;
                    white-space: nowrap;
                    width: 100%;
                }
                .chevron {
                    position: relative;                    
                    left: -4px;
                    top: 4px;
                }
                .leaf-spacer {
                    width: 15px;
                    display: inline-block;
                }
            </style>

            <div class$="[[determineNodeWrapperClass(rootBranch, rootBranchBordered, data)]]">
                <button id="rootNode" class$="[[determineNodeClass(data, expanded)]]" on-click="clickHandler">
                    <template is="dom-if" if="[[isBranch(data)]]">
                        <template is="dom-if" if="[[!expanded]]">
                            <dr-zp-icon type="chevron-right" size="sm" display="inline" class="chevron"></dr-zp-icon>
                        </template>

                        <template is="dom-if" if="[[expanded]]">
                            <dr-zp-icon type="chevron-down" size="sm" display="inline"class="chevron"></dr-zp-icon>
                        </template>
                    </template>

                    <template is="dom-if" if="[[!isBranch(data)]]">
                        <span class="leaf-spacer"></spanb>
                    </template>

                    [[data.name]]
                </button>

                <template is="dom-repeat" items="[[data.nodes]]" as="node">
                    <template is="dom-if" if="[[expanded]]">
                        <dr-zp-tree-node data="[[node]]"></dr-zp-tree-node>
                    </template>
                </template>
            </div>        
        `;

        return template;
    }

    static get properties() {
        return {
            data: {
                type: Object,
                observer: 'dataChanged'
            },
            rootBranch: {
                type: Boolean,
                value: false
            },
            rootBranchBordered: {
                type: Boolean,
                value: false
            },
            expanded: {
                type: Boolean,
                value: false
            },
        }
    }

    clickHandler() {
        if (this.isBranch(this.data)) {
            this.expanded = !this.expanded;
            this.$.rootNode.setAttribute("aria-expanded", this.expanded);
        }
        else {
            this.dispatchEvent(new CustomEvent('tree-navigation', {
                bubbles: true,
                composed: true,
                detail: {
                    data: this.data
                }
            }));
        }
    }

    determineNodeWrapperClass(rootBranch, rootBranchBordered, data) {
        var cls = "node-wrapper";

        if (rootBranch) {
            cls += "-root ";

            if (rootBranchBordered && rootBranchBordered) {
                cls += " bordered";
            }
        }

        return cls;
    }

    determineNodeClass(data) {
        var cls = "leaf-name";

        if (data.nodes !== undefined) {
            cls = "branch-name";
        }

        return cls;
    }

    dataChanged() {
        if (this.data.expanded) {
            this.expanded = this.data.expanded;
        }
        if (this.isBranch(this.data)) {
            this.$.rootNode.setAttribute("aria-expanded", this.expanded);
        }
    }

    isBranch(data) {
        return data.nodes !== undefined && data.nodes.length > 0;
    }
}

window.customElements.define('dr-zp-tree-node', NodeComp);



