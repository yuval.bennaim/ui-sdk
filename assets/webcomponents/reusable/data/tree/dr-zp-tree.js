import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../core/dr-zp-base-component.js';
import './dr-zp-tree-node.js';

class TreeComp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
           
            <template is="dom-repeat" items="[[data]]" as="node">
                <dr-zp-tree-node data="[[node]]" root-branch="true" root-branch-bordered="[[rootBranchBordered]]"></dr-zp-tree-node>
            </template>
        `;

        return template;
    }

    static get properties() {
        return {
            data: {
                type: Array
            },
            rootBranchBordered: {
                type: Boolean,
                value: false
            }
        }
    }
}

window.customElements.define('dr-zp-tree', TreeComp);