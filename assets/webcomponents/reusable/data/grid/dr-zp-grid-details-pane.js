import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../core/dr-zp-base-component.js';
import './renderers/cell/dr-zp-grid-text-renderer.js';

class GridDetailsPane extends BaseComponent {
    static get template() {
        return html`
            <style>
                .details-header {
                    padding: 10px;
                    border-bottom: 2px solid #3e3e3e;
                    position: relative;
                    font-weight: bold;
                    top: 0px;
                    font-size: 14px;
                }
                .details-scroller {
                    padding: 10px;
                    overflow-y: auto;
                    overflow-x: hidden;
                    position: absolute;
                    top: 40px;
                    left: 0;
                    right: 0;
                    bottom: 0;
                }
                .remove-icon {
                    float: right;
                    font-size: 12px;
                    font-weight: bold;
                    cursor: pointer;
                }
                .details-field {
                    margin-bottom: 20px;
                }
                .details-field-table {
                    width:100%;
                }
                .details-field-key-cell {
                    color: #666;
                    vertical-align: top;
                    padding-right: 20px;
                    padding-bottom: 20px;
                    white-space: normal;
                    width: 35%;
                }
                .details-field-val-cell {
                    color: #ddd;
                    padding-bottom: 20px;
                    vertical-align: top;
                    word-break: break-all;
                    width: 65%;
                }
                .fixed-header-border {
                    position: absolute;
                    width: 10px;
                    top: 2px;
                    bottom: 0px;
                    left: 2px;
                    height: 10px;
                    cursor: ew-resize;
                    margin-left: 1px;
                    /* border-left: 1px solid red; */
                    z-index: 1200;
                    background-color: orange;
                }
                .fixed-header-border:hover {
                    border-color: #40a9b9;
                }
            </style>

            <div>
                <div class="details-header">
                    Record Details
                    <span class="remove-icon" on-click="closeClicked" title="Close details Pane">X</span>
                </div>

                <!-- <div class="fixed-header-border" id="border_details" title="Drag to resize" on-mousedown="dragHeaderStart" on-mousemove="dragHeaderMove" on-mouseup="dragHeaderEnd" on-mouseenter="dragHeaderEnd"></div> -->



                <div class="details-scroller">
                    <!-- build automatic list by column order -->

                    <div class="details-field">
                        <table class="details-field-table">
                            <template is="dom-repeat" items="[[fields]]" as="col" index-as="r">
                                <template is="dom-if" if="[[notHidden(col)]]">
                                    <tr>
                                        <td class="details-field-key-cell">
                                            [[col.caption]]
                                        </td>
                                        <td class="details-field-val-cell">                                 
                                            <dr-zp-bindable-html value="[[getRenderer(r, col)]]" on-dom-change="bindableDomChanged"></dr-zp-bindable-html>
                                        </td>
                                    </tr>

                                    <!-- <tr>
                                        <td style="width:100%; border-bottom: 1px dashed #9999994d; padding: 5px 0px 5px 0px;">
                                            <b>[[col.caption]]:</b><br>                                                                   
                                            <dr-zp-bindable-html value="[[getRenderer(r, col)]]" on-dom-change="bindableDomChanged"></hx-bindable-html>
                                        </td>
                                    </tr> -->
                                </template>
                            </template>
                        </table>
                    </div>
                </div>

            </div>
        `;
    }

    static get properties() {
        return {
            name: {
                type: String,
                value: 'fe-grid-details-pane'
            },
            item: {
                type: Object,
                observer: 'itemObserved'
            },
            columns: {
                type: Array,
                observer: 'columnsObserved'
            },
            fields: {
                type: Array
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    // dragHeaderStart(e) {
    //     var elem = e.target || e.srcElement;
    //     this.dragging = true;
    //     this.dragStartX = e.x;
    //     console.info('dragHeaderStart', e, this.dragStartX);
    // }

    // dragHeaderMove(e) {
    //     if (this.dragging) {
    //         //console.info('dragHeaderMove');

    //         // var posX = e.x;
    //         // var offset = posX - this.dragStartX;
    //         // var col = this.getColumnDefByKey(this.dragColumn);
    //         // col.width = Math.max(40, this.startColWidth + offset);
    //         // this.columns = this._helper_copy(this.columns);
    //     }
    // }

    // dragHeaderEnd(e) {
    //     console.info('dragHeaderEnd');
    //     this.dragging = false;

    //     if (this.dragColumn !== undefined) {
    //         // this.columns = this._helper_copy(this.columns);
    //         // delete this.dragStartX;
    //         // delete this.dragColumn;
    //         // delete this.startColWidth;
    //     }
    // }

    itemObserved() {
        this.buildFieldlist();
    }

    columnsObserved() {
        this.buildFieldlist();
    }

    notHidden(item, hiddenKey) {
        var val = item.hidden;
        return !(val === true);
    }

    buildFieldlist() {
        if (this.item != null) {
            var columnsVisible = this.columns.filter(col => col.key.indexOf('_') != 0);

            for (var c = 0; c < columnsVisible.length; c++) {
                var col = columnsVisible[c];
                col.val = this.item[col.field];
            }

            this.fields = this.utils._helper_copy(columnsVisible);
        }
        else {
            this.fields = [];
        }
    }

    getColumnDefByKey(key) {
        for (var c = 0; c < this.columns.length; c++) {
            var col = this.columns[c];

            if (col.key === key) {
                return col;
            }
        }

        return null;
    }

    bindableDomChanged(e) {
        var renderer;

        this.utils._helper_findNodeById(e.currentTarget, 'renderer', (node) => {
            renderer = node;
        });

        if (renderer) { //find the renderer, inject the col and row based on ndex and field
            var keyAtt = renderer.attributes['key'].value;
            var col = this.getColumnDefByKey(keyAtt); //get from col def
            // renderer.setCol(col);
            // renderer.setObj(this.item);
        }
    }

    getRenderer(index, col) {
        if (this.item != null) {
            var htmlStr = "";
            var renderer = col.cellRenderer;

            if (renderer === undefined) { //no renderer specified for this type so default to text
                if (col.type === 'link') {
                    htmlStr = '<dr-zp-grid-text-renderer id="renderer" index="' + index + '" key="' + col.key + '" field="' + col.field + '" val="' + this.item[col.field] + '" link="true" details="true"></dr-zp-grid-text-renderer>';
                }
                else {
                    htmlStr = '<dr-zp-grid-text-renderer id="renderer" index="' + index + '" key="' + col.key + '" field="' + col.field + '" val="' + this.item[col.field] + '" details="true"></dr-zp-grid-text-renderer>';
                }
            }
            else {
                htmlStr = '<' + renderer + ' id="renderer" index="' + index + '" key="' + col.key + '" field="' + col.field + '" val="' + this.item[col.field] + '" details="true"></' + renderer + '>';
            }

            return htmlStr;
        }
    }

    // isFieldType(field, type) {
    //     return field === type;
    // }

    closeClicked() {
        this.dispatchEvent(new CustomEvent('details-close', {}));
    }
}

window.customElements.define('dr-zp-grid-details-pane', GridDetailsPane);