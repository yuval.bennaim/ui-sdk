import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { Debouncer } from '../../../../vendor/@polymer/polymer/lib/utils/debounce.js';
import { timeOut } from '../../../../vendor/@polymer/polymer/lib/utils/async.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import { BaseComponent } from '../../../core/dr-zp-base-component.js';
import '../../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';
import '../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';

import './dr-zp-grid-column-config.js';
import '../../../core/misc/dr-zp-spinner.js';
import '../../../core/misc/dr-zp-icon.js';
import '../../../core/containers/dr-zp-pill-box.js';

import './renderers/header/dr-zp-grid-header-renderer.js';
import './renderers/header/dr-zp-grid-header-checkbox-renderer.js';

import './renderers/cell/dr-zp-grid-text-renderer.js';
import './renderers/cell/dr-zp-grid-number-renderer.js';
import './renderers/cell/dr-zp-grid-checkbox-renderer.js';
import './renderers/cell/dr-zp-grid-link-renderer.js';
import './renderers/cell/dr-zp-grid-button-renderer.js';
import './renderers/cell/dr-zp-grid-boolean-renderer.js';
import './renderers/cell/dr-zp-grid-array-renderer.js';
import './renderers/cell/dr-zp-grid-color-renderer.js';
import './renderers/cell/dr-zp-grid-image-renderer.js';
import './renderers/cell/dr-zp-grid-status-renderer.js';
import './renderers/cell/dr-zp-grid-icon-renderer.js';
import './renderers/cell/dr-zp-grid-toggle-renderer.js';
import './renderers/cell/dr-zp-grid-actions-renderer.js';
import './renderers/cell/dr-zp-grid-icon-text-renderer.js';
import './renderers/cell/dr-zp-grid-action-button-renderer.js';

export class FEGrid extends BaseComponent {

    static get renderersTemplate() {
        let renderersTemplate = html`
            <template is="dom-if" if="[[isFieldType(col.type, 'text')]]">
                <dr-zp-grid-text-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-text-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'number')]]">
                <dr-zp-grid-number-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-number-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'integer')]]">
                <dr-zp-grid-number-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-number-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'link')]]">
                <dr-zp-grid-link-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-link-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'button')]]">
                <dr-zp-grid-button-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-button-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'checkbox')]]">
                <dr-zp-grid-checkbox-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-checkbox-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'boolean')]]">
                <dr-zp-grid-boolean-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-boolean-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'toggle')]]">
                <dr-zp-grid-toggle-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-toggle-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'image')]]">
                <dr-zp-grid-image-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-image-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'date')]]">
                <dr-zp-grid-text-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-text-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'array')]]">
                <dr-zp-grid-array-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-array-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'color')]]">
                <dr-zp-grid-color-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-color-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'status')]]">
                <dr-zp-grid-status-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-status-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'icon')]]">
                <dr-zp-grid-icon-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-icon-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'actions')]]">
                <dr-zp-grid-actions-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-actions-renderer>
            </template>

            <template is="dom-if" if="[[isFieldType(col.type, 'icon-text')]]">
                <dr-zp-grid-icon-text-renderer field="[[col.field]]" col="[[col]]" obj="[[row]]"></dr-zp-grid-icon-text-renderer>
            </template>
        `;

        return renderersTemplate;
    }

    static get template() {
        let superTemp = super.template;
        let renderersStr = this.renderersTemplate;
        let gridStyles = this.styleStore.getSheetStyles("grid.css");

        let template = html`
            ${superTemp}
            ${gridStyles}

            <template is="dom-if" if="[[isInitialized(loading)]]">
                <template is="dom-if" if="[[hasFilterableColumns(columns)]]">
                    <dr-zp-pill-box pills="[[filters]]" lookups="[[lookups]]" filter-sets="[[filterSets]]" actions="[[pillActions]]" icon="filter" display-field="display" entity="[[entity]]"></dr-zp-pill-box>
                </template>
            </template>

            <div class$="[[calculateWrapperClass(columns)]]">
                <div class="table-buffer">
                    <template is="dom-if" if="[[hasTitle(gridTitle)]]">
                        <span class="grid-title">[[gridTitle]]</span>
                    </template>

                    <span class="grid-icons">
                        <dr-zp-grid-column-config class="config" select-title="Columns" id="colConfig" gid="[[gid]]" columns="{{columns}}" updater="[[updater]]"></dr-zp-grid-column-config>
                    </span>
                    <template is="dom-if" if="[[isAllowActionSlot(allowActionSlot)]]">
                        <span class="grid-btns">
                            <dr-zp-grid-action-button-renderer>
                             <slot></slot>
                            </dr-zp-grid-action-button-renderer>
                        </span>
                    </template>
                </div>

                <div class="table-background-outter">
                    <div id="inner" class="table-background-inner" style$="[[calculateInnerStyle(details)]]">
                        <template is="dom-if" if="[[hasVisibleColumns(columnsVisible)]]">

                            <!-- LEFT DOCK -->

                            <template is="dom-if" if="[[hasLeftDockedColumns(columnsLeft)]]">
                                <div id="dockLeft" class="dock-left" style$="[[determineLeftDockStyle(leftDockWidth)]]">
                                    <div id="scrollerDockedLeft" class="table-no-scroll">
                                        <table id="tableDosckedLeft" class="grid-data-table" style$="[[isinitialized(initialized)]]">
                                            <template is="dom-repeat" items="[[data]]" as="row" index-as="r">
                                                <tr id="row_dock_left_[[r]]" on-mouseenter="rowEnter" on-mouseleave="rowLeave" on-click="selectRow" class$="{{calculateRowClass(selected, row, 'docked-left')}}">
                                                    <template is="dom-repeat" items="[[columnsLeft]]" as="col" index-as="c">
                                                        <template is="dom-if" if="[[showCol(col)]]">
                                                            <td class="grid-data-table-cell" style$="{{calculateStyle(col)}}">
                                                                ${renderersStr}
                                                            </td>
                                                        </template>
                                                    </template>
                                                </tr>
                                            </template>
                                            <tr>
                                                <template is="dom-repeat" items="[[columnsLeft]]" as="col" index-as="h">
                                                    <th id="header_dock_left_[[col.key]]" class="invisible-header-docked" style$="{{calculateStyle(col)}}">
                                                        &nbsp;
                                                    </th>
                                                </template>
                                            </tr>
                                        </table>
                                    </div>

                                    <!-- docked fixed headers-->

                                    <div id="fixedHeadersDockLeft" class$="[[determineHeaderClass(initialized, resizing, 'docked-left')]]">
                                        <template is="dom-repeat" items="[[columnsLeft]]" as="col" index-as="h">
                                            <div class="fixed-headers-border"></div>

                                            <div on-click="sortClicked" class$="[[determineFixedHeaderClass(col)]]" id="fixed_dock_left_[[col.key]]"> 
                                                <template is="dom-if" if="[[isFieldType(col.type, 'checkbox')]]">
                                                    <dr-zp-grid-header-checkbox-renderer col="[[col]]" data="[[data]]"></dr-zp-grid-header-checkbox-renderer>
                                                </template>
                
                                                <template is="dom-if" if="[[!isFieldType(col.type, 'checkbox')]]">    
                                                    <dr-zp-grid-header-renderer style="width: 100%" col="[[col]]"></dr-zp-grid-header-renderer>
                                                </template>
                                            </div>

                                            <div class="fixed-header-border-no" id="border_dock_left_[[col.key]]" title="Drag to resize column '[[col.key]]'"></div>
                                        </template>
                                    </div>
                                </div>
                            </template>
                        
                            <!-- SCROLLABLE -->

                            <div id="scrollableCenter" class="dock-center" style$="[[determineScrollableCenterStyle(leftDockWidth, rightDockWidth)]]">
                                <div id="scroller" class="table-scroll">
                                    <table id="table" class="grid-data-table" style$="[[isinitialized(initialized)]]">
                                        <template is="dom-repeat" items="[[data]]" as="row" index-as="r">
                                            <tr id="row_[[r]]" on-click="selectRow" on-mouseenter="rowEnter" on-mouseleave="rowLeave" class$="{{calculateRowClass(selected, row)}}" draggable="[[allowDrag]]" on-dragstart="drag" on-drop="drop" on-dragover="allowDrop">
                                                <template is="dom-repeat" items="[[columnsVisible]]" as="col" index-as="c">
                                                    <template is="dom-if" if="[[showCol(col)]]">
                                                        <td class="grid-data-table-cell" style$="{{calculateStyle(col)}}">
                                                            ${renderersStr}
                                                        </td>
                                                    </template>
                                                </template>

                                                <th id="header_spacer" class="filler" style="width:100%">&nbsp;</th>
                                            </tr>
                                        </template>
                                        <tr>
                                            <template is="dom-repeat" items="[[columnsVisible]]" as="col" on-dom-change="domReady" index-as="h">
                                                <th id="header_[[col.key]]" class="invisible-header" style$="{{calculateStyle(col)}}">
                                                    &nbsp;
                                                </th>
                                            </template>

                                            <th id="header_spacer" class="filler-header invisible-header" style="width:100%">&nbsp;</th>
                                        </tr>
                                    </table>
                                </div>

                                <!-- fixed headers -->

                                <div id="fixedHeaders" class$="[[determineHeaderClass(initialized, resizing)]]" on-mousedown="dragHeaderStart" on-mousemove="dragHeaderMove" on-mouseup="dragHeaderEnd" on-mouseenter="dragHeaderEnd">
                                    <template is="dom-repeat" items="[[columnsVisible]]" as="col" index-as="h">
                                        <div class="fixed-headers-border"></div>

                                        <div on-click="sortClicked" class$="[[determineFixedHeaderClass(col)]]" id="fixed_[[col.key]]"> 
                                            <template is="dom-if" if="[[isFieldType(col.type, 'checkbox')]]">
                                                <dr-zp-grid-header-checkbox-renderer col="[[col]]" data="[[data]]"></dr-zp-grid-header-checkbox-renderer>
                                            </template>
            
                                            <template is="dom-if" if="[[!isFieldType(col.type, 'checkbox')]]">    
                                                <dr-zp-grid-header-renderer style="width: 100%" col="[[col]]"></dr-zp-grid-header-renderer>
                                            </template>
                                        </div>

                                        <div class="fixed-header-border" id="border_[[col.key]]" title="Drag to resize column '[[col.key]]'"></div>
                                    </template>

                                    <div id="fixed_spacer" class="fixed-header filler-header">&nbsp;</div>
                                </div>
                            </div>


                            <!-- RIGHT DOCK -->

                            <template is="dom-if" if="[[hasRightDockedColumns(columnsRight)]]">
                                <div id="dockRight" class="dock-right" style$="[[determineRightDockStyle(rightDockWidth)]]">

                                    <div id="scrollerDockedRight" class="table-no-scroll">
                                        <table id="tableDosckedRight" class="grid-data-table" style$="[[isinitialized(initialized)]]">
                                            <template is="dom-repeat" items="[[data]]" as="row" index-as="r">
                                                <tr id="row_dock_right_[[r]]" on-click="selectRow"  on-mouseenter="rowEnter" on-mouseleave="rowLeave" class$="{{calculateRowClass(selected, row, 'docked-left')}}">
                                                    <template is="dom-repeat" items="[[columnsRight]]" as="col" index-as="c">
                                                        <template is="dom-if" if="[[showCol(col)]]">
                                                            <td class="grid-data-table-cell-right" style$="{{calculateStyle(col)}}">
                                                                ${renderersStr}
                                                            </td>
                                                        </template>
                                                    </template>
                                                </tr>
                                            </template>
                                            <tr>
                                                <template is="dom-repeat" items="[[columnsRight]]" as="col" index-as="h">
                                                    <th id="header_dock_right_[[col.key]]" class="invisible-header-docked" style$="{{calculateStyle(col)}}">
                                                        &nbsp;
                                                    </th>
                                                </template>
                                            </tr>
                                        </table>
                                    </div>
                    

                                    <div id="fixedHeadersDockRight" class$="[[determineHeaderClass(initialized, resizing, 'docked-right')]]">
                                        <template is="dom-repeat" items="[[columnsRight]]" as="col" index-as="h">
                                            <div class="fixed-headers-border"></div>

                                            <div on-click="sortClicked" class$="[[determineRightFixedHeaderClass(col)]]" id="fixed_dock_right_[[col.key]]"> 
                                                <template is="dom-if" if="[[isFieldType(col.type, 'checkbox')]]">
                                                    <dr-zp-grid-header-checkbox-renderer col="[[col]]" data="[[data]]"></dr-zp-grid-header-checkbox-renderer>
                                                </template>
                
                                                <template is="dom-if" if="[[!isFieldType(col.type, 'checkbox')]]">    
                                                    <dr-zp-grid-header-renderer style="width: 100%" col="[[col]]"></dr-zp-grid-header-renderer>
                                                </template>
                                            </div>

                                            <template is="dom-if" if="[[notLastRightDockColumn(columnsRight, h)]]">
                                                <div class="fixed-header-border-no" id="border_dock_right_[[col.key]]" title="Drag to resize column '[[col.key]]'"></div>
                                            </template>
                                        </template>
                                    </div>

                                </div>
                            </template>



                        </template>
                    </div>
                </div> 
                
                <div class$="[[determineLoadingClass(loading)]]">
                    <dr-zp-spinner class="loading-spinner" label="[[loadingMessage]]" size="xl"></dr-zp-spinner>
                </div>

                <dr-zp-modal
                    id="nameModal"
                    is-open="{{saveModalOpen}}"
                    modal-title="[[getString('sdk-filter-set-name', locale)]]"
                    size="sm"
                    show-footer="true"
                    confirmation-label="[[getString('sdk-save', locale)]]"
                    cancel-label="[[getString('sdk-cancel', locale)]]">

                    <div slot="slot-content">
                        <dr-zp-input value="{{fsName}}" placeholder="Enter Filter Set Name"></dr-zp-input>
                    </div>
                </dr-zp-modal>

                <dr-zp-modal
                    id="removeModal"
                    is-open="{{removeModalOpen}}"
                    modal-title="[[getString('sdk-confirm', locale)]]"
                    size="sm"
                    show-footer="true"
                    confirmation-label="[[getString('sdk-ok', locale)]]"
                    cancel-label="[[getString('sdk-cancel', locale)]]">

                    <div slot="slot-content">
                        [[getString('sdk-filter-set-remove', locale)]]: [[filterSetToDelete.key]]
                    </div>
                </dr-zp-modal>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            gid: {
                type: String
            },
            entity: {
                type: String,
                value: "Items"
            },
            initialized: {
                type: Boolean,
                value: false
            },
            loading: {
                type: Boolean,
                notify: true,
                observer: 'loadingObserved'
            },
            loadingMessage: {
                type: String
            },
            gridTitle: {
                type: String
            },
            primaryKey: {
                type: String
            },
            inc: {
                type: Number,
                value: 0,
                observer: 'incObserved'
            },
            headerRenderers: {
                type: Array,
                value: []
            },
            cellRenderers: {
                type: Array,
                value: []
            },
            columns: {
                type: Array,
                observer: 'columnsObserved',
                notify: true
            },
            data: {
                type: Array,
                observer: 'dataObserved'
            },
            drillinFilter: {
                type: Object,
                value: null,
                observer: 'drillinFilterObserved'
            },
            filters: {
                type: Array,
                value: []
            },
            filterSets: {
                type: Array,
                value: {}
            },
            lookups: {
                type: Array,

            },
            pillActions: {
                type: Array,
                value: ["apply", "save"]
            },
            details: {
                type: Number
            },
            allowDrag: {
                type: Boolean,
                value: false,
            },
            disabled: {
                type: Boolean,
                value: false,
            },
            selected: {
                type: Object,
                value: null
            },
            allowExport: {
                type: Boolean,
                value: false
            },
            allowSearch: {
                type: Boolean,
                value: false
            },
            search: {
                type: String
            },
            resizing: {
                type: Boolean,
                value: false
            },
            initialized: {
                type: Boolean,
                value: false
            },
            selectedRows: {
                type: Array,
                notify: true,
                value: []
            },
            allowActionSlot: {
                type: Boolean,
                value: false
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.loadingMessage = this.loadingMessage == undefined ? this.getString("sdk-loading", this.locale) : this.loadingMessage;
        this.updater = 0;

        this.filterKey = this.gid + "_filterSets";

        let fsCopy = this.utils._helper_copy(this.filterSets);

        if(localStorage.getItem(this.filterKey)) {
            this.filterSets = JSON.parse(localStorage.getItem(this.filterKey));
        }

        if(!this.utils._helper_isEmpty(fsCopy)) {
            console.info('fsCopy', fsCopy);
            Object.assign(this.filterSets, fsCopy);
        }

        document.addEventListener('tab-changed', () => {
            this.calculateHeadersDelayed();
        });

        this.addEventListener('checkbox-change', e => {
            let row = e.detail.row;

            let index = this.data.findIndex(item => {
                return item.id === row.id;
            });

            this.data[index] = row;
            this.data = this.utils._helper_copy(this.data);

            this.countSelected();
        });

        this.addEventListener('row-all-selected', e => {
            let state = e.detail.state;
            console.info('row-all-selected:' + state);

            for (let r = 0; r < this.data.length; r++) {
                let row = this.data[r];

                if (!row._selectionDisabled) {
                    row._selected = state;
                }
            }

            this.data = this.utils._helper_copy(this.data);
            this.countSelected();
        });

        this.$.colConfig.addEventListener('filters-reset', e => {
            this.calculateHeadersDelayed();
        });

        this.addEventListener('dr-zp-pill-height-changed', (e) => {
            this.pillboxHeight = Math.max(65, e.detail.height + 30);
        });

        this.addEventListener('dr-zp-pill-box-remove', (e) => {
            console.info('dr-zp-pill-box-remove', e.detail.list);
            this.filters = e.detail.list;

            if (this.filters.length === 0) {
                this.filterData();
            }
        });

        this.addEventListener('dr-zp-pill-box-clear', (e) => {
            console.info('dr-zp-pill-box-clear', e.detail.list);
            this.filters = e.detail.list;
            this.filterData();
        });

        this.addEventListener('column-filter-add', e => {
            let col = e.detail.col;
            let name = col.caption;

            let filter = this.filters.find(filter => filter.field === col.sortKey);

            if (filter == null) { //already exists
                let sortKey = col.sortKey || col.field;
                let filter = { "name": name, "value": "", editable: true, field: col.sortKey, sortKey: sortKey };
                this.filters.push(filter);
                this.filters = this.utils._helper_copy(this.filters);
            }
        });

        this.addEventListener('dr-zp-pill-value-change', e => {
            let pill = e.detail.pill;
            this.filters = e.detail.list;
            this.columns = this.utils._helper_copy(this.columns);
            console.info('dr-zp-pill-value-change', pill);
        });

        this.addEventListener('dr-zp-pill-box-add', (e) => {
            let pill = e.detail.pill;
            console.info('dr-zp-pill-box-add', pill);
            let col = this.columns.find(col => col.sortKey === name);
            let filter = this.filters.find(filter => filter.field === pill.field);

            if (filter == null) {
                let displayName = pill.caption;

                if (displayName.startsWith('%')) {
                    let key = displayName.substring(1);
                    displayName = this.getString(key, this.locale);
                }

                filter = { "name": displayName, "value": "", editable: true, field: pill.field, sortKey: col.sortKey };
                this.filters.push(filter);
                this.filters = this.utils._helper_copy(this.filters);
            }
        });

        this.addEventListener('dr-zp-pill-box-action', (e) => {
            let action = e.detail.action;
            this.filters = e.detail.list;
            console.info('dr-zp-pill-box-action', action);

            if (action === 'apply') {
                this.filterData();
            }
            else if(action === 'save') {            
                this.saveModalOpen = true;
            }
        });

        this.addEventListener('dr-zp-pill-box-lookup-remove', (e) => {
            console.info('dr-zp-pill-box-lookup-remove', e.detail.list);
            this.filters = e.detail.list;
        });

        this.addEventListener('dr-zp-pill-box-filterset-remove', (e) => {
            this.removeModalOpen = true;
            this.filterSetToDelete = e.detail.filterSet;
            console.info('dr-zp-pill-box-filterset-remove', this.filterSetToDelete);
        });

        this.addEventListener('dr-zp-pill-box-lookup-add', (e) => {
            let lookup = e.detail.lookup;
            let option = e.detail.option;
            let col = this.columns.find(col => col.sortKey === lookup.name);
            console.info('dr-zp-pill-box-lookup-add', lookup, option);

            let filter = this.filters.find(filter => filter.field === lookup.name);

            if (filter != null) {//if filter exists for field add it as a RegEx OR
                if (!filter.value) {
                    filter.value = option.value;
                }
                else {
                    filter.value += "|" + option.value;
                }
            }
            else {
                filter = {"name": lookup.resourceKey, "value": option.value, editable: true, field: lookup.name, sortKey: col.sortKey};
                this.filters.push(filter);
            }

            this.filters = this.utils._helper_copy(this.filters);
        });

        this.addEventListener('reset-row-heights', (e) => {
            let lookup = e.detail.lookup;
            let option = e.detail.option;
            console.info('reset-row-heights', lookup, option);
            this.calculateRowHeights(true);
        });

        this.$.nameModal.addEventListener('dr-zp-modal-confirmation-clicked', e => {
            this.saveModalOpen = false;

            if (this.fsName != null && this.fsName.length > 0 && this.filterSets[this.fsName] == null) {
                this.filterSets[this.fsName] = this.filters;
            }

            let str = JSON.stringify(this.filterSets);
            localStorage.setItem(this.filterKey, str);
            this.filterSets = this.utils._helper_copy(this.filterSets);
            this.fsName = "";
        });

        this.$.nameModal.addEventListener('dr-zp-modal-cancel-clicked', e => {
            console.info('nameModal cancel clicked');
            this.saveModalOpen = false;
            this.fsName = "";
        });

        this.$.removeModal.addEventListener('dr-zp-modal-cancel-clicked', e => {
            console.info('removeModal cancel clicked');
            this.removeModalOpen = false;
        });

        this.$.removeModal.addEventListener('dr-zp-modal-confirmation-clicked', e => {
            this.removeModalOpen = false;
            console.info('remove FS', this.filterSetToDelete);
            delete this.filterSets[this.filterSetToDelete.key];
            this.filterSets = this.utils._helper_copy(this.filterSets);
            localStorage.setItem(this.filterKey,JSON.stringify(this.filterSets));
            delete this.filterSetToDelete;
        });
    }

    filterData() {
        this.dispatchEvent(new CustomEvent('filter-change', {
            detail: {
                filters: this.filters
            }
        }));
    }

    drillinFilterObserved() {
        console.info('drillinFilterObserved', this.drillinFilter);

        if(this.drillinFilter) {
            let col = this.columns.find(col => col.sortKey === this.drillinFilter.field);
            let filterName = col ? col.caption : this.drillinFilter.field;
            let filter = { "name": filterName, "value": this.drillinFilter.value, editable: true, field: this.drillinFilter.field, sortKey: this.drillinFilter.field};
            this.filterSets['drill-in'] = [filter];
        }
        else {
            delete this.filterSets['drill-in'];
        }
    }

    countSelected() {
        let selected = this.data.filter(item => {
            return item._selected === true;
        });
        this.set("selectedRows", selected);
        this.selectedCount = selected.length;
    }

    domReady() {
        this.calculateHeadersDelayed();
        let scroller = dom(this.root).querySelector("#scroller");
        let fixedHeaders = dom(this.root).querySelector("#fixedHeaders");
        let inner = dom(this.root).querySelector("#inner");

        if (inner != undefined) {
            inner.addEventListener('scroll', (e) => {
                inner.scrollLeft = 0;
            });
        }

        if (scroller != undefined) {
            scroller.addEventListener('scroll', (e) => {
                document.dispatchEvent(new CustomEvent("escape-action", {
                    detail: {}
                }));

                this.syncScrollers();

                let offsetLeft = scroller.scrollLeft;
                offsetLeft *= -1;
                fixedHeaders.style.left = offsetLeft + "px";
            });
        }

        window.addEventListener("resize", () => { this.calculateHeaders(); });
    }

    hasData(data) {
        return this.data != null && this.data.length > 0;
    }

    notLastRightDockColumn(columnsRight, h) {
        let ret = h < columnsRight.length - 1;
        return ret;
    }

    hasLeftDockedColumns(columnsLeft) {
        let ret = this.columnsLeft != null && this.columnsLeft.length > 0
        return ret;
    }

    hasRightDockedColumns(columnsRight) {
        let ret = this.columnsRight != null && this.columnsRight.length > 0
        return ret;
    }

    hasVisibleColumns(columnsVisible) {
        return this.columnsVisible.length > 0;
    }

    hasTitle(gridTitle) {
        return this.gridTitle !== null;
    }

    isAllowExport(allowExport) {
        return this.allowExport;
    }
    isAllowActionSlot(allowActionSlot) {
        return this.allowActionSlot;
    }
    isAllowSearch(allowSearch) {
        return this.allowSearch;
    }

    computeGridColumnsStyle(col, columns) {
        if (columns !== null && columns.length > 0) {
            let percent = Math.round(100 / columns.length);
            let style = "width: " + percent + "%";
            return style;
        }
    }

    getDataValue(row, col) {
        let val = row[col.field];
        return val;
    }

    isFirstColumn(col) {
        return col === 0;
    }

    determineLoadingClass(loading) {
        let cls = 'table-loading';

        if (!loading) {
            cls += ' table-loading-hidden';
        }

        return cls;
    }

    isInitialized(loading) {
        return !this.loading;
    }

    hasFilterableColumns(columns) {
        if(columns) {
            this.filterableColumns = columns.filter((col) => {
                return col.filterable;
            });

            return this.filterableColumns.length > 0;
        }

        return false;
    }

    calculateWrapperClass(columns) {
        let cls = 'table-wrapper';

        if (!this.hasFilterableColumns(columns)) {
            cls += '-no-filter';
        }

        return cls;
    }

    dragHeaderStart(e) {
        let elem = e.target || e.srcElement;
        let cls = elem.className;

        if (cls.indexOf("fixed-header-border") > -1) {
            let id = elem.id;
            let key = id.substring(id.indexOf('_') + 1);
            let col = this.getColumnDefByKey(key);
            this.dragColumn = key;
            this.dragStartX = e.x;
            this.startColWidth = col.width;
            this.resizing = true;
        }
    }

    dragHeaderMove(e) {
        if (this.dragColumn !== undefined) {
            this._debouncer = Debouncer.debounce(this._debouncer, timeOut.after(10), () => {
                let posX = e.x;
                let offset = posX - this.dragStartX;
                let col = this.getColumnDefByKey(this.dragColumn);

                if (col != null) {
                    let minWidth = col.minWidth || 40;
                    col.width = Math.max(minWidth, this.startColWidth + offset);
                    this.columns = this.utils._helper_copy(this.columns);
                }
            });
        }
    }

    dragHeaderEnd(e) {
        this.resizing = false;

        if (this.dragColumn !== undefined) {
            delete this.dragStartX;
            delete this.dragColumn;
            delete this.startColWidth;
            this.updater++;
        }
    }

    getColumnDefByKey(key) {
        for (let c = 0; c < this.columns.length; c++) {
            let col = this.columns[c];

            if (col.key === key) {
                return col;
            }
        }

        return null;
    }

    allowDrop(e) {
        if (this.allowDrag) {
            e.preventDefault();
        }
        else {
            return false;
        }
    }

    drag(e) {
        if (this.allowDrag) {
            let row = e.model.__data.row;
            let id = row[this.primaryKey];
            e.dataTransfer.setData("text", id);
        }
        else {
            return false;
        }
    }

    drop(e) {
        e.preventDefault();
        let sourceId = e.dataTransfer.getData("text");
        let targetRow = e.model.__data.row;
        let targetId = targetRow[this.primaryKey];

        if (targetId != sourceId) { //not dropping on same
            this.dispatchEvent(new CustomEvent('drag-complete', {
                detail: {
                    source: sourceId,
                    target: targetId
                }
            }));
        }
    }

    isEmpty(data) {
        return this.data.length == 0;
    }

    calculateInnerStyle(details) {
        let style = "";

        if (details > -1) {
            style += 'right: ' + details + 'px';
        }

        return style;
    }

    determineRightDockStyle(rightDockWidth) {
        let str = "";

        if (this.hasRightDockedColumns(this.columnsRight)) {
            str = "width: " + this.rightDockWidth + "px";
        }

        return str;
    }

    determineLeftDockStyle(columnsLeft) {
        let str = "";

        if (this.hasLeftDockedColumns(columnsLeft)) {
            str = "width: " + this.leftDockWidth + "px";
        }

        return str;
    }

    isinitialized(initialized) {
        let str = "";

        if (!initialized) {
            str = "visibility: hidden";
        }

        return str;
    }

    determineScrollableCenterStyle(leftDockWidth, rightDockWidth) {
        let str = "";

        if (this.hasLeftDockedColumns(leftDockWidth)) {
            str = "left: " + this.leftDockWidth + "px";
        }

        if (this.hasRightDockedColumns(rightDockWidth)) {
            str += "; right: " + (this.rightDockWidth + 2) + "px";
        }

        return str;
    }

    determineHeaderClass(initialized, resizing, suffix) {
        let cls = "fixed-headers";

        if (suffix) {
            cls += "-" + suffix;
        }

        if (this.resizing) {
            cls += " resizing";
        }

        if (!initialized) {
            cls += " hidden";
        }

        return cls;
    }

    determineRightFixedHeaderClass(col) {
        let cls = "fixed-header-right";

        if (col.sortable) {
            cls = " pointered";
        }

        return cls;
    }

    determineFixedHeaderClass(col) {
        let cls = "fixed-header";

        if (col.sortable) {
            cls = " pointered";
        }

        return cls;
    }

    calculateDisabledPaneClass(disabled) {
        let cls = "hidden";

        if (this.disabled) {
            cls = "table-disabled-pane";
        }
        return cls;
    }

    calculateStyle(col) {
        let style = "";

        if (col.width != null) {
            style += 'width: ' + col.width + 'px; min-width: ' + col.width + 'px; ';

            if (col.width === '*') {
                style += 'width: 100%; min-width: 100%;';
            }
            else {
                style += 'width: ' + col.width + 'px; min-width: ' + col.width + 'px; ';
            }
        }

        if (col.wrap === undefined) {
            style += 'white-space: nowrap';
        }
        else if (col.wrap) {
            style += 'white-space: normal';
        }
        else {
            style += 'white-space: nowrap';
        }

        return style;
    }

    calculateHeaders() {
        if (this.data != null && this.columnsVisible !== null && this.columnsVisible.length > 0) {
            let offset = 0;
            let sum = 0;
            let headerHeight = 0;
            let rows = dom(this.root).querySelectorAll(".grid-data-table-row");
            this.spacerheader = this.spacerheader || dom(this.root).querySelector("#header_spacer");
            this.spacerfixed = this.spacerfixed || dom(this.root).querySelector("#fixed_spacer");
            this.fixedHeaders = this.fixedHeaders || dom(this.root).querySelector("#fixedHeaders");

            if (this.spacerfixed !== null) {
                let wid = this.spacerheader.clientWidth;
                this.spacerfixed.style.width = (wid + 1) + "px";

                for (let c = 0; c < this.columnsVisible.length; c++) {
                    let col = this.columnsVisible[c];
                    let header = dom(this.root).querySelector("#header_" + col.key);
                    let fixed = dom(this.root).querySelector("#fixed_" + col.key);
                    let border = dom(this.root).querySelector("#border_" + col.key);

                    if (header !== null) {
                        wid = header.clientWidth;
                        sum += wid;
                        offset += wid;
                        fixed.style.width = wid + "px";
                        fixed.style.minWidth = wid + "px";

                        if (border !== null) {
                            border.style.left = (offset - 2) + "px";
                        }
                    }
                }
            }

            //if we have docked columns, sync row heights

            let scrollerDockedLeft = dom(this.root).querySelector("#scrollerDockedLeft");
            let scrollerDockedRight = dom(this.root).querySelector("#scrollerDockedRight");

            if (scrollerDockedLeft || scrollerDockedRight) {
                this.calculateRowHeights(false);
            }

            if (scrollerDockedLeft) {  // calculate leftDockWidth
                let wid = 0;
                let offset = 0;

                for (let c = 0; c < this.columnsLeft.length; c++) {
                    let col = this.columnsLeft[c];
                    let leftHeader = dom(this.root).querySelector("#header_dock_left_" + col.key);

                    if (leftHeader) {
                        let leftFixed = dom(this.root).querySelector("#fixed_dock_left_" + col.key);
                        let leftBorder = dom(this.root).querySelector("#border_dock_left_" + col.key);
                        wid = leftHeader.clientWidth;
                        offset += wid;
                        leftFixed.style.width = wid + "px";
                        leftFixed.style.minWidth = wid + "px";

                        if (leftBorder !== null) {
                            leftBorder.style.left = (offset - 1) + "px";
                        }
                    }
                }

                this.leftDockWidth = offset;
            }

            if (scrollerDockedRight) { // calculate leftDockWidth
                let wid = 0;
                let offset = 0;

                for (let z = 0; z < this.columnsRight.length; z++) {
                    let col = this.columnsRight[z];
                    let rightHeader = dom(this.root).querySelector("#header_dock_right_" + col.key);

                    if (rightHeader) {
                        let rightFixed = dom(this.root).querySelector("#fixed_dock_right_" + col.key);
                        let rightBorder = dom(this.root).querySelector("#border_dock_right_" + col.key);
                        wid = rightHeader.clientWidth;
                        offset += wid;
                        rightFixed.style.width = wid + "px";
                        rightFixed.style.minWidth = wid + "px";

                        if (rightBorder !== null) {
                            rightBorder.style.left = (offset - 2) + "px";
                        }
                    }
                }

                this.rightDockWidth = offset;
            }

            if (rows.length === this.data.length) {
                this.initialized = true;
            }
        }
    }

    calculateRowHeights(reset) {
        for (let r = 0; r < this.data.length; r++) {
            let rowElem = dom(this.root).querySelector("#row_" + r);
            let rowElemLeft = dom(this.root).querySelector("#row_dock_left_" + r);
            let rowElemRight = dom(this.root).querySelector("#row_dock_right_" + r);
            let h1 = 0, h2 = 0, h3 = 0, hMax = 0;

            if (rowElem) {
                h1 = rowElem.clientHeight;
                hMax = h1;
            }

            if (rowElemLeft) {
                h2 = rowElemLeft.clientHeight;
                hMax = Math.max(hMax, h2);
            }

            if (rowElemRight) {
                h3 = rowElemRight.clientHeight;
                hMax = Math.max(hMax, h3);
            }

            if (reset) {
                if (rowElem) {
                    rowElem.style.height = "";
                }

                if (rowElemLeft) {
                    rowElemLeft.style.height = "";
                }

                if (rowElemRight) {
                    rowElemRight.style.height = "";
                }
            }
            else {
                if (rowElem) {
                    rowElem.style.height = hMax + "px";
                }

                if (rowElemLeft) {
                    rowElemLeft.style.height = hMax + "px";
                }

                if (rowElemRight) {
                    rowElemRight.style.height = hMax + "px";
                }
            }
        }

        this.syncScrollers();
    }

    syncScrollers() {
        let scroller = dom(this.root).querySelector("#scroller");

        if (scroller != undefined) {
            let scrollerDockedLeft = dom(this.root).querySelector("#scrollerDockedLeft");
            let scrollerDockedRight = dom(this.root).querySelector("#scrollerDockedRight");

            if (scrollerDockedLeft) {
                scrollerDockedLeft.scrollTop = scroller.scrollTop;
            }

            if (scrollerDockedRight) {
                scrollerDockedRight.scrollTop = scroller.scrollTop;
            }
        }
    }

    sortClicked(e) {
        let col = e.model.__data.col;

        if (col.sortable) {
            if (col.sorter) {
                col.asc = !col.asc
            }
            else {
                for (let c = 0; c < this.columns.length; c++) { //clear all sorter
                    let colz = this.columns[c];

                    if (colz.sorter !== undefined) {
                        delete colz.sorter;
                    }
                }

                col.sorter = true;
                col.asc = true;
            }

            let newcolumnDefs = JSON.parse(JSON.stringify(this.columns));
            this.columns = newcolumnDefs;

            this.dispatchEvent(new CustomEvent('sort-change', {
                detail: {
                    column: col
                }
            }));
        }
    }

    hasFilters() {
        return this.filters !== undefined;
    }

    exportClicked(e) {
        this.dispatchEvent(new CustomEvent('export-action', {}));
    }

    incObserved() {
        this.calculateHeadersDelayed();
    }

    scrollToSelected(row) {
        let selector = `#row_${row}`;
        let rowElem = dom(this.root).querySelector(selector);
        console.info('selectedObserved', this.selected, row, rowElem);

        if (rowElem != null) {
            rowElem.scrollIntoView();
        }
    }

    dataObserved() {
        if (this.data != null && this.data.length === 0) {
            this.calculateHeaders();
        }

        this.calculateRowHeights(true);

        this._debouncer = Debouncer.debounce(this._debouncer, timeOut.after(500), () => {
            this.inc++;
            let scroller = dom(this.root).querySelector("#scroller");
            scroller.scrollTop = 0;
            this.initiateLoading();
        });
    }

    columnsObserved() {
        if(this.columns) {
            this.initiateLoading();
            this.columnsLeft = this.columns.filter(col => col.visible && !col.hidden && col.dock === 'left');
            this.columnsRight = this.columns.filter(col => col.visible && !col.hidden && col.dock === 'right');
            this.columnsVisible = this.columns.filter(col => col.visible && !col.hidden && col.dock == null);
            this.inc++;
        }
    }

    initiateLoading() {
        this.calculateHeadersDelayed();
    }

    caclulateHeaderSortClass(col) {
        let cls = "fa fa-sort";

        if (this.ff) {
            cls += " header-sort-ff";
        }
        else {
            cls += " header-sort";
        }

        return cls;
    }

    calculateHeadersDelayed() {
        setTimeout(() => {
            this.calculateHeaders();
        }, 100);
    }

    showCol(col) {
        let hidden = col.hidden != undefined && col.hidden === true;
        return col.visible && !hidden;
    }

    isFieldType(field, type) {
        return field === type;
    }

    deselect() {
        this.set('selected', null);
    }

    selectRow(e) {
        let src = e.srcElement;

        if (src.tagName != "FE-GRID-CHECKBOX-RENDERER") {
            let row = e.model.__data.row;

            let id1 = row[this.primaryKey];
            let id2 = "";

            if (this.selected !== null) {
                id2 = this.selected[this.primaryKey];
            }

            this.calculateRowHeights(false);

            if (id1 === undefined || id1 !== id2) {
                this.selected = row;

                this.dispatchEvent(new CustomEvent('row-selected', {
                    detail: {
                        row: this.selected
                    }
                }));
            }
        }
    }

    rowEnter(e) {
        let row = e.model.__data.r;
        let leftDockRow = dom(this.root).querySelector("#row_dock_left_" + row);
        let scrollableRow = dom(this.root).querySelector("#row_" + row);
        let rightDockRow = dom(this.root).querySelector("#row_dock_right_" + row);

        if (leftDockRow) {
            leftDockRow.classList.add('grid-data-table-row-hovered');
        }

        if (scrollableRow) {
            scrollableRow.classList.add('grid-data-table-row-hovered');
        }

        if (rightDockRow) {
            rightDockRow.classList.add('grid-data-table-row-hovered');
        }
    }

    rowLeave(e) {
        let row = e.model.__data.r;
        let leftDockRow = dom(this.root).querySelector("#row_dock_left_" + row);
        let scrollableRow = dom(this.root).querySelector("#row_" + row);
        let rightDockRow = dom(this.root).querySelector("#row_dock_right_" + row);

        if (leftDockRow) {
            leftDockRow.classList.remove('grid-data-table-row-hovered');
        }

        if (scrollableRow) {
            scrollableRow.classList.remove('grid-data-table-row-hovered');
        }

        if (rightDockRow) {
            rightDockRow.classList.remove('grid-data-table-row-hovered');
        }
    }

    calculateRowClass(selected, row, suffix) {
        let cls = "grid-data-table-row";

        if (suffix) {
            cls += "-" + suffix;
        }

        let clone = this.utils._helper_copy(row);
        clone = this.utils._helper_sanitizeDisplayProperties(clone);

        if (selected) {
            let cloneSelected = this.utils._helper_copy(this.selected);
            cloneSelected = this.utils._helper_sanitizeDisplayProperties(cloneSelected);

            if (JSON.stringify(clone) == JSON.stringify(cloneSelected)) {
                cls += " row-selected";
            }
        }

        return cls;
    }
}

window.customElements.define('dr-zp-grid', FEGrid);