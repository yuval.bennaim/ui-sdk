import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../core/dr-zp-base-component.js';
import './dr-zp-column-config-multi-select.js';
import '../../../core/form/dr-zp-button.js';

class GridColumnConfig extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
        ${superTemp}    
        <dr-zp-column-config-multi-select id="multiselect" dropdowntype="icon" icon="list" actions="[[controls]]" items="{{columnsVisible}}" field="caption" field-checked="visible"></dr-zp-column-config-multi-select>`;
        return template;
    }

    static get properties() {
        return {
            name: {
                type: String,
                value: 'fe-grid-column-config'
            },
            id: {
                type: String,
            },
            gid: {
                type: String,
            },
            columns: {
                type: Array,
                value: [1, 2, 3],
                notify: true,
                observer: 'columnsObserved'
            },
            columnsVisible: {
                type: Array,
                value: []
            },
            controls: {
                type: Array,
                value: [
                    { key: 'restore', label: '%sdk-restore-defaults' }
                ]
            },
            updater: {
                type: Number,
                observer: 'updaterObserved'
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();

        this.$.multiselect.addEventListener('multi-select-change', e => {
            var items = e.detail.items;
            console.info("multi-select-change", items);
            this.columns = this.utils._helper_copy(items);
            this.saveConfig();
        });

        this.$.multiselect.addEventListener('multi-select-action', e => {
            var action = e.detail.action;
            console.info("multi-select-action", action);

            if (e.detail.action === 'restore') {
                this.restoreAllColumns();
            }
        });

        this.$.multiselect.addEventListener('drag-complete', e => {
            var source = e.detail.source;
            var target = e.detail.target;
            var sourceIndex = this._findColumnIndexByKey(source);
            var targetIndex = this.columns.length;
            console.info("drag-complete", source, target);

            if (target !== null) {
                targetIndex = this._findColumnIndexByKey(target);
            }

            this.columns.move(sourceIndex, targetIndex)
            this.reorderColumns();
            this.columns = this.utils._helper_copy(this.columns);
            this.saveConfig();
        });
    }

    updaterObserved() {
        if (this.updater > 0) {
            this.saveConfig();
        }
    }

    reorderColumns(key) {
        for (var c = 0; c < this.columns.length; c++) {
            var col = this.columns[c];
            col.order = c;
        }
    }

    _findColumnIndexByKey(key) {
        for (var c = 0; c < this.columns.length; c++) {
            var col = this.columns[c];

            if (col.key === key) {
                return c;
            }
        }

        return null;
    }

    columnsObserved(newValue, oldValue) {
        if (this.columns != null && this.columns.length > 0) {
            this.columnsVisible = this.columns.filter(col => !col.hidden);

            if (oldValue == null) {
                this.defaultColumns = this.utils._helper_copy(this.columns);
                var key = this.gid + "_columns";
                var lsCols = localStorage.getItem(key);

                if (lsCols !== null) {
                    lsCols = JSON.parse(lsCols);
                    this.restoreConfig(lsCols);
                }
            }
        }
    }

    restoreConfig(overrides) {
        console.info('restoreConfig', overrides);
        var reorder = false;

        for (var c = 0; c < this.columns.length; c++) {
            var col = this.columns[c];
            var key = col.key;

            var overrideCol = overrides.find((element) => {
                return element.key === key;
            });

            if (overrideCol != null) {
                col.visible = overrideCol.visible;

                if(overrideCol.dock == undefined) {
                    delete col.dock;
                }
                else {
                    col.dock = overrideCol.dock;
                }

                if (overrideCol.width != null) {
                    col.width = overrideCol.width
                }

                if (overrideCol.order != null) {
                    reorder = true;
                    col.order = overrideCol.order;
                }
            }
        }

        if (reorder) {
            this.columns.sort((a, b) => {
                return a.order - b.order;
            });
        }

        this.columns = this.utils._helper_copy(this.columns);
    }

    saveConfig() {
        var key = this.gid + "_columns";
        var cols = [];

        for (var c = 0; c < this.columns.length; c++) {
            var col = this.columns[c];
            var fieldKey = col.key;

            if(col.width == null) {
                col.width = 120;
            }

            var cobj = { "key": fieldKey, "visible": col.visible, "dock": col.dock, "width": col.width, "order": col.order };
            cols.push(cobj);
        }

        console.info('saveConfig', this.columns);

        var val = JSON.stringify(cols);
        localStorage.setItem(key, val);
    }

    restoreAllColumns() {
        console.info('restoreAllColumns', this.defaultColumns);
        this.columns = this.utils._helper_copy(this.defaultColumns);
        this.reorderColumns();
        this.columns = this.utils._helper_copy(this.columns);
        this.saveConfig();

        var event = new CustomEvent('filters-reset', { bubbles: true, composed: true, detail: { columns: this.columns } });
        this.dispatchEvent(event);
    }
}

window.customElements.define('dr-zp-grid-column-config', GridColumnConfig);