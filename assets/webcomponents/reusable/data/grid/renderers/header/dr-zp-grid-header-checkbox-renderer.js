import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/form/dr-zp-checkbox.js';

class GridHeaderCheckboxRenderer extends BaseComponent {
  static get template() {
    return html`
        <style> 
            :host {
                width: 100%;
            }
            .cb {
                margin: 4px;
                position: relative;
                top: -3px;
            }
            .content-wrapper {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100%;
                width: 100%;
                overflow: hidden;
            }        
        </style>

        <div class="content-wrapper">
            <dr-zp-checkbox class="cb" checked="{{checked}}" mixed="{{checkedMix}}"></dr-zp-checkbox>
        </div>
        `;
    }

    static get properties() {
        return {
            col: {
                type: Object,
                notify: true
            },
            data: {
                type: Array,
                observer: 'dataObserver'
            },
            checked: {
                type: Boolean,
                value: false
            },
            checkedMix: {
                type: Boolean,
                value: false
            },
            alignment: {
                type: String
            },
            disabled: {
                type: Boolean,
                value: false
            }
        }
    }
    
    connectedCallback() {
        super.connectedCallback();

        this.addEventListener('checkbox-clicked', e => {
            this.checked = e.detail.checked;
            this._onCheckedChange();
        });
    }

    _evaluateSelectedData() {
        var selected = this.data.filter(item => {
            return item._selected === true;
        });

        return selected.length;
    }

    _onCheckedChange() {
        if(!this.disabled) {
            var event = new CustomEvent('row-all-selected', {bubbles: true, composed: true, detail: {state: this.checked}});
            this.dispatchEvent(event);
        }
    }

    setCol(col) {
        this.col = col;
    }

    _getCssClass() {
        return "fe-checkbox" + (this.alignment ? " " + this.alignment : "");
    }

    dataObserver() {
        var selected = this._evaluateSelectedData();
        this.checked = selected === this.data.length;
        this.checkedMix = selected > 0 && selected < this.data.length;
    }
}

window.customElements.define('dr-zp-grid-header-checkbox-renderer', GridHeaderCheckboxRenderer);