import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import "../../../../../core/form/dr-zp-label.js";

class GridHeaderRenderer extends BaseComponent {

    static get template() {
        var superTemp = super.template;
        const sdkStyles = this.styleStore.getSheetStyles("sdk.css");
        
        var template = html`
            ${superTemp}
            ${sdkStyles}

            <style> 
                .header-content-wrapper {
                    display: flex;
                    align-items: center;
                    justify-content: space-between;
                    height: 100%;
                    width: 100%;
                    overflow: hidden;
                }
                .sort-wrapper {
                    margin-right: 10px;
                }       
                .label {
                    width: 100%;
                } 
            </style>

            <div class="header-content-wrapper">
                <template is="dom-if" if="[[!col.headerInvisible]]">
                    <dr-zp-label class="dr-zp-label label" is-transparent="true" label="[[getHeaderString(col.caption, locale)]]"></dr-zp-label>
                </template>

                <div class="sort-wrapper">

                    <template is="dom-if" if="[[isSortable(col)]]">

                        <button class="link" on-focus="focused">

                            <template is="dom-if" if="[[col.sorter]]">
                                <template is="dom-if" if="[[col.asc]]">
                                    <dr-zp-icon type="arrow-up" size="sm" title="[[getString('sdk-sort-asc', locale)]]"></dr-zp-icon>
                                </template> 

                                <template is="dom-if" if="[[!col.asc]]">
                                    <dr-zp-icon type="arrow-down" size="sm" title="[[getString('sdk-sort-desc', locale)]]"></dr-zp-icon>
                                </template>
                            </template>

                            <template is="dom-if" if="[[!col.sorter]]">
                                <dr-zp-icon type="arrows-vertical" size="sm" title="[[getString('sdk-sortable', locale)]]"></dr-zp-icon>
                            </template>   

                        </button>
                    </template>

                    <template is="dom-if" if="[[isFilterable(col)]]">
                        <button class="link" on-click="filterClicked" on-focus="focused">
                            <dr-zp-icon type="filter" size="sm" title="[[generteFilterTitle(col, locale)]]"></dr-zp-icon>
                        </button>
                    </template>
                </div>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {                   
            col: {
                type: Object,
                observer: 'colObserved'
            },
            value: {
                type: String
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.init = true;
    }

    focused(e) {
        //console.info('focused', this.col.key);
    }

    getHeaderString(header, locale) {
        var str;

        if(header.startsWith('%')) {
            var key = header.substring(1);
            str = this.getString(key, this.locale);
        }
        else {
            str = this.col.caption;
        }

        return str;
    }

    isSortable(col) {
        return col.sortable;
    }

    isFilterable(col) {
        return col.filterable;
    }

    colObserved() {
        this.label = this.getString(this.col.caption);
        this.set('value', this.col.filter);
    }

    generteFilterTitle(col, locale) {
        let str = this.getString('sdk-filter-by', locale, this.label);
        return str;
    }    

    filterClicked(e) {
        e.stopPropagation();

        this.dispatchEvent(new CustomEvent('column-filter-add', {
            bubbles: true,
            composed: true,
            detail: {
                col: this.col
            }
        }));
    }
}

window.customElements.define('dr-zp-grid-header-renderer', GridHeaderRenderer);