import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/form/dr-zp-dropdown.js';

class GridListRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                :host {
                    height: 100%;
                    width: 100%;
                    display: block;
                } 
                toggle {
                    width: 100px;
                }
                .dropdown-ie {
                    position: relative;
                    top: -10px;
                    left: 0px;
                    width: calc(100% + 5px);
                }
            </style>

            <div class$="[[calculateDropdownClass(col)]]">
                <div class="toggle">
                    <dr-zp-dropdown id="list" items="[[items]]" selecteditem="[[value]]" dropdowntype="bordered" disabled={{disable}}></dr-zp-dropdown>
                </div>
            </div>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            items: {
                type: Array
            },
            field: {
                type: String
            },
            disable: {
                type: Boolean,
                value: false
            },
            value: {
                type: String,
                value: 'name'
            }
        }
    }
    ready() {
        super.ready();

        this.$.list.addEventListener('dropdown-change', e => {
            this.obj[this.field] = e.detail.selected;
            this.obj = this.utils._helper_copy(this.obj);
        });
    }

    calculateDropdownClass(col) {
        var cls = "";

        if (this.ie11) {
            cls += "dropdown-ie";
        }

        return cls;
    }

    colObserved() {
        if(this.col !== undefined && this.col.options !== undefined) {
            this.items = this.col.options;
        }
    }
    
    objObserved() {
        if(this.obj !== undefined) {
            this.value = this.obj[this.field];
        }
    }
}

window.customElements.define('dr-zp-grid-list-renderer', GridListRenderer);