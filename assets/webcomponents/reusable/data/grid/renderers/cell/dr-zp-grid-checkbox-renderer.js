import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/form/dr-zp-checkbox.js';

class GridCheckboxRenderer extends BaseComponent {
  static get template() {
        return html`
            <style> 
                :host {
                    width: 100%;
                }
                .content-wrapper {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 100%;
                    width: 100%;
                    overflow: hidden;
                }    
                .cb {
                    margin-bottom: 5px;
                }    
            </style>

            <div class="content-wrapper">
                <dr-zp-checkbox id="checkbox" class="cb" disabled="{{disabled}}" checked="{{checked}}"></dr-zp-checkbox>
            </div>
        `;
    }

    static get properties() {
        return {
            col: {
                type: Object,
                observer: 'colObserved'
            },
            obj: {
                type: Object,
                observer: 'objObserved',
                notify: true
            },
            field: {
                type: String
            },
            disabled: {
                type: Boolean,
                value: false
            },
            checked: {
                type: Boolean,
            },
            alignment: {
                type: String
            }
        }
    }
    
    connectedCallback() {
        super.connectedCallback();

        this.$.checkbox.addEventListener('checkbox-clicked', e => {
            this.checked = e.detail.checked;
            this._onCheckedChange();
        });
    }

    colObserved() {
        if(this.col != undefined && this.col.width != undefined) {
            this.width = this.col.width;

            if(this.obj !== undefined && this.field !== undefined) {
                this.objObserved();
            }
        }
    }

    objObserved() {
        if(this.obj !== undefined) {
            this.checked = this.obj[this.field];

            if(this.obj.val !== undefined) {
                this.checked = this.obj.val;  
            }

            this.disabled = this.obj._selectionDisabled;
        }
    }

    _onCheckedChange() {
        if(!this.disabled) {
            this.obj[this.field] = this.checked;
            var event = new CustomEvent('checkbox-change', {bubbles: true, composed: true, detail: {row: this.obj}});
            this.dispatchEvent(event);
        }
    }

    _getCssClass() {
        return "fe-checkbox center";
    }
}

window.customElements.define('dr-zp-grid-checkbox-renderer', GridCheckboxRenderer);