import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { dom } from '../../../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import "../../../../../../vendor/lodash/lodash.js";
import '../../../../../core/form/dr-zp-input.js';
import '../../../../../core/misc/dr-zp-icon.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';

class GridTextRenderer extends BaseComponent {
    static get template() {
        return html`
            <style>
                .background-transparent {background-color: transparent !important}    
                a{color:#40a9b9;font-weight:600;text-decoration:none;cursor: pointer;}
                a:hover,a:focus{color:#2c7580;text-decoration:none;}
                a:focus{outline:thin dotted;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
                .full-size {
                    height: 100%;
                    width: 100%;
                }
                .pointered {
                    cursor: pointer;
                }
                .padding-sides-10 {
                    padding-left: 10px;
                    padding-right: 10px;
                }
                .trunc {
                    overflow: hidden;
                    text-overflow: ellipsis;
                    text-align: right;
                    height: 100%;
                    display: block;
                    word-break: break-word;
                }
                .no-events {
                    pointer-events: none;
                }
                #root {
                    user-select: text;
                }
                .icon {
                    position: relative;
                    top: 2px;
                    margin-right: 4px;
                }
                .inner-text {
                }
            </style>

            <div id="root" class$="{{calcTooltipClass(showTooltip, obj)}}">
                <div class$="[[addSelector()]]" title="[[display]]" style="width: [[width]]px">

                    <template is="dom-if" if="[[hasIconMap(col)]]">
                        <span style$="[[generateIconStyle(obj)]]">
                            <dr-zp-icon class="icon" type="[[getIcon(obj)]]" size="md"></dr-zp-icon>
                        </style>
                    </template>  

                    <template is="dom-if" if="[[editable]]">
                        <dr-zp-input id="input" value={{display}}></dr-zp-input>
                    </template>
                    <template is="dom-if" if="[[!editable]]">
                        <span class="inner-text">[[display]]</span>
                    </template>                        
                </div>
            </div>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            field: {
                type: String
            },
            details: {
                type: Boolean,
                value: false
            },
            editable: {
                type: Boolean,
                value: false
            },
            action: {
                type: String
            },
            display: {
                type: String
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();

        this.$.root.addEventListener("dom-change", (event) => {
            var input = dom(event.currentTarget).querySelector("#input");

            if (input != undefined) {
                input.addEventListener('input-changed', (e) => {
                    var val = e.detail.value;
                    this.obj[this.field] = val;
                    var event = new CustomEvent('data-change', { bubbles: true, composed: true, detail: { row: this.obj, action: this.action, column: this.field } });
                    this.dispatchEvent(event);
                });
            }
        });
    }

    calcTooltipClass(showTooltip, obj) {
        return "full-size";
    }

    colObserved() {
        if (this.col !== undefined && this.col.width !== undefined) {
            if (this.details) {
                this.width = "100%";
            }
            else {
                this.width = this.col.width;
                if (this.obj !== undefined && this.field !== undefined) {
                    this.objObserved();
                }
            }
        }
    }

    objObserved() {
        if (this.obj !== undefined) {
            const fieldValue = _.get(this.obj, this.field);
            this.display = fieldValue;

            if (this.obj.val !== undefined) {
                this.display = this.obj.val;
            }
            else if (this.col !== undefined) {
                if (this.col.type === 'date') {
                    var ms = this.obj[this.field];
                    this.display = new Date(ms).toUTCString();
                }
                else {
                    this.display = this.obj[this.field];
                }

                if (this.col.editable) {
                    this.editable = true;
                }
            }
        }
    }

    generateIconStyle(obj) {
        if (this.col && this.col.iconMap) {
            var val = obj !== undefined && obj[this.col.iconField];

            if (val) {
                var icon = this.col.iconMap[val];

                if (icon && icon.color) {
                    return "color: " + icon.color;
                }
            }
        }
    }

    getIcon(obj) {
        if (this.col && this.col.iconMap && this.col.iconField) {
            var val = obj !== undefined && obj[this.col.iconField];

            if (val) {
                var iconKey = this.col.iconMap[val];

                if (iconKey && iconKey.icon) {
                    return iconKey.icon;
                }
            }
        }
    }

    hasIconMap(col) {
        return col && col.iconMap != null;
    }

    addSelector() {
        if (this.col !== undefined) {
            return "trunc full-size ".concat(this.col.key);
        } else {
            return "trunc full-size";
        }
    }
}

window.customElements.define('dr-zp-grid-number-renderer', GridTextRenderer);
