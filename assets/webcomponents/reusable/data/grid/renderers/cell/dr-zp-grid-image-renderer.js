import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';

class GridColorRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                :host {padding-left: 0px;}
                .color-chip-wrap {
                    display: block;
                    padding: 5px;
                }
                .color-chip {
                    display: block;
                    width: 2em;
                    height: 2em;
                }
            </style>
            
            <template is="dom-if" if="true">
                <div class="img-wrap">
                    <img src="[[display]]" style="[[styleStr]]" title="[[title]]">
                </div>
            </template>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            field: {
                type: String
            },
            display: {
                type: String
            },
            styleStr: {
                type: String
            },
            title: {
                type: String
            }
        }
    }

    objObserved() {
        if (this.obj !== undefined) {
            const fieldValue = this.obj[this.field];
            this.display = fieldValue.src;
            this.title = fieldValue.caption;
            this.styleStr = "";

            if(fieldValue.width !== undefined) {
                this.styleStr += "width: " + fieldValue.width;  
            }
            if(fieldValue.height !== undefined) {
                this.styleStr += "height :" + fieldValue.height;  
            }
        }
    }
}

window.customElements.define('dr-zp-grid-image-renderer', GridColorRenderer);