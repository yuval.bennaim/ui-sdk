import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/form/dr-zp-button.js';

class GridLinkRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                #root {
                    padding: 5px;
                    user-select: none;
                }
            </style>

            <div id="root">
                <dr-zp-button type="primary" size="small" label="[[action]]" on-click="clickAction" full-width="true"></dr-zp-button>                      
            </div>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            action: {
                type: String
            },
            field: {
                type: String,
                value: 'enabled'
            }
        }
    }    

    connectedCallback() {
        super.connectedCallback();
    }

    colObserved() {
        if(this.col != undefined) {
            this.action = this.col.action;
        }
    }

    clickAction(e) {
        console.info('clickAction...');
        var event = new CustomEvent('row-button-click', {bubbles: true, composed: true, detail: {row: this.obj, action: this.action, column: this.field}});
        this.dispatchEvent(event);
    }
}

window.customElements.define('dr-zp-grid-button-renderer', GridLinkRenderer);

    