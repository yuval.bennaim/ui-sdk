import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/misc/dr-zp-icon.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';
import '../../../../../core/form/dr-zp-input.js';

class GridIconRenderer extends BaseComponent {
    static get template() {
        return html`
            <style>
                #root {
                    padding: 5px;
                    user-select: none;
                }
                .icon {
                    display: inline-block;
                }
                .inner-text {
                    padding-right: 15px;
                }
            </style>
            
            <div id="root">         
                <template is="dom-repeat" items="[[items]]">
                    <dr-zp-icon type="[[item.icon]]" size="md" class="icon" title="[[item.icon]]"></dr-zp-icon>
                    <template is="dom-if" if="[[editable]]">
                        <dr-zp-input id="input" value={{item.text}}></dr-zp-input>
                    </template>
                    <template is="dom-if" if="[[!editable]]">
                        <span class="inner-text">[[item.text]]</span>
                    </template>
                </template>
            </div>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            items: {
                type: Array
            },
            field: {
                type: String,
                value: 'systems'
            },
            editable: {
                type: Boolean,
                value: false
            }
        }
    }

    colObserved() {
        if (this.col != undefined && this.col.width != undefined) {
            this.width = this.col.width;

            if (this.obj !== undefined && this.field !== undefined) {
                this.objObserved();
            }
        }
        if (this.obj !== undefined && this.col !== undefined && this.col.editable) {
            this.editable = true;
        }
    }

    objObserved() {
        if (this.obj !== undefined) {
            var val = this.obj[this.col.key];

            if (typeof val === 'string') {
                this.items = [val];
            }
            else {
                this.items = val;
            }
        }
    }
}

window.customElements.define('dr-zp-grid-icon-text-renderer', GridIconRenderer);