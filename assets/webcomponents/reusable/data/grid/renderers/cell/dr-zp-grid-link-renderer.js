import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';

class GridLinkRenderer extends BaseComponent {
    
    static get template() {
        const superTemp = super.template;

        var template = html`
            ${superTemp}

            <style>
                .dr-zp-btn--link {
                    height: 22px !important;
                }
                #root {
                    user-select: none;
                }
                .trunc {
                    position: relative;
                    top: -4px;
                    text-align: left;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    height: 100%;
                    display: inline-block;
                    word-break: break-word;
                    padding: 0;
                }
                .full-size {
                    height: 100%;
                    width: 100%;
                }
                .pointered {
                    cursor: pointer;
                }
            </style>

            <div id="root" class$="{{calcTooltipClass(showTooltip, obj)}}">
                <div class$="[[addSelector()]]" title="[[display]]" style="width: [[width]]px">
                    <button class="trunc full-width dr-zp-btn btn-dropdown dr-zp-btn--link" on-click="clickAction">
                        [[display]]
                        <span id="tabber" title="open [[url]] in new Tab" class="material-icons" style="top: 2px; margin-left: 5px; position: relative;">open_in_new</span>
                    </button>
                </div>                         
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            field: {
                type: String
            },
            details: {
                type: Boolean,
                value: false
            },
            editable: {
                type: Boolean,
                value: false
            },
            action: {
                type: String
            },
            display: {
                type: String
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    calcTooltipClass(showTooltip, obj) {
        var cls = "full-size";

        if (this.link) {
            cls += " pointered";
        }

        return cls;
    }

    colObserved() {
        if (this.col != undefined && this.col.width != undefined) {
            if (this.details) {
                this.width = "100%";
            }
            else {
                this.width = this.col.width;

                if (this.obj !== undefined && this.field !== undefined) {
                    this.objObserved();
                }
            }
        }
    }

    objObserved() {
        if (this.obj !== undefined) {
            this.display = this.obj[this.field];

            if(this.display && this.display.caption) {
                this.display = this.display.caption;
            }

            if(this.obj[this.field].url) {
                this.url = this.obj[this.field].url;
            }
        }
    }

    addSelector() {
        return "full-size " + (this.col && this.col.key !== undefined ? this.col.key : "");
    }

    clickAction(e) {
        if(e.target.id == "tabber") {
            let target = "_new";

            if(this.obj[this.field].target != null) {
                target = this.obj[this.field].target;
            }

            window.open(this.url, target);
        }
        else { //dispatch selection event
            this.dispatchEvent(new CustomEvent('link-action', {
                bubbles: true,
                composed: true,
                detail: {
                    row: this.obj
                }
            }));
        }
    }
}

window.customElements.define('dr-zp-grid-link-renderer', GridLinkRenderer);

