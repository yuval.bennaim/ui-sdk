import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/misc/dr-zp-icon.js';

class GridArrayRenderer extends BaseComponent {
    static get template() {
        return html`
            <style>
                :host {
                    height: 100%;
                    width: 100%;
                    display: block;
                    cursor: pointer;
                } 
                #root {
                    user-select: none;
                }
                .collapsed-label {
                    position: relative;
                    left: 0px;
                    top: -3px;
                }
            </style>

            <template is="dom-if" if="true">
                <div id="root" on-click="toggleList">
                    <template is="dom-if" if="[[!isExpanded(col, obj)]]">
                        <dr-zp-icon type="chevron-right" size="sm" class="toggle-icon" title="[[getString('sdk-array-render-exapnd', locale)]]"></dr-zp-icon>
                        <span class="collapsed-label">[[showStr]]</span>
                    </template>
                    <template is="dom-if" if="[[isExpanded(col, obj)]]">
                        <dr-zp-icon type="chevron-down" size="sm" class="toggle-icon"></dr-zp-icon>                        
                        <div class="list">
                            <template is="dom-repeat" items="[[items]]">
                                <div class="list-item">
                                    [[item]]
                                </div>
                            </template>
                        </div>
                    </template>
                </div>
            </template>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            items: {
                type: Array
            },
            disable: {
                type: Boolean,
                value: false
            },
            field: {
                type: String,
                value: 'paths'
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    isExpanded(col, obj) {
        if (col !== undefined && obj !== undefined && (col.expanded || obj.expanded)) { //expanded or collapsed
            return true
        }

        return false;
    }

    toggleList(e) {
        this.obj.expanded = !this.obj.expanded;
        this.obj = this.utils._helper_copy(this.obj);

        if(!this.obj.expanded) {
            this.dispatchEvent(new CustomEvent('reset-row-heights', {
                bubbles: true,
                composed: true,
                detail: {}
            }));
        }
    }

    colObserved() {
        if (this.col != undefined && this.col.width != undefined) {
            this.width = this.col.width;

            if (this.obj !== undefined && this.field !== undefined) {
                this.objObserved();
            }
        }
    }

    objObserved() {
        if (this.obj !== undefined) {
            if (this.obj.val !== undefined) {
                this.items = this.obj.val;
            }
            else {
                this.items = this.obj[this.col.key];
            }

            this.showStr = this.getString('sdk-collapsed-label', this.locale, this.items.length);
        }
    }
}

window.customElements.define('dr-zp-grid-array-renderer', GridArrayRenderer);