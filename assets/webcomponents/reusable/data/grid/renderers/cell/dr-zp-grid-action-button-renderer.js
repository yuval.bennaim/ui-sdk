import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';

class GridActionButtonRenderer extends BaseComponent {
    static get template() {

        var template = html`

            <style>
                :host {
                    height: 100%;
                    width: 100%;
                    display: block;
                } 
                
                
            </style>

            <div class="content-wrapper">
            <slot></slot>
            </div>
        `;

        return template;
    }

}

window.customElements.define('dr-zp-grid-action-button-renderer', GridActionButtonRenderer);