import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import {dom} from '../../../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';

class GridBooleanRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                :host {
                    height: 100%;
                    width: 100%;
                    display: block;
                }     
                #root {
                    user-select: none;
                }       
            </style>
            
            <template is="dom-if" if="true">
                <div id="root">[[label]]</div>
            </template>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            field: {
                type: String
            },
            state: {
                type: Boolean,
            },
            falseLabel: {
                type: String,
                value: 'false'
            },
            trueLabel: {
                type: String,
                value: 'true'
            },
        }
    }

    colObserved() {
        if(this.col != undefined && this.col.width != undefined) {
            this.width = this.col.width;

            if(this.obj !== undefined && this.field !== undefined) {
                this.objObserved();
            }
        }
    }

    objObserved() {
        if(this.obj !== undefined && this.col != null) {
            if(this.obj.val !== undefined) {
                this.state = this.obj.val;  
            }
            else {
                this.state = this.obj[this.field];
            }

            if(this.col.trueLabel !== undefined) {
                this.trueLabel = this.col.trueLabel;
            }

            if(this.col.falseLabel !== undefined) {
                this.falseLabel = this.col.falseLabel ;
            }

            this.label = this.state ? this.trueLabel : this.falseLabel;
        }
    }
}

window.customElements.define('dr-zp-grid-boolean-renderer', GridBooleanRenderer);