import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/misc/dr-zp-toggle.js';

class GridToggleRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                :host {
                    height: auto;
                    display: block;
                }
                toggle {
                    width: 100px;
                }
            </style>
            <div class="toggle">
                <dr-zp-toggle id="toggle" onlabel="Hello" offlabel="Goodbye" checked="{{checked}}"></dr-zp-toggle>
            </div>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            field: {
                type: String
            },
            disabled: {
                type: Boolean,
                value: true
            },
            checked: {
                type: Boolean,
            },
            off: {
                type: String
            },
            on: {
                type: String
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();

        this.$.toggle.addEventListener('dr-zp-toggle-clicked', e => {
            var state = e.detail.checked;
            this.obj[this.field] = state;
            var event = new CustomEvent('cell-change', {bubbles: true, composed: true, detail: {row: this.obj, state: this.state}});
            this.dispatchEvent(event);
        });
    }

    colObserved() {
        if(this.col != undefined) {
            this.on = this.col.on;
            this.off = this.col.off;

            if(this.col["disabled"] !== undefined) {
                this.disabled = this.col["disabled"];
            }
        }
    }

    objObserved() {
        if(this.obj !== undefined) {
            if(this.obj.val !== undefined) {
                this.checked = this.obj.val;  
            }
            else {
                this.checked = this.obj[this.field];
            }
        }
    }
}

window.customElements.define('dr-zp-grid-toggle-renderer', GridToggleRenderer);