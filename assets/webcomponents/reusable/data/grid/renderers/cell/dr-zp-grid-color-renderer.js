import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';

class GridColorRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                :host {padding-left: 0px;}
                .color-chip-wrap {
                    display: block;
                    padding: 5px;
                }
                .color-chip {
                    display: block;
                    width: 2em;
                    height: 2em;
                }
            </style>
            
            <template is="dom-if" if="true">
                <div class="color-chip-wrap">
                    <span class="color-chip" style$="[[getColor(obj, field)]]">&nbsp;</span>
                </div>
            </template>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object
            },
            field: {
                type: String
            }
        }
    }

    getColor(obj, field) {
        var style;

        if(obj !== undefined) {
            if(obj.val !== undefined) {
                style = "background-color: " + obj.val;  
            }
            else {
                var clr = obj[field];

                if(!clr.startsWith('#')) {
                    clr = '#' + clr;
                }

                style = "background-color: " + clr;  
            }
        }
        else {
            style = "background-color: grey";
        }

        return style;
    }
}

window.customElements.define('dr-zp-grid-color-renderer', GridColorRenderer);