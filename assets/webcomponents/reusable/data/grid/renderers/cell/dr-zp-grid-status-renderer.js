import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';

class GridStatusRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                :host {padding-left: 0px;}
                .color-chip-wrap {
                    display: block;
                    padding: 5px;
                }
                .color-chip {
                    display: inline-block;
                    width: 12px;
                    height: 12px;
                    border-radius: 50%;
                    margin-right: 10px;
                }
            </style>
            
            <template is="dom-if" if="true">
                <div class="color-chip-wrap">
                    <span class="color-chip" style$="[[getColor(obj, field)]]">&nbsp;</span>
                    [[status]]
                </div>
            </template>
        `;
    }

    static get properties() {
        return {
            col: {
                type: Object
            },
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            field: {
                type: String
            }
        }
    }

    objObserved() {
        if(this.obj !== undefined && this.col != null) {
            this.status = this.obj[this.field];
        }
    }

    getColor(obj, field) {
        this.status = obj ? obj[field] : '';
        var style = "";
        
        if(this.col !== undefined && this.status) {
            var st = this.col.iconMap[this.status];

            if(st) {
                style = "background-color: " + st.color;
            }
        }
        
        return style;
    }
}

window.customElements.define('dr-zp-grid-status-renderer', GridStatusRenderer);