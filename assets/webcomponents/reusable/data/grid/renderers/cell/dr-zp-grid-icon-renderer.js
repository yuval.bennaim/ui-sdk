import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/misc/dr-zp-icon.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';

class GridIconRenderer extends BaseComponent {
  static get template() {
        return html`
            <style>
                #root {
                    user-select: none;
                }
                .icon {
                    display: inline-block;
                }
            </style>
            
            <div id="root">         
                <template is="dom-repeat" items="[[items]]">
                    <dr-zp-icon type="[[item]]" size="md" class="icon" title="[[item]]"></dr-zp-icon>  
                </template>
            </div>
        `;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            items: {
                type: Array
            },
            field: {
                type: String,
                value: 'systems'
            }
        }
    }

    colObserved() {
        if(this.col != undefined && this.col.width != undefined) {
            this.width = this.col.width;

            if(this.obj !== undefined && this.field !== undefined) {
                this.objObserved();
            }
        }
    }
    
    objObserved() {
        if(this.obj !== undefined) {
            var val = this.obj[this.col.key];

            if(typeof val === 'string') {
                this.items = [val];  
            }
            else {
                this.items = val;
            }
        }        
    }
}

window.customElements.define('dr-zp-grid-icon-renderer', GridIconRenderer);