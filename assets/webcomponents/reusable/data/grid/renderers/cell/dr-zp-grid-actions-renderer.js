import { html } from '../../../../../../vendor/@polymer/polymer/polymer-element.js'
import { BaseComponent } from '../../../../../core/dr-zp-base-component.js';
import '../../../../../core/form/dr-zp-dropdown.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import '../../../../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';

class GridActionsRenderer extends BaseComponent {
    static get template() {
        const superTemp = super.template;

        var template = html`
            ${superTemp}

            <style>
                :host {
                    height: 100%;
                    width: 100%;
                    display: block;
                }   
                .floating-list {
                    position: fixed;
                    display: none;     
                    z-index: 6000;  
                    box-shadow: -5px 5px 10px var(--color-drop-shadow); 
                }
            </style>

            <div class="centered">
                <button class="dr-zp-btn--link" on-click="openDropdown">
                    <dr-zp-icon type="ellipsis" size="md"></dr-zp-icon>
                </button>

                <div id="list" class="floating-list" on-mouseleave="closeDropdown">
                    <dr-zp-list id="list" items="[[items]]" field="caption" selectable="[[selectable]]"></dr-zp-list>
                </div>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            obj: {
                type: Object,
                observer: 'objObserved'
            },
            col: {
                type: Object,
                observer: 'colObserved'
            },
            items: {
                type: Array
            },
            disable: {
                type: Boolean,
                value: false
            },
            selectable: {
                type: Boolean,
                value: false
            },
            field: {
                type: String
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();

        this.$.list.addEventListener('list-change', e => {
            var action = e.detail.selected.caption;
            var url = e.detail.selected.url;
            var event = new CustomEvent('list-action', { bubbles: true, composed: true, detail: { row: this.obj, action: e.detail.selected } });
            this.dispatchEvent(event);
        });

        document.addEventListener('escape-action', this.escapeActionHandler.bind(this));
    }

    openDropdown(e) {
        let elem = e.currentTarget;
        let list = this.$.list;
        let wid = window.innerWidth;
        let rect = elem.getBoundingClientRect();

        if (this.col.dock === 'left') {
            list.style.top = rect.top + "px";
            list.style.left = rect.left + "px";
        }
        else if (this.col.dock === 'right') {
            list.style.top = rect.top + "px";
            list.style.right = (wid - rect.right) + "px";
        }
        else {
            list.style.top = rect.top + "px";
            list.style.left = rect.left + "px";
        }

        list.style.display = "block";
    }

    escapeActionHandler(e) {
        this.closeDropdown(e);
    }

    closeDropdown() {
        let list = this.$.list;
        list.style.display = "none";
    }

    colObserved() {
        if (this.col != undefined && this.col.options != undefined) {
            this.items = this.col.options;
        }
    }

    objObserved() {
        if (this.obj !== undefined) {
            var actions = this.obj.actions;

            if (actions != undefined) {
                if (actions.length == 0) {
                    this.set('disable', true);
                }

                this.items = actions;
            }
        }
    }
}

window.customElements.define('dr-zp-grid-actions-renderer', GridActionsRenderer);