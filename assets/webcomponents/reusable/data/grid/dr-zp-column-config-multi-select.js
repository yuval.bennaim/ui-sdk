import { html } from '../../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../core/dr-zp-base-component.js';
import '../../../core/form/dr-zp-checkbox.js';
import '../../../core/form/dr-zp-label.js';
import '../../../core/form/dr-zp-button.js';
import '../../../core/misc/dr-zp-material-icon.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
        ${superTemp}

        <style>
            .dropdown {
                z-index: 2200;
            }
            .config-scroller {
                max-height: 300px;
                overflow-y: auto;
            }
            .multi-select-li {
                display:block;
            }
            .multi-select-li:hover {
                background-color: var(--color-action-secondary--flip) !important
            }
            .multi-select-checkbox {
                display: inline-block;
                padding: 2px 6px 6px 6px;
            }
            .multi-select-checkbox:hover {
                background-color: none !important
            }
        </style>

        <div class$="{{computedClass(opened)}}" on-mouseleave="closeDropdown">
            <button class="dr-zp-btn--link" on-click="_clickHandler" disabled="{{disabled}}">
               <dr-zp-material-icon type="settings"></dr-zp-material-icon>
            </button>

            <div class="dropdown-list dropdown-list-icon no-scroll" style="overflow-x: hidden;">
                <div class="config-header" style="padding:5px; font-weight: bold">
                    [[getString('sdk-column-config', locale)]]
                </div>

                <ul class="config-scroller">
                    <template is="dom-repeat" items="[[items]]">
                        <li class="multi-select-li" draggable="[[allowDrag]]" on-dragstart="drag" on-drop="drop" on-dragover="allowDrop">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 100%">
                                        <dr-zp-checkbox class="multi-select-checkbox" id="cb_[[index]]" multiselect label="[[getDataValue(item, field, locale)]]" checked="[[getCheckedState(item, fieldChecked)]]"></dr-zp-checkbox>
                                    </td>

                                    <td style="text-align: right; padding: 0 5px 0 0;">
                                        <template is="dom-if" if="[[item.dock]]">
                                            <button id="dock_[[index]]" class="link multi-select-dock" on-click="_undockClickHandler" title="Undock">U</button>
                                        </template>

                                        <template is="dom-if" if="[[!item.dock]]">
                                            <button id="dock_[[index]]" class="link multi-select-dock" on-click="_dockLeftClickHandler" title="Dock Left">L</button>
                                            <span>&nbsp;</span>
                                            <button id="dock_[[index]]" class="link multi-select-dock" on-click="_dockRightClickHandler" title="Dock Right">R</button>
                                        </template>
                                    </td>
                                </tr>
                            </table>
                        </li>                
                    </template>
                </ul> 
                   
                <ul class="config-non-scroller">
                    <template is="dom-repeat" items="[[actions]]" as="action">
                        <li>
                            <dr-zp-button id="[[action.key]]" type="link" class="action-link full-width" size="small" label="[[action.label]]"></dr-zp-button>
                        </li>                   
                    </template>
                </ul>   
            </div>
        </div>`;

        return template;
    }

    static get properties() {
        return {
            label: {
                type: String,
                value: 'Select'
            },
            dropdown: {
                type: String
            },
            items: {
                type: Array,
                notify: true
            },
            actions: {
                type: Array,
                value: []
            },
            field: {
                type: String,
                value: 'name'
            },
            fieldChecked: {
                type: String,
                value: 'selected'
            },
            opened: {
                type: Boolean,
                value: false
            },
            icon: {
                type: String
            },
            disabled: {
                type: Boolean,
                value: false
            },
            fullWidth: {
                type: Boolean,
                value: false
            },
            allowDrag: {
                type: Boolean,
                value: true
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
        document.addEventListener('escape-action', this.escapeActionHandler.bind(this));
        this.setupEvents();
    }

    setupEvents() {
        var cbs = dom(this.root).querySelectorAll('dr-zp-checkbox');
        var buttons = dom(this.root).querySelectorAll('dr-zp-button');

        if (cbs.length === 0) {
            setTimeout(() => {
                this.setupEvents();
            });
        }
        else {
            cbs.forEach((cb) => {
                cb.addEventListener('checkbox-clicked', e => {
                    var checked = e.detail.checked;
                    var index = e.currentTarget.id.split('_')[1];
                    this.items[index][this.fieldChecked] = checked;
                    this.items = this.utils._helper_copy(this.items);

                    this.dispatchEvent(new CustomEvent('multi-select-change', {
                        detail: {
                            items: this.items
                        }
                    }));
                });
            });

            buttons.forEach((button) => {
                button.addEventListener('dr-zp-button-click', e => {
                    var source = e.detail.source;
                    var action = source.id;

                    this.dispatchEvent(new CustomEvent('multi-select-action', {
                        detail: {
                            action: action
                        }
                    }));
                });
            });
        }
    }

    escapeActionHandler(e) {
        if (this.opened) {
            this.set('opened', false);
        }
    }

    _clickHandler(e) {
        this.set('opened', !this.opened);
    }

    _dockLeftClickHandler(e) {
        console.info('_dockClickHandler', e);
        var index = e.currentTarget.id.split('_')[1];
        this.items[index].dock = 'left';
        this.items = this.utils._helper_copy(this.items);

        this.dispatchEvent(new CustomEvent('multi-select-change', {
            detail: {
                items: this.items
            }
        }));
    }

    _dockRightClickHandler(e) {
        console.info('_dockRightClickHandler', e);
        var index = e.currentTarget.id.split('_')[1];
        this.items[index].dock = 'right';
        this.items = this.utils._helper_copy(this.items);

        this.dispatchEvent(new CustomEvent('multi-select-change', {
            detail: {
                items: this.items
            }
        }));
    }

    _undockClickHandler(e) {
        console.info('_undockClickHandler', e);
        var index = e.currentTarget.id.split('_')[1];
        var item = this.items[index]
        delete this.items[index].dock;
        this.items = this.utils._helper_copy(this.items);

        this.dispatchEvent(new CustomEvent('multi-select-change', {
            detail: {
                items: this.items
            }
        }));
    }

    computedClass(opened) {
        var cls = "dropdown";

        if (this.fullWidth) {
            cls += " full-width";
        }

        if (opened) {
            cls += " open";
        }

        return cls;
    }

    getCheckedState(item, fieldChecked) {
        return item[this.fieldChecked];
    }

    getDataValue(item, field, locale) {
        if (item != null) {
            var display = item;

            if (this.field != null) {
                display = item[this.field];

                if (display.startsWith('%')) {
                    var key = display.substring(1);
                    display = this.getString(key, this.locale);
                }
            }

            return display;
        }
    }

    closeDropdown(e) {
        if (this.opened && e.relatedTarget != undefined) {
            var parent = e.relatedTarget.parentElement;

            if (parent === null || parent.className.indexOf('dropdown-list') === -1 && parent.className.indexOf('dropdown') === -1) {
                this.set('opened', false);
            }
        }
    }

    allowDrop(e) {
        if (this.allowDrag) {
            e.preventDefault();
        }
        else {
            return false;
        }
    }

    drag(e) {
        if (this.allowDrag) {
            var item = e.model.__data.item;
            e.dataTransfer.setData("text", item.key);
        }
        else {
            return false;
        }
    }

    drop(e) {
        e.preventDefault();
        var elem = e.target || e.srcElement;
        var sourceId = e.dataTransfer.getData("text");
        var item = e.model.__data.item;
        var targetId = item.key;

        if (elem.id.indexOf("control") > -1) {
            targetId = null;
        }

        if (targetId !== sourceId) { //not dropping on same
            this.dispatchEvent(new CustomEvent('drag-complete', {
                detail: {
                    source: sourceId,
                    target: targetId
                }
            }));
        }
    }
}

window.customElements.define('dr-zp-column-config-multi-select', Comp);