import { BaseComponent } from './dr-zp-base-component.js';

export class ModuleBase extends BaseComponent {

    connectedCallback() {
        super.connectedCallback();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        console.info('disconnectedCallback...');
        document.removeEventListener('context-navigation-action', this.contextNavigationActionHandler);
    }

    async initResources(name) {
        let app = this.getAppInstance();
        if (app) {
            this.modules = app.modules;
        } else {
            //This is used while mocking the components
            this.modules = [{
                route: name,
                name: name,
                base_path: `./assets/modules-dev/${name}`
            }];
            this.app = app = { page: name, appVersion: '1.0.0-test' };
        }
        
        this.module = this.modules.find(module => module.name === name); //get my module
    }

    parseRoute(moduleName, exact) {
        var ix = top.location.hash.indexOf(moduleName);

        if (ix > -1) {
            var pathStr = top.location.hash.substring(ix);
            ix = pathStr.indexOf('/');
            var tokens = pathStr.split('/');
            var viewStr = "";
            
            for(var t = 1; t < tokens.length; t++) {
                viewStr += tokens[t];

                if(t < tokens.length-1) {
                    viewStr += "/";
                }
            }
            
            this.selectedNav = this.findPathInMenu(viewStr, exact);

            if (this.selectedNav == null) {
            }
            else {
                document.dispatchEvent(new CustomEvent("context-navigation-action", {
                    detail: {
                        module: { route: moduleName },
                        nav: this.selectedNav
                    }
                }));
            }
        }
    }

    hasModules(modules) {
        return this.modules != null && this.modules.length > 0;
    }

    contextNavigationActionHandler(e) {
        var active = this.isActive();

        if (active) { //if match on this module, handle internal navigation
            var module = e.detail.module;
            var nav = e.detail.nav;
            var rejected = false;
            var context = "";

            if(nav != null && nav.sub_route == undefined) {   
                this.selectedNav = this.findPathInMenu(nav);
            }
            else {
                this.selectedNav = nav;
            }

            console.info('context-navigation-action', active, module, this.selectedNav);

            if (this.selectedNav == null || this.selectedNav.webcomponent_name == null) {
                this.wcStr = '<h3 class="error-message">No view found for: ' + nav + '</h3>';
            }
            else { // check if the route is permitted on this site
                if (this.module.routes_restrict_include != undefined) {
                    if (this.module.routes_restrict_include.indexOf(this.selectedNav.sub_route) === -1) {
                        rejected = true;
                    }
                }

                if (this.module.routes_restrict_exclude != undefined) {
                    if (this.module.routes_restrict_exclude.indexOf(this.selectedNav.sub_route) !== -1) {
                        rejected = true;
                    }
                }

                if (rejected) {
                    this.wcStr = '<h3 class="error-message">The route [' + this.selectedNav.sub_route + '] is not permitted on this instance of [' + this.module.display_name + ']</h3>';
                }
                else {
                    this.wcStr = '<' + this.selectedNav.webcomponent_name + ' context="' + context + '"></' + this.selectedNav.webcomponent_name + '>';
                }
            }
        }

        return active;
    }


    isActive() {
        var active = (this.module.route === this.app.page);
        return active;
    }

    hasModuleIcon(module) {
        return module.icon !== undefined;
    }

    getModuleDispayName(module, locale) {
        var str = '';

        if (module != null) {
            str = this.getString(module.display_name, this.locale);
        }

        return str;
    }

    getModuleIcon(module, svgClass, pngClass) {
        var str = "";

        if (module.icon !== undefined) { //use the icon specified in the manifest
            if (module.icon.indexOf('.png') > -1) {
                if (module.base_path == null) {
                    str = '<dr-zp-image src="' + module.icon + '" styler="' + pngClass + '"></dr-zp-image>';
                }
                else {
                    str = '<dr-zp-image src="' + module.base_path + "/" + module.icon + '" styler="' + pngClass + '"></dr-zp-image>';
                }
            }
            else {
                str = '<dr-zp-icon type="' + module.icon + '" styler="' + svgClass + '"></dr-zp-icon>';
            }
        }
        return str;
    }

    findPathInMenu(path, exact = false) {
        console.info('findPathInMenu', path, exact);

        if (this.menu) {
            for (var m = 0; m < this.menu.navigation.length; m++) {
                var menu = this.menu.navigation[m];

                if (menu.menu_group && menu.navigation != null) {
                    for (var i = 0; i < menu.navigation.length; i++) {
                        var menuItem = menu.navigation[i];

                        if (exact && path === menuItem.sub_route) {
                            return menuItem;
                        }
                        else if(!exact && path.indexOf(menuItem.sub_route) > -1) {
                            return menuItem;
                        }
                    }
                }
                else {

                    if (exact && menu.sub_route === path) {
                        return menu;
                    }
                    else if(!exact && path.indexOf(menu.sub_route) > -1) {
                        return menu;
                    }
                }
            }
        }

        return null;
    }

    parseSubRoute(sub_route) {
        this.selectedNav = this.findPathInMenu(sub_route);
        var url = top.location.origin + '/#/' + this.module.route + '/' + this.selectedNav.sub_route;
        history.pushState(url, url, url);
        
        document.dispatchEvent(new CustomEvent("context-navigation-action", {
            detail: {
                module: this.module,
                nav: this.selectedNav
            }
        }));
    }

    checkIfRouteRestrictions(nav) {
        console.info('checkIfRouteRestrictions', nav);
        let ret = true;
        let viewName = nav;

        if(nav.sub_route) {
            viewName = nav.sub_route;
        }

        let override = localStorage.getItem("feature_override");

        if(override) {
            let overRideObj = JSON.parse(override);

            if(overRideObj[viewName]) { //feature flag override exists
                ret = true;
            }
        }
        else if (this.module.routes_restrict_exclude && (this.module.routes_restrict_exclude.indexOf(viewName) > -1 )) {
            ret = false;
        }

        return ret;
    }

    checkRouteRestrictions() { // filter menu for route restrictions
        let override = (localStorage.getItem("feature_override") != null) ? JSON.parse(localStorage.getItem("feature_override")) : null;

        for (var m = 0; m < this.menu.navigation.length; m++) {
            var menu = this.menu.navigation[m];

            if (menu.menu_group) {
                for (var i = 0; i < menu.navigation.length; i++) {
                    var menuItem = menu.navigation[i];
                    let viewName = menuItem.sub_route;

                    if(override && override[viewName]) {
                        console.info('feture flag override for', viewName);
                    }
                    else {
                        if (this.module.routes_restrict_exclude && this.module.routes_restrict_exclude.indexOf(menuItem.sub_route) > -1) {
                            menuItem.hidden = true;
                        }

                        if (this.module.routes_restrict_include && this.module.routes_restrict_include.indexOf(menuItem.sub_route) === -1) {
                            menuItem.hidden = true;
                        }
                    }
                }
            }
            else {
                if (this.module.routes_restrict_exclude && this.module.routes_restrict_exclude.indexOf(menu.sub_route) > -1) {
                    menu.hidden = true;
                }

                if (this.module.routes_restrict_include && this.module.routes_restrict_include.indexOf(menu.sub_route) === -1) {
                    menu.hidden = true;
                }
            }
        }
    }
}