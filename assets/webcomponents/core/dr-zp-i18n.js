import { Utils } from './dr-zp-utils.js';

let instance = null;

/* singleton */

export class I18n {
    constructor() {
        if (!instance) {
            instance = this;
            this.utils = new Utils();
            this.defaultLocale = 'English'; //default
            this.locale = this.defaultLocale;
            this.missings = [];
        }

        return instance;
    }

    addBundle(bundle) {
        if (this.resources == null) {
            this.resources = bundle;
        }
        else {
            for (var localeKey in bundle) {
                var locale = this.resources[localeKey]; //only allow existing locales

                if (locale !== undefined) {
                    var localeObj = bundle[localeKey];

                    for (var key in localeObj) {
                        var val = this.resources[localeKey][key];  //check if alreay exists

                        if (val === undefined) {
                            this.resources[localeKey][key] = localeObj[key]; //add it to the respective locale
                        }
                        else {
                            //console.info("Rejecting duplicate entry for", locale, key);
                        }
                    }
                }
                else {
                    //console.info("Rejecting unsupported locale", locale,);
                }
            }
        }

        document.dispatchEvent(new CustomEvent("locale-changed", {
            detail: { "locale": this.locale }
        }));
    }

    getLocalizedBundle() {
        let bundle = this.resources[this.locale];
        return bundle;
    }

    setLocale(locale) {
        console.info("setLocale", locale);

        if (locale == null) {
            locale = this.defaultLocale;
        }

        this.locale = locale;

        document.dispatchEvent(new CustomEvent("locale-changed", {
            detail: {
                "locale": this.locale,
                "bundle": this.getLocalizedBundle()
            }
        }));
    }

    getCurrentLocale() {
        return this.locale;
    }

    getLocales() {
        var arr = [];

        for (var key in this.resources) {
            arr.push(key);
        }

        return arr;
    }

    parseString(str, values) {
        var index = 0;

        str = str.replace(/\{\d+\}/g, (substr) => {
            return values[index++];
        });

        return str;
    }

    logAllMissingResources() {
        console.info("%clogAllMissingResources", "color: blue;", this.missings);

        /**/
    }
    pseudoTranslateString(stringValueString) {
        let currchar = '';
        let DNTCharFound = 0;
        let DNTCharCounter = 0;
        let expansionFactor = 0;
        let HTML_entity_string = '';
        let i = 0;
        let inMarkupFlag = 0;
        let inPlaceholderFlag = 0;
        let markupCharCounter = 0;
        let markupCloseFound = 0;
        let markupOpenFound = 0;
        let nextchar = '';
        let newPseudoStrLength = 0;
        let pseudoStrLength = 0;
        let pseudoedString = '';
        let pseudoedLengthRemaining;
        let prevchar = '';
        let pseudoCharValue = '';
        let re = {
            search: (exp, str) => {
                let regex = new RegExp(exp, 'g');
                return str.search(regex);
            },
            sub: (search, replacement, target) => {
                return target.replace(new RegExp(search, 'g'), replacement);
            },
            match: (regex, paragraph) => {
                return paragraph.match(regex);
            }
        }
        let pseudo_additional_char = '𠮯';
        let unicode = (char) => {
            return {
                translate: (encodingWidth) => {
                    if (encodingWidth === 'wide') {
                        return char.replace(/[A-Za-z0-9]/g, (s) => { return String.fromCharCode(s.charCodeAt(0) + 0xFEE0); });
                    }
                    if (encodingWidth === 'ascii') {
                        return char.replace(/[Ａ-Ｚａ-ｚ０-９]/g, (s) => { return String.fromCharCode(s.charCodeAt(0) - 0xFEE0); });
                    }
                }
            };
        };
        // Iterate over string to calculate pseudoable string length
        while (i < stringValueString.length) {
            if (i + 1 >= stringValueString.length) {
                nextchar = '';
            } else {
                nextchar = stringValueString[i + 1];
            }
            if (i - 1 <= 0) {
                prevchar = '';
            } else {
                prevchar = stringValueString[i - 1];
            }

            currchar = stringValueString[i];
            if (currchar === '&' && nextchar === '#') {
                while (true) {
                    HTML_entity_string = HTML_entity_string + currchar;
                    i++;
                    currchar = stringValueString[i];
                    if (currchar === ';') {
                        HTML_entity_string = HTML_entity_string + currchar;
                        break;
                    }
                }
                i++;
                pseudoStrLength++;
                continue;
            }
            if (inPlaceholderFlag === 0) {
                if (currchar === '{' && nextchar === '{') {
                    console.log('char {')
                    inPlaceholderFlag = 1;
                    i++;
                    continue;
                }
            } else if (currchar === '}' && prevchar === '}') {
                console.log('char }')
                inPlaceholderFlag = 0;
                i++;
                continue;
            } else {
                markupCharCounter = markupCharCounter + 1;
                i++;
                continue;
            }
            if (inMarkupFlag === 0) {
                markupOpenFound = re.search('[<{]', currchar);
                if (markupOpenFound !== -1) {
                    inMarkupFlag = 1;
                } else {
                    DNTCharFound = re.search('[\\\\]', currchar);
                    if (DNTCharFound !== -1) {
                        i++;
                        DNTCharCounter = DNTCharCounter + 1;
                    } else {
                        pseudoStrLength++;
                    }
                }
            } else {
                markupCloseFound = re.search('[}>]', currchar);
                if (markupCloseFound !== -1) {
                    inMarkupFlag = 0;
                } else {
                    markupCharCounter++;
                }
            }
            i++;
        }
        if (pseudoStrLength <= 3) {
            expansionFactor = 4;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        } else if ((4 <= pseudoStrLength && pseudoStrLength <= 7)) {
            expansionFactor = 3;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        } else if ((8 <= pseudoStrLength && pseudoStrLength <= 10)) {
            expansionFactor = 2.5;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        } else if ((11 <= pseudoStrLength && pseudoStrLength <= 20)) {
            expansionFactor = 2;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        } else if ((21 <= pseudoStrLength && pseudoStrLength <= 30)) {
            expansionFactor = 1.8;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        } else if ((31 <= pseudoStrLength && pseudoStrLength <= 50)) {
            expansionFactor = 1.6;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        } else if ((51 <= pseudoStrLength && pseudoStrLength <= 70)) {
            expansionFactor = 1.5;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        } else if (pseudoStrLength >= 71) {
            expansionFactor = 1.3;
            newPseudoStrLength = pseudoStrLength * expansionFactor - 4;
        }

        newPseudoStrLength = parseInt(String((Math.floor(Math.round(newPseudoStrLength) / 2)) * 2), 10);
        pseudoedLengthRemaining = newPseudoStrLength;

        i = 0;
        while (i < stringValueString.length) {
            if (i + 1 >= stringValueString.length) {
                nextchar = '';
            } else {
                nextchar = stringValueString[i + 1];
            }

            if (i - 1 <= 0) {
                prevchar = '';
            } else {
                prevchar = stringValueString[i - 1];
            }

            currchar = stringValueString[i];
            if (currchar === '&' && nextchar === '#') {
                while (true) {
                    HTML_entity_string = HTML_entity_string + currchar;
                    i++;
                    currchar = stringValueString[i];
                    if (currchar === ';') {
                        HTML_entity_string = HTML_entity_string + currchar;
                        break;
                    }
                }
                i++;
                pseudoedString = pseudoedString + HTML_entity_string;
                continue;
            }

            if (inPlaceholderFlag === 0) {
                if (currchar === '{' && nextchar === '{') {
                    pseudoedString = pseudoedString + currchar;
                    inPlaceholderFlag = 1;
                    i++;
                    continue;
                }
            } else {
                if (currchar === '}' && prevchar === '}') {
                    inPlaceholderFlag = 0;
                    pseudoedString = pseudoedString + currchar;
                    i++;
                } else {
                    pseudoedString = pseudoedString + currchar;
                    markupCharCounter++;
                    i++;
                }
                continue;
            }

            if (inMarkupFlag === 0) {
                markupOpenFound = re.search('[<{]', currchar);
                if (markupOpenFound !== -1) {
                    pseudoedString = pseudoedString + currchar;
                    inMarkupFlag = 1;
                } else {
                    DNTCharFound = re.search('["\\\\]', currchar);
                    if (DNTCharFound !== -1) {
                        pseudoedString = pseudoedString + currchar;
                    } else if (pseudoedLengthRemaining >= 2) {
                        pseudoCharValue = unicode(currchar).translate('wide');
                        pseudoedString = pseudoedString + pseudoCharValue;
                        pseudoedLengthRemaining = pseudoedLengthRemaining - 2;
                    } else {
                        pseudoedString = pseudoedString + currchar;
                    }
                }
            } else {
                markupCloseFound = re.search('[}>]', currchar);
                if (markupCloseFound !== -1) {
                    inMarkupFlag = 0;
                    pseudoedString = pseudoedString + currchar;
                } else {
                    pseudoedString = pseudoedString + currchar;
                }
            }

            i++;
        }

        if (pseudoedLengthRemaining > 0) {
            const pad_string = '~'.repeat(pseudoedLengthRemaining);
            pseudoedString = pseudoedString + pad_string;
        }

        stringValueString = pseudoedString;

        const unicodeEscapeFound = re.search('\\\\ｕ([０１２３４５６７８９]*[0-9]*)', stringValueString);
        if (unicodeEscapeFound !== -1) {
            const reverted_numbers = unicode(unicodeEscapeFound[0]).translate('ascii');
            stringValueString = re.sub('\\\\ｕ([０１２３４５６７８９]*[0-9]*)', reverted_numbers, stringValueString);
        }

        stringValueString = (pseudo_additional_char + '') + stringValueString;
        stringValueString = ('[' + stringValueString) + ']';

        return stringValueString;
    }

    getString(field, args0, args1, args2, args3, args4, args5, args6, args7, args8, args9) { //add as many as needed
        if (this.resources !== undefined) {
            if (field && field.startsWith && field.startsWith('%')) {
                field = field.substring(1);
            }

            if (field === undefined) {
                console.info("getString", field, this.locale);
            }
            else if (this.locale.toLowerCase() === "smoke") {
                var str = this.resources[this.defaultLocale][field];
                if (str !== undefined) {
                    var smoked = this.pseudoTranslateString(str)
                }
                else {
                    this.missings[field] = "Missing Default String";
                    //console.info("%cMissing String for: {" + field + "}, NO default locale", "color: red;");
                    return "";
                }
                if (args0 != undefined) {
                    delete arguments['0'];
                    var argumentsArr = this.utils._helper_toArray(arguments, true);
                    smoked = this.parseString(smoked, argumentsArr);

                }
                return smoked;
            }
            else if (this.resources[this.locale][field] !== undefined) {
                var str = this.resources[this.locale][field];

                if (args0 != undefined) {
                    delete arguments['0'];
                    var argumentsArr = this.utils._helper_toArray(arguments, true);
                    str = this.parseString(str, argumentsArr);
                }

                delete this.missings[field];
                return str;
            }
            else {
                var str = this.resources[this.defaultLocale][field];

                if (str != undefined) {
                    if (args0 != undefined) {
                        delete arguments['0'];
                        var argumentsArr = this.utils._helper_toArray(arguments, true);
                        str = this.parseString(str, argumentsArr);
                    }

                    this.missings[field] = "Missing [" + this.locale + "] localized String";
                    //console.info("%cMissing Localized String for: [" + field + "], using default locale instead", "color: red;");
                    return str;
                }
                else {
                    this.missings[field] = "Missing Default String";
                    //console.info("%cMissing String for: [" + field + "], NO default locale", "color: red;");
                    return "";
                }
            }
        }
        else {
            return "";
        }
    }
}
