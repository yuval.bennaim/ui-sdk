import { html } from '../../vendor/@polymer/polymer/polymer-element.js';

let instance = null;

export class StyleStore {
    constructor() {
        if (!instance) {
            instance = this;
            this.cssMap = [];
            this.theme = "dark";

            for (var s = 0; s < top.document.styleSheets.length; s++) {
                var sheet = top.document.styleSheets[s];

                try {
                    if(sheet.href != null && sheet.href.indexOf('dr-zp-light-var') > -1) {
                        this.theme = "light";
                    }
                    
                    if (sheet.href != null) {
                        const ix = sheet.href.lastIndexOf("/") + 1;
                        const name = sheet.href.substring(ix);
                        this.cssMap[name] = sheet;
                    }
                }
                catch (err) {
                    console.info('Erorr processing', sheet.href);
                }
            }
        }

        return instance;
    }

    getTheme() {
        return this.theme;
    }

    getSheetStyles(name) {
        let sheetStr = this.cssMap[name+".str"];

        if(sheetStr == null) { //do this once
            const sheet = this.cssMap[name];
            sheetStr = "<style>";

            for (let r = 0; r < sheet.rules.length; r++) {
                let rule = sheet.rules[r];
                let cssText = rule.cssText;
                sheetStr += cssText + "\n";
            }

            sheetStr += "</style>";
            this.cssMap[name+".str"] = sheetStr;
        }

        const styleStr = html`${sheetStr}`;
        return styleStr || html` `;
    }
}