let instance = null;

export class TokenStore {
    constructor() {
        if (!instance) {
            instance = this;
        }

        return instance;
    }

    setAuthClient(authClient) { //add as many as needed
        this.authClient = authClient;
    }

    getAccessToken() { //add as many as needed
        if(this.accessToken) { //in dev mode
            return this.accessToken;
        }
        else {
            return this.authClient.tokenManager.get('accessToken');
        }
    }
}