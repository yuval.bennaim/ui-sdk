import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../dr-zp-base-component.js';

class Comp extends BaseComponent {
    constructor() {
        super();

        this.height = null;
        this.el = null;
        this.transition = 'height 300ms ease-in';
    }

    static get template() {
        var superTemp = super.template;
        var template = html`
        ${superTemp}
        <div id="collapseElement" class="dr-zp-collapse">
            <slot></slot>
        </div>
      `;

        return template;
    }

    static get properties() {
        return {
            isOpen: {
                type: Boolean,
                value: false,
                observer: '_isOpenChanged'
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();

        this.el = this.$.collapseElement;
        this.height = this.el.scrollHeight;
        if (this.isOpen) this.el.style.height = this.height + 'px';
        this.el.style.transition = this.transition;

        this.isOpen ? this._expandElement() : this._collapseElement();
    }

    _isOpenChanged() {
        this.dispatchEvent(new CustomEvent("dr-zp-collapse-changed", {
            detail: { source: this }
        }));

        this.isOpen ? this._expandElement() : this._collapseElement();
    }

    _collapseElement() {
        var el = this.$.collapseElement;

        requestAnimationFrame(() => {
            el.style.transition = this.transition;
            requestAnimationFrame(() => {
                el.style.height = '0px';
            })
        })
        el.setAttribute('aria-hidden', true);
    }

    _expandElement() {
        var el = this.$.collapseElement;
        el.style.height = el.scrollHeight + 'px';
        el.removeAttribute('aria-hidden');
    }

}

window.customElements.define('dr-zp-collapse', Comp); 