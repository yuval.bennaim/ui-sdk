import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';


class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}
      <style>
        .panel__body { padding: 0 20px !important; }
      </style>

      <div class$="{{_calculateHeaderClass(transparentHeader)}}">
        <div class="dr-zp-panel__header">
          <div class="dr-zp-panel__title">
            <slot name="panel-title"></slot>
          </div>
          <div class="dr-zp-panel__icons">
            <slot name="panel-icons"></slot>
          </div>
        </div>
        <div class="dr-zp-panel__body">
          <slot></slot>
        </div>
      </div>
    `;

    return template;
  }

  static get is() { return 'dr-zp-panel'; }

  static get properties() {
    return {
      transparentHeader: {
        type: Boolean,
        value: false
      }
    }
  }

  _calculateHeaderClass(transparentHeader) {
    var headerClass = "dr-zp-panel";
    if(this.transparentHeader) headerClass += ' dr-zp-panel--transparent-header'
    return headerClass;
  }
}

window.customElements.define('dr-zp-panel', Comp);
