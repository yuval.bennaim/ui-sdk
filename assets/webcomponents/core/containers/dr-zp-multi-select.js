import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../form/dr-zp-checkbox.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';

export class MultiSelect extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
        ${superTemp}
        <div class$="{{computedClass(opened)}}" on-mouseleave="closeDropdown">
            
            <template is="dom-if" if="[[!isIcon(dropdowntype)]]">
                <button class$="[[computeButtonClass(dropdowntype)]]" on-click="_clickHandler" disabled="{{disabled}}">
                    <span data-label="label">[[label]]</span>
                    <span class="dropdown-icon">
                        <dr-zp-icon display="flex" type="chevron-down"></dr-zp-icon>
                    </span>
                </button>
            </template>

            <template is="dom-if" if="[[isIcon(dropdowntype)]]">
                <button class="link grid-icon" on-click="_clickHandler" disabled="{{disabled}}">
                    <dr-zp-icon display="flex" type="[[icon]]" ></dr-zp-icon>
                </button>
            </template>

            <ul class$="[[computeDropdownClass(dropdowntype)]]">
                <template is="dom-repeat" items="[[items]]">
                    <li>
                        <dr-zp-checkbox class="multi-select-checkbox" id="cb_[[index]]" multiselect label="[[getDataValue(item, field)]]" checked="[[getCheckedState(item, fieldChecked)]]"></dr-zp-checkbox>
                    </li>                   
                </template>
            </ul>
        </div>
        `;

        return template;
    }

    static get properties() {
        return {
            label: {
                type: String,
                value: 'Select'
            },
            dropdown: { type: String },
            items: {
                type: Array,
                notify: true
            },
            field: {
                type: String,
                value: 'name'
            },
            fieldChecked: {
                type: String,
                value: 'selected'
            },
            opened: {
                type: Boolean,
                value: false
            },
            icon: {
                type: String
            },
            dropdowntype: {
                //this is to speciy the type of the dropdown box. the possible value could be
                // "link", "icon", "bordered" and "default" , if no dropdowntype is specifyed, then the type is "default"
                type: String,
                value: "default"
            },
            disabled: {
                type: Boolean,
                value: false
            },
            fullWidth: {
                type: Boolean,
                value: false
            },
            listenerList: {
                type: Array,
                value: [],
            },
        }
    }

    static get observers() {
        return [
            '_itemsChange(items.length)'
        ]
    }

    _itemsChange() {
        var cbs = dom(this.root).querySelectorAll('dr-zp-checkbox');
        //Clear old Event Listeners
        cbs.forEach((cb) => {
            cb.removeEventListener('checkbox-clicked', e => this.onCheckBoxClicked(e));
        });
        this.listenerList = [];
        setTimeout(() => {
            this.setupCheckboxes();
        });
    }

    connectedCallback() {
        super.connectedCallback();
        document.addEventListener('escape-action', this.escapeActionHandler.bind(this));
        this.setupCheckboxes();
    }

    setupCheckboxes() {
        var cbs = dom(this.root).querySelectorAll('dr-zp-checkbox');

        if (cbs.length === 0 && this.items.length > 0) {
            setTimeout(() => {
                this.setupCheckboxes();
            });
        }
        else {
            cbs.forEach((cb) => {
                if (this.listenerList.indexOf(cb.getAttribute('id')) === -1) {
                    this.listenerList.push(cb.getAttribute('id'));
                    cb.addEventListener('checkbox-clicked', e => this.onCheckBoxClicked(e));
                }
            });
        }
    }

    onCheckBoxClicked(e) {
        var checked = e.detail.checked;
        var index = e.currentTarget.id.split('_')[1];
        this.items[index][this.fieldChecked] = checked;
        this.items = this.utils._helper_copy(this.items);

        this.dispatchEvent(new CustomEvent('multi-select-change', {
            detail: {
                items: this.items
            }
        }));
    }

    escapeActionHandler(e) {
        if (this.opened) {
            this.set('opened', false);
        }
    }

    _clickHandler(e) {
        this.set('opened', !this.opened);
    }

    computedClass(opened) {
        var cls = "dr-zp-dropdown dropdown";

        if (this.fullWidth) {
            cls += " full-width";
        }

        if (opened) {
            cls += " open";
        }

        return cls;
    }

    isIcon(dropdowntype) {
        return dropdowntype === "icon";
    }

    computeButtonClass(dropdowntype) {
        var cls = "dr-zp-btn dr-zp-btn-dropdown full-width";
        switch (dropdowntype) {
            case 'link':
                cls += ' dr-zp-btn--link';
                break;
            case 'bordered':
                cls += ' dr-zp-btn--hollow';
                break;
            default:
                cls += ' dr-zp-btn--solid';
        }
        return cls;
    }

    computeDropdownClass(dropdowntype) {
        var cls = "dropdown-list";

        if (this.isIcon(dropdowntype)) {
            cls += ' dropdown-list-icon';
        }

        return cls;
    }

    getCheckedState(item, fieldChecked) {
        return item[this.fieldChecked];
    }

    getDataValue(item, field) {
        if (item != null) {
            var display = item;

            if (this.field != null) {
                display = item[this.field];
            }

            return display;
        }
    }

    closeDropdown(e) {
        if (this.opened && e.relatedTarget != undefined) {
            var parent = e.relatedTarget.parentElement;

            if (parent === null || parent.className.indexOf('dropdown-list') === -1 && parent.className.indexOf('dropdown') === -1) {
                this.set('opened', false);
            }
        }
    }
}

window.customElements.define('dr-zp-multi-select', MultiSelect);