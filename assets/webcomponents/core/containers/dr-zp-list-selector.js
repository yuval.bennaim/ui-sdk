import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import '../form/dr-zp-input.js'
import '../form/dr-zp-button.js'
import '../misc/dr-zp-icon.js';
import { BaseComponent } from '../dr-zp-base-component.js';
import "../../../vendor/@polymer/polymer/lib/elements/dom-if.js";
import "../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js";

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}

      <style>
        .list-input{display:flex;align-items:baseline;flex-wrap:wrap;}
        .list-input-buttons{display:flex;align-items:center;justify-content:center;flex-wrap:wrap;}
        #list-selector-input{flex:1;}
        dr-zp-button{padding-left:16px;}
        .items{padding-top:15px;}
        .item{display:flex;align-items:center;}
        .item-input{flex:1;}
        .item-remove{color:var(--color-disposition-danger);padding:0px 16px;cursor:pointer;}
      </style>
      
      <div class="dr-zp-input-wrapper">
        <div class="list-input">
          <dr-zp-input id="list-selector-input" value="{{value}}" label="[[label]]"></dr-zp-input>
          <div class="list-input-buttons">
            <dr-zp-button id="add-item" label="[[getString('sdk-add', locale)]]" on-click="_addItem"></dr-zp-button>
            <dr-zp-button id="remove-item" label="[[getString('sdk-remove-all', locale)]]" danger="true" on-click="_removeAllItems"></dr-zp-button>
          </div>
        </div>
        <div class="items">
          <template is="dom-repeat" items="{{list}}">
            <div class="item">
              <dr-zp-input class="item-input" value="{{item}}"></dr-zp-input>
              <button class="item-remove" on-click="_removeItem" item$="{{item}}" aria-label="remove item" aria-pressed="false">
                <dr-zp-icon type="x" size="lg" display="flex"></dr-zp-icon>
              </button>
            </div>
          </template>
        </div>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      label: {
        type: String,
        value: ''
      },
      value: {
        type: String,
        value: ''
      },
      list: {
        type: Array,
        value: [],
        notify: true
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();

    this.$['list-selector-input'].addEventListener("keyup", (e) => {
      if (e.keyCode === 13) { // ENTER pushed
        e.preventDefault();
        this._addItem();
      }
    });
  }

  _addItem(e) {
    var item = this.value;
    this.list = this.list && this.list.length > 0 ? [...this.list, item] : [item];

    this.dispatchEvent(new CustomEvent('dr-zp-list-selector-add', {
      bubbles: true,
      composed: true,
      detail: {
        action: "add",
        item,
        list: this.list
      }
    }));

    this.value = '';
  }

  _removeAllItems(e) {
    this.dispatchEvent(new CustomEvent('dr-zp-list-selector-remove-all', {
      bubbles: true,
      composed: true,
      detail: {
        action: "remove-all",
        list: this.list
      }
    }));

    this.list = [];
  }

  _removeItem(e) {
    var item = e.target.closest('button').getAttribute('item');
    var list = this.list.slice();
    var index = list.indexOf(item);
    list.splice(index, 1);
    this.list = list;
    this.dispatchEvent(new CustomEvent('dr-zp-list-selector-remove', {
      bubbles: true,
      composed: true,
      detail: {
        action: "remove",
        item,
        list: this.list
      }
    }));
    e.target.closest('button').setAttribute('aria-pressed', 'true');
  }
}

window.customElements.define('dr-zp-list-selector', Comp);
