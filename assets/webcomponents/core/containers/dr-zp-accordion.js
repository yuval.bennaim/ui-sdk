import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
            <style>
                .panel-closed{display:none;}
                .panel-closed.in{display:block;}
                .panel-body {
                    padding:10px;
                }
                .panel-body:before,.panel-body:after{content:" ";display:table;}
                .panel-body:after{clear:both;}
               
                .panel-heading{padding:10px 15px;border-top-right-radius:3px;border-top-left-radius:3px;}
                .panel-heading .panel-indicator__icon-container{height:22px;}
                .panel-heading .panel-indicator-icon{
                    font-size: 8px;
                    border-radius: 50%;
                    padding: 5px;
                    float: right;
                }
                .pull-right{float:right!important;}
                .panel-body {margin: 0px}
                .accordion-item {
                    padding: 2px;
                }
                .accordion-item-bordered {
                    border: 1px solid #777;
                    padding: 2px;
                    margin-bottom: 2px;
                }
                .panel-heading {
                    padding: 5px 5px 2px 5px;
                    background: transparent !important;
                    border: none;
                    cursor: pointer;
                    width: 100%;
                    text-align: left;
                }
                .panel-heading:focus {
                    outline: 0 !important;
                    outline-offset: 0 !important;
                    box-shadow: none !important;
                    color: var(--color-action-primary--bright) !important
                }
            </style>

            <div class="dr-zp-panel  dr-zp-panel__header--collapsible panel-group" id="accordion">
                <template is="dom-repeat" items="[[items]]">
                    <div class$="[[computePaneClass(bordered)]]">
                        <button id$="btn_[[index]]" aria-controls$="content_[[index]]" aria-expanded="false" class$="{{computeToggleClass(inc, item)}}" on-click="toggleSection">
                            
                            [[getTitle(item, locale)]]
                            
                            <span class="pull-right dr-zp-panel__collapse-toggle">
                                <template is="dom-if" if="[[!item.expanded]]">
                                    <dr-zp-icon type="chevron-right" size="sm" ></dr-zp-icon>
                                </template>

                                <template is="dom-if" if="[[item.expanded]]">
                                    <dr-zp-icon type="chevron-down" size="sm" ></dr-zp-icon>
                                </template>
                            </span>
                        </button>

                        <div id$="content_[[index]]" role="region" aria-labelledby$="btn_[[index]]" class$="[[computeContentClass(inc, item)]]">
                            <div class="dr-zp-panel__body panel-body">
                                <slot name="{{getSlotName(item)}}"></slot><!-- inject slot content -->
                            </div>
                        </div>
                    </div>    
                </template>
            </div>        
        `;

        return template;
    }

    static get properties() {
        return {
            name: {
                type: String,
                value: 'dr-zp-accordion'
            },
            inc: {
                type: Number,
                value: 0
            },
            bordered: {
                type: Boolean,
                value: false
            },
            items: {
                type: Array
            },
            titleKey: {
                type: String,
            },
            primaryKey: {
                type: String,
            },
            localized: {
                type: Boolean,
                value: false
            }
        }
    }

    getTitle(item, locale) {
        var title = item[this.titleKey];

        if (this.localized) {
            title = this.getString(title, this.locale) || title;
        }

        return title;
    }

    getSlotName(item) {
        var name = item[this.primaryKey]
        return name;
    }

    toggleSection(e) {
        var item = e.model.__data.item;
        item.expanded = !item.expanded;
        var isExpanded = e.target.getAttribute('aria-expanded') == 'true';
        this.items = JSON.parse(JSON.stringify(this.items));
        this.inc++;

        //e.target.blur();

        this.dispatchEvent(new CustomEvent('accordion-change', {
            detail: {
                item: item
            }
        }));
        e.target.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
    }


    computePaneClass(bordered) {
        var cls = "accordion-item";

        if (bordered) {
            cls += "-bordered";
        }

        return cls;
    }

    computeToggleClass(inc, item) {
        var cls = "dr-zp-panel__title panel-heading";

        if (item.expanded) {
            cls += " open";
        }

        return cls;
    }

    computeContentClass(inc, item) {
        var cls = "dr-zp-panel__collapse dr-zp-collapse dr-zp-collapse--open panel-panel-closed panel-closed";

        if (item.expanded) {
            cls += " in";
        }

        return cls;
    }
}

window.customElements.define('dr-zp-accordion', Comp);



