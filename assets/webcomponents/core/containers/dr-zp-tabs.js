import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var sdkStyleTemplate = super.styleStore.getSheetStyles("sdk.css");
    var template = html`
      ${superTemp}
      ${sdkStyleTemplate}

      <style>
        :host {display: block;height: 100%;width: 100%;}
        button{border:none;}
        button:focus{outline: none;box-shadow: none !important;color: var(--action-primary) !important;}
        .tabs--column{display:flex !important;}
        .tabs__list--column > .tab{min-width: 8em;}
        .tab-content{padding:15px;display: block;}
        .tab-content { width: 100%; position: relative; }
        .tab-content > .tab-pane{display:none;}
        .tab-content > .active{display:block;}
        .fade{opacity:0;transition:opacity 0.15s linear;}
        .fade.in{opacity:1;}
        .dr-zp-tabs {width: 100%; height: 100%;}
      </style>
      
      <div class$="[[computeTabsClass(direction)]]">
        <ul class$="[[computeTabsListClass(direction)]]" role="tablist" style$="[[computeTabListStyle(direction, tabWidth)]]">
          <template is="dom-repeat" items="[[items]]">
            <div class="dr-zp-tab full-width lefted hydrated">
              <button role="tab"
                disabled="[[item.disabled]]"
                class$="[[computeTabClass(index, activeindex, direction)]]"
                data-toggle="tab"
                on-click="setActiveIndex"
                index=[[index]]
                id="tab_[[item.id]]"
                aria-controls$=[[item.id]]
                aria-expanded="{{isActiveIndex(index, activeindex)}}"
                aria-selected="[[isSelected(index, activeindex)]]">
                [[getTitle(item, locale)]]
              </button>  
            </div>          
          </template>
        </ul>

        <div id="myTabContent" class$="[[computeTabContentClass(direction)]]">
          <template is="dom-repeat" items="[[items]]">
            <div
              tabindex="0"
              role="tabpanel"
              class$="[[computeTabPaneClass(index, activeindex)]]"
              id=[[item.id]]>
              <slot name="{{getSlotName(item)}}"></slot>
            </div>
          </template>
        </div>
      </div>
    `;

    return template;
  }

  static get is() { return 'dr-zp-tabs'; }

  static get properties() {
    return {
      name: {
        type: String,
        value: 'fe-gridtemplate-tabs'
      },
      items: {
        type: Array,
        value: []
      },
      activeindex: {
        type: Number,
        value: 0
      },
      tabWidth: {
        type: Number,
        value: null
      },
      titleKey: {
        type: String,
      },
      primaryKey: {
        type: String,
      },
      direction: {
        type: String,
        value: 'row'
      },
      localized: {
        type: Boolean,
        value: false
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();
  }

  isSelected(index, activeindex) {
    var sel = false;
    if(this.activeindex == index){
      sel = true;
    } 

    return sel;
  }

  computeTabListStyle(direction, tabWidth) {
    var style = "";

    if (direction === "column" && tabWidth != null) {
        style = "min-width: " + tabWidth + "px";
    }

    return style;
  }

  computeTabsClass(direction) {
    return `dr-zp-tabs dr-zp-tabs--${direction} hydrated`;
  }

  computeTabsListClass(direction) {
    return `dr-zp-tabs__list dr-zp-tabs__list--${direction}`;
  }

  computeTabClass(index, activeindex, direction) {
    var cls = "dr-zp-list-item dr-zp-list-item--tab hydrated full-width lefted";

    if (index === activeindex) {
      cls += " active";
    }

    if (direction === 'row') {
      cls += " dr-zp-list-item--border-bottom"
    } else {
      cls += " dr-zp-list-item--border-left"
    }
    this.isSelected(index);
    return cls;
  }

  computeTabContentClass(direction) {
    return `tab-content tab-content--${direction}`;
  }

  computeTabPaneClass(index, activeindex) {
    var cls = "tab-pane fade";

    if (index === activeindex) {
      cls += " active in";
    }

    return cls;
  }

  setActiveIndex(e) {
    this.activeindex = e.currentTarget.index;

    this.dispatchEvent(new CustomEvent('tab-changed', {
      bubbles: true,
      composed: true,
      detail: {
        index: this.activeindex
      }
    }));
  }

  isActiveIndex(index) {
    return index === this.activeindex;
  }

  getTitle(item, locale) {
    var title = item[this.titleKey];

    if (this.localized) {
      title = this.getString(title, this.locale) || title;
    }

    return title;

  }

  getSlotName(item) {
    return `slot_${item[this.primaryKey]}`;
  }
}

window.customElements.define('dr-zp-tabs', Comp);