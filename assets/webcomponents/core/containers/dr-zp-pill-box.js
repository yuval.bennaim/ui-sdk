import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import '../form/dr-zp-input.js'
import '../form/dr-zp-button.js'
import '../form/dr-zp-dropdown.js'
import '../misc/dr-zp-icon.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import "../../../vendor/@polymer/polymer/lib/elements/dom-if.js";
import "../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js";

class Comp extends BaseComponent {
  static get template() {
      var superTemp = super.template;
      var template = html`
      ${superTemp}

      <style>
        .pillbox-wrapper {
          position: absolute;
          z-index: 5000;
          padding: 0 10px 0 10px;
          border: 1px solid var(--border--emphasis);
          background-color: var(--background-surface);
          border-radius: 0px;
          left: 0;
          right: 0;
          white-space: nowrap;
        }
        .pillbox-wrapper-expanded {
          height: 200px !important;
        }
        .add-wrapper {
          background-color: var(--color-background-interactive);
          position: absolute;
          top: 36px;
          right: -1px;
          box-shadow: -10px 20px 20px hsl(0deg 0% 1% / 80%);
          border: 1px solid var(--border--emphasis);
          border-top: none;
          padding: 10px;
        }
        .lookup-wrapper {
            position: absolute;
            padding-top: 10px;
            padding-bottom: 10px;
            background-color: var(--background-overlay);
            /* border: 1px solid var(--border--emphasis); */
            box-shadow: 0px 22px 20px rgb(0 0 0 / 70%);
            left: 14px;
            right: 14px;
            top: 35px;
            height: 180px;
            white-space: normal;
        }
        .lookup-scroller {
            display: flex;
            align-items: stretch;
            overflow: auto;
            position: absolute;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 30px;
        }
        .pills-wrapper {
          overflow-x: auto;
          overflow-y: hidden;  
          height: 100%;
        }
        .pill {
          position: relative;
          top: 1px;
          font-size: 11px;
          padding: 1px 3px 0px 8px;
          background-color: #266498;
          border-radius: 10px;
          display: inline-block;
          margin: 2px;
          white-space: nowrap;
        }
        .pill-inner {
          display: flex;
          margin-top: 3px;
        }
        .close-icon {
          width: 16px;
          height: 16px;
          cursor: pointer;
        }
        .filter-icon {
          width: 16px;
          height: 16px;
          cursor: pointer;
          position: relative;
          top: 1px;
        }
        .pill-title {
          position: relative;
          top: -1px;
          font-weight: bold;
        }
        .pill-buttons {
          margin-left: auto;
          white-space: nowrap;
          display: none;
        }
        .pill-button {
          margin-left: 10px;
        }
        .edit-field {
          background-color: transparent;
          color: var(--color-text-primary);
          border: none;
          padding: 2px;
          min-width: 40px;
          position: relative;
          top: -1px;
        }
        .edit-field:focus {
          outline: none;
          background-color: var(--background-overlay);
        }
        .lookup-icon {
          cursor: pointer;
        }
        .lookup-box {
            flex-grow: 1;
            border-right: 1px solid var(--border--emphasis);
            margin-top: 10px;
            padding-left: 10px;
            padding-right: 10px;
            display: inline-block;
            font-size: 11px;
            overflow: auto;
        }        
        .filter-sets-box {
            flex-grow: 1;
            border-right: none !important;
        }
        .lookups-title {
          font-weight: bold;
          margin: 10px;
        }
        .lookup-header {
          font-weight: bold;
          margin-bottom: 5px;
          text-transform: uppercase;
          font-size: 10px;
        }
        .lookup-option {
          margin-bottom: 5px;
          cursor: pointer;
        }
        .lookup-option:hover {
          color: var(--color-active--primary);
        }
        .details-closer {
            cursor: pointer;
            font-size: 10px;
            position: relative;
            top: 2px;
        }
        .option-icon {
          position: relative;
          top: 4px;
        }
        .pill-wrapper-table {
            width: 100%;
            height: 34px;
            position: relative;
            left: -6px;
        }
        .pill-table {
          width: 100%;
          height: 100%;
        }
        .pill-table td {
          vertical-align: middle;
        }
        .pill-cell {
          white-space: normal;
          width: 100%;
        }
        .lookup-link {
          font-size: 11px;
          font-weight: normal;
        }
        .delete-link {
            font-size: 10px;
            font-weight: normal;
            float: right;
          }
        .filter-placeholder
        {
            cursor: pointer;
            position: relative;
            left: 6px;
            font-size: 10px;
            font-style: italic;
        }
        .lookups-closer {
            left: 0;
            right: 0;
            position: absolute;
            bottom: 0px;
            height: 30px;
            background-color: var(--fill);
            text-align: center;
        }
        .closer-icon {
            position: relative;
            top: 4px;
        }
      </style>
      
      <div class="pillbox-wrapper"">

        <table class="pill-wrapper-table">
          <tr>
            <td class="pill-cell" id="pw" on-dom-change="pillsDomChanged">

              <template is="dom-if" if="[[!hasPills(pills)]]">
                <span class="filter-placeholder" on-click="toggleLookups">[[generateFilterText(entity, lookups, locale)]]</span>
              </template>

              <template is="dom-if" if="[[hasPills(pills)]]">
                <template is="dom-repeat" items="[[pills]]" as="pill" index-as="p">
                  <div class="pill">
                    <table class="pill-table">
                      <tr>
                        <td>
                          <template is="dom-if" if="[[isEditable(pill)]]">
                            <span class="pill-title">[[generatePillLabel(pill, locale)]]:</span>
                          </template> 
                          <template is="dom-if" if="[[!isEditable(pill)]]">
                            [[getDisplayable(pill)]]
                          </template>   
                        </td>
      
                        <template is="dom-if" if="[[isEditable(pill)]]">
                          <td>
                            <input id="input" type="text" class="edit-field" value="{{pill.value}}" aria-label="[[getAriaInputLabel(pill, locale)]]" on-change="pillChanged" style$="[[calcInputWidth(pill)]]"=></input>
                          </td>
                        </template> 
      
                        <td>
                          <button class="link close-icon" on-click="removePillHandler">
                            <dr-zp-icon type="x-circle" size="sm" title="[[getString('sdk-remove', locale)]]" aria-label="[[getAriaButtonLabel(pill, locale)]]"></dr-zp-icon>
                          </button>
                        </td>
                      </tr>
                    </table>
                  </div>
                </template>
              </template>
            </td>

            <td class="nowrap righted">
                <template is="dom-if" if="[[hasPills(pills)]]">

                    <template is="dom-repeat" items="[[actions]]" as="action">
                        <dr-zp-button size="small" type="link" class="pill-button" label="[[getActionString(action, locale)]]" on-click="actionPill"></dr-zp-button>
                    </template>

                    <dr-zp-button size="small" type="link" class="pill-button" label="[[getString('sdk-clear', locale)]]" on-click="removeAllPills"></dr-zp-button>
                </template>

                <template is="dom-if" if="[[hasLookupsOrFilters(lookups, filterSets)]]">
                    <dr-zp-button size="small" type="link" class="pill-button" label="[[getString('sdk-filters', locale)]]" on-click="toggleLookups"></dr-zp-button>
                </template>
            </td>
          </tr>
        </table>
        
        <template is="dom-if" if="[[showLookupsFunc(showLookups)]]">
            <div class="lookup-wrapper" on-mouseleave="closeLookups">  

                <div class="lookup-scroller"> 
                    <template is="dom-repeat" items="[[lookups]]" as="lookup" index-as="r">
                        <div class="lookup-box" style$="[[determineLookupBoxStyle(r)]]">
                            <div class="lookup-header">
                                [[getLookupDisplayName(lookup, locale)]]
                            </div>
                            <template is="dom-repeat" items="[[lookup.options]]" as="option">
                                <div class="lookup-option">
                                    <button class="link lookup-link" on-click="selectLookup">
                                        <template is="dom-if" if="[[hasOptionIcon(option)]]">
                                            <span style$="[[generateIconStyle(option)]]">
                                            <dr-zp-icon class="option-icon" type="[[option.icon.icon]]" size="sm"></dr-zp-icon>
                                            </span>
                                        </template>

                                        <span style$="[[generateOptionStyle(pills, lookup, option)]]">
                                            [[option.value]] ([[option.count]])
                                        </span>
                                    </button>

                                </div>
                            </template>
                        </div>
                    </template>   

                    <!-- filtersets -->
                
                    <template is="dom-if" if="[[hasFilterSets(filterSets)]]">
                        <div class="lookup-box filter-sets-box">
                            <div class="lookup-header">
                                [[getString('sdk-my-filter-sets', locale)]]
                            </div>
                     
                            <template is="dom-repeat" items="[[_toArray(filterSets)]]" as="fs">
                                <div class="lookup-option">                                   
                                    <button class="link lookup-link" on-click="selectFilterSetHandler">
                                        [[fs.key]]
                                    </button>
                                    <button class="link delete-link" on-click="deleteFilterSet">
                                        (remove)
                                    </button>
                                </div>
                            </template>
                           
                            
                        </div>
                    </template>
                </div>
                
                <div class="lookups-closer">
                    <button class="link details-closer" title="Click to Close Preset Filters" on-click="closeLookups">
                        <dr-zp-icon class="closer-icon" type="chevron-up" size="sm" title="[[getString('sdk-close', locale)]]"></dr-zp-icon>
                        [[getString('sdk-close', locale)]]
                    </button>
                </div>

            </div>
        </template>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      label: {
        type: String,
        value: ''
      },
      value: {
        type: String,
      },
      displayField: {
        type: String
      },
      entity: {
        type: String,
        value: ""
      },
      icon: {
        type: String,
        value: null
      },
      showLookups: {
        type: Boolean,
        value: false
      },
      filterSets: {
        type: Object,
        value: null,
        observer: 'filterSetsObserved'
     },
      lookups: {
        type: Array,
        value: []
      },
      actions: {
        type: Array,
        value: []
      },
      pills: {
        type: Array,
        value: [],
        notify: true
      },
      selectedOption: {
        type: Object,
        value: {}
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();  
    window.addEventListener("resize", () => {this.calculateLookupOffset();});

    this.addEventListener('list-change', (e) => {
      var item = e.detail.selected;

      this.dispatchEvent(new CustomEvent('dr-zp-pill-box-add', {
        bubbles: true,
        composed: true,
        detail: {
          pill: item
        }
      }));
    });

    setTimeout(() => {
      this.calculateLookupOffset();
    }, 200);
  }

    filterSetsObserved() {
        if(this.filterSets) {
            console.info('filterSetsObserved', this.filterSets);
            let drillinFS = this.filterSets['drill-in'];

            if(drillinFS) { //if we have a drill-in, select it
                let drillinObj = {key: "drill-in", label: "drill-in", val: drillinFS};
                this.selectFilterSet(drillinObj);
            }
        }
    }

    isUserFilterSets(filterSet) {
        return filterSet.key != 'drill-in';
    }


    _toArray(obj) {
        let arr = this.utils._helper_toArray(obj);
        return arr;
    }

  generateFilterText(entity, lookups, locale) {
    var filterStr = this.getString('sdk-filter-entity', locale, this.entity);
    return filterStr;
  }

  pillsDomChanged(e) {
    this.calculateLookupOffset();
  }

  getActionString(action, locale) {
      let key = 'sdk-' + action;
      return this.getString(key, locale);
  }

  calculateLookupOffset() {
    var pw = dom(this.root).querySelector("#pw");
    this.lookupsOffset = pw.clientHeight + 5;

    this.dispatchEvent(new CustomEvent('dr-zp-pill-height-changed', {
      bubbles: true,
      composed: true,
      detail: {
        height: this.lookupsOffset
      }
    }));
  }

  determineLookupBoxStyle(index) {
    if(index == this.lookups.length -1) { //last
      return "flex-grow: 8";
    }
    else {
      return null;
    }
  }

  calcInputWidth(pill) {
    var str = pill.value;
    var input = dom(this.root).querySelector("#pw");
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    ctx.font = input.style.font;
    var textWidth = parseInt(ctx.measureText(str).width) + 20;
    return "width: " + textWidth + "px";
  }

  closeLookups() {
    this.showLookups = false;
    this.showAddList = false;
  }

  toggleLookups() {
      this.showLookups = !this.showLookups;
      this.calculateLookupOffset();
  }

  determineLookupIcon(showLookups) {
    return showLookups ? 'chevron-down-circle' : 'chevron-right-circle';
  }

  toggleshowAddList() {
    this.showAddList = !this.showAddList;
  }

  showAddListFunc(showAddList) {
    return this.showAddList;
  }

  showLookupsFunc(showLookups) {
    return this.showLookups;
  }

  getLookupDisplayName(lookup, locale) {
    var displayName = this.getString(lookup.resourceKey, this.locale);
    return displayName;
  }

  getAriaInputLabel (pill, locale) {
    return `${this.getString("sdk-filter-text", this.locale)} ${this.generatePillLabel(pill, locale)}`;
  }

  getAriaButtonLabel (pill, locale) {
    var labelText = this.getString("sdk-close-filter", this.locale);
    if(this.isEditable(pill)) {
      labelText += ` ${this.generatePillLabel(pill, locale)}`;
    } else {
      labelText += ` ${this.getDisplayable(pill, locale)}`;
    }
    return labelText;
  }

  getDisplayable(pill) {
    var str = '';

    if(typeof pill === 'string') {
      str = pill;
    }
    else {
      str = pill[this.displayField];
    }

    return str;
  }

  generateOptionStyle(pills, lookup, option) {
    var style = "";

    if(this.optionIsCurrentlyFiltered(lookup, option) > -1) {
      style = "font-weight: bold;";
    }

    if(option && option.icon == null) {
        style += "position: relative; top: 6px;"; 
    }

    return style;
  }

  optionIsCurrentlyFiltered(lookup, option) {
    var ix = -1;

    if (this.pills && (typeof lookup == "object" && lookup && lookup.name) && (typeof option == "object" && option && option.value)) {
      var field = lookup.name;
      var val = option.value;

      for(var p = 0; p < this.pills.length; p++) {
        var pill = this.pills[p];

        if(pill.field === field) {
          if(pill.value != null && pill.value.indexOf('|') > -1) { //regex OR, split to tokens
            var tokens = pill.value.split('|');

            for(var t = 0; t < tokens.length; t++) {
              if(tokens[t] === val) {
                ix = p;
                p = this.pills.length;
              }
            }
          }
          else if(pill.value == val) {
            ix = p;
            p = this.pills.length;
          }
        }
      }
    }

    return ix;
  }

    generateIconStyle(option) {
        var style;

        if(option.icon != null && option.icon.color != null) {
            style = "color: " + option.icon.color; 
        }

        return style;
    }

    hasFilterSets(filterSets) {
        let ret = this.filterSets != null && !this.utils._helper_isEmpty(filterSets);
        return ret;
    }

    hasOptionIcon(option) {
        return option.icon != null;
    }

    hasLookupsOrFilters(lookups, filterSets) {
        let retA = this.hasLookups(lookups);
        let retB = this.hasFilterSets(filterSets);
        let ret = retA || retB;
        return ret;
    }

    hasLookups(lookups) {
        return this.lookups != null && this.lookups.length > 0;
    }

    hasPills(pills) {
        return this.pills && this.pills.length > 0;
    }

    generatePillLabel(pill, locale) {
        let name = pill.name || pill;

        if(name.startsWith('%')) {
            let key = name.substring(1);
            name = this.getString(key, this.locale);
        }

        return name;
    }

  isEditable(pill) {
    return pill.editable;
  }

  pillChanged(e) {
    var val = e.srcElement.value;
    var pill = e.model.__data.pill;
    pill.value = val;
    this.pills = this.utils._helper_copy(this.pills);
    this.calculateLookupOffset();
    e.srcElement.blur();

    this.dispatchEvent(new CustomEvent('dr-zp-pill-value-change', {
      bubbles: true,
      composed: true,
      detail: {
        list: this.pills,
        pill: pill
      }
    }));
  }
  
    actionPill(e) {
        var action = e.model.__data.action;

        this.dispatchEvent(new CustomEvent('dr-zp-pill-box-action', {
            bubbles: true,
            composed: true,
            detail: {'action': action, list: this.pills}
        }));
    }

  removeAllPills(e) {
    console.info('removeAllPills');
    this.pills.length = 0;
    this.pills = this.utils._helper_copy(this.pills);
    this.calculateLookupOffset();

    this.dispatchEvent(new CustomEvent('dr-zp-pill-box-clear', {
      bubbles: true,
      composed: true,
      detail: {
        list: this.pills
      }
    }));
  }

  removePillHandler(e) {
    var pill = e.model.__data.pill;
    var ix = e.model.__data.itemsIndex
    this.removePill(ix, false);
  }

  removePill(ix, lookup) {
    console.info('removePill', ix);
    this.pills.splice(ix, 1);
    this.pills = this.utils._helper_copy(this.pills);
    this.calculateLookupOffset();

    if(lookup) {
        this.dispatchEvent(new CustomEvent('dr-zp-pill-box-lookup-remove', {
            bubbles: true,
            composed: true,
            detail: {
                list: this.pills
            }
        }));
    }
    else {
        this.dispatchEvent(new CustomEvent('dr-zp-pill-box-remove', {
            bubbles: true,
            composed: true,
            detail: {
                list: this.pills
            }
        }));
    }
  }

  selectFilterSetHandler(e) {
    let fs = e.model.__data.fs;
    console.info('selectFilterSetHandler', fs);
    this.selectFilterSet(fs);

    }

    selectFilterSet(fs) {
        console.info('selectFilterSet', fs);
        this.pills = [];

        for(let f = 0; f < fs.val.length; f++) {
            let fld = fs.val[f];
            let filter = {"name": fld.name, "value": fld.value, field: fld.field, sortKey: fld.sortKey, editable: true};
            this.pills.push(filter);
        }

        this.pills = this.utils._helper_copy(this.pills);
    }

    deleteFilterSet(e) {
        let fs = e.model.__data.fs;
        console.info('deleteFilterSet', fs);

        this.dispatchEvent(new CustomEvent('dr-zp-pill-box-filterset-remove', {
            bubbles: true,
            composed: true,
            detail: {
                filterSet: fs
            }
        }));
    }

  selectLookup(e) {
    if (e &&  e.model.__data) {
      this.selectedLookup = e.model.__data.lookup;
      this.selectedOption = e.model.__data.option;
    }

    var ix = this.optionIsCurrentlyFiltered(this.selectedLookup, this.selectedOption);

    if(ix > -1) {
      var pill = this.pills[ix];

      if(pill.value.indexOf('|') > -1) { //regex OR, split to tokens
        var tokens = pill.value.split('|');
        var newVal = "";

        for(var t = 0; t < tokens.length; t++) {
          var token = tokens[t];

          if(this.selectedOption && token !== this.selectedOption.value && token.length > 0) {
            newVal += token + "|";
          }
        }

        if(newVal.endsWith('|')) {
          newVal = newVal.substring(0, newVal.length - 1);
        }

        pill.value = newVal;
        this.pills = this.utils._helper_copy(this.pills);

        this.dispatchEvent(new CustomEvent('dr-zp-pill-box-lookup-remove', {
          bubbles: true,
          composed: true,
          detail: {
            list: this.pills
          }
        }));
      }
      else {
        this.removePill(ix, true);
      }
    }
    else {
      this.calculateLookupOffset();

      this.dispatchEvent(new CustomEvent('dr-zp-pill-box-lookup-add', {
        bubbles: true,
        composed: true,
        detail: {
          lookup: this.selectedLookup,
          option: this.selectedOption,
        }
      }));
    }
  }

  addPill(e) {
    this.showAddList = true;
  }
}

window.customElements.define('dr-zp-pill-box', Comp);