import { html, PolymerElement } from '../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../core/dr-zp-base-component.js';

class BindableHtml extends BaseComponent {
    static get template() {
        const superTemp = super.template;
        var sdkStyleTemplate = super.styleStore.getSheetStyles("sdk.css");

        var template = html`
            ${superTemp}
            ${sdkStyleTemplate}

            <span style$="[[_computeStyles(display)]]" inner-h-t-m-l="[[value]]"></span> 
        `;

        return template;
    }

    static get properties() {
        return {
            value: {
                type: String
            },
            display: {
                type: String,
                value: 'inline'
            }
        }
    }

    _computeStyles(display) {
        var styles = `display: ${display};`;
        return styles;
    }
}

window.customElements.define('dr-zp-bindable-html', BindableHtml);
