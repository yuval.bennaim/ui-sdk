import moment from '../../vendor/moment/dist/moment-with-locales.js';

export class Utils {

    constructor() {
        if (!Array.prototype.move) { //Polyfill
            Array.prototype.move = function (from, to) {
                this.splice(to, 0, this.splice(from, 1)[0]);
                return this;
            };
        }

        if (!Array.prototype.find) { //Polyfill
            Array.prototype.find = function (predicate) {
                if (this == null) {
                    throw new TypeError('Array.prototype.find called on null or undefined');
                }
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }
                var list = Object(this);
                var length = list.length >>> 0;
                var thisArg = arguments[1];
                var value;

                for (var i = 0; i < length; i++) {
                    value = list[i];
                    if (predicate.call(thisArg, value, i, list)) {
                        return value;
                    }
                }
                return undefined;
            };
        }


        String.prototype.startsWith = function (search, pos, ignoreCase) {
            if (ignoreCase) {
                return this.substr(!pos || pos < 0 ? 0 : +pos, search.length).toLowerCase() === search.toLowerCase();
            }
            else {
                return this.substr(!pos || pos < 0 ? 0 : +pos, search.length) === search;
            }
        };


        if (!String.prototype.endsWith) { //Polyfill
            String.prototype.endsWith = function (search, this_len) {
                if (this_len === undefined || this_len > this.length) {
                    this_len = this.length;
                }
                return this.substring(this_len - search.length, this_len) === search;
            };
        }
    }

    _getSafeValue(fn, defaultVal) {
        try {
            return fn();
        } catch (e) {
            return defaultVal;
        }
    }

    //helpers

    _helper_isEdge(userAgent) {
        var edge = navigator.userAgent.toLowerCase().indexOf('edge') > -1;
        return edge;
    }

    _helper_isFF(userAgent) {
        var ff = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        return ff;
    }

    _helper_isIE11(userAgent) {
        var ie = navigator.userAgent.toLowerCase().indexOf('trident/7.0') > -1;
        return ie;
    }

    _helper_initiateWCImport(path, successCallback, errorCallback) {
        console.info("initiateWCImport", path);
        window.PolymerHelper.importModule(path, successCallback, errorCallback);
    }

    _helper_sanitizeHTML(htmlText) { //remove html tags
        var plainText = new DOMParser().parseFromString(htmlText, 'text/html').body.textContent;
        return plainText;
    }

    _helper_uppercase(input) {
        if (typeof input === "string") {
            return input.toUpperCase();
        }
        else {
            return input;
        }
    }

    _helper_lowercase(input) {
        if (typeof input === "string") {
            return input.toLowerCase();
        }
        else {
            return input;
        }
    }

    _helper_lastToken(input, splitter) {
        if (typeof input === "string" && input !== undefined && input !== null) {
            var tokens = input.split(splitter);
            var last = tokens[tokens.length - 1];
            return last;
        }
        else {
            return input;
        }
    }

    _helper_templateMixin(childElement, parentElement) {
        function getLastStyleNode(template) {
            let styles = template.content.querySelectorAll('style');
            return styles[styles.length - 1];
        }

        let superClass = customElements.get(parentElement);
        var childTemplate = undefined.import(childElement, 'template');
        var parentTemplate = superClass.template.cloneNode(true);
        let sNode = getLastStyleNode(parentTemplate);
        let refNode;

        if (sNode) {
            refNode = sNode.nextSibling;
        } else {
            refNode = parentTemplate.content.firstChild;
        }

        parentTemplate.content.insertBefore(childTemplate.content, refNode);

        return class extends superClass {
            static get template() {
                return parentTemplate;
            }
        }
    }

    _helper_buildQuesryString(obj) {
        var qs = "";

        for (var key in obj) {
            var val = obj[key];

            if (val != null) { //null or undefined
                val = encodeURIComponent(val);
                val = val.replace(/\./g, "\u00B7");
                qs += key + "=" + val + "&";
            }
        }

        if (qs.endsWith('&')) {
            qs = qs.substr(0, qs.length - 1);
        }

        return qs;
    }

    _helper_sanitizeDisplayProperties(obj) {
        var copy = this._helper_copy(obj);

        for (var key in copy) {
            if (key.startsWith("_")) {
                delete copy[key];
            }
        }

        return copy;
    }

    _helper_findAllNodes(node, tagName, resultArr) {
        if (node.tagName != undefined && node.tagName != "STYLE" && node.tagName !== "IFRAME") {

            if (node.tagName.toLowerCase() === tagName.toLowerCase()) {
                resultArr.push(node);
            }
            else {
                var children = node.children;

                if (children.length == 0 && node.root != undefined) { //try the shadow DOM of a WC
                    children = node.root.children;
                }

                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    this._helper_findAllNodes(child, tagName, resultArr);

                    if (i == children.length - 1) {
                        return resultArr;
                    }
                }
            }
        }
    }

    _helper_findNode(node, tagName) {
        if (node.tagName != undefined && node.tagName != "STYLE" && node.tagName !== "IFRAME") {

            if (node.tagName.toLowerCase() === tagName.toLowerCase()) {
                return node;
            }
            else {
                var children = node.children;

                if (children.length == 0 && node.root != undefined) { //try the shadow DOM of a WC
                    children = node.root.children;
                }

                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    return this._helper_findNode(child, tagName);
                }
            }
        }
    }

    _helper_findNodeById(node, id, findCallback) {
        //console.info('_helper_findNodeById', node.id, node);

        if (node.tagName != undefined && node.tagName != "STYLE" && node.tagName !== "IFRAME") {
            if (node.id.toLowerCase() === id.toLowerCase()) {
                findCallback(node);
                return node;
            }
            else {
                var children = node.children;

                if (children.length == 0 && node.root != undefined) { //try the shadow DOM of a WC
                    children = node.root.children;
                }

                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    this._helper_findNodeById(child, id, findCallback);
                }
            }
        }
    }

    _helper_traverseDOM(node, count, max, visitorFunc) {
        if (node.tagName != undefined && node.tagName != "STYLE" && node.tagName !== "IFRAME") {
            visitorFunc(node);

            if (count < max) {
                count++;
                var children = node.children;

                if (children.length == 0 && node.root != undefined) { //try the shadow DOM of a WC
                    children = node.root.children;
                }

                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    return this.traverseDOM(child, count, max, visitorFunc);
                }
            }
            else {
                console.info("exited on MAX: " + max + " count = " + count);
            }
        }
    }

    _helper_isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }

        return true;
    }

    _helper_toArray(obj, valOnly, includeNulls, strings) { //convert an Object properties to a key/val array
        var array = [];

        for (var key in obj) {
            var val = obj[key];
            var str = key;

            if (includeNulls || val != undefined) {
                if (valOnly) {
                    array.push(val);
                }
                else {
                    if (strings !== undefined) {// string replace for key
                        str = strings.getString(key);

                        if (str === null) {
                            str = key;
                        }
                    }

                    array.push({
                        label: str,
                        key: key,
                        val: val
                    });
                }
            }
        }

        return array;
    }

    _helper_arrayToString(arr) {
        var str = "";

        for (var i = 0; i < arr.length; i++) {
            var obj = arr[i];
            var first;

            for (var key in obj) {
                if (first === undefined) {
                    first = key;
                }
            }

            str += obj[first];

            if (i < arr.length - 1) {
                str += ", ";
            }
        }

        return str;;
    }

    _helper_copy(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    _helper_sortObject(o) {
        var sorted = {},
            key, a = [];

        for (key in o) {
            if (o.hasOwnProperty(key)) {
                a.push(key);
            }
        }

        a.sort();

        for (key = 0; key < a.length; key++) {
            sorted[a[key]] = o[a[key]];
        }
        return sorted;
    }

    _helper_compare(objA, objB) {
        objA = this.sortObject(objA);
        objB = this.sortObject(objB);

        var str1 = JSON.stringify(objA);
        var str2 = JSON.stringify(objB);
        var modified = str1 !== str2;
        return modified;
    }

    _helper_date(input, format) {
        var date = new Date(input),
            day = date.getDate(),
            month = date.getMonth() + 1,
            year = date.getFullYear(),
            hours = date.getHours(),
            minutes = date.getMinutes(),
            seconds = date.getSeconds();

        if (!format) {
            format = "MM/dd/yyyy";
        }

        format = format.replace("MM", month.toString().replace(/^(\d)$/, '0$1'));

        var yearHandler = function () {
            if (format.indexOf("yyyy") > -1) {
                format = format.replace("yyyy", year.toString());
            } else if (format.indexOf("yy") > -1) {
                format = format.replace("yy", year.toString().substr(2, 2));
            }
        };
        var hhmmssHandler = function () {
            if (format.indexOf("hh") > -1) {
                if (hours > 12) {
                    hours -= 12;
                }
                if (hours === 0) {
                    hours = 12;
                }
                format = format.replace("hh", hours.toString().replace(/^(\d)$/, '0$1'));
            }
            if (format.indexOf("mm") > -1) {
                format = format.replace("mm", minutes.toString().replace(/^(\d)$/, '0$1'));
            }
            if (format.indexOf("ss") > -1) {
                format = format.replace("ss", seconds.toString().replace(/^(\d)$/, '0$1'));
            }
            format = format.replace("dd", day.toString().replace(/^(\d)$/, '0$1'));
        };
        var timeHandler = function () {
            if (format.indexOf("t") > -1) {
                if (hours > 11) {
                    format = format.replace("t", "pm");
                } else {
                    format = format.replace("t", "am");
                }
            }
            if (format.indexOf("HH") > -1) {
                format = format.replace("HH", hours.toString().replace(/^(\d)$/, '0$1'));
            }
        };

        yearHandler();
        timeHandler();
        hhmmssHandler();
        return format;
    }

    _helper_convertTimeDays(time, seconds) {
        time.second = seconds % 60;
        time.minute = ((seconds - time.second) / 60) % 60;
        time.hour = parseInt(((seconds - time.second - time.minute) / 3600) % 24);
        time.day = parseInt(((seconds - time.hour - time.second - time.minute) / 86400));
    }

    _helper_convertTime(time, seconds) {
        time.second = seconds % 60;
        time.minute = ((seconds - time.second) / 60) % 60;
        time.hour = (seconds - time.second - time.minute * 60) / 3600;
    }

    _helper_getTimeString(time, strings) {
        var timeObj = {};
        this.convertTimeDays(timeObj, time);
        if (strings !== undefined) {
            return (timeObj.day ? timeObj.day + ' ' + strings.getString('quar_aging_unit_days') + ' ' : '')
                + (timeObj.hour ? timeObj.hour + ' ' + strings.getString('hours') + ' ' : '')
                + (timeObj.minute ? timeObj.minute + ' ' + strings.getString('minutes') + ' ' : '')
                + (timeObj.second ? timeObj.second + ' ' + strings.getString('seconds') : '');
        }
        else {
            return "";
        }
    }

    _helper_getValidationPattern(patternName) {
        var patterns = {
            standardString: '^(?:\\s*[A-Za-z0-9~!@#\\$%\\^*()_+{}|:\"?`\\-=[\\]\\\\;\',\\./])*\\s*$',
            domain_or_ipv4: '^[a-zA-Z0-9\\.\\-]+$',
            md5: '^[\\da-fA-F]{32}$',
            password: '^.{6,}$'
        }

        return patterns[patternName];
    }

    _helper_parseQueryParams(queryString) {
        if (!queryString) {
            return null;
        }

        // remove ? in the beginning if passed
        queryString = queryString.substring(0, 1) === '?' ? queryString.substring(1) : queryString;
        let queryParamPairs = queryString.split('&');
        let queryParams = {};

        queryParamPairs.forEach((pairStr) => {
            var keyValue = pairStr.split('='),
                key = keyValue[0],
                value = keyValue[1];

            // if already exists, should be converted to array
            if (queryParams.hasOwnProperty(key)) {
                // make it an array if not already
                if (!_.isArray(queryParams[key])) {
                    queryParams[key] = [queryParams[key]];
                }
                queryParams[key].push(value);
            }
            queryParams[key] = value;
        });

        return queryParams;
    }

    _helper_extractInitials(name) {
        var parts = name.split(' ')
        var initials = ''

        for (var i = 0; i < parts.length; i++) {
            if (parts[i].length > 0 && parts[i] !== '') {
                initials += parts[i][0]
            }
        }

        return initials
    }

    _helper_capitalize(str) {
        return str.toLowerCase().replace(/\b./g, function (a) { return a.toUpperCase(); });
    }

    // Convert category name to kebab case for web component name.
    _helper_kebabCase(str) {
        var kebab = '';
        for (var i = 0; i < str.length; i++) {
            if (str[i] === ' ' || str[i] === '_') {
                kebab += '-';
            }
            else if (str[i] >= 'A' && str[i] <= 'Z') {
                kebab += '-';
                kebab += String(str[i]).toLowerCase();
            }
            else {
                kebab += str[i];
            }
        }
        return kebab;
    }

    /*this.utils._helper_loadStyleSheet( '../../css/some.css', function( success, link ) {
        if ( success ) {
            console.info('success');
        }
        else {
            console.info('failed');
        }
    } ); */

    _helper_loadStyleSheet(path, fn, scope) {
        var head = document.getElementsByTagName('head')[0], // reference to document.head for appending/ removing link nodes
            link = document.createElement('link');           // create the link node
        link.setAttribute('href', path);
        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('type', 'text/css');

        var sheet, cssRules;
        // get the correct properties to check for depending on the browser
        if ('sheet' in link) {
            sheet = 'sheet'; cssRules = 'cssRules';
        }
        else {
            sheet = 'styleSheet'; cssRules = 'rules';
        }

        var interval_id = setInterval(function () {                    // start checking whether the style sheet has successfully loaded
            try {
                if (link[sheet] && link[sheet][cssRules].length) { // SUCCESS! our style sheet has loaded
                    clearInterval(interval_id);                     // clear the counters
                    clearTimeout(timeout_id);
                    fn.call(scope || window, true, link);           // fire the callback with success == true
                }
            } catch (e) { } finally { }
        }, 10),                                                   // how often to check if the stylesheet is loaded
            timeout_id = setTimeout(function () {       // start counting down till fail
                clearInterval(interval_id);            // clear the counters
                clearTimeout(timeout_id);
                head.removeChild(link);                // since the style sheet didn't load, remove the link node from the DOM
                fn.call(scope || window, false, link); // fire the callback with success == false
            }, 15000);                                 // how long to wait before failing

        head.appendChild(link);  // insert the link node into the DOM and start loading the style sheet

        return link; // return the link node;
    }

    getLocaleTime(time) {
        var localePrefix = 'en';

        if (localStorage.getItem("locale") === "Japanese") {
            localePrefix = 'ja';
        }

        try {
            moment.locale(localePrefix);
            return moment(time).format('LLLL');
        }
        catch(error) {
            console.error(error);
        }
    }
}