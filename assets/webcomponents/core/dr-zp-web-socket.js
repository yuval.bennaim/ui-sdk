import {html, PolymerElement} from '../../../vendor/@polymer/polymer/polymer/polymer-element.js';

class PITWWebSocket extends PolymerElement {
    static get template() {
        return html`
            <style>
                :host {
                    display: none;
                }
            </style>
        `;
    }

    static get properties() {
        return {
            socket: {
                type: Object
            },
            route: {
                type: String,
                observer: 'changebserved'
            }
        }
    }

    changebserved(newValue, oldValue) {
        if(this.socket === undefined) {
            var host = top.location.host;
            this.url = "ws://" + host + "/ws/messages";
            this.socket = new WebSocket(this.url, this.protocol);
            this.socket.onerror = this.onError.bind(this);
            this.socket.onopen = this.onOpen.bind(this);
            this.socket.onmessage = this.onMessage.bind(this);
            this.socket.onclose = this.onClose.bind(this);
        }
    }

    onError(error) {
        console.info("onError", event);
    }
    
    onOpen(event) {
        var event = new CustomEvent('socket-open', {});
        this.dispatchEvent(event);
        console.info("Web Socket connection established...");
    }

    onClose(event) {
        console.info("onClose", event);
    }

    onMessage(event) {
        var event = new CustomEvent('socket-message', {detail: {msg: event.data}});
        this.dispatchEvent(event);
    }

    send(message) {
        message = JSON.stringify(message);
        this.socket.send(message);
    }

    close() {
        this.socket.close();
    }
}

window.customElements.define('dr-zp-web-socket', PITWWebSocket);