import { html, PolymerElement } from '../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { Utils } from './dr-zp-utils.js';
import { I18n } from './dr-zp-i18n.js';
import { TokenStore } from './dr-zp-token-store.js';
import { IconStore } from './dr-zp-icon-store.js';
import { StyleStore } from '../core/dr-zp-style-store.js';
import { MockStore } from '../core/dr-zp-mock-store.js';

export class BaseComponent extends PolymerElement {

    constructor() {
        super();
        this.utils = new Utils();
        this.i18n = new I18n();
        this.mockStore = new MockStore();
        this.tokenStore = new TokenStore();
        this.iconStore = new IconStore();
        this.styleStore = new StyleStore();
        this.locale = 0;
    }
    
    connectedCallback() {
        super.connectedCallback();

        document.addEventListener('locale-changed', (e) => {
            this.locale++;
        });
    }

    getAppInstance() {
        if (this.app == undefined) {
            var wrapper = document.querySelector("dr-zp-app-wrapper");
            this.app = wrapper && dom(wrapper.shadowRoot) && dom(wrapper.shadowRoot).querySelector('dr-zp-app');
        }

        return this.app;
    }

    getImagePath(imgPath) {
        let homeModule = this.getAppInstance().selected;
        if (homeModule) {
            return `${homeModule.base_path}/${imgPath}`;
        }
        return "";
    }
    
    static get template() {
        this.styleStore = new StyleStore();
        var stylesStr = this.styleStore.getSheetStyles("dr-zp-base.css");
        var sdkStylesStr = this.styleStore.getSheetStyles("sdk.css");

        return html`      
            ${stylesStr}  
            ${sdkStylesStr}
        `;
    }

    parseModuleSubRoute(sub_route){
        let app = this.getAppInstance();
        var homeView = app.selected.webcomponents.find(plugin => plugin.name === 'home');
        app.shadowRoot.querySelector("#base-module").shadowRoot.querySelector(homeView.webcomponent).parseSubRoute(sub_route);
    }

    getString(key, locale, args0, args1, args2, args3, args4, args5, args6, args7, args8, args9) {
        if (this.i18n === undefined) {
            this.i18n = new I18n();
        }

        return this.i18n.getString(key, args0, args1, args2, args3, args4, args5, args6, args7, args8, args9);
    }
}