import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../../core/dr-zp-bindable-html.js';

class Comp extends BaseComponent {
    static get template() {
        var template = html`

            <dr-zp-bindable-html id="bindable" value="[[svgStr]]" display="[[display]]"></dr-zp-bindable-html>
        `;

        return template;
    }

    static get properties() {
        return {
            display: {
                type: String,
                value: 'inline-block'
            },
            type: {
                type: String,
                observer: 'propChanged'
            },
            size: {
                type: String,
                value: 'md',
                observer: 'propChanged'
            },
            styler: {
                type: String,
                value: '',
                observer: 'propChanged'
            },
            svgStr: {
                type: String,
                value: ''
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    propChanged() {
        if (this.type != null) {
            var svg = this.iconStore.getIcon(this.type);
            this.svgStr = '<span class="dr-zp-icon dr-zp-icon--' + this.size + ' svg hydrated ' + this.styler + '">' + svg + '</span>';
        }
    }
}

window.customElements.define('dr-zp-icon', Comp);