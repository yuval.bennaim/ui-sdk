import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}

            <div role="alert" aria-label="[[label]]" class$="[[_computeSpinnerClass(isInline)]]">
                <div class$="[[_computeSpinnerContainerClass(size)]]">
                    <div class="dr-zp-spinner__ring"></div>
                </div>
                <div class="dr-zp-spinner__label">
                    [[label]]
                </div>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            size: {
                type: String,
                value: 'md'
            },
            label: {
                type: String,
                value: ''
            },
            isInline: {
                type: Boolean,
                value: false
            },
        }
    }

    _computeSpinnerClass(isInline) {
        return `dr-zp-spinner ${this.isInline ? 'dr-zp-spinner--inline' : ''}`;
    }

    _computeSpinnerContainerClass(size) {
        return `dr-zp-spinner__container dr-zp-spinner__container--${this.size}`;
    }
}

window.customElements.define('dr-zp-spinner', Comp);