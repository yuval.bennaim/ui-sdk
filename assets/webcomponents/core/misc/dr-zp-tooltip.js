import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import { Debouncer } from '../../../vendor/@polymer/polymer/lib/utils/debounce.js';
import { timeOut } from '../../../vendor/@polymer/polymer/lib/utils/async.js';
import '../../core/dr-zp-bindable-html.js';

class Comp extends BaseComponent {
    static get template() {
        return html`
            <style>
                .titletx {
                    display: inline;
                    position: relative;
                    top: -4px;
                    left: -23px;
                    background-color: var(--background-overlay, #2c3a4e);
                }
                .titletx-show {
                    opacity: 1;
                }
                .titletx-hide {
                    opacity: 0;
                }
                .tooltip-content {
                    background-color: var(--background-overlay, #2c3a4e);
                    color: var(--text, #e9edf1);
                    padding: 8px;
                    border-radius: 5px;
                    bottom: var(--my-bottom-px);
                    left: var(--my-left-px);
                    position: absolute;
                    z-index: 110;
                    max-width: 900px;
                    width: var(--my-width);
                    white-space: normal;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    pointer-events: none;
                    font-weight: bold;
                    -webkit-box-shadow: 2px 2px 8px rgb(0 0 0 / 25%);
                    box-shadow: 2px 2px 8px rgb(0 0 0 / 25%);
                }
                .titletx-show:before {
                    border: solid;
                    border-color: var(--my-border-color);
                    border-width: var(--my-border-width);
                    content: "";
                    left: var(--my-arrow-left);
                    position: absolute;
                    z-index: 107;
                    top: var(--my-arrow-top);
                }
            </style>
            <aria-label aria-label="[[text]]">
            <span id="tooltip" on-mouseover="mouseOver" on-mouseout="mouseOut" title="[[text]]">
            <slot></slot>
                <span role="text" class$="{{computeTooltipClass(showTooltip)}}">
                    <dr-zp-bindable-html value="[[text]]" class="tooltip-content"></dr-zp-bindable-html>
                </span>
            </span>
            </aria-label>
        `;
    }

    static get properties() {
        return {
            text: {
                type: String
            },
            size: {
                type: String
            },
            tooltipWidth: {
                type: Number,
                value: 200
            },
            showTooltip: {
                type: Boolean,
                value: false
            },
            offset: {
                type: Number,
                value: -55
            },
            placement: {
                type: String,
                value: 'top'
            },
            bottomPx: {
                type: Number,
                value: 8
            },
            borderColor: {
                type: String,
                value: 'var(--background-overlay, #2c3a4e) transparent'
            },
            borderWidth: {
                type: String,
                value: '6px 6px 0 6px'
            },
            arrowLeft: {
                type: Number,
                value: 6
            },
            arrowTop: {
                type: Number,
                value: -8
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
        if (this.size === 'large') {
            this.arrowLeft = 5;
            this.arrowTop = -3;
        }

        if (!this.ie11) {// Using system tooltip for IE11, otherwise remove it
            this.$.tooltip.removeAttribute('title');
        }
    }

    mouseOver(e) {
        if (!this.ie11) {
            this.exited = false;
            var bounds = this.getBoundingClientRect();
            var width = window.innerWidth;
            var off = width - (bounds.x + this.tooltipWidth);
            var tooltip = this.$.tooltip.style;

            tooltip.setProperty('--my-width', '130px');
            this.setArrowsBorders(tooltip);

            if (bounds.x <= 100) {
                this.offset = -100 + bounds.x;

                if (this.placement === 'right' || this.placement === 'left') {
                    this.offset = -40 + bounds.x;
                    this.bottomPx = -27;
                    this.setTooltipRight(tooltip);
                } else if (this.placement === 'bottom') {
                    this.offset = -100 + bounds.x;
                    this.bottomPx = -60;
                    this.setTooltipBottom(tooltip);
                }
            }
            else if (off <= 0) {
                this.offset = -170;

                if (this.placement === 'right' || this.placement === 'left') {
                    this.offset = -236;
                    this.bottomPx = -32;
                    this.setTooltipLeft(tooltip);
                }

                if (this.placement === 'bottom') {
                    this.bottomPx = -60;
                    this.setTooltipBottom(tooltip);
                }

            } else {
                if (this.placement === 'right') {
                    this.offset = 32;
                    this.bottomPx = -27;
                    this.setTooltipRight(tooltip);
                } else if (this.placement === 'bottom') {
                    this.bottomPx = -60;
                    this.setTooltipBottom(tooltip);
                } else if (this.placement === 'left') {
                    this.offset = -152;
                    this.bottomPx = -27;
                    this.setTooltipLeft(tooltip);
                }
            }

            tooltip.setProperty('--my-left-px', `${this.offset}px`);
            tooltip.setProperty('--my-bottom-px', `${this.bottomPx}px`);

            this._debouncer = Debouncer.debounce(this._debouncer, timeOut.after(1000), () => {
                if (!this.exited) {
                    this.showTooltip = true;
                    this.exited = false;
                }
                else {
                    this.exited = false;
                }
            });
        }
    }

    mouseOut(e) {
        this.showTooltip = false;
        this.exited = true;
    }

    setArrowsBorders(tooltip) {
        tooltip.setProperty('--my-border-color', `${this.borderColor}`);
        tooltip.setProperty('--my-border-width', `${this.borderWidth}`);
        tooltip.setProperty('--my-arrow-top', `${this.arrowTop}px`);
        tooltip.setProperty('--my-arrow-left', `${this.arrowLeft}px`);
    }
    setTooltipRight(tooltip) {
        this.borderColor = 'transparent var(--background-overlay, #2c3a4e)';
        this.borderWidth = '6px 6px 6px 0';
        this.arrowLeft = 26;
        this.arrowTop = 6;

        this.setArrowsBorders(tooltip);
    }

    setTooltipBottom(tooltip) {
        this.borderColor = 'var(--background-overlay, #2c3a4e) transparent';
        this.borderWidth = '0px 6px 6px 6px';
        this.arrowTop = 25;

        this.setArrowsBorders(tooltip);
    }

    setTooltipLeft(tooltip) {
        this.borderColor = 'transparent var(--background-overlay, #2c3a4e)';
        this.borderWidth = '6px 0 6px 6px';
        this.arrowLeft = -6;
        this.arrowTop = 6;

        this.setArrowsBorders(tooltip);
    }

    computeTooltipClass() {
        var cls = "titletx";

        if (this.showTooltip) {
            cls += " titletx-show";
        }
        else {
            cls += " titletx-hide";
        }

        return cls;
    }
}

window.customElements.define('dr-zp-tooltip', Comp);