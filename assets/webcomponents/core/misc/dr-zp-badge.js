import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}
      
      <div class$="[[_computeBadgeClass(alert)]]">
        [[label]]
      </div> 
    `;

    return template;
  }

  static get is() { return 'dr-zp-badge'; }

  static get properties() {
    return {
      label: {
        type: String,
        value: ""
      },
      alert: {
        type: Boolean,
        value: false
      }
    }
  }

  _computeBadgeClass(alert) {
    return `dr-zp-badge ${this.alert ? 'dr-zp-badge--alert' : ''}`;
  }
}

window.customElements.define('dr-zp-badge', Comp);