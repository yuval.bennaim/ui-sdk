import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import './dr-zp-icon.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-if.js'

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}

      <style>
        .notification--list{border-top:none!important;border-bottom:none!important;}
        .notification__upper-right-content{align-self:baseline!important;}
      </style>

      <div class$="[[_computeNotificationClass(type, alertType, withIcon, isBanner)]]" aria-live="assertive">
        <template is="dom-if" if="[[showBannerIcon]]">
            <div class$="[[_computerNotificationIconClass(type, alertType)]]">
            <div class="dr-zp-notification__icon__wrapper">
                <dr-zp-icon type="[[iconType]]" size="lg"></dr-zp-icon>
            </div>
            </div>
        </template>

        <div class="dr-zp-notification__content">
            <div class="dr-zp-notification__content__source">
            <div class="dr-zp-notification-source-slot">
                [[source]]
            </div>
            </div>
            <div class="dr-zp-notification__content__message">
            <template is="dom-if" if="[[showListIcon]]">
                <div class$="[[_computerNotificationIconClass(type, alertType)]]">
                <div class="dr-zp-notification__icon__wrapper">
                    <dr-zp-icon type="[[iconType]]" size="md"></dr-zp-icon>
                </div>
                </div>
            </template>
            <div class="dr-zp-notification-message-slot">
                [[message]]
            </div>
            </div>
        </div>

        <template is="dom-if" if="[[_isDismissable(dismissable)]]">
            <button class="dr-zp-notification__upper-right-content dr-zp-notification__close-icon" on-click="closeClicked">
            <dr-zp-icon aria-label="[[getString('sdk-notification-close-label', locale)]]" type="x" size="md"></dr-zp-icon>
            </button>
        </template>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      source: {
        type: String,
        value: ''
      },
      message: {
        type: String,
        value: ''
      },
      type: {
        type: String,
        value: 'banner' // 'list'
      },
      isBanner: {
        type: Boolean,
        computed: '_computeIsBanner(type)'
      },
      dismissable: {
        type: Boolean,
        vale: true,
        observer: 'dismissibleChanged'
      },
      alertType: {
        type: String,
        value: 'success' // 'warning', 'error', 'info'
      },
      iconType: {
        type: String,
        computed: '_computeIconType(alertType)'
      },
      withIcon: {
        type: Boolean,
        value: false
      },
      showBannerIcon: {
        type: Boolean,
        computed: '_computeShowBannerIcon(isBanner, withIcon)'
      },
      showListIcon: {
        type: Boolean,
        computed: '_computeShowListIcon(isBanner, withIcon)'
      },
    }
  }

  constructor() {
    super();
    this.dismissable = true;
  }

  connectedCallback() {
    super.connectedCallback();

    document.addEventListener('escape-action', (e) => {
      this.closeClicked();
    });
  }

  dismissibleChanged() {
    console.log('dismissibleChanged', this.dismissable);
  }

  _isDismissable(dismissable) {
    const isDis = this.dismissable == true;
    console.log('_isDismissable', isDis);
    return isDis;
  }

  _computeNotificationClass(type, alertType, withIcon, isBanner) {
    var cls = `dr-zp-notification dr-zp-notification--${this.type} dr-zp-notification--${this.alertType}`;

    if (!this.withIcon || !this.isBanner) {
      cls += ` dr-zp-notification--${this.alertType}-border`;
    }

    return cls;
  }

  _computerNotificationIconClass(type, alertType) {
    return `dr-zp-notification__icon dr-zp-notification__icon--${this.type} dr-zp-notification__icon--${this.alertType}`
  }

  _computeIsBanner(type) {
    return type === 'banner';
  }

  _computeIconType(alertType) {
    switch (alertType) {
      case 'success':
        return "check-circle";
      case 'warning':
        return "alert-circle";
      case 'error':
        return "alert-triangle";
      case 'info':
        return "info";
      default:
        return "info";
    }
  }

  _computeShowBannerIcon(isBanner, withIcon) {
    return isBanner && withIcon;
  }

  _computeShowListIcon(isBanner, withIcon) {
    return !isBanner && withIcon;
  }

  closeClicked(e) {
    this.dispatchEvent(new CustomEvent('dr-zp-notification-close-clicked', {
      bubbles: true,
      composed: true,
      detail: { target: this }
    }));
  }
}

window.customElements.define('dr-zp-notification', Comp);