import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../dr-zp-base-component.js';
import '../form/dr-zp-button.js';
import './dr-zp-icon.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}
      
      <div role="dialog" aria-modal="true" id="modal" class$="[[computeModalClass(isOpen)]]">
        <div class$="[[computeModalDialogClass(size)]]">
          <div class="dr-zp-modal__content">
            <div class="dr-zp-modal__header">
              <div class="dr-zp-modal__header__title">{{modalTitle}}</div>
              <button id="closebutton" aria-label="[[getString('sdk-close-popup', locale)]]" class="dr-zp-modal__header__close-icon" on-click="cancelClicked">
                <dr-zp-icon type="x" size="lg"></dr-zp-icon>
              </button>
            </div>

            <div class="dr-zp-modal__body">
              <slot name="slot-content"></slot>
            </div>
            <div class="dr-zp-modal__footer">
              <template is="dom-if" if={{showFooter}}>
                <dr-zp-button type="secondary" label="[[cancelLabel]]" on-click="cancelClicked"></dr-zp-button>
                <dr-zp-button type="primary" danger="[[danger]]" label="[[confirmationLabel]]" on-click="confirmationClicked" disabled={{disabled}}></dr-zp-button>
              </template>
            </div>
          </div>
        </div>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      isOpen: {
        type: Boolean,
        value: false,
        observer: 'isOpenChanged'
      },
      size: {
        type: String,
        value: "md"
      },
      modalTitle: {
        type: String,
      },
      cancelLabel: {
        type: String,
      },
      confirmationLabel: {
        type: String,
      },
      danger: {
        type: Boolean,
        value: false,
      },
      showFooter: {
        type: Boolean,
        value: false
      },
      disabled: {
        type: Boolean,
        value: false
      },
      transparent: {
        type: Boolean,
        value: false,
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();

    if (this.originalParent == null) {
      this.originalParent = this.parentElement;
    }

    document.addEventListener('escape-action', this.escapeActionHandler.bind(this));
  }

  escapeActionHandler(e) {
    if (this.isOpen) {
      this.cancelClicked();
    }
  }

  computeModalClass(isOpen) {
    if (this.transparent) {
      return `dr-zp-modal ${this.isOpen ? 'dr-zp-modal--open modal--open--transparent' : ''}`;
    } else {
      return `dr-zp-modal ${this.isOpen ? 'dr-zp-modal--open' : ''}`;
    }
  }

  computeModalDialogClass(size) {
    return `dr-zp-modal__dialog dr-zp-modal__dialog--${this.size}`;
  }

  isOpenChanged() {
    if (this.isOpen) {
      document.dispatchEvent(new CustomEvent("modal-open", {
        detail: {
          source: this
        }
      }));
      this.$.closebutton.focus();
    }
    else if (this.originalParent) {
      document.dispatchEvent(new CustomEvent("modal-close", {
        detail: {
          source: this,
          originalParent: this.originalParent
        }
      }));
    }
    
  }

  cancelClicked(e) {
    this.isOpen = false;

    this.dispatchEvent(new CustomEvent('dr-zp-modal-cancel-clicked', {
      bubbles: true,
      composed: true,
      detail: { isOpen: this.isOpen }
    }));
  }

  confirmationClicked(e) {
    this.isOpen = false;

    this.dispatchEvent(new CustomEvent('dr-zp-modal-confirmation-clicked', {
      bubbles: true,
      composed: true,
      detail: { isOpen: this.isOpen }
    }));
  }
}

window.customElements.define('dr-zp-modal', Comp);