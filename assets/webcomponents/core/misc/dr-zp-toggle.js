import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var template = html`
            <style>
                .toggle {
                    display:block;
                    padding: 5px;
                    color: var(--color-text-white);
                }
                .toggle-checkbox {
                    width: 100%;
                    text-align: right;
                    display: flex;
                    align-items: center;
                    background-color: var(--active-secondary);
                    border: 1px solid var(--color-text-background-grey-eight);
                    border-radius: 24px;
                    padding: 1px;
                    font-weight: bold;
                    color: var(--text--reverse);
                }
                .toggle-checkbox:focus {
                    outline: none!important;
                    border: 1px solid var(--active-secondary);
                    box-shadow: 0 0 4px white;
                }
                .toggle-checkbox--checked {
                    flex-direction: row-reverse;
                    text-align: left;
                    background-color: var(--color-action-primary);
                }
                .toggle-checkbox--disabled {
                    background-color:var(--color-disabled--primary);
                    cursor:not-allowed;
                }
                .toggle-checkbox-label {
                    padding: 0px 10px;
                    width: calc(100%);
                    folt-weight: bold;
                }
                .toggle-circle {
                    border: 1px solid #464646b8;
                    background-color: var(--color-active--tertiary);
                    border-radius: 20px;
                    width: 40px;
                    height: 20px;
                }
            </style>
            
            <div class="toggle">
                <button aria-pressed="[[checked]]" class$="[[_computeToggleCheckboxClass(checked, disabled)]]" disabled="{{disabled}}" on-click="_handleToggleClicked">
                    <div class="toggle-circle">&nbsp;</div>
                    <div class="toggle-checkbox-label">[[toggleLabel]]</div>
                </button>
            </div> 
        `;

        return template;
    }

    static get properties() {
        return {
            offlabel: {
                type: String,
                value: 'Off'
            },
            onlabel: {
                type: String,
                value: 'On'
            },
            toggleLabel: {
                type: String,
                computed: '_computeToggleLabel(checked, offlabel, onlabel)'
            },
            checked: {
                type: Boolean,
                value: false,
                notify: true
            },
            disabled: {
                type: Boolean,
                value: false
            }
        }
    }

    _computeToggleCheckboxClass(checked, disabled) {
        var cls = 'toggle-checkbox'

        if (checked) cls += ' toggle-checkbox--checked';
        if (disabled) cls += ' toggle-checkbox--disabled';

        return cls;
    }

    _computeToggleLabel(checked, offlabel, onlabel) {
        return checked ? onlabel : offlabel;
    }

    _handleToggleClicked(e) {
        this.checked = !this.checked;
        this.dispatchEvent(new CustomEvent('dr-zp-toggle-clicked', {
            detail: {
                checked: this.checked
            }
        }));
    }
}

window.customElements.define('dr-zp-toggle', Comp);