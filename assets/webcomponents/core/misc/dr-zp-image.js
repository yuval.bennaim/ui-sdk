import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var sdkStyleTemplate = super.styleStore.getSheetStyles("sdk.css");

        return html`
            ${sdkStyleTemplate}

            <template is="dom-if" if="[[hasSrc(src)]]">
                <img src="[[src]]" class$="[[getStyler(styler)]]" alt="[[alt]]" />
            </template>
        `;
    }

    static get properties() {
        return {
            src: {
                type: String,
                value: null
            },
            styler: {
                type: String,
                value: ''
            },
            alt: {
                type: String,
                value: ''
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    getStyler(styler) {
        return this.styler;
    }

    hasSrc(src) {
        return src != null;
    }
}

window.customElements.define('dr-zp-image', Comp);