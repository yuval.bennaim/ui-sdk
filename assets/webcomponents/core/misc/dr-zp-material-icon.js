import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
        ${superTemp}
            <span class="material-icons" style="[[styler]]">[[type]]</span>
        `;

        return template;
    }

    static get properties() {
        return {
            display: {
                type: String,
                value: 'inline-block'
            },
            type: {
                type: String,
            },
            styler: {
                type: String,
                value: ''
            },
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }
}

window.customElements.define('dr-zp-material-icon', Comp);