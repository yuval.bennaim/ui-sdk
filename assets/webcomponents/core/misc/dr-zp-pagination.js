import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import './dr-zp-icon.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}
  
      <div class="dr-zp-numeric-pagination">

        <template is="dom-if" if="[[perPageOpt]]">
          <span class="dr-zp-numeric-pagination__per-page dr-zp-numeric-pagination__page-count-num">
            <span class="dr-zp-numeric-pagination__per-page-label">
              [[generateUnitsPerString(locale, unit)]]
            </span>
            <template is="dom-repeat" items=[[perPageOpt]]>
              <button
                class$="[[_computeActivePerPageClass(item, increment)]]"
                pg-num$="[[item]]"
                on-click="_perPageChangeHandler"
                aria-label="[[_perPageLabel(item)]] [[generateUnitsPerString(locale, unit)]]"
              >
                [[_perPageLabel(item)]]
              </button>
            </template>
          </span>
        </template>

        <span class="dr-zp-numeric-pagination__page-count">
          <span class="dr-zp-numeric-pagination__page-count-num">
            [[generatePagerString(activePage, increment, total, totalPages, locale, unit)]]
          </span>
        </span>

        <span class="dr-zp-numeric-pagination__page-change">
          <button
            on-click="_pageChangeHandler"
            on-focusout="buttonFocusOut"
            pg-num="1"
            class$="[[_computeChangePageButtonClass(activePage, 1)]]"
            disabled$="[[_computeDisabledButton(activePage, 1)]]"
            aria-label = "GoTo First Page"
          >
            <dr-zp-icon display="flex" type="chevrons-left"></dr-zp-icon>
          </button>

          <button
            on-click="_pageChangeHandler"
            on-focusout="buttonFocusOut"
            pg-num$="[[prevPage]]"
            class$="[[_computeChangePageButtonClass(activePage, 1)]]"
            disabled$="[[_computeDisabledButton(activePage, 1)]]"
            aria-label = "GoTo Previous Page"
          >
            <dr-zp-icon display="flex" type="chevron-left"></dr-zp-icon>
          </button>

          <template is="dom-repeat" items=[[pageNumbers]]>
            <button
              aria-pressed="false"
              class$="[[_computeActivePageClass(item, activePage)]]"
              pg-num$="[[item]]"
              on-click="_pageChangeHandler"
              on-focusout="buttonFocusOut"
              aria-label="Page [[item]]">
              [[item]]
            </button>
          </template>

          <button
            on-click="_pageChangeHandler"
            on-focusout="buttonFocusOut"
            pg-num$="[[nextPage]]"
            class$="[[_computeChangePageButtonClass(activePage, totalPages)]]"
            disabled$="[[_computeDisabledButton(activePage, totalPages)]]"
            aria-label = "GoTo Next Page">
            <dr-zp-icon display="flex" type="chevron-right"></dr-zp-icon>
          </button>

          <button
            on-click="_pageChangeHandler"
            on-focusout="buttonFocusOut"
            pg-num$="[[totalPages]]"
            class$="[[_computeChangePageButtonClass(activePage, totalPages)]]"
            disabled$="[[_computeDisabledButton(activePage, totalPages)]]"
            aria-label = "GoTo Last Page">
            <dr-zp-icon display="flex" type="chevrons-right"></dr-zp-icon>
          </button>
        </span>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      activePage: {
        type: Number,
        value: 1
      },
      prevPage: {
        type: Number,
        computed: "_computePrevPage(activePage)"
      },
      nextPage: {
        type: Number,
        computed: "_computeNextPage(activePage)"
      },
      pageNumbers: {
        type: Array,
        computed: "_computerPageNumbers(activePage, totalPages)"
      },
      increment: {
        type: Number,
        value: 1,
        observer: 'incrementChanged'
      },
      total: {
        type: Number
      },
      unit: {
        type: String,
        value: 'sdk-units'
      },
      totalPages: {
        type: Number,
        computed: "_computeTotalPages(increment, total)"
      },
      perPageOpt: {
        type: Array
      }
    }
  }

  buttonFocusOut(e) {
    var btn = e.target.closest('button');
    btn.setAttribute("aria-pressed", false);
  }

  incrementChanged() {
    this.activePage = 1;
  }

  _computePrevPage(activePage) {
    return activePage - 1;
  }

  _computeNextPage(activePage) {
    return activePage + 1;
  }

  _computerPageNumbers(activePage, totalPages) {
    var pages = [];
    // Set up default +/-
    var numForward = activePage + 2;
    var numBack = activePage - 2;
    // Adjust if we're towards the beginning or the end
    if (numBack <= 0) numForward -= numBack - 1; // Off by one error because we don't use zero
    if (numForward > totalPages) numBack -= numForward - totalPages;
    // Create the actual page set to display
    for (var i = numBack; i <= numForward && i <= totalPages; i++) {
      if (i > 0 && i <= totalPages) pages.push(i);
    }
    return pages;
  }

  _computeTotalPages(increment, total) {
    return increment === -1 ? 1 : Math.ceil(total / increment);
  }

  generateUnitsPerString(locale, unit) {
    var unitLocale = this.getString(unit, this.locale) || unit;
    var perPageStr = this.getString('sdk-rows-per-page', locale, unitLocale);
    return perPageStr;
  }

  generatePagerString(activePage, increment, total, totalPages, locale, unit) {
    let str = '';

    if (total != null) {
      var first = (activePage - 1) * increment + 1;
      var last = activePage * increment;
      if (increment === -1) last = 1;
      if (activePage === totalPages) last = total;

      var pageStr = this.getString('sdk-page');
      var ofStr = this.getString('sdk-of');
      var unitLocale = this.getString(unit, this.locale) || unit;

      str = `${pageStr} ${this.activePage} ${ofStr} ${totalPages} (${total} ${unitLocale})`;
    }

    return str;
  }

  _computeChangePageButtonClass(activePage, firstOrLastPage) {
    var cls = 'dr-zp-numeric-pagination__page-btn';
    if (activePage == firstOrLastPage) cls += ' dr-zp-numeric-pagination__page-btn--disabled';
    return cls;
  }

  _computeDisabledButton(activePage, firstOrLastPage) {
    return activePage == firstOrLastPage;
  }

  _computeActivePageClass(page, activePage) {
    var cls = 'dr-zp-numeric-pagination__page-btn';
    if (activePage === page) cls += ' dr-zp-numeric-pagination__page-btn--active';
    return cls;
  }

  _computeActivePerPageClass(perPage, increment) {
    var cls = 'dr-zp-numeric-pagination__per-page-btn';
    if (increment === perPage) cls += ' dr-zp-numeric-pagination__per-page-btn--active';
    return cls;
  }

  _perPageLabel(perPage) {
    return perPage === -1 ? this.getString('sdk-all') : perPage;
  }

  _pageChangeHandler(e) {
    var btn = e.target.closest('button');
    var number = parseInt(btn.getAttribute('pg-num'));
    btn.setAttribute("aria-pressed", true);
    if (number > 0 && number <= this.totalPages) {
      this.activePage = number;
      this.dispatchEvent(new CustomEvent('pagination-page-change', {
        bubbles: true,
        composed: true,
        detail: { page: number }
      }));
    }
  }

  _perPageChangeHandler(e) {
    var number = parseInt(e.target.getAttribute('pg-num'));
    this.increment = number;
    if (this.activePage > this.totalPages) this.activePage = 1;
    this.dispatchEvent(new CustomEvent('pagination-per-page-change', {
      bubbles: true,
      composed: true,
      detail: { perPage: number }
    }));
  }
}

window.customElements.define('dr-zp-pagination', Comp);