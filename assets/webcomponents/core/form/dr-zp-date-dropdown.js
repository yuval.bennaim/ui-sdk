import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../form/dr-zp-calendar.js';
import moment from '../../../vendor/moment/dist/moment.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
        ${superTemp}

        <style>
            .dropdown {
                min-width: 120px;
            }
            .dropdown-list {
                background-color: transparent !important;
                overflow: hidden !important;
            }
            .close-icon {
                position: absolute;
                top: 4px;
                right: 4px;
            }
        </style>

        <div class$="{{computedClass(opened)}}">
            <button class$="[[computeButtonClass(dropdowntype)]]" on-mouseleave="closeDropdown" on-click="_clickHandler" disabled="{{disabled}}" title="[[getString('sdk-dropdown-button-tooltip', locale)]]" aria-label="[[getLabel(date)]]">
                <span data-label="label">[[getDataValue(date)]]</span>
                <span class="dropdown-icon">
                    <dr-zp-icon display="flex" type="calendar"></dr-zp-icon>
                </span>
            </button>
            <div class="dropdown-list" on-mouseleave="closeDropdown">
                <dr-zp-calendar id="calendar" date="[[date]]"></dr-zp-calendar>
            </div>
        </div>
        `;

        return template;
    }

    static get properties() {
        return {
            dropdown: { type: String },
            field: { type: String },
            opened: {
                type: Boolean,
                value: false
            },
            date: {
                type: Object,
                notify: true
            },
            dropdowntype: {
                //this is to speciy the type of the dropdown box. the possible value could be
                // "link", "icon", "bordered" and "default" , if no dropdowntype is specifyed, then the type is "default"
                type: String,
                value: "default"
            },
            disabled: {
                type: Boolean,
                value: false
            },
            fullWidth: {
                type: Boolean,
                value: false
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        document.addEventListener('escape-action', this.escapeActionHandler.bind(this));

        this.$.calendar.addEventListener('dr-zp-calendar-date-changed', e => {
            var inputDate = moment(e.detail.date).format('L');
            this.date = e.detail.date;
        });
    }

    getLabel(date) {
        var dateLabel = moment().format("Do MMMM YYYY");
        if(date) {
            dateLabel = moment(date).subtract(1, 'months').format("Do MMMM YYYY");
        }
        return dateLabel;
    }

    escapeActionHandler(e) {
        if (this.opened) {
            this.set('opened', false);
        }
    }

    _clickHandler(e) {
        this.set('opened', !this.opened);
    }

    computedClass(opened) {
        var cls = "dr-zp-dropdown dropdown";

        if (this.fullWidth) {
            cls += " full-width";
        }

        if (opened) {
            cls += " open";
        }

        return cls;
    }
    computeButtonClass(dropdowntype) {
        var cls = "full-width dr-zp-btn";
        switch (dropdowntype) {
            case 'link':
                cls += ' dr-zp-btn--link';
                break;
            case 'bordered':
                cls += ' dr-zp-btn--hollow';
                break;
            default:
                cls += ' btn-primary';
        }
        return cls;
    }

    getDataValue(date) {
        if (date) {
            var display = moment([date.year, date.month - 1, date.day]).format('L');
            return display;
        }


    }
    closeDropdown(e) {
        if (this.opened && e.relatedTarget != undefined) {
            var parent = e.relatedTarget.parentElement;

            if (parent === null || parent.className.indexOf('dropdown-list') === -1 && parent.className.indexOf('dropdown') === -1) {
                this.set('opened', false);
            }
        }
    }
}

window.customElements.define('dr-zp-date-dropdown', Comp);