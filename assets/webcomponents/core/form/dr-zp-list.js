import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import { dom } from '../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
class Comp extends BaseComponent {

    static get template() {
        const superTemp = super.template;

        var template = html`
            ${superTemp}
        
            <div tabindex="0" role="listbox" class="dropdown-list-open dropdown-list-scrollable">
                <template is="dom-repeat" items="[[items]]">
                   <button role="option" aria-selected$="[[computeAriaPressed(index, selecteditem, selectable)]]" disabled$="[[isDisabled(item)]]" class$="[[computeListClass(index, selecteditem, selectable)]]" style="padding: 10px;" on-click="handleItemClick">
                        <template is="dom-if" if="[[item.icon]]">
                            <dr-zp-icon type="[[item.icon]]" size="sm" style="position: relative; left: -5px;"></dr-zp-icon>
                        </template>
                        
                        <span style="width: calc(100% - 15px); text-align: left;">[[getDataValue(item, locale)]]</span>

                        <template is="dom-if" if="[[item.children]]">
                            <dr-zp-icon type="chevron-right" size="sm" style="position: relative; left: 10px; top: 1px;"></dr-zp-icon>
                        </template>
                    </button>
                </template>
            </div>
            
        `;

        return template;
    }

    static get properties() {
        return {
            items: {
                type: Array
            },
            field: { type: String },
            opened: {
                type: Boolean,
                value: false,
            },
            selecteditem: {
                type: Object,
                value: null,
                notify: true
            },
            selectable: {
                type: Boolean,
                value: true
            },
            localized: {
                type: Boolean,
                value: false
            },
            scrollable: {
                type: Boolean,
                value: false
            },
            listStyle: {
                type: String,
                value: null
            },
            fullWidth: {
                type: Boolean,
                value: false
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        document.addEventListener('locale-changed', this.localeChangedHandler.bind(this));
    }

    localeChangedHandler(e) {
        this.locale++;
    }

    isDisabled(item) {
        if(item.enabled == null) {
            return false;
        }
        else {
            return !item.enabled;
        }
    }

    handleItemClick(e) {
        var item = e.model.__data.item;
        console.info('handleItemClick', item);
        this.selecteditem = item
        this.dispatchEvent(new CustomEvent('list-change', {
            bubbles: true,
            composed: true,
            detail: {
                selected: item
            }
        }));
    }
    computeAriaPressed(index, selecteditem, selectable) {
        var ariaVal = "false";

        if (this.selectable && this.items[index] === selecteditem) {
            ariaVal = "true";
        }
        return ariaVal;
    }
    computeListClass(index, selecteditem, selectable) {
        var cls = "dr-zp-list-item list-item full-width";

        if (this.selectable && this.items[index] === selecteditem) {
            cls += " active";
        }
        return cls;
    }
    computeListStyle(listStyle) {
        if (listStyle) {
            return listStyle;
        }

        return null;
    }

    getDataValue(item, locale) {
        if (item != null) {
            var display = item;

            if (this.field != null) {
                display = item[this.field];
            }

            if (display != undefined && display.startsWith && display.startsWith('%')) {
                var key = display.substring(1);
                display = this.getString(key, this.locale) || key;
            }

            if (this.localized) { //similar to above, the values are I18n keys
                display = this.getString(display, this.locale) || display;
            }

            return display;
        }
    }
}

window.customElements.define('dr-zp-list', Comp);