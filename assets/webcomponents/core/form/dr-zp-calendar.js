import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import './dr-zp-dropdown.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import moment from '../../../vendor/moment/dist/moment.js';

class Calendar extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}

      <style>
        .dropdown {
          width: 100%;
          display: inline-grid;
        }
        .calendar {
          width: 100%;
          display: inline-block;
          background:var(--color-background-primary);
          padding:0;
        }
        .calendar-table {
          background: var(--color-background-primary);
          padding: 15px;
        }
        .calendar th {
          padding: 6px;
        }
        .calendar td:hover {
          border: 1px solid var(--color-action-primary);
        }
        .calendar td button:focus {
          outline: 0px solid transparent !important;
          outline-offset: 0;
          box-shadow: none !important;
          color:var(--color-text-primary) !important;
        }
        .calendar td:focus-within {
          border-color:var(--color-action-primary);
          color:var(--color-action-primary);
          border: 1px solid var(--color-action-primary);
        }
        .table-calender {
          width: 100%;
        }
      </style>


      <div id="[[id]]" class="dr-zp-calendar calendar">
        <div class="calendar-table">
          <table class="table-calender">
            <thead>
              <tr class="dr-zp-calendar__weekday-names">
                <th colspan="4" class="month">
                  <dr-zp-dropdown localized id="monthsDD" aria-label="[[getString('sdk-calendar-selected-month', locale, selectedMonth.text)]]" class="dropdown" list-style="max-height:200px;" dropdowntype="bordered" items="[[months]]" field="text" selecteditem="{{selectedMonth}}"></dr-zp-dropdown>
                </th>
                <th colspan="3" class="month">
                  <dr-zp-dropdown localized id="yearsDD" aria-label="[[getString('sdk-calendar-selected-year', locale, selectedYear.text)]]" class="dropdown" list-style="max-height:200px;" dropdowntype="bordered" items="[[years]]" field="text" selecteditem="{{selectedYear}}"></dr-zp-dropdown>
                </th>
              </tr>

              <tr class="dr-zp-calendar__weekday-names">
                <template is="dom-repeat" items={{daysOfWeek}}>
                  <th class="dr-zp-calendar__weekday-names__weekday-name">{{item}}</th>
                </template>
              </tr>
            </thead>

            <tbody>
              <template is="dom-repeat" items="{{calendar}}" as="row" index-as="row_id">
                <tr class="dr-zp-calendar__week">
                  <template is="dom-repeat" items="{{row}}" as="col" index-as="col_id">
                    <td id="calendarCell_{{row_id}}_{{col_id}}" class$="{{computeDateActive(col,inc)}}">
                      <button aria-selected="[[getSelected(col,inc)]]" aria-label="[[getDayLabel(col)]]" on-click="dateClicked">{{col.day}}</button>
                    </td>
                  </template>
                </tr>
              </template>
            </tbody>
          </table>
        </div>
      </div>
    `;

    return template;
  }

  static get properties() {
    return {
      id: {
        type: String,
        value: 'dr-zp-calendar'
      },
      date: {
        type: Object,
        observer: '_dateChanged'
      },
      daysOfWeek: {
        type: Object,
        computed: 'computeDaysOfWeek()'
      },
      calendar: {
        type: Array,
        value: []
      },
      yearMonth: {
        type: Object,
      },
      months: {
        type: Array,
      },
      years: {
        type: Array
      },
      selectedMonth: {
        type: String
      },
      selectedYear: {
        type: String
      },
      type: {
        type: String,
        value: 'Input'
      },
      inc: {
        type: Number,
        value: 0
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();
    this.calcMonthsYears();
    if (this.type && this.type == 'single') {
      this.type = 'selected';
    }
    this.type = this.type.charAt(0).toUpperCase() + this.type.slice(1);
    this.yearMonth = {
      year: moment().get('year'),
      month: moment().get('month')
    };

    if (this.date && moment([this.date.year, this.date.month - 1, this.date.day])._isValid) {
      this.yearMonth = {
        year: this.date.year,
        month: this.date.month - 1
      }
    } else {
      var start = moment();
      this.date = {
        year: start.get('year'),
        month: start.get('month') + 1,
        day: start.get('date'),
        hour: start.get('hour'),
        minute: start.get('minute'),
        second: start.get('second')
      };

      setTimeout(() => {
        this.dispatchEvent(new CustomEvent("dr-zp-calendar-date-changed", {
          bubbles: true,
          composed: true,
          detail: {
            date: this.date,
            id: this.id
          }
        }));
      }, (0));
    }

    this.$.monthsDD.addEventListener('dropdown-change', e => {
      console.info('dropdown-change', e.detail.selected);
      this.yearMonth.month = e.detail.selected.val;
      this.date.month = this.yearMonth.month;
      this.date = this.utils._helper_copy(this.date);
      this.buildCalendar();
      this.calcMonthsYears();
    });

    this.$.yearsDD.addEventListener('dropdown-change', e => {
      console.info('dropdown-change', e.detail.selected);
      this.yearMonth.year = e.detail.selected.val;
      this.date.year = this.yearMonth.year;
      this.date = this.utils._helper_copy(this.date);
      this.buildCalendar();
      this.calcMonthsYears();
    });

    this.buildCalendar();

  }

  getDayLabel(col) {
    return moment(col).format("Do MMMM YYYY");
  }

  getSelected(col, inc) {
    var tabind = false;
    if (this.date && col.date.format('L') == moment([this.date.year, this.date.month - 1, this.date.day]).format('L')) {
      tabind = true;
    }
    return tabind;
  }

  computeDaysOfWeek() {
    return [0, 1, 2, 3, 4, 5, 6].map(function (n) { return moment().day(n).format("dd"); });
  }

  _dateChanged(newVal, oldVal) {
    if (newVal) {
      if (!moment([newVal.year, newVal.month - 1, newVal.day])._isValid) {
        if (oldVal && oldVal.year) {
          this.date = this.utils._helper_copy(oldVal);
        }
      } else {
        if (this.months !== undefined) {
          this.selectedMonth = this.months.find((month) => {
            return month.val === newVal.month;
          });
        }
        if (this.years !== undefined) {
          this.selectedYear = this.years.find((year) => {
            return year.val === newVal.year;
          });
        }
      }
      this.yearMonth = {
        year: this.date.year,
        month: this.date.month - 1
      };
    } else {
      this.calcMonthsYears();
      var start = moment();
      this.date = {
        year: start.get('year'),
        month: start.get('month') + 1,
        day: start.get('date'),
        hour: start.get('hour'),
        minute: start.get('minute'),
        second: start.get('second')
      };
      this.yearMonth = {
        year: moment().get('year'),
        month: moment().get('month')
      };
    }

    this.inc++;
    this.buildCalendar();

    if (this.date != null) {
      setTimeout(() => {
        this.dispatchEvent(new CustomEvent("dr-zp-calendar-date-changed", {
          bubbles: true,
          composed: true,
          detail: {
            date: this.date,
            id: this.id
          }
        }));
      }, (0));
    }
  }

  calcMonthsYears() {
    this.months = [
      { val: 1, text: 'sdk-january' },
      { val: 2, text: 'sdk-february' },
      { val: 3, text: 'sdk-march' },
      { val: 4, text: 'sdk-april' },
      { val: 5, text: 'sdk-may' },
      { val: 6, text: 'sdk-june' },
      { val: 7, text: 'sdk-july' },
      { val: 8, text: 'sdk-august' },
      { val: 9, text: 'sdk-september' },
      { val: 10, text: 'sdk-october' },
      { val: 11, text: 'sdk-november' },
      { val: 12, text: 'sdk-december' },
    ];

    var tmp = [];
    for (var i = moment().year() - 5; i <= moment().year() + 5; i++) {
      tmp.push({ val: i, text: i });
    }
    this.years = tmp;
  }

  // generateOptionElements(select, list, defValue) {
  //   if (select) {
  //     (list).forEach(item => {
  //       var option = document.createElement('option');
  //       option.textContent = item.text;
  //       option.value = item.val;
  //       option.style.color = '#333';
  //       select.appendChild(option);
  //     });
  //     select.value = defValue;
  //   }
  // }

  buildCalendar() {
    this.calendar = [];
    if (this.yearMonth) {
      var firstOfMonth = moment([this.yearMonth.year, this.yearMonth.month, 1]);
      var firstDayOfMonthOffset = firstOfMonth.day();
      var columns = [];

      for (var i = 0; i < 6 * 7; i++) {
        var date = firstOfMonth.clone().add(i - firstDayOfMonthOffset, 'days')
        if (i % 7 == 0) {
          columns = [];
        }
        columns.push({
          date: date,
          year: date.year(),
          month: date.month(),
          day: date.date(),
          identifier: date.format("YYYY-MM-DD"),
        });
        if ((i + 1) % 7 == 0) this.calendar.push(columns);
      }
    }
  }

  dateClicked(e) {
    var col = e.model.__data.col;
    var old;

    if (this.date) {
      old = this.date;
    } else {
      old = {
        hour: 0,
        minute: 0,
        second: 0
      };
    }

    this.date = {
      year: col.year,
      month: col.month + 1,
      day: col.day,
      hour: old.hour,
      minute: old.minute,
      second: old.second
    };
  }

  computeDateActive(col, inc) {
    if (col) {
      if (this.date && col.date.format('L') == moment([this.date.year, this.date.month - 1, this.date.day]).format('L')) {
        return "dr-zp-calendar__week__day selected";
      }

      if (col.month !== this.yearMonth.month) {
        return "dr-zp-calendar__week__day disabled";
      }
    }
    return "dr-zp-calendar__week__day";
  }
}

window.customElements.define('dr-zp-calendar', Calendar)