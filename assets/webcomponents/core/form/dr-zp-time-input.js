import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { Debouncer } from '../../../vendor/@polymer/polymer/lib/utils/debounce.js';
import { timeOut } from '../../../vendor/@polymer/polymer/lib/utils/async.js';
import './dr-zp-numeric-input.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}

      <style>
        .separator{padding: 0 8px;}
        dr-zp-numeric-input{width:5em;}
        dr-zp-numeric-input#show-time{width:6em;}
        .dr-zp-date-range-picker__datetime-inputs__start-inputs__time-wrapper {justify-content: initial;}
      </style>
      
      <div id="[[id]]" class="dr-zp-date-range-picker__datetime-inputs__start-inputs__time-wrapper">
        <dr-zp-numeric-input
          id="hours-input"
          class="dr-zp-input-wrapper--time"
          label="[[getString('sdk-hours', locale)]]"
          min="0"
          max="23"
          value="{{hour::input-changed}}"
          disabled$="[[disabled]]"
        ></dr-zp-numeric-input>
        <div class="separator">:</div>
        <dr-zp-numeric-input 
          id="minutes-input"
          class="dr-zp-input-wrapper--time"
          label="[[getString('sdk-minutes', locale)]]"
          min="0" 
          max="59" 
          value="{{minute::input-changed}}"
          disabled$="[[disabled]]"
        ></dr-zp-numeric-input>
        <div class="separator">:</div>
        <dr-zp-numeric-input
          id="seconds-input"
          class="dr-zp-input-wrapper--time"
          label="[[getString('sdk-seconds', locale)]]"
          min="0" 
          max="59" 
          value="{{second::input-changed}}"
          disabled$="[[disabled]]"
        ></dr-zp-numeric-input>
        <div class="separator"></div>
        <dr-zp-numeric-input
          id="show-time"
          class="dr-zp-input-wrapper--time"
          label="[[getString('sdk-time', locale)]]"
          value="[[displaytime]]" 
          readonly="true" 
          hidden$="{{!showTime}}"
        ></dr-zp-numeric-input>
      </div> 
    `;

    return template;
  }

  static get properties() {
    return {
      id: {
        type: String,
        value: 'dr-zp-time-input'
      },
      hour: {
        type: Number,
        value: 0,
        observer: "changeHour"
      },
      minute: {
        type: Number,
        value: 0,
        observer: "changeMinute"
      },
      second: {
        type: Number,
        value: 0,
        observer: "changeSecond"
      },
      showTime: {
        type: Boolean,
        value: false
      },
      disabled: {
        type: Boolean,
        value: false
      }
    }
  }

  changeHour() {
    this.hour = Math.max(this.hour, 0);
    this.hour = Math.min(this.hour, 23);
    this.changeTime();
  }

  changeMinute() {
    this.minute = Math.max(this.minute, 0);
    this.minute = Math.min(this.minute, 59);
    this.changeTime();
  }

  changeSecond() {
    this.second = Math.max(this.second, 0);
    this.second = Math.min(this.second, 59);
    this.changeTime();
  }

  changeTime() {
    this.displaytime = parseInt(this.hour * 3600 + this.minute * 60 + this.second);

    this._debouncer = Debouncer.debounce(this._debouncer, timeOut.after(1000), () => {
      var event = new CustomEvent('time-input-change', {
        bubbles: true,
        composed: true,
        detail: {
          id: this.id,
          hour: this.hour,
          minute: this.minute,
          second: this.second,
          value: this.displaytime
        }
      });
      this.dispatchEvent(event);
    });

  }

  reset() {
    this.hour = 0;
    this.minute = 0;
    this.second = 0;
  }
}

window.customElements.define('dr-zp-time-input', Comp);