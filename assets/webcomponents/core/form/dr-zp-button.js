import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../../core/misc/dr-zp-material-icon.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-if.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}

            <button id="button" disabled="[[disabled]]" type="button" class$="[[determineButtonClass(type, size, fullWidth)]]" on-keypress="buttonKeyPressed" on-click="buttonClicked">
                <template is="dom-if" if="[[hasIcon(icon)]]">  
                   <dr-zp-material-icon type="[[icon]]" styler="font-size: 16px; position: relative; top: 3px;"></dr-zp-material-icon>
                </template>

                <template is="dom-if" if="[[hasLabel(label)]]">
                    [[getLabelValue(label, locale)]]
                </template>            

                <slot name="slot_content"></slot>
            </button>
        `;

        return template;
    }

    static get properties() {
        return {
            id: { type: String, value: '' },
            data: { type: Object, value: null },
            label: { type: String, value: null },
            type: { type: String, value: 'primary' },
            danger: { type: Boolean, value: false },
            icon: { type: String, value: null },
            size: { type: String, value: 'medium' },
            fullWidth: { type: Boolean, value: false },
            disabled: { type: Boolean, value: false }
        }
    }

    connectedCallback() {
        super.connectedCallback();
    }

    getLabelValue(label, locale) {
        if (this.label != null) {
            var display = this.label;

            if (display != undefined && display.startsWith && display.startsWith('%')) {
                var key = display.substring(1);
                display = this.getString(key, this.locale).toUpperCase();
            }
            return display;
        }
    }

    determineButtonClass(type, size, fullWidth) {
        var cls = "dr-zp-btn dr-zp-btn--solid";

        if (type === undefined || type === 'primary') {
            if (this.danger) {
                cls += " dr-zp-btn--danger-solid";
            }
            else {
                cls += " dr-zp-btn--primary";
            }
        }
        else if (type === 'secondary') {
            if (this.danger) {
                cls += " dr-zp-btn--danger-hollow";
            }
            else {
                cls += " dr-zp-btn--hollow";
            }
        }
        else if (type === 'link') {
            if (this.danger) {
                cls += " dr-zp-btn--danger-link";
            }
            else {
                cls += " dr-zp-btn--link";
            }
        }

        if (size === undefined || size === 'medium') {
            cls += " dr-zp-btn--md";
        }
        else if (size === 'small') {
            cls += " dr-zp-btn--sm";
        }
        else if (size === 'large') {
            cls += " dr-zp-btn--lg";
        }

        if (fullWidth) {
            cls += " full-width";
        }

        return cls;
    }

    hasIcon(icon) {
        return this.icon !== null;
    }

    hasLabel(label) {
        return this.label !== null;
    }

    buttonKeyPressed(e) {
    }

    buttonClicked() {

        this.dispatchEvent(new CustomEvent("dr-zp-button-click", {
            detail: {
                source: this,
                data: this.data
            }
        }));
    }
}

window.customElements.define('dr-zp-button', Comp);