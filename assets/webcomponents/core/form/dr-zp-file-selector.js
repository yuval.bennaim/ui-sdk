import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}

      <style>
        input[type="file"] {
          display: none;
        }
        .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }
      </style>
      
      <div id="dropArea" class$="{{_computeInputClass(is_onfocus, hasValidationError)}}" on-drop="_handleDrop">
        <button id="fileUpload" aria-role="file" aria-describedby="message" aria-label$="[[labelUpdate(label, files)]]" aria-invalid$="[[isValid(hasValidationError)]]" class="custom-file-upload" on-click="openFileDialog">

          <template is="dom-if" if="[[hasIcon(icon)]]">
            <dr-zp-icon type="[[icon]]" size="sm" style="position: relative; top: 3px;"></dr-zp-icon>
          </template>

          [[label]]

          <input
            id="fileInput"
            type="file"
            files="{{files}}"
            class="input"
            on-focus="_onFocus"
            on-blur="_onBlur"
            disabled$="{{disabled}}"
            multiple$="[[multiple]]"
          />
        </button>

        <div class="input dr-zp-field__highlight">
          <div class="content"></div>
        </div>
        <div id="message" class="dr-zp-field__hint">
          {{message}}
        </div>
      </div> 
    `;

    return template;
  }

  static get properties() {
    return {
      files: {
        type: FileList
      },
      is_onfocus: {
        type: Boolean,
        value: false
      },
      label: {
        type: String,
        value: ""
      },
      icon: {
        type: String,
        value: null
      },
      message: {
        type: String,
        value: ""
      },
      multiple: {
        type: Boolean,
        value: false
      },
      hasValidationError: {
        type: Boolean,
        value: false
      },
      disabled: {
        type: Boolean,
        value: false
      }
    }
  }

  connectedCallback() {
    super.connectedCallback();
    var fileInput = this.$.fileInput;

    if (fileInput) {
      fileInput.onchange = (e) => {
        this._filesSelected(e.target.files);
      };
    }

    var dropArea = this.$.dropArea;

    ['dragenter', 'dragover', 'dragleave'].forEach(eventName => {
      dropArea.addEventListener(eventName, this._preventDefaults, false)
      document.body.addEventListener(eventName, this._preventDefaults, false)
    });
  }
  isValid(hasValidationError) {
    return hasValidationError ? "true" : "false";
  }

  openFileDialog(e) {
    this.$.fileInput.click();
  }

  hasIcon(icon) {
    return this.icon != null;
  }

  _computeInputClass(is_onfocus, hasValidationError) {
    var cls = "fileInput dr-zp-field";

    if (is_onfocus) {
      cls = `${cls} focus`;
    }

    if (hasValidationError) {
      cls = `${cls} danger`;
    }

    return cls;
  }

  _onFocus(e) {
    this.is_onfocus = true;
    this.$.fileInput.select();
  }

  _onBlur(e) {
    if (!this.files) {
      this.is_onfocus = false;
    }
  }

  labelUpdate(label, files) {
    var selected = this.getString("sdk-selected", this.locale);
    var no = this.getString("sdk-no", this.locale);
    var files = this.getString("sdk-files", this.locale);
    var file = this.getString("sdk-file", this.locale);
    var labeledVal = `${no} ${files} ${selected}, ${label}`;

    if (files) {
      if (this.files && this.files.length > 0) {
        if (this.files.length === 1) {
          labeledVal = `${this.files[0].name} ${file} ${selected}`;
        } else {
          labeledVal = `${this.files.length} ${files} ${selected}`;
        }
      }
    }
    return labeledVal;
  }

  _filesSelected(files) {
    this.files = files;
    if (this.files && this.files.length > 0) this._onFocus();

    this.dispatchEvent(new CustomEvent('file-selection-changed', {
      bubbles: true,
      composed: true,
      detail: { files: this.files }
    }));
  }

  _preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
  }

  _handleDrop(e) {
    this._preventDefaults(e);
    var files = e.dataTransfer.files;
    this.dragOver = false;

    if (files.length > 0) {
      this._filesSelected(files);
    }
  }
}

window.customElements.define('dr-zp-file-selector', Comp);