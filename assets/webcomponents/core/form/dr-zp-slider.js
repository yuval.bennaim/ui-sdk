import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../../core/misc/dr-zp-icon.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-if.js';

class Comp extends BaseComponent {

    static get template() {
        var superTemp = super.template;
        
        var template = html`
            ${superTemp}

            <div class="dr-zp-input-wrapper dr-zp-input-wrapper--range dr-zp-input-wrapper__element">
                <template is="dom-if" if="[[isLabel(label)]]">
                    <label class="dr-zp-input__label">
                        {{label}}
                    </label>
                </template>
                <input class$="{{_computeInputClass(orient)}}" style$="{{_computeInputStyle(percent)}}"
                    type="range"
                    min="{{min}}"
                    max="{{max}}"
                    step="{{step}}"
                    orient$="{{orient}}"
                    value="{{value::input}}"
                    disabled$="{{disabled}}"
                    id="myRange"
                    aria-valuemin="{{min}}"
                    aria-valuenow="{{value::input}}"
                    aria-valuemax="{{max}}"
                    aria-label=[[label]]
                    role="slider"
                    tabindex="0"
                />

                <template is="dom-if" if="[[isShowValue(show)]]">
                    <label class="dr-zp-input__label dr-zp-label">
                        [[generateValueLabel(valueLabel, value, locale)]]
                    </label>
                </template>
            </div>
        `;

        return template;
    }
    static get properties() {
        return {
            min: {
                type: Number,
                value: 0
            },
            max: {
                type: Number,
                value: 100
            },
            value: {
                type: Number,
                notify: true,
                observer: "_valueChanged",
                value: 0
            },
            valueLabel: {
                type: String,
                value: "sdk-value"
            },
            disabled: {
                type: Boolean,
                value: false
            },
            step: {
                type: Number,
                value: 1
            },
            label: {
                type: String,
                value: ""
            },
            show: {
                type: Boolean,
                value: false
            },
            orient: {
                type: String,
                value: ""
            }
        }
    }

    _valueChanged() {
        if (!this.disabled) {
            var event = new CustomEvent('slider-change', {
                bubbles: true,
                composed: true,
                detail: {
                    value: this.value
                }
            });
            this.dispatchEvent(event);
        }

        this.percent = this.value / this.max * 100;
    }

    generateValueLabel(valueLabel, value, locale) {
        return (this.getString(this.valueLabel, this.locale) || this.valueLabel) + " = " + Math.round(this.value);
    }

    isLabel(label) {
        return this.label;
    }

    isShowValue(show) {
        return this.show;
    }

    _computeInputClass(orient) {
        var cls = "dr-zp-input";

        if (this.orient == "vertical") {
            cls = "dr-zp-input";
        }

        return cls;
    }

    _computeInputStyle(percent) {
        if (!this.disabled) {
            //var str = `background: linear-gradient(to right, var(--color-active--primary) 0%, var(--color-active--primary) ${this.percent}%, var(--color-disabled--primary) ${this.percent}%, var(--color-disabled--primary) 100%)`;
            var str = ``;
            return str;
        }
    }
}

window.customElements.define('dr-zp-slider', Comp);