import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}
      <style>
        #input{margin-top:10px}
        label {font-weight: bold;}
      </style>
      
      <div class$="{{_computeInputClass(is_onfocus, hasValidationError)}}">
        <label>
          {{label}}
          <input
            id="input"
            type="{{input_type}}"
            class="dr-zp-input"
            value="{{value::input}}"
            pattern="^[0-9]*"
            min="[[min]]"
            max="[[max]]"
            step="[[step]]"
            on-focus="_onFocus"
            on-blur="_onBlur"
            autocomplete$="{{autocomplete}}"
            readonly$="{{readonly}}"
            disabled$="{{disabled}}"
            aria-describedby="error"
            aria-invalid$="[[isValid(hasValidationError)]]"
          />
        </label>

        <div class="input dr-zp-field__highlight">
          <div class="content"></div>
        </div>
        <div id="error" class="dr-zp-field__hint">
          {{message}}
        </div>
      </div> 
    `;

    return template;
  }

  static get is() { return 'dr-zp-numeric-input'; }

  static get properties() {
    return {
      value: {
        type: Number,
        notify: true,
        value: "",
        observer: "_valueChanged"
      },
      input_type: {
        type: String,
        value: "number"
      },
      label: {
        type: String,
        value: ""
      },
      message: {
        type: String,
        value: ""
      },
      min: {
        type: Number
      },
      max: {
        type: Number
      },
      step: {
        type: Number,
        value: 1
      },
      key: {
        type: String
      },
      autocomplete: {
        type: String,
        value: "off"
      },
      readonly: {
        type: Boolean,
        value: false
      },
      hasValidationError: {
        type: Boolean,
        value: false
      },
      is_onfocus: {
        type: Boolean,
        value: false
      },
      disabled: {
        type: Boolean,
        value: false
      }
    }
  }

  isValid(hasValidationError) {
    return hasValidationError ? "true" : "false";
  }

  _computeInputClass(is_onfocus, hasValidationError) {
    var cls = "dr-zp-field dr-zp-input-wrapper--number";

    if (is_onfocus) {
      cls = `${cls} focus`;
    }

    if (hasValidationError) {
      cls = `${cls} danger`;
    }

    return cls;
  }

  _onFocus(e) {
    this.is_onfocus = true;
    this.$.input.select();
  }

  _onBlur(e) {
    if (!this.value) {
      this.is_onfocus = false;
    }
  }

  _valueChanged() {
    if (this.value) {
      this.is_onfocus = true;
      this.hasValidationError = this.value < this.min || this.value > this.max;
    }

    var event = new CustomEvent('input-changed', {
      bubbles: true,
      composed: true,
      detail: {
        value: this.value
      }
    });

    this.dispatchEvent(event);
  }
}

window.customElements.define('dr-zp-numeric-input', Comp);