import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import './dr-zp-calendar.js';
import './dr-zp-input.js';
import './dr-zp-time-input.js';
import './dr-zp-checkbox.js';
import './dr-zp-button.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import moment from '../../../vendor/moment/dist/moment.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}

      <style>
        .calendars {
          display:flex;
          justify-content: space-evenly;
          border-bottom: 1px solid var(--color-border);
        }
        .datepicker {
          background:var(--color-background-primary);
        }
        .datepicker-form {
          padding: 16px;
        }
        .timezone {
          display:flex;
          align-items: baseline;
        }
        dr-zp-input {
          padding-right: 8px;
        }
      </style>
      
      <div class="dr-zp-datepicker datepicker">
        <div class="calendars">
          <dr-zp-calendar id="start-date-calendar" date="{{startDate}}"></dr-zp-calendar>
          <dr-zp-calendar id="end-date-calendar" date="{{endDate}}"></dr-zp-calendar>
        </div>
        <div class="datepicker-form dr-zp-datepicker__inputs">
          <div class="dr-zp-datepicker__inputs__datetime-inputs">
            <div class="dr-zp-datepicker__inputs__datetime-inputs">
              <dr-zp-input id="start-date" label="[[getString('sdk-start-date', locale)]]" value="[[startDateString]]" readonly="true"></dr-zp-input>
              <dr-zp-time-input
                id="start-time-input"
                hour="{{startDate.hour}}"
                minute="{{startDate.minute}}"
                second="{{startDate.second}}"
              ></dr-zp-time-input>
            </div>
            <div class="dr-zp-datepicker__inputs__datetime-inputs">
              <dr-zp-input id="end-date" label="[[getString('sdk-end-date', locale)]]" value="[[endDateString]]" readonly="true"></dr-zp-input>
              <dr-zp-time-input
                id="end-time-input"
                hour="{{endDate.hour}}"
                minute="{{endDate.minute}}"
                second="{{endDate.second}}"
              ></dr-zp-time-input>
            </div>
          </div>
          <div class="timezone">
            <dr-zp-input label="[[getString('sdk-time-sandard', locale)]]" value="{{timezone}}"></dr-zp-input>
            <dr-zp-checkbox class="dr-zp-datepicker__inputs__datetime-inputs__checkbox" checked="{{checked}}" label="[[getString('sdk-set-as-default', locale)]]"></dr-zp-checkbox>
          </div>
          <div class="dr-zp-datepicker__buttons">
            <dr-zp-button class="dr-zp-datepicker__buttons__cancel" id="cancel" type="secondary" label="[[getString('sdk-cancel', locale)]]" on-click="cancelClicked"></dr-zp-button>
            <dr-zp-button class="dr-zp-datepicker__buttons__apply" id="apply" type="primary" label="[[getString('sdk-apply', locale)]]" on-click="applyClicked"></dr-zp-button>
          </div>
        </div>
        
      </div> 
    `;

    return template;
  }

  static get properties() {
    return {
      startDate: {
        type: Object,
        observer: '_startDateChanged',
        notify: true
      },
      endDate: {
        type: Object,
        observer: '_endDateChanged',
        notify: true
      },
      startDateString: {
        type: String,
        computed: "_computeDateString(startDate)"
      },
      endDateString: {
        type: String,
        computed: "_computeDateString(endDate)"
      },
      timezone: {
        type: String,
        value: ''
      },
      checked: {
        type: Boolean,
        value: false
      },
    }
  }



  connectedCallback() {
    super.connectedCallback();
    if (!this.startDate) {
      var start = moment();
      this.startDate = {
        year: start.get('year'),
        month: start.get('month') + 1,
        day: start.get('date'),
        hour: start.get('hour'),
        minute: start.get('minute'),
        second: start.get('second')
      };
    }

    if (!this.endDate) {
      var end = moment().add(1, 'month');
      this.endDate = {
        year: end.get('year'),
        month: end.get('month') + 1,
        day: end.get('date'),
        hour: end.get('hour'),
        minute: end.get('minute'),
        second: end.get('second')
      };
    }

    document.addEventListener('dr-zp-calendar-date-changed', (e) => {
      var { id, date } = e.detail;
      if (id === 'start-date-calendar') this.startDate = date;
      if (id === 'end-date-calendar') this.endDate = date;
    });

    // this.$.startDateCalendar.addEventListener('dr-zp-calendar-date-changed', (e) => {
    //   var date = e.detail.date;
    //   var id = e.target.id
    //   if (id === 'startDateCalendar') this.startDate = date;
    // });
    // this.$.endDateCalendar.addEventListener('dr-zp-calendar-date-changed', (e) => {
    //   var date = e.detail.date;
    //   var id = e.target.id
    //   if (id === 'endDateCalendar') this.endDate = date;
    // });

    document.addEventListener('time-input-change', (e) => {
      var { id, hour, minute, second } = e.detail;
      if (id === 'start-time-input') {
        this.startDate = { ...this.startDate, hour, minute, second };
      }
      if (id === 'end-time-input') {
        this.endDate = { ...this.endDate, hour, minute, second };
      }
    });
  }
  _startDateChanged(newVal, oldVal) {
    if (newVal) {
      if (!moment([newVal.year, newVal.month - 1, newVal.day])._isValid) {
        if (oldVal && oldVal.year) {
          this.startDate = this.utils._helper_copy(oldVal);
        }
      }
    }
  }

  _endDateChanged(newVal, oldVal) {
    if (newVal) {
      if (!moment([newVal.year, newVal.month - 1, newVal.day])._isValid) {
        if (oldVal && oldVal.year) {
          this.endDate = this.utils._helper_copy(oldVal);
        }
      }
    }
  }
  _computeDateString(date) {
    if (date) {
      var selectedDateStr = moment([date.year, date.month - 1, date.day]).format('L');
      return selectedDateStr;

    }
  }

  cancelClicked(e) {
    this.dispatchEvent(new CustomEvent('dr-zp-datepicker-cancel-clicked', {
      bubbles: true,
      composed: true,
      detail: { action: 'cancel' }
    }));
  }

  applyClicked(e) {
    this.dispatchEvent(new CustomEvent('dr-zp-datepicker-apply-clicked', {
      bubbles: true,
      composed: true,
      detail: {
        action: 'apply',
        startDate: this.startDate,
        endDate: this.endDate,
        timezone: this.timezone,
        checked: this.checked
      }
    }));
  }
}

window.customElements.define('dr-zp-datepicker', Comp);