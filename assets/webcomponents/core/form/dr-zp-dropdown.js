import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../misc/dr-zp-icon.js';

class Comp extends BaseComponent {
    
    static get template() {
        const superTemp = super.template;

        var template = html`
            ${superTemp}

            <div class$="{{computedClass(opened)}}">
                <button class$="[[computeButtonClass(dropdowntype)]]" style$="[[computeButtonStyle(buttonStyle)]]" on-mouseleave="closeDropdown" on-click="_clickHandler" 
                    disabled="{{disabled}}" title="[[getString('sdk-dropdown-button-tooltip', locale)]]" aria-haspopup="listbox" 
                    aria-selected$="[[selecteditem]]" tabindex="0" aria-label="[[getAriaLabel(selecteditem)]]">
                    <template is="dom-if" if="[[isType(dropdowntype, 'icon')]]">
                        <dr-zp-icon display="flex" type="[[icon]]"></dr-zp-icon>
                    </template>

                    <template is="dom-if" if="[[!isType(dropdowntype, 'icon')]]">
                        <template is="dom-if" if="[[selecteditem.icon]]">
                            <dr-zp-icon type="[[selecteditem.icon]]" size="sm" style="position: relative; top: 3px; left: -5px;"></dr-zp-icon>
                        </template>
                        
                        <span data-label="label" class="dropdown-label truncated">
                            <template is="dom-if" if="[[!hasSelectedItem(selecteditem)]]">
                                [[getString('sdk-select', locale)]]
                            </template>
                            <template is="dom-if" if="[[hasSelectedItem(selecteditem)]]">
                                [[getDataValue(selecteditem, locale)]]
                            </template>
                        </span>
                        <span class$="[[computeIconClass(dropdowntype)]]">
                            <dr-zp-icon display="flex" type="chevron-down"></dr-zp-icon>
                        </span>
                    </template>
                </button>

                <div class$="[[computeListClass(scrollable)]]" style$="[[computeListStyle(listStyle)]]" on-mouseleave="closeDropdown" id="item_list_div" tabindex="-1" role="listbox" aria-label="dropdown">
                    <template is="dom-repeat" items="[[items]]">
                        <button id$="option[[index]]" class="dr-zp-list-item--dropdown-item dr-zp-list-item dropdown-item full-width" on-click="handleItemClick" role="option" aria-selected$="[[getSelected(item,selecteditem)]]">
                            <template is="dom-if" if="[[item.icon]]">
                                <dr-zp-icon type="[[item.icon]]" size="sm" style="position: relative; left: -5px;"></dr-zp-icon>
                            </template>
                            
                            <span style="width: calc(100% - 15px); text-align: left;">[[getDataValue(item, locale)]]</span>

                            <template is="dom-if" if="[[item.children]]">
                                <dr-zp-icon type="chevron-right" size="sm" style="position: relative; left: 10px; top: 1px;"></dr-zp-icon>
                            </template>
                        </button>
                    </template>
                </div>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            name: {
                type: String,
                value: 'fe-dropdown'
            },
            dropdown: { type: String },
            items: {
                type: Array,
                observer: 'itemsChanged'
            },
            field: { type: String },
            opened: {
                type: Boolean,
                value: false
            },
            selecteditem: {
                type: Object,
                value: null,
                notify: true
            },
            localized: {
                type: Boolean,
                value: false
            },
            dropdowntype: {
                //this is to speciy the type of the dropdown box. the possible value could be
                // "link", "icon", "bordered" and "default" , if no dropdowntype is specifyed, then the type is "default"
                type: String,
                value: "default"
            },
            icon: {
                type: String
            },
            disabled: {
                type: Boolean,
                value: false
            },
            scrollable: {
                type: Boolean,
                value: false
            },
            listStyle: {
                type: String,
                value: null
            },
            buttonStyle: {
                type: String,
                value: null
            },
            fullWidth: {
                type: Boolean,
                value: false
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        document.addEventListener('escape-action', this.escapeActionHandler.bind(this));
        document.addEventListener('locale-changed', this.localeChangedHandler.bind(this));
    }

    getSelected(item, selecteditem) {
        var sel = "false";
        if(selecteditem === item) {
            sel = "true"
        }
        return sel;
    }
    
    getAriaLabel (selecteditem) {
        var dropdownLabel = this.getString("sdk-select-dropdown", this.locale);
        if(this.hasSelectedItem(selecteditem)) {
            dropdownLabel = `${this.getDataValue(selecteditem, this.locale)} ${this.getString("sdk-selected", this.locale)}`;
        }
        return dropdownLabel;
    }
    
    selectedOptionId(item, selecteditem) {
        return selecteditem == item && "selected_option";
    }

    localeChangedHandler(e) {
        this.locale++;
    }

    itemsChanged() {
        //console.info('itemsChanged', this.items);
    }

    hasSelectedItem(selecteditem) {
        return this.selecteditem != null;
    }

    escapeActionHandler(e) {
        if (this.opened) {
            this.set('opened', false);
        }
    }

    _clickHandler(e) {
        this.set('opened', !this.opened);
    }

    handleItemClick(e) {
        //once any item in the dropdown list is clicked, the selecteditem value would be changed to the one
        //that is just cliced; also will dispatch a "dropdown" event
        e.stopPropagation();
        var item = e.model.__data.item;

        if (item.children != null && item.children.length > 0) { //drill in to children
            if (this.drillStack == null) {
                this.drillStack = [];
            }

            var orig = this.utils._helper_copy(this.items);
            this.drillStack.push(orig);
            this.items = this.utils._helper_copy(item.children);
            this.items.unshift({ name: this.getString('sdk-back', this.locale) + ' [' + this.drillStack.length + "]", type: 'back', icon: 'chevron-left' });
        }
        else if (item.type === 'back') {
            var prevItems = this.drillStack.pop();
            this.items = this.utils._helper_copy(prevItems);
        }
        else {
            this.selecteditem = item;
            this.opened = false;

            this.dispatchEvent(new CustomEvent('dropdown-change', {
                bubbles: true,
                composed: true,
                detail: {
                    selected: item
                }
            }));
        }
    }

    computedClass(opened) {
        var cls = "dr-zp-dropdown dropdown ";

        if (this.fullWidth) {
            cls += " full-width";
        }

        if (opened) {
            cls += " open";
        }

        return cls;
    }

    isType(dropdowntype, type) {
        return this.dropdowntype === type;
    }

    computeListClass(scrollable) {
        var cls = "dr-zp-dropdown-item dropdown-list";

        if (scrollable) {
            cls += ' dropdown-list-scrollable';
        }

        if (this.dropdowntype === 'bordered') {
            cls += ' dropdown-list-bordered';
        }

        return cls;
    }

    computeButtonStyle(buttonStyle) {
        if (buttonStyle) {
            return buttonStyle;
        }

        return null;
    }

    computeListStyle(listStyle) {
        if (listStyle) {
            return listStyle;
        }

        return null;
    }

    computeIconClass(dropdowntype) {
        var cls = "dropdown-icon";

        if (dropdowntype === 'link') {
            cls += '-link';
        }

        return cls;
    }

    computeButtonClass(dropdowntype) {
        var cls = "full-width dr-zp-btn dr-zp-btn--solid btn-dropdown";

        switch (dropdowntype) {
            case 'icon':
                cls += ' dr-zp-btn--link';
                break;
            case 'link':
                cls += ' dr-zp-btn--link';
                break;
            case 'bordered':
                cls += ' dr-zp-btn--hollow';
                break;
            default:
                cls += ' btn-primary';
        }
        return cls;
    }

    getDataValue(item, locale) {
        if (item != null) {
            var display = item;

            if (this.field != null) {
                display = item[this.field];
            }

            if (display != undefined && display.startsWith && display.startsWith('%')) {
                var key = display.substring(1);
                display = this.getString(key, this.locale);
            }

            if (this.localized) { //similar to above, the values are I18n keys
                display = this.getString(display, this.locale) || display;
            }

            return display;
        }
    }

    closeDropdown(e) {
        if (this.opened && e.relatedTarget != undefined) {
            var parent = e.relatedTarget.parentElement;

            if (parent === null || parent.className.indexOf('dropdown-list') === -1 && parent.className.indexOf('dropdown') === -1) {
                this.set('opened', false);
            }
        }
    }
}

window.customElements.define('dr-zp-dropdown', Comp);