import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}
      <style>
        #input{margin-top:10px; font-size: 12px;}
        label {font-weight: bold;}
        .dr-zp-field {font-size: 12px;}
        </style>
    
      <div class$="{{_computeInputClass(is_onfocus, hasValidationError)}}">
        <label>
          {{label}}
          <textarea
            id="input"
            cols="{{cols}}"
            rows="{{rows}}"
            class="dr-zp-input"
            value="{{value::input}}"
            on-focus="_onFocus"
            on-blur="_onBlur"
            disabled$="{{disabled}}"
            aria-multiline="true"
            aria-describedby="error"
            aria-invalid$="[[isValid(hasValidationError)]]"
            style$="[[determineTextareStyle(noresize)]]"
          ></textarea>
        </label>

        <div class="input dr-zp-field__highlight">
          <div class="content"></div>
        </div>
        <div id="error" class="dr-zp-field__hint">
          {{message}}
        </div>
      </div> 
    `;

    return template;
  }

  static get is() { return 'dr-zp-textarea'; }

  static get properties() {
    return {
      value: {
        type: String,
        value: "",
        notify: true,
        observer: "_valueChanged"
      },
      noresize: {
        type: Boolean,
        value: false
      },
      is_onfocus: {
        type: Boolean,
        value: false
      },
      label: {
        type: String,
        value: ""
      },
      message: {
        type: String,
        value: ""
      },
      hasValidationError: {
        type: Boolean,
        value: false
      },
      disabled: {
        type: Boolean,
        value: false
      },
      key: {
        type: String
      },
      cols: {
        type: Number,
        value: 20
      },
      rows: {
        type: Number,
        value: 2
      }
    }
  }

  isValid(hasValidationError) {
    return hasValidationError ? "true" : "false";
  }

  _computeInputClass(is_onfocus, hasValidationError) {
    var cls = "dr-zp-field dr-zp-input-wrapper--textarea";

    if (is_onfocus) {
      cls = `${cls} focus`;
    }

    if (hasValidationError) {
      cls = `${cls} danger`;
    }

    return cls;
  }

  _onFocus(e) {
    this.is_onfocus = true;
    this.$.input.select();
  }

  _onBlur(e) {
    if (!this.value) {
      this.is_onfocus = false;
    }
  }

  determineTextareStyle(noresize) {
      if(noresize) {
          return "resize: none";
      }
  }

  _valueChanged() {
    this.is_onfocus = this.value && this.value.length > 0;

    if (this.is_onfocus) {
      var event = new CustomEvent('textarea-change', {
        bubbles: true,
        composed: true,
        detail: {
          value: this.value.trim()
        }
      });
      this.dispatchEvent(event);
    }
  }
}

window.customElements.define('dr-zp-textarea', Comp);