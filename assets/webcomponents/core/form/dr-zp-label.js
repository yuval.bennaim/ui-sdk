import { html } from "../../../vendor/@polymer/polymer/polymer-element.js";
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-if.js';

class Comp extends BaseComponent {

  static get template() {
    const superTemp = super.template;

    var template = html`
        ${superTemp}

          <style>
            .trunc{
            position: absolute;
              left: 0;
              right: 0;
              white-space: nowrap;  
              text-overflow: ellipsis;
              overflow: hidden;	
            }	
            .label-inner {	
              white-space: nowrap;	
              width: 100%;	
            }	
          </style>

          <div tabindex="0" class$="[[determineLabelClass(isTransparent)]]">
              <template is="dom-if" if="[[hasLabel(label)]]">
                <div class="trunc" title="[[label]]">
                  [[label]]
                </div>
              </template>      
          </div>
        `;

    return template;
  }

  static get properties() {
    return {
      id: { type: String, value: "" },
      isTransparent: { type: Boolean, value: false },
      label: { type: String, value: null }
    };
  }

  connectedCallback() {
    super.connectedCallback();
  }

  hasLabel(label) {
    return this.label !== null;
  }

  determineLabelClass(isTransparent) {
    var cls = "dr-zp-label label-inner";
    if (!isTransparent) cls += " dr-zp-label--background";
    return cls;
  }
}

window.customElements.define("dr-zp-label", Comp);
