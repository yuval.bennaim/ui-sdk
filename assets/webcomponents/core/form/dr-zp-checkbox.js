import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-if.js'
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../misc/dr-zp-icon.js';

class Comp extends BaseComponent {

    static get template() {
        const superTemp = super.template;

        var template = html`
            ${superTemp}

            <div class="checkbox-wrapper" on-click="_checkboxClicked">

                <template is="dom-if" if="[[isMixed(mixed)]]">
                    <button role="checkbox" aria-checked="[[checked]]" class="checkbox-button" disabled="[[disabled]]" aria-label="[[label]]">
                        <dr-zp-icon class="checkbox-checked" type="cb-mixed" size="sm"></dr-zp-icon>
                    </button>
                </template>

                <template is="dom-if" if="[[!isMixed(mixed)]]">

                    <button role="checkbox" aria-checked="[[checked]]" class="checkbox-button" disabled="[[disabled]]" aria-label="[[label]]">
                        <template is="dom-if" if="[[isChecked(checked)]]">
                            <dr-zp-icon class="checkbox-checked" type="cb-checked" size="sm"></dr-zp-icon>
                        </template>
                        <template is="dom-if" if="[[!isChecked(checked)]]">
                            <dr-zp-icon class="checkbox-unchecked" type="cb-empty" size="sm"></dr-zp-icon>
                        </template>
                    </button>
                </template>

                <span>[[label]]</span>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            label: {
                type: String,
            },
            multiselect: {
                type: Boolean,
                value: false
            },
            disabled: {
                type: Boolean,
                value: false
            },
            mixed: {
                type: Boolean,
                value: false
            },
            checked: {
                type: Boolean,
                value: false,
                notify: true
            }
        }
    }

    isChecked(checked) {
        return this.checked;
    }

    isMixed(mixed) {
        return this.mixed;
    }
    _checkboxClicked() {

        if (!this.disabled) {
            this.checked = !this.checked;

            this.dispatchEvent(new CustomEvent('checkbox-clicked', {
                bubbles: true,
                composed: true,
                detail: {
                    checked: this.checked
                }
            }));
        }
    }

}

window.customElements.define('dr-zp-checkbox', Comp);
