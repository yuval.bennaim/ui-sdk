import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../dr-zp-base-component.js';

class Comp extends BaseComponent {
  static get template() {
    var superTemp = super.template;
    var template = html`
      ${superTemp}

      <style>
        #input{margin-top:10px;}
        label {font-weight: bold;}
        .dr-zp-field {}
      </style>
      
      <div class$="{{_computeInputClass(is_onfocus, hasValidationError)}}">
        <label>
          {{label}}
          <input
            id="input"
            role="inputfield"
            type="{{input_type}}"
            class="dr-zp-input"
            value="{{value::input}}"
            placeholder="{{placeholder}}"
            on-focus="_onFocus"
            on-blur="_onBlur"
            readonly$="{{readonly}}"
            disabled$="{{disabled}}"
            aria-describedby="error"
            aria-invalid$="[[isValid(hasValidationError)]]"
          />
        </label>

        <div class="input dr-zp-field__highlight">
          <div class="content"></div>
        </div>
        <div id="error" class="dr-zp-field__hint">
          {{message}}
        </div>
      </div> 
    `;

    return template;
  }

  static get is() { return 'dr-zp-input'; }

  static get properties() {
    return {
      value: {
        type: String,
        value: "",
        notify: true,
        observer: "_valueChanged"
      },
      input_type: {
        type: String,
        value: "text"
      },
      is_onfocus: {
        type: Boolean,
        value: false
      },
      label: {
        type: String,
        value: ""
      },
      placeholder: {
        type: String,
        value: ""
      },
      message: {
        type: String,
        value: ""
      },
      readonly: {
        type: Boolean,
        value: false
      },
      key: {
        type: String
      },
      hasValidationError: {
        type: Boolean,
        value: false
      },
      disabled: {
        type: Boolean,
        value: false
      }
    }
  }

  isValid(hasValidationError) {
    return hasValidationError ? "true" : "false";
  }

  _computeInputClass(is_onfocus, hasValidationError) {
    var cls = "dr-zp-field dr-zp-input-wrapper--text";

    if (is_onfocus) {
      cls = `${cls} focus`;
    }

    if (hasValidationError) {
      cls = `${cls} danger`;
    }

    return cls;
  }

  _onFocus(e) {
    this.is_onfocus = true;
    this.$.input.select();
  }

  _onBlur(e) {
    if (!this.value) {
      this.is_onfocus = false;
    }
  }

  _valueChanged() {
    this.is_onfocus = this.value && this.value.length > 0;

    var event = new CustomEvent('input-changed', {
      bubbles: true,
      composed: true,
      detail: {
        value: this.value
      }
    });

    this.dispatchEvent(event);
  }
}

window.customElements.define('dr-zp-input', Comp);