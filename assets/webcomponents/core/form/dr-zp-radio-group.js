import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';


class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;
        var template = html`
            ${superTemp}
            <div style$="[[determineStyle(items, direction)]]" role="radiogroup" tabindex="0" aria-label="Radio Group">
                <template is="dom-repeat" items="[[items]]" on-dom-change="domChanged">
                    <div class$="[[computeCheckedClass(item, selectedItem)]]" on-click="handleItemClick">
                        <div class="dr-zp-radio__checkmark"><div class="dr-zp-radio__checkmark__dot"></div></div>
                        <input id="item_[[index]]" name="[[name]]" value="[[item]]" type="radio" class="dr-zp-radio__input" aria-checked="[[getCheck(item, selectedItem)]]" tabindex$="[[getTabIndex(item, selectedItem)]]" aria-label="[[getDataValue(item, locale)]]">
                        <label for="item_[[index]]" class="dr-zp-radio__label"><span>[[getDataValue(item, locale)]]</span></label>
                    </div>
                </template>
            </div>
        `;

        return template;
    }

    static get properties() {
        return {
            items: {
                type: Array,
                value: []
            },
            selectedItem: {
                type: Object,
                notify: true,
                observer: 'selectedChanged'
            },
            direction: {
                type: String,
                value: 'horizontal'
            },
            name: {
                type: String
            },
            localized: {
                type: Boolean,
                value: false
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.selectedIcon = '';
    }

    getCheck(item, selectedItem) {
        var checked = false;
        if (this.selectedItem === item) {
            checked = true
        }
        return checked;
    }

    getTabIndex(item, selectedItem) {
        var indexVal = -1;
        if (this.selectedItem === item) {
            indexVal = 0
        }
        return indexVal;
    }

    getDataValue(item, locale) {
        var display = item;

        if (this.localized) {
            display = this.getString(display, this.locale) || display;
        }

        return display;
    }

    computeCheckedClass(item, selectedItem) {
        var cls = "dr-zp-radio hydrated";
        if (this.selectedItem === item) {
            cls += " dr-zp-radio--checked"
        }
        return cls;
    }

    getIndex(index) {
        var indexVal = 1;
        if (index != 0) {
            indexVal = -1;
        }
        return indexVal;
    }

    selectedChanged() {
        this.selectedRadio();
    }

    domChanged() {
        this.selectedRadio();
    }

    selectedRadio() {
        var index = this.items.indexOf(this.selectedItem);
        var id = "item_" + index;
        var radio = dom(this.root).querySelector("#" + id);

        if (radio) {
            radio.checked = true;
            radio.ariaChecked = true;
        }
    }

    determineStyle(items, direction) {
        var style = "cursor: pointer;";

        if (this.direction === 'horizontal') {
            style += 'display: inline-flex';
        }

        return style;
    }

    handleItemClick(e) {
        var item = e.model.__data.item;
        var index = e.model.__data.index;
        this.selectedItem = item;
        var radio = dom(this.root).querySelector("#item_" + index);
        radio.checked = true;
        radio.focus();

        this.dispatchEvent(new CustomEvent('radio-group-change', {
            bubbles: true,
            composed: true,
            detail: {
                selected: item
            }
        }));
    }


}

window.customElements.define('dr-zp-radio-group', Comp);