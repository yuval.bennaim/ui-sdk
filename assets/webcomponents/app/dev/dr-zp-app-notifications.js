import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { dom } from '../../../vendor/@polymer/polymer/lib/legacy/polymer.dom.js'
import { BaseComponent } from '../../../webcomponents/core/dr-zp-base-component.js';
import '../../core/misc/dr-zp-notification.js'

class Comp extends BaseComponent {
    static get template() {
        var appStyleTemplate = super.styleStore.getSheetStyles("sdk.css");

        return html`
            ${appStyleTemplate}

            <style>
                .notification-wrapper {
                    padding: 5px 5px 0px 20px;
                    width: auto;
                    opacity: 1;
                    position: relative;
                    right: 0%;
                    transition: 1s;
                }
                .added {
                    right: -100%;
                    opacity: 1;
                }
                .deleted {
                    right: -100%;
                    opacity: 1;
                }
            </style>
        `;
    }

    connectedCallback() {
        super.connectedCallback(); 

        document.addEventListener("dr-zp-notification", (event) => {    
            console.info("dr-zp-notification", event.detail);
            var notif = event.detail;

            var div = document.createElement("DIV");
            div.id = "notif_" + notif.timestamp;
            div.className = "notification-wrapper added";

            setTimeout(() => {
                div.className = "notification-wrapper";
            }, 500);

            div.innerHTML = `<dr-zp-notification alert-type="${notif.type}" source="${notif.source}" message="${notif.message}" with-icon="true"></dr-zp-notification>`;
            this.root.appendChild(div);

            if(notif.autodismissable && notif.duration && notif.timestamp) {
                setTimeout(() => {
                    var selector = "#notif_" + notif.timestamp;
                    var divx = dom(this.root).querySelector(selector);

                    if(divx) {
                        divx.className = "notification-wrapper deleted";

                        setTimeout(() => {
                            this.root.removeChild(divx);
                        }, 500);
                    }
                    
                }, notif.duration);
            }
        });

        document.addEventListener('dr-zp-notification-close-clicked', (e) => {
            var target = e.detail.target.parentElement;
            target.parentNode.removeChild(target);
        })
    }
}

window.customElements.define('dr-zp-app-notifications', Comp);