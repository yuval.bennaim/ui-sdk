import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../core/dr-zp-base-component.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;

        return html`
            ${superTemp}

            <div class$="[[determinHeaderClass(locale)]]">
                <img class="logo" alt="logo" src$="[[getLogoSrc(theme)]]" on-click="navigateHome" title="[[getString('shell-home', locale)]]">

                <div class="product">
                    [[getModuleDispayName(selected, locale)]]
                </div>

                <div class="icons">     
                
                    <!--<div class$="[[determineNavigationClass(selectedIcon)]]">
                        <template is="dom-repeat" items="[[navigation]]" as="nav">

                            <template is="dom-if" if="[[!isHidden(nav)]]">
                                <div class$="[[determineNavigationLinkClass(nav, selectedNav)]]" on-click="openSubNav" on-mouseleave="closeSubNav">
                                    <template is="dom-if" if="[[!isMenuGroup(nav)]]">
                                        <button class="link" on-click="subnavigate">[[nav.menu_name]]
                                            <span class="caret-down">
                                                <dr-zp-icon type="chevron-down" size="sm" style="visibility: hidden;"></dr-zp-icon>
                                            </span>
                                        </button>
                                    </template>

                                    <template is="dom-if" if="[[isMenuGroup(nav)]]">
                                        <button class="link" on-keypress="navKeyPressed">[[nav.menu_name]]
                                            <span class="caret-down">
                                                <dr-zp-icon type="chevron-down" size="sm" ></dr-zp-icon>
                                            </span>
                                        </button>

                                        <div class$="[[determineSubNavigationClass(nav)]]">
                                            <template is="dom-repeat" items="[[nav.navigation]]" as="subnav">
                                                <template is="dom-if" if="[[!isHidden(subnav)]]">
                                                    <button class="sub-menu-navigation-item" on-click="subnavigate">[[subnav.menu_name]]</button>
                                                </template>  
                                            </template>
                                        </div>
                                    </template>  
                                </div>
                            </template> 
                        </template>
                    </div>-->

                    <!--<template is="dom-if" if="[[hasModules(modules)]]">
                        <button class="link header-icon" title="[[getString('shell-app-switcher-tooltip', locale)]]" on-click="openApps" on-keypress="openApps">
                            <dr-zp-icon type="grid" size="md"></dr-zp-icon>
                        </button>
                    </template>-->

                    <button class="link header-icon" on-click="openUserMenu" on-keypress="avatarKeyPressed">

                            <dr-zp-icon type="menu" size="md"></dr-zp-icon>

                            <div class$="[[determineUserMenuClass(userMenu, locale)]]" on-mouseleave="closeUserMenu">
                                <span class="user-menu-navigation-item-no-hover">
                                    [[getString('shell-locale', locale)]]:&nbsp;
                                    <dr-zp-dropdown id="localesDD" dropdowntype="link" items="[[locales]]" selecteditem="{{user.locale}}" localized button-style="padding-left: 10px !important"></dr-zp-dropdown>
                                </span>

                                <span class="user-menu-navigation-item-no-hover">
                                    [[getString('shell-theme', locale)]]:&nbsp;
                                    <dr-zp-dropdown id="themesDD" dropdowntype="link" items="[[themes]]" selecteditem="{{theme}}" localized button-style="padding-left: 10px !important"></dr-zp-dropdown>
                                </span>
                            </div>

                    </button>
                    
                </div>
            </div>

            <div class="header-ribbon"></div>            

            <template is="dom-if" if="[[hasModules(modules)]]">
                <div class$="[[determineAppsRibbonClass(showAppsRibbbon)]]" on-mouseleave="closeApps">
                    <div class="apps-ribbon-arrow no-events"></div>
                    <template is="dom-repeat" items="[[modules]]" as="module">

                        <template is="dom-if" if="[[showInApps(module)]]">
                            <button class$="[[determineAppClass(module, selected)]]" on-click="navigate">
                                <dr-zp-bindable-html value="[[getModuleIcon(module, 'app-icon-svg', 'app-icon-png')]]"></dr-zp-bindable-html><br>

                                <div class="app-name">[[module.tile_name]]</div>

                                <!--<template is="dom-if" if="[[module.dev]]">
                                    <div class="app-dev">[[getString('shell-dev-only', locale)]]</div>
                                </template>-->

                                <template is="dom-if" if="[[isExternal(module)]]">
                                    <div class="app-external">[[getString('shell-external', locale)]]</div>
                                </template>
                            </button>   
                         </template>                         
                    </template>
                    </div>
                </div>
            </template>
        `;
    }

    static get properties() {
        return {
            route: {
                type: Object,
                notify: false
            },
            user: {
                type: Object
            },
            defaultRoute: {
                type: String
            },
            selected: {
                type: Object,
                notify: false
            },
            navigation: {
                type: Array,
                value: []
            },
            modules: {
                type: Array,
                value: []
            },
            showAppsRibbbon: {
                type: Boolean,
                value: false
            },
            userMenu: {
                type: Boolean,
                value: false
            },
            locales: {
                type: Array
            },
            themes: {
                type: Array,
                value: []
            }
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.selectedIcon = '';
        this.theme = this.styleStore.getTheme();
        var themeOverride = localStorage.getItem("theme");

        if (this.theme !== themeOverride) {
            this.themeChange(themeOverride);
        }

        this.$.themesDD.addEventListener('dropdown-change', e => {
            console.info('themeDD dropdown-change', e.detail.selected);
            var theme = e.detail.selected;
            this.themeChange(theme);
            localStorage.setItem("theme", theme);
        });

        this.$.localesDD.addEventListener('dropdown-change', e => {
            var locale = e.detail.selected;
            console.info('localesDD dropdown-change', locale);
            this.changeLocale(e);
            localStorage.setItem("locale", locale);
        });

        document.addEventListener('keydown', (e) => {
            const code = e.code;
            //console.info('globalKeyDown', code);         

            if (code === "Escape") {
                this.closeAll();

                document.dispatchEvent(new CustomEvent("escape-action", {
                    detail: {}
                }));
            }
            else if (code.indexOf("Enter") > -1) {
                document.dispatchEvent(new CustomEvent("enter-action", {
                    detail: {}
                }));
            }
        });
    }

    themeChange(theme) {
        setTheme(theme);
        document.dispatchEvent(new CustomEvent("theme-changed", {
            detail: {"theme": theme}
        }));
    }

    getModuleIcon(module, svgClass, pngClass) {
        var str = "";

        if (module.icon !== undefined) { //use the icon specified in the manifest
            if (module.icon.indexOf('.png') > -1) {
                if (module.base_path == null) {
                    str = '<dr-zp-image src="' + module.icon + '" styler="' + pngClass + '"></dr-zp-image>';
                }
                else {
                    str = '<dr-zp-image src="' + module.base_path + "/" + module.icon + '" styler="' + pngClass + '"></dr-zp-image>';
                }
            }
            else {
                str = '<dr-zp-icon type="' + module.icon + '" styler="' + svgClass + '"></dr-zp-icon>';
            }
        }
        return str;
    }

    getModuleDispayName(module, locale) {
        var str = '';

        if (module != null) {
            str = this.getString(module.display_name, this.locale);
        }

        return str;
    }

    getLogoSrc(theme) {
        if (theme === 'dark') {
            return './assets/images/dr_logo_dark.png';
        }
        else { //light theme
            return './assets/images/dr_logo_light.jpeg';
        }
    }    

    isCurrentLocale(locale, userLocale) {
        console.info('isCurrentLocale', locale, userLocale);
        var isCurrent = (userLocale === locale);
        return isCurrent;
    }

    showPrefs() {
        console.info('showPrefs...');
    }

    logOut() {
        console.info('logOut...');

        document.dispatchEvent(new CustomEvent("logout-action", {
            detail: {}
        }));
    }

    changeLocale(e) {
        var locale = e.detail.selected;
        console.info('changeLocale', locale);
        this.user.locale = locale;
        this.i18n.setLocale(this.user.locale);
        this.user = this.utils._helper_copy(this.user);
    }

    hasWC(selected, comp) {
        if (selected != null && selected.webcomponents != null) {
            var theView = this.selected.webcomponents.find(module => module.name === comp);
            return theView != null;
        }

        return false;
    }

    hasModules(modules) {
        return this.modules != null && this.modules.length > 0;
    }

    isSelected(module) {
        if (this.selected == null) {
            return false;
        }
        else {
            return module.route === this.selected.route;
        }
    }

    isSelectedNav(nav, selectedNav) {
        if (this.selectedNav == null) {
            return false;
        }
        else {
            var match = false;

            if (nav != undefined) {
                if (nav.navigation != undefined) {
                    var subnavArr = nav.navigation;

                    for (var s = 0; s < subnavArr.length; s++) {
                        var sub = subnavArr[s];
                        sub._parent = nav.sub_route;

                        if (sub.sub_route != null && sub.sub_route === this.selectedNav) {
                            match = true;
                        }
                        else if (sub.sub_route != null && sub.sub_route === this.selectedNav.sub_route) {
                            match = true;
                        }
                        else if (sub.sub_route != null && typeof this.selectedNav === "string" && this.selectedNav.indexOf(sub.sub_route) > -1) {
                            match = true;
                        }
                    }
                }

                var cond1 = nav.sub_route === selectedNav;
                var cond2 = selectedNav.sub_route != undefined && nav.sub_route === selectedNav.sub_route;
                return cond1 || cond2 || match;
            }
        }
    }

    isAdmin(module) {
        return (module.admin !== undefined && module.admin == true);
    }

    isExcluded(module) {
        var exclude = module.exclude !== undefined && module.exclude === true;
        return exclude;
    }

    isDev(module) {
        return module.dev !== undefined && module.dev === true;
    }

    showInApps(module) {
        // var admin = this.isAdmin(module);
        // var dev = this.isDev(module);
        // var excluded = this.isExcluded(module) && !module.external;
        // var subscribed = module.subscribed === undefined || module.subscribed === true;
        var show = true; //admin || !excluded;
        return show;
    }

    isExternal(module) {
        return module.external !== undefined && module.external === true;
    }

    determinHeaderClass(locale) {
        var cls = "header-inner";
        return cls;
    }

    determineNavigationClass(selectedIcon) {
        var cls = "navigation";
        return cls;
    }

    determineNavigationLinkClass(nav, selectedNav) {
        var cls = "navigation-link";

        if (this.isSelectedNav(nav, this.selectedNav)) {
            cls += " navigation-link-selected";
        }

        return cls;
    }

    determineSubNavigationClass(nav) {
        var cls = "sub-menu-navigation";

        if (nav._opened) {
            cls += " sub-menu-navigation-open";
        }

        return cls;
    }

    determineUserMenuClass(userMenu, locale) {
        var cls = "user-menu-navigation";

        if (userMenu) {
            cls += " user-menu-navigation-open";
        }

        return cls;
    }

    determineAppClass(module, selected) {
        var cls = "app-icon";

        if (this.isSelected(module, selected)) {
            cls += " app-icon-selected";
        }

        return cls;
    }

    determineAppsRibbonClass(showAppsRibbbon) {
        var cls = "apps-ribbon-hide";

        if (showAppsRibbbon) {
            cls = "apps-ribbon-show";
        }

        return cls;
    }

    determineLinkClass(route, module, selected) {
        var cls = 'fe-btn fe-btn--lg fe-btn--link';

        if (this.route != null && module != null) {
            var path = this.route.path.substring(1);

            if (module.route === path) {
                cls += " tab-selected";
            }
        }

        return cls;
    }

    isHidden(nav) {
        var hidden = false;

        if (nav !== undefined && nav.hidden !== undefined) {
            hidden = nav.hidden;
        }

        return hidden;
    }

    isMenuGroup(nav) {
        return nav !== undefined && nav.menu_group !== undefined;
    }

    openUserMenu(e) {
        this.closeApps();
        this.userMenu = true;
    }

    closeUserMenu(e) {
        this.userMenu = false;
    }

    closeAll(e) {
        this.closeApps();
        this.closeUserMenu(e);

        for (var n = 0; n < this.navigation.length; n++) {
            this.navigation[n]._opened = false;
        }

        this.navigation = this.utils._helper_copy(this.navigation);
    }

    openSubNav(e) {
        this.closeApps();
        var nav = e.model.__data.nav;
        nav._opened = true;
        this.navigation = this.utils._helper_copy(this.navigation);
    }

    closeSubNav(e) {
        var nav = e.model.__data.nav;
        nav._opened = false;
        this.navigation = this.utils._helper_copy(this.navigation);
    }

    getUserInitials(user) {
        if (user != null) {
            return this.user.initials;
        }
        else {
            return null;
        }
    }

    openSearch() {
        console.info("openSearch...");
    }

    openApps() {
        this.showAppsRibbbon = true;
    }

    closeApps(e) {
        this.showAppsRibbbon = false;
    }

    navigateHome(e) {
        this.selectedNav = null;

        document.dispatchEvent(new CustomEvent("navigation-home-action", {
            detail: {}
        }));
    }

    navigate(e) {
        this.closeApps();
        var module = e.model.__data.module;
        let appInstance = this.getAppInstance();
        var currentModule = this.utils._getSafeValue(() => appInstance.selected.name, '');
        if (module.external) {
            /* make user option to open in same or new tab */
            window.open(module.location, '_self');
            //window.open(module.location, '_blank');
        }
        else {
            if (module.route !== currentModule) {
                this.selectedNav = null;
                document.dispatchEvent(new CustomEvent("navigation-action", {
                    detail: {
                        module: module
                    }
                }));
            }
        }
    }

    avatarKeyPressed(e) {
        console.info('avatarKeyPressed', e.code);

        if (e.code === 'Space' || e.code === 'Enter') {
            this.openUserMenu(e);
        }
    }

    navKeyPressed(e) {
        console.info('navKeyPressed', e.code);

        if (e.code === 'Space' || e.code === 'Enter') {
            this.openSubNav(e);
        }
    }

    subnavigate(e, detail, enav) {
        var nav = enav || e.model.__data.nav || e.model.__data.subnav;
        this.selectedNav = nav;
        console.info('subnavigate', nav);
        var url = top.location.origin + '/#/' + this.selected.route + '/' + nav.sub_route;
        history.pushState(url, url, url);

        document.dispatchEvent(new CustomEvent("context-navigation-action", {
            detail: {
                module: this.selected,
                nav: this.selectedNav
            }
        }));
    }
}

window.customElements.define('dr-zp-app-header', Comp);