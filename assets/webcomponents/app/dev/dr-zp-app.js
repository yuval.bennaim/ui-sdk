import { html } from '../../../vendor/@polymer/polymer/polymer-element.js';
import { BaseComponent } from '../../../webcomponents/core/dr-zp-base-component.js';

import '../../../vendor/@polymer/polymer/iron-ajax/iron-ajax.js';
import '../../../vendor/@polymer/polymer/app-route/app-location.js';
import '../../../vendor/@polymer/polymer/app-route/app-route.js';

import '../../../vendor/@polymer/polymer/lib/elements/dom-if.js';
import '../../../vendor/@polymer/polymer/lib/elements/dom-repeat.js';

import '../../../webcomponents/core/dr-zp-bindable-html.js';

import '../../core/form/dr-zp-button.js';
import '../../core/form/dr-zp-calendar.js';
import '../../core/form/dr-zp-checkbox.js';
import '../../core/form/dr-zp-datepicker.js';
import '../../core/form/dr-zp-date-dropdown.js';
import '../../core/form/dr-zp-radio-group.js';
import '../../core/form/dr-zp-dropdown.js';
import '../../core/form/dr-zp-list.js';
import '../../core/form/dr-zp-file-selector.js';
import '../../core/form/dr-zp-input.js';
import '../../core/form/dr-zp-color-input.js';
import '../../core/form/dr-zp-numeric-input.js';
import '../../core/form/dr-zp-textarea.js';
import '../../core/form/dr-zp-slider.js';
import '../../core/form/dr-zp-time-input.js';
import '../../core/form/dr-zp-label.js';

import '../../core/misc/dr-zp-image.js';
import '../../core/misc/dr-zp-icon.js';
import '../../core/misc/dr-zp-modal.js';
import '../../core/misc/dr-zp-pagination.js';
import '../../core/misc/dr-zp-notification.js';
import '../../core/misc/dr-zp-spinner.js';
import '../../core/misc/dr-zp-tooltip.js';
import '../../core/misc/dr-zp-toggle.js';
import '../../core/misc/dr-zp-badge.js';

import '../../core/containers/dr-zp-tabs.js';
import '../../core/containers/dr-zp-collapse.js';
import '../../core/containers/dr-zp-list-selector.js';
import '../../core/containers/dr-zp-panel.js';
import '../../core/containers/dr-zp-multi-select.js';
import '../../core/containers/dr-zp-accordion.js';
import '../../core/containers/dr-zp-pill-box.js';

import '../../../webcomponents/reusable/data/tree/dr-zp-tree.js';
import '../../../webcomponents/reusable/data/grid/dr-zp-grid.js';

import './dr-zp-app-header.js';
import './dr-zp-app-notifications.js';

class Comp extends BaseComponent {
    static get template() {
        var superTemp = super.template;

        return html`
            ${superTemp}
            
            <app-location route="{{route}}" use-hash-as-path></app-location>
            <app-route route="{{route}}" pattern="/:page" data="{{routeData}}" tail="{{subroute}}"></app-route>

            <iron-ajax id="getShellResources" method="GET" url="./assets/webcomponents/app/shell-i18n-resources.json?v=[[appVersion]]" on-response="handleResourcesResponse" handle-as="json"></iron-ajax>
            <iron-ajax id="getSDKResources" method="GET" url="./assets/sdk-i18n-resources.json?v=[[appVersion]]" on-response="handleResourcesResponse" handle-as="json"></iron-ajax>
            <iron-ajax id="getModuleResources" method="GET" on-response="handleResourcesResponse" handle-as="json"></iron-ajax>
            <iron-ajax id="getmodules" method="GET" url="../../modules.json" on-response="handleModulesResponse" handle-as="json"></iron-ajax>

            <div class$="[[determineAppClass(authenticated)]]">

                <dr-zp-app-header class="header" route="{{route}}" default-route="{{defaultRoute}}" locales="[[locales]]" themes="[[themes]]" user="[[user]]" modules="[[modules]]" selected="[[selected]]" navigation="[[navigation]]" selected-nav="[[selectedNav]]"></dr-zp-app-header>
                
                <dr-zp-app-notifications class="notifications-banner"></dr-zp-app-notifications>
 
                <div id="contentWrapper" class="view-entering">
                    <div class="view">
                        <dr-zp-bindable-html id="base-module" value="[[wcStr]]"></dr-zp-bindable-html>
                    </div>
                </div>
            </div>

            <div id="modalWrapper" class$="[[determineModalWrapperClass(modal)]]"></div>
        `;
    }

    static get properties() {
        return {
            user: {
                type: Object,
                value: null
            },
            devmode: {
                type: Boolean,
                value: false
            },
            modules: {
                type: Array,
                value: []
            },
            selected: {
                type: Object,
                observer: 'selectedChanged'
            },
            selectedNav: {
                type: Object
            },
            wcStr: {
                type: String,
                value: ''
            },
            defaultRoute: {
                type: String,
                value: '',
            },
            loading: {
                type: Boolean,
                value: true
            },
            dev: {
                type: Boolean,
                value: false
            },
            error: {
                type: Boolean,
                value: false
            },
            modal: {
                type: Boolean,
                value: false
            },
            authenticated: {
                type: Boolean,
                value: false
            },
            navigation: {
                type: Array,
                value: []
            },
            themes: {
                type: Array,
                value: ["dark", "light"]
            },
            appVersion: {
                type: String,
                value: '0.0.1-default',
            },
        }
    }

    static get observers() {
        return [
            'routePageChanged(routeData.page)'
        ]
    }

    async connectedCallback() {
        super.connectedCallback();

        this.$.getShellResources.generateRequest();
        this.$.getSDKResources.generateRequest();

        const response = await fetch('/package.json')
        const appPackage = await response.json();

        this.appVersion = appPackage.version;
        this.devoverride = localStorage.getItem("devoverride") || false;

        if (this.devoverride || location.hostname === 'localhost' || location.hostname === '127.0.0.1') {
            console.info('DEV MODE ! bypassing SSO');
            this.dev = true;
            await this.logInSuccess();
        }
        else {
            await this.authInit();
        }

        if(this.error) {
            this.wcStr = '<h3 class="error-message">Failed to obtain Tenent data</h3>';
        }
        else {
            window.addEventListener('popstate', (event) => {
                var loc = document.location.href;
                var route = '';
                var ix = loc.indexOf('#');
                console.info("popstate", loc);

                if (ix > 0) {
                    route = loc.substring(ix + 2);
                }

                if (route === '') {
                    this.deselect();
                }
                else {
                    route = route.substring(0, route.indexOf('/'));
                    this.selected = this.modules.find(module => module.route === route);
                    this.selectPlugin();
                }

                if (this.selected == null) {
                    this.wcStr = '<h3 class="error-message">No Module match for ' + route + '</h3>';
                }
                else if(this.selectedNav != null) {
                    document.dispatchEvent(new CustomEvent("context-navigation-action", {
                        detail: {
                            module: this.selected,
                            nav: this.selectedNav
                        }
                    }));
                }
            });

            document.addEventListener("navigation-action", (event) => {
                var module = event.detail.module;
                console.info("navigation-action", module);

                if (module == null) {
                    //this.selected = this.modules[0];
                    top.location = this.modules[0].location;
                }
                else {
                    if (module.external != null && module.external) {
                        top.location = module.location;
                    }
                    else {
                        this.selected = module;
                        var url = top.location.origin + '/#/' + this.selected.route;

                        if (this.selected.subroute) {
                            url += "/" + this.selected.subroute;
                        }

                        history.pushState(url, url, url);
                    }
                }
            });

            window.addEventListener("message", (event) => {
                if (event.data.type === "authorization_response") {
                    console.info("message", event.data);
                }
                else {
                    try {
                        var str = event.data;
                        var obj = JSON.parse(str);

                        if (obj.type !== undefined && obj.type === 'menu' && obj.navigation !== undefined) {
                            this.navigation = obj.navigation;
                        }
                        else if (obj.type === 'innernav') {
                            console.info('receiveMessage', obj);
                            var hash = obj.location.hash.substring(1);
                            var replaceUrl = '#/' + this.selected.route + hash;
                            history.replaceState(null, null, replaceUrl);
                        }
                    } catch (err) { }
                }
            }, false);

            this.$.getmodules.generateRequest();
            //this.getTiles([]);

            document.addEventListener("navigation-home-action", (event) => {
                this.selected = this.homeModule;
                var url = top.location.origin + '/#/' + this.homeModule.route;
                history.pushState(url, url, url);
            });

            document.addEventListener("modal-open", (event) => {
                console.info("modal-open", event.detail);
                var source = event.detail.source;
                this.$.modalWrapper.appendChild(source);
                this.modal = true;
            });

            document.addEventListener("modal-close", (event) => {
                console.info("modal-close", event.detail);
                var source = event.detail.source;
                var originalParent = event.detail.originalParent;
                originalParent.appendChild(source);
                this.modal = false;
            });

            document.addEventListener("logout-action", (event) => {
                this.logOut();
            });
        }
    }

    async authInit() {
        console.info('authInit...');
        this.authClient = new OktaAuth(oauthConfig);

        try {
            const session = await this.authClient.session.get();

            if (session.status === 'ACTIVE') {
                console.info("Successfully signed user in, checking tokens ...");
                let idToken = await this.authClient.tokenManager.get('idToken');
                const accessToken = await this.getToken();

                this.tokenStore.setAuthClient(this.authClient);

                if (idToken && accessToken) {
                    await this.logInSuccess();
                } else {
                    console.info("At least one token was missing, redirecting to sign in...");
                    await this.logIn();
                }
            } else {
                console.info("Session is no longer active, redirecting to sign in...");
                await this.logIn();
            }

        } catch (anErr) {
            console.error("Unable to authorize this session: " + anErr);
            await this.logIn();
        }
    }

    logOut() {
        var reply = window.confirm("Confirm Logout ?");

        if (reply) {
            this.authenticated = false;
            this.authClient.signOut();
        }
    }

    hasPermission(permission) {
        if (this.permissions == null) {
            return false;
        }

        return this.permissions.indexOf(permission) > -1;
    }

    async getPermissions() {
        const accessToken = await this.getToken();
        const decodedToken = await this.authClient.token.decode(accessToken.accessToken);
        const permissions = decodedToken && decodedToken.payload.fe_permissions;
        return permissions;
    }

    async logIn() {
        sessionStorage.setItem("urlToRedirect", location.hash);

        // Providing UNI with additional information
        document.cookie = `fe_login_client_id=${oauthConfig.clientId}; Expires=Session; domain=datarobot.com; Secure; SameSite=Strict`;

        const oauthOptions = {
            redirectUri: oauthConfig.redirectUri,
            scopes: oauthConfig.scopes
        };

        await this.authClient.token.getWithRedirect(oauthOptions);
    }

    async logInSuccess() {
        console.info("Successfully signed in user");
        this.loading = true;
        this.authenticated = true;

        if (!this.dev) {
            const accessToken = await this.getToken();
            this.permissions = await this.getPermissions();
            const permissionsString = this.permissions.join(",");
            let idToken = await this.authClient.tokenManager.get('idToken');

            this.user = {
                email: idToken.claims.email,
                name: idToken.claims.name,
                firstName: idToken.claims.name.split(' ')[0],
                lastName: idToken.claims.name.split(' ')[1],
                initials: this.utils._helper_extractInitials(idToken.claims.name),
                locale: this.i18n.defaultLocale,
                customer_number: idToken.claims.fe_customer_number
            }
        }
        else {
            const accessToken = "hocuspocus";
            this.user = {
                "email":"gordon.shumway@alf.com",
                "createdAt":"2020-12-07T18:48:37.000Z",
                "firstName":"Gordon",
                "lastName":" Shumway",
                "name":"Gordon Shumway",
                "initials":"GS",
            };
        }

        if (localStorage.getItem("locale") != null) {
            var locale = localStorage.getItem("locale");
            this.user.locale = locale;
        }
        else {
            this.user.locale = this.i18n.defaultLocale;
        }

        this.user = this.utils._helper_copy(this.user);

        if(!this.dev) {
            this.initAnalytics();
        }

        this.i18n.setLocale(this.user.locale);
    }

    initAnalytics() {
        let blockAnalytics = localStorage.getItem("blockAnalytics") || false;

        if(!blockAnalytics) {
            console.debug('initAnalytics', this.user);
            let okta = JSON.parse(localStorage.getItem('okta-token-storage'));

            aptrinsic("identify",
            { //User Fields
            
                "id": this.user.sub, // Required for logged in app users
                "tenantId": this.user.fe_tenant_id,
                "email": this.user.email,
                "firstName": this.user.firstName,
                "lastName": this.user.lastName,
                "signUpDate": Date.now(), //unix time in ms
                "userHash": "" // optional transient for HMAC identification
            },
            { //Account Fields
                "id":"datarobot", //Required
                "name":"datarobot Inc",
            });
        }
    }

    getToken() {
        return this.authClient.tokenManager.get('accessToken');
    }

    getUser() {
        return this.user;
    }

    determineAppClass(authenticated) {
        var cls = 'app';

        if (!authenticated) {
            cls += ' hidden';
        }

        return cls;
    }

    determineModalWrapperClass(modal) {
        var cls = ' modal-wrapper';

        if (modal) {
            cls += '-open';
        }

        return cls;
    }

    determineSideNavClass(navigation) {
        var cls = 'side-nav';

        if (this.navigation == null || this.navigation.length == 0 || this.noneVisible()) {
            cls = 'side-nav-hidden';
        }

        return cls;
    }

    noneVisible() {
        var visibles = this.navigation.filter((nav) => {
            return !nav.hidden;
        });

        return visibles.length === 0;
    }

    determineViewClass(navigation) {
        var cls = 'view';

        if (this.navigation == null || this.navigation.length === 0 || this.noneVisible()) {
            cls = 'view-no-nav';
        }

        return cls;
    }

    handleResourcesResponse(data) {
        var bundle = data.detail.response;
        console.info('handleResourcesResponse', bundle);
        this.i18n.addBundle(bundle);
        this.locales = this.i18n.getLocales();
    }

    async handleModulesResponse(data) {
        this.modules = data.detail.response;
        console.info('handleModulesResponse', this.modules);

        if (!this.dev && !this.devoverride) { //filer dev if not localhost
            this.modules = this.modules.filter((item) => {
                return !(item.dev !== undefined || item.dev === true);
            });
        }

        this.modules.sort((a, b) => {
            return a.order - b.order;
        });

        this.modules = this.utils._helper_copy(this.modules);
        this.homeModule = this.modules.find(module => module.home == true);

        if (this.homeModule == null && this.modules.length > 0) {
            this.homeModule = this.modules[0];
        }

        if (this.page == null && this.modules.length > 0) {
            this.page = this.homeModule.route;
            var url = top.location.origin + '/#/' + this.homeModule.route;
            history.pushState(url, url, url);
        }

        // preload designated modules

        for (var m = 0; m < this.modules.length; m++) {
            var module = this.modules[m];

            if (module.preload) {
                await this.loadModule(module, 'home');
            }
        }

        this.selectPlugin();
    }

    selectPlugin() {
        if (this.modules != null && this.modules.length > 0 && this.page != null) {
            for (var p = 0; p < this.modules.length; p++) {
                if (this.modules[p].route === this.page) { //match route to page
                    this.selected = this.modules[p];

                    if (this.route != null && this.route.path != null) {
                        var ix = this.route.path.indexOf('/');
                        var tempStr = this.route.path.substring(ix + 1);
                        ix = tempStr.indexOf('/');
                        this.selectedNav = tempStr.substring(ix + 1);
                    }
                }
            }
        }
    }

    routePageChanged(page) {
        if (this.route.path === '' || this.route.path === '/') {
            this.deselect();
        }
        else {
            this.page = page;
            this.selectPlugin();
        }
    }

    isDev(dev) {
        return this.dev;
    }

    deselect() {
        this.wcStr = '';
        this.selected = null;
        this.navigation = [];
        this.selectedNav = null;
    }

    async loadModule(module, view) {
        console.info('loadModule', module);

        if(!module.loaded) {
            let response = await fetch(module.base_path + "/package.json");
            let microAppPackage = await response.json();
            var theView = module.webcomponents.find(plugin => plugin.name === view);
            var basePath = module.base_path + ((this.devmode) ? '/dev' : '/prod');
            let url = basePath + "/" + theView.webcomponent + ".js";
            
            if(!this.devmode) {
                url += "?v=" + microAppPackage.version;
            }

            this.utils._helper_initiateWCImport(url, this.loadHandler.bind(this), this.errorHandler.bind(this));

            //load module's i18n resources

            let resourceBbasePath = module.base_path;
            this.$.getModuleResources.url = resourceBbasePath + "/" + module.strings;
            this.$.getModuleResources.generateRequest();

            module.loaded = true;
        }
    }


    async selectedChanged() {
        console.info('selectedChanged', this.selected), this.selectedNav;

        if (this.selected === null) {
            this.deselect();
            this.wcStr = '';
        }
        else if (this.selected != null && this.selected.webcomponents != null) {
            this.page = this.selected.route;
            
            var homeView = this.selected.webcomponents.find(plugin => plugin.name === 'home');
            var newStr = '<' + homeView.webcomponent + '></' + homeView.webcomponent + '>';

            if (homeView === undefined) {
                this.wcStr = '<h3 class="error-message">No Home View found</h3>';
            }
            else if (this.wcStr !== newStr) {
                await this.loadModule(this.selected, 'home');
                this.wcStr = newStr;
                this.navigation = [];
            }
        }
    }

    loadHandler(href) {
    }

    errorHandler(data, href) {
        console.info("errorHandler", href);
        this.deselect();
    }
}

window.customElements.define('dr-zp-app', Comp);
