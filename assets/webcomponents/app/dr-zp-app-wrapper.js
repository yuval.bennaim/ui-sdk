import { html, PolymerElement } from '../../vendor/@polymer/polymer/polymer-element.js';
import '../../vendor/@polymer/polymer/import-href.js';

class AppWrapper extends PolymerElement {
    static get template() {
        return html`
            <dr-zp-app devmode="[[devmode]]" cdn="[[cdn]]"></dr-zp-app>
        `;
    }

    static get properties() {
        return {
            devmode: {
                type: Boolean,
                value: false
            },
            wcStr: {
                type: String,
                value: ''
            }
        }
    }

    connectedCallback() {
        super.connectedCallback(); 

        if(location.hostname === 'localhost' || location.hostname === '127.0.0.1') {
            this.dev = true;
            this.devmode = true; //when running locally, we default to dev code unless we explicitly specify an override to prod below
        }
        else { //running on AWS
            this.dev = false;
            this.devmode = false;
        }

        if(localStorage.getItem("overridemode") != null) {
            var override = localStorage.getItem("overridemode");
            console.info("***** override", override);

            if(override === 'prod') {
                this.devmode = false;
            }
            else  if(override === 'dev') {
                this.devmode = true;
            }
        }

        let modeStr = (this.devmode) ? 'DEV' : 'PROD';
        console.info("%cExecution Mode", "color: blue;", modeStr);
        var path = './assets/webcomponents/app/' + ((this.devmode) ? 'dev' : 'prod') + '/dr-zp-app.js';

        if(this.devmode) { //load the dev version
            window.PolymerHelper.importModule(path);
        }
        else { //load the prod version
            window.PolymerHelper.importModule(path);
        }
    }
}

window.customElements.define('dr-zp-app-wrapper', AppWrapper);
