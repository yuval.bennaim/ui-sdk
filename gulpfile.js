const { task, series, src, dest, watch } = require('gulp');
const del = require('del');
const { exec, execSync } = require('child_process');
const readdirSync = require('fs').readdirSync;
const fs = require('fs');
const semver = require('semver');

var timestamp = new Date().getTime();
var isoDate = new Date().toISOString();
var buildDate = isoDate.substring(0, isoDate.lastIndexOf('.'));
let appSampleGulpFile = 'assets/modules-dev/module-wizard/app-sample/module/gulpfile.js';
let moduleDevPath = 'assets/modules-dev';
let modulesPath = 'assets/modules';
let globalWatcher;
let repos = require('./repos.json');
var utils = require('./utils.js');
var fileDelimeter = process.platform === "win32" ? "\\" : "/";

let watcherFilePath = function (updatedModules) {
    var path = [];
    updatedModules.map((eachModule) => {
        path.push(`./assets/modules-dev/${eachModule.folder}/**/*.*`);
    });
    return path;
}

function watcherFun(pathToBeWatch, updatedModules) {
    console.log(pathToBeWatch, updatedModules)
    globalWatcher = watch(pathToBeWatch, { persistent: true, ignoreInitial: false }).on('change', filePath => {
        console.log("Current Watching Paths", globalWatcher.getWatched());
        updatedModules.map((eachModule) => {
            var changedModuleName = filePath.split(fileDelimeter);
            if (changedModuleName.indexOf(eachModule.folder) !== -1) {
                pushDest(eachModule);
            }
        });
    })
}

let run_watch = function () {
    let updatedModules = JSON.parse(fs.readFileSync('./repos.json'));
    var pathToBeWatch = watcherFilePath(updatedModules);
    console.log("path", pathToBeWatch);
    watcherFun(pathToBeWatch, updatedModules);
}

let listOfFolders = ['assets/webcomponents/app'];

console.log(timestamp + " buildDate = " + buildDate);

/***** Common  */

function runCommand(command, options, msg, cb) {
    console.log(`Building command ${command} with ${JSON.stringify(options)}`);

    let gulpProcess = exec(command, options);

    gulpProcess.stdout.pipe(process.stdout);

    gulpProcess.stderr.pipe(process.stderr);

    gulpProcess.on("exit", function (err) {
        if (err) {
            console.log(err);
            console.log(`${msg} FAILED...`);
            process.exit();
        }
        else {
            console.log(`${msg} SUCCESS...`);
            cb();
        }
    });
}

function runFolderBuild(eachFolder, cb) {
    console.log(`Building Folder ${eachFolder}`);

    runCommand('gulp', { cwd: eachFolder }, `Gulp build for ${eachFolder}`, cb);
}

function runDevModuleBuild(eachModule, cb) {
    console.log(`Building ${eachModule}`);

    runCommand('gulp', { cwd: `${moduleDevPath}/${eachModule}` }, `Gulp build for ${eachModule}`, cb);
}

function runModuleBuild(eachModule, cb) {
    console.log(`Building ${eachModule}`);

    runCommand('gulp', { cwd: `${modulesPath}/${eachModule}` }, `Gulp build for ${eachModule}`, cb);
}

function runDevModulePublish(eachModule, cb) {
    console.log(`Building ${eachModule}`);

    runCommand('gulp npm-publish', { cwd: `${moduleDevPath}/${eachModule}` }, `Gulp build for ${eachModule}`, cb);
}

function runEpsPackageVersionReplace(eachModule, cb) {
    console.log(`Building ${eachModule}`);

    runCommand('gulp ui-shell-pkg-version-replace', { cwd: `${modulesPath}/${eachModule}` }, `Gulp build for ${eachModule}`, cb);
}

function getDirectories(source) {
    if (fs.existsSync(source)) {
        return readdirSync(source, { withFileTypes: true })
            .filter(dirEntry => dirEntry.isDirectory())
            .map(dirEntry => dirEntry.name)
    }
    else {
        return [];
    }
}

function fetchSrc(eachModule) {
    return utils.copyFolders(`${eachModule.path}/${eachModule.folder}/module/**/*`, `./${moduleDevPath}/${eachModule.folder}`);
}

function pushDest(eachModule) {
    return utils.copyFolders(`${moduleDevPath}/${eachModule.folder}/**/*`, `${eachModule.path}/${eachModule.folder}/module`);
}

function pushBaseFiles(eachModule) {
    return utils.copyFolders(appSampleGulpFile, `${moduleDevPath}/${eachModule.folder}`)
}

function getRemotePackageVersion() {
    try {
        const npmShowCommand = `npm show --json`;
        return JSON.parse(execSync(npmShowCommand));
    } catch (error) {
        console.error(error);
    }
}

function getLocalPackageVersion() {
    try {
        return JSON.parse(fs.readFileSync('package.json'));
    } catch (error) {
        console.error(error);
    }
}

task('prep', function () {
    console.log('\n\n-----------------------------------\n\nTASK prep');
    return del(['build', './assets/webcomponents/prod', './assets/webcomponents/coverage']);
})

task('build', function (cb) {
    console.log('\n\n-----------------------------------\n\nTASK build');

    runCommand('polymer build', {}, 'Polymer build', cb);
});

task('complete', function () {
    console.log('\n\n-----------------------------------\n\nTASK complete');

    return src(['./build/es6-bundled/component-prod-bundle.js'])
        .pipe(dest('./assets/webcomponents/prod'));
});

task('cleanup', function () {
    console.log('\n\n-----------------------------------\n\nTASK cleanup');
    return del(['build']);
})

task('run-node', function (cb) {
    watch('./repos.json').on("change", function () {
        globalWatcher.close();
        run_watch();
    });
    run_watch();
    runCommand('node dev/cdn_server.js', {}, `Dev Server`, cb);

})

let listOfFoldersBuild = listOfFolders.map((eachFolder) => {
    task(`run-build-${eachFolder}`, (cb) => runFolderBuild(eachFolder, cb));
    return `run-build-${eachFolder}`;
});

/****** Shell Tasks */

let listOfDevModules = getDirectories(moduleDevPath);

let listOfDevModulesBuild = listOfDevModules.map((eachModule) => {
    task(`shell-build-${eachModule}`, (cb) => runDevModuleBuild(eachModule, cb));
    return `shell-build-${eachModule}`;
});

let listOfDevModulesPublish = listOfDevModules.map((eachModule) => {
    task(`shell-npm-publish-${eachModule}`, (cb) => runDevModulePublish(eachModule, cb));
    return `shell-npm-publish-${eachModule}`;
});

let listOfDestCopy = repos.map((eachModule) => {
    task(`shell-dest-copy-${eachModule.folder}`, () => pushDest(eachModule));
    return `shell-dest-copy-${eachModule.folder}`;
});

let listOfSrcCopy = repos.map((eachModule) => {
    task(`shell-src-copy-${eachModule.folder}`, () => fetchSrc(eachModule));
    return `shell-src-copy-${eachModule.folder}`;
});

let listOfAppInits = repos.map((eachModule) => {
    task(`shell-app-init-${eachModule.folder}`, async (cb) => {
        let response = await utils.appInit(eachModule);
        if (response.code == 404) {
            cb(response.message);
        } else if (response.code == 500) {
            console.log('****------' + response.message + '------****');
            cb();
        } else {
            cb()
        }
    });
    return `shell-app-init-${eachModule.folder}`;
});

let listOfBaseFilesCopy = repos.map((eachModule) => {
    task(`shell-base-file-copy-${eachModule.folder}`, () => pushBaseFiles(eachModule));
    return `shell-base-file-copy-${eachModule.folder}`;
});

let pkgDetails = getLocalPackageVersion();

task('shell:publish', function (done) {
    let npmPkgDetails = getRemotePackageVersion();
    let npmPkgVersion = npmPkgDetails["dist-tags"].latest;

    let pkgVersion = pkgDetails.version;

    let newVersion;
    if (semver.gt(pkgVersion, npmPkgVersion)) {
        //Publish Local Package Version
        console.log("Publish Local Package Version")
        newVersion = pkgVersion;
    } else {
        //Increment and Publish new Version
        console.log("Increment and Publish new Version")
        newVersion = semver.inc(npmPkgVersion, 'patch');
    }
    console.log("Final Version", newVersion);

    exec(`npm version ${newVersion}`, { maxBuffer: Infinity }, function (err, stdout, stderr) {
        if (err != null) {
            console.log(err);
            console.log('NPM Publish FAILED...');
            done(err);
        }
        else {
            console.log('NPM Publish SUCCESS...');
            done();
        }
    });
})

if (repos.length) {
    task('shell:import', series(...listOfSrcCopy));
    // Going forword it is recommended to use Modules wizard.
    // task('shell:app-init', series(listOfAppInits, listOfSrcCopy))
}

task('shell:prod',
    series(
        'prep',
        'build',
        'complete',
        ...listOfFoldersBuild,
        ...listOfDevModulesBuild,
        'cleanup')
);

task('shell:serve', series(...listOfBaseFilesCopy, ...listOfDestCopy, 'run-node'));

/********* Site Tasks */

let listOfModules = getDirectories(modulesPath);

//To Show Tasks only for Site Setup
//_integrity check to make sure it happens only in the eps-ui-shell when pulled from artifactory
if (listOfModules.length && listOfModules.indexOf("eps-ui-shell") == -1 && pkgDetails["_integrity"]) {

    let listOfModulesBuild = listOfModules.map((eachModule) => {
        task(`site-build-${eachModule}`, (cb) => runModuleBuild(eachModule, cb));
        return `site-build-${eachModule}`;
    });

    let listOfModulesEpsPackageReplace = listOfModules.map((eachModule) => {
        task(`site-pkg-version-replace-${eachModule}`, (cb) => runEpsPackageVersionReplace(eachModule, cb));
        return `site-pkg-version-replace-${eachModule}`;
    });

    task('site:prod',
        series(
            'prep',
            'build',
            'complete',
            ...listOfFoldersBuild,
            ...listOfModulesBuild)
    );

    if (listOfModulesEpsPackageReplace.length) {
        task('site:update-pkg-version', series(...listOfModulesEpsPackageReplace));
    }
}
